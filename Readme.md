# Version Information

We will be using the [AndroidAutoVersion gradle plugin](https://github.com/alexfu/androidautoversion) for tracking version numbers in this project.  
The version file is located in the project directory.  For this project it is childwatch-client/childwatch-client/dayCareClientKiosk/version

The version file contains the major, minor and patch number.  It should be kept current in bitbucket for new builds.  

## Versioning Guidelines

We will use the semversion guidelines for deciding whether to bump the major, minor or patch version of a release.  Those guidelines are [here](http://semver.org/).

## Bumping Versions

To bump a version, a gradle command will need to be executed in the current branch of the code being worked on.  The three ways to bump version are:

./gradlew bumpPatch
./gradlew bumpMinor
./gradlew bumpMajor

Run the applicable commands from the project directory and check the updated version file into source control.

Version updates: 
- bump the appropriate type of version (major, minor, patch)
- check-in and merge to master (will kick off deploy)
- tag repository with the new version number (version.txt)
