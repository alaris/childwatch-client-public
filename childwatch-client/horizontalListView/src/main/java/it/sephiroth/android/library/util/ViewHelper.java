package it.sephiroth.android.library.util;

import android.view.View;

public class ViewHelper {

	protected View				view;

	public ViewHelper(View view) {
		this.view = view;
	}

	public void postOnAnimation(Runnable action) {
		view.postOnAnimation(action);
	}

	public void setScrollX(int value) {
		view.setScrollX(value);
	}

	public boolean isHardwareAccelerated() {
		return view.isHardwareAccelerated();
	}

}
