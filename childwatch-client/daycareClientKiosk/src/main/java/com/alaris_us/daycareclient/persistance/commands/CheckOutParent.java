package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.WorkoutArea;

public class CheckOutParent extends Command<Member> {

	private final Member m_parent;
	private final List<WorkoutArea> m_workoutAreas;

	public CheckOutParent(Member parent, List<WorkoutArea> workoutAreas) {
		this(parent, workoutAreas, null, null);
	};

	public CheckOutParent(Member parent, List<WorkoutArea> workoutAreas, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_parent = parent;
		m_workoutAreas=workoutAreas;
	}

	@Override
	public void executeSource() {
		m_dataSource.getMembersDAO().removeWorkoutAreasNoPersist(m_parent, m_workoutAreas);
		if (m_parent.getWasOverridden())
			m_parent.setWasOverridden(false);
		
		m_dataSource.getMembersDAO().update(m_parent, this);
	}

	@Override
	public void handleResponse(Member member) {
	}

}