package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class SignUpSelectMemberItem extends RelativeLayout implements BoundUi<SignUpSelectMemberItemModel>{

	private SignUpSelectMemberItemModel m_data;
	
	private ImageView m_checkBox;
	private ImageView m_memberImage;
	private TextView m_memberName;
	private TextView m_invalidMarker;
	
	public SignUpSelectMemberItem(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public SignUpSelectMemberItem(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		View.inflate(getContext(), R.layout.view_listitem_signupselectuser, this);
		
		m_memberImage = (ImageView) findViewById(R.id.view_listitem_ymcamember_image);
		m_memberName = (TextView) findViewById(R.id.view_listitem_ymcamember_name); 
		m_checkBox = (ImageView) findViewById(R.id.view_listitem_ymcamember_checkmark);
		m_invalidMarker = (TextView) findViewById(R.id.view_listitem_ymcamember_invalidmarker);
		
	}
	
	public Member getMember() {
		return m_data.getMember();
	}
	
	public SignUpSelectMemberItemModel getModel() {
		return m_data; 
	}

	public void setWidth(int width){
		ListViewFunctions.setTileWidth(this, width);
	}
	
	@Override
	public void bind(SignUpSelectMemberItemModel dataSource) {
		m_data = dataSource;
		
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberName, "Text")), m_data, "MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberImage, "BackgroundColor")), m_data, "MemberBG", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Width")), m_data, "Width", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberImage, "Alpha")), m_data, "Alpha", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_checkBox, "Visibility")), m_data, "CheckBoxRes", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_invalidMarker, "Visibility")), m_data, "InactiveMarkerVisibility", BindingMode.ONE_WAY);
	}
}
