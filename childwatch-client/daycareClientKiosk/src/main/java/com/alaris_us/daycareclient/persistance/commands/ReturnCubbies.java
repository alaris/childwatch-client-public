package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Member;

public class ReturnCubbies extends Command<List<Cubby>> {

	private final List<Member> m_children;

	public ReturnCubbies(List<Member> children) {
		this(children, null, null);
	};

	public ReturnCubbies(List<Member> children, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_children=children;
	}

	@Override
	public void executeSource() {
		m_dataSource.getCubbiesDAO().returnCubbies(m_children, this);
	}

	@Override
	public void handleResponse(List<Cubby> cubbies) {
		/*List<Cubby> uniqueCubbies=new ArrayList<Cubby>();
		for(Cubby cubby : cubbies){
			if(!uniqueCubbies.contains(cubby))
				uniqueCubbies.add(cubby);
		}
		m_model.setFamilyCubbies(uniqueCubbies);*/
	}

}