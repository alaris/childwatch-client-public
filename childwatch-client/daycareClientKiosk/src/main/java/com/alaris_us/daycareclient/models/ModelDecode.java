package com.alaris_us.daycareclient.models;

import android.graphics.Bitmap;

public class ModelDecode {
	private ModelDecodeUpdateListener listener;
	private boolean mFinished = false;
	private boolean mPassed = false;
	private int mThreadID = -1;
	private String mResult = "DEFAULT RESULT";
	private Bitmap mBitmap=null;
			
	public ModelDecode(int threadID) {
		mThreadID=threadID;
	}
	public void setResult(String newValue/*, Bitmap newBitmap*/){
		listener.stateChanged(mThreadID, mFinished=true, mPassed=true, mResult=newValue/*, mBitmap=newBitmap*/);
	}
	public void setFailed() {
		listener.stateChanged(mThreadID, mFinished=true, mPassed=false, ""/*, null*/);
	}
	public void setPassed(boolean newValue) {
		mPassed=newValue;
	}
	public boolean isFinished() {
		return mFinished;
	}
	public boolean isPassed() {
		return mPassed;
	}
	public String getResult(){
		return mResult;
	}
	public int getThreadID(){
		return mThreadID;
	}
	public Bitmap getBitmap(){
		return mBitmap;
	}
	public void setDecodeUpdateListener(ModelDecodeUpdateListener newListener) {
		listener=newListener;
	}
	public void removeDecodeUpdateListener() {
		listener=null;
	}
	public void reset(int threadID){
		mThreadID=threadID;
		mBitmap=null;
		mFinished = false;
		mPassed = false;
		mResult = "DEFAULT RESULT";
	}
}
