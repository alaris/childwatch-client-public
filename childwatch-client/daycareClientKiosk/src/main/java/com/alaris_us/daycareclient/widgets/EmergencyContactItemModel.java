package com.alaris_us.daycareclient.widgets;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.models.EmergencyReentryScreenViewModel.ReentrySetForAllClickListener;
import com.alaris_us.daycareclient.models.EmergencyScreenViewModel.SetForAllClickListener;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycaredata.EmergencyNumber;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;

public class EmergencyContactItemModel {

	private TrackableField<Member> m_member;
	private TrackableField<EmergencyNumber> m_itemContact1;
	private TrackableField<EmergencyNumber> m_itemContact2;

	private TrackableBoolean m_isSelected;
	private ClearClickListener m_clearListener;
	private SetForAllClickListener m_setForAllListener;
	private ReentrySetForAllClickListener m_reentrySetForAllListener;

	private Integer m_height;
	private Integer m_color;
	private Integer m_width;
	
	private List<Member> m_allParents;

	public EmergencyContactItemModel() {
		//this(null, null, null, null);
	}

	public EmergencyContactItemModel(Member member, Integer width, Integer color, List<Member> parents, SetForAllClickListener setForAllListener) {
		m_member = new TrackableField<Member>(member);
		m_itemContact1 = new TrackableField<EmergencyNumber>(new EmergencyNumber());
		m_itemContact2 = new TrackableField<EmergencyNumber>(new EmergencyNumber());
		m_isSelected = new TrackableBoolean(false);
		m_width = width;
		m_color = color;
		m_clearListener = new ClearClickListener();
		m_setForAllListener = setForAllListener;
		m_allParents = parents;
		extractDataFromMember(member);
	}
	
	public EmergencyContactItemModel(Member member, Integer width, Integer color, ReentrySetForAllClickListener reentrySetForAllListener) {
		m_member = new TrackableField<Member>(member);
		m_itemContact1 = new TrackableField<EmergencyNumber>(new EmergencyNumber());
		m_itemContact2 = new TrackableField<EmergencyNumber>(new EmergencyNumber());
		m_isSelected = new TrackableBoolean(false);
		m_width = width;
		m_color = color;
		m_clearListener = new ClearClickListener();
		m_reentrySetForAllListener = reentrySetForAllListener;
		extractDataFromMember(member);
	}

	private void extractDataFromMember(Member member) {
		List<EmergencyNumber> emergencyContacts = member.listEmergencyNumbers();
		EmergencyNumber contact1 = new EmergencyNumber();
		EmergencyNumber contact2 = new EmergencyNumber();
		List<Member> realParents = new ArrayList<Member>();
		if (emergencyContacts != null && !emergencyContacts.isEmpty()) {
			m_itemContact1.set(emergencyContacts.get(0).clone());
			if (emergencyContacts.size() >= 2)
				m_itemContact2.set(emergencyContacts.get(1).clone());
		} else {
			for (Member mem : m_allParents) //TODO make a function to do this better
				if (DaycareHelpers.isAdult(mem.getBirthDate(), 18))
					realParents.add(mem);
			if (realParents != null && !realParents.isEmpty()){
				contact1.setName(realParents.get(0).getFirstName() + " " + realParents.get(0).getLastName());
				JSONArray pins = realParents.get(0).getUserPIN();
				try {
					if (pins != null)
						contact1.setPhone(pins.getString(0));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				m_itemContact1.set(contact1);
				
				if (realParents.size() >= 2){
					contact2.setName(realParents.get(1).getFirstName() + " " + realParents.get(1).getLastName());
					pins = realParents.get(1).getUserPIN();
					try {
						if (pins != null)
							contact2.setPhone(pins.getString(0));
					} catch (JSONException e) {
						e.printStackTrace();
					}
					m_itemContact2.set(contact2);
				}
			}
		}
	}

	public Member getMember() {
		return m_member.get();
	}

	public void setMember(Member member) {
		m_member.set(member);
	}

	public String getItemMemberName() {
		return m_member.get().getFirstName() + " " + m_member.get().getLastName();
	}

	public void setItemContact1(EmergencyNumber contact) {
		m_itemContact1.set((contact==null)?new EmergencyNumber(): contact.clone());
	}

	public void setItemContact2(EmergencyNumber contact) {
		m_itemContact2.set((contact==null)?new EmergencyNumber(): contact.clone());
	}

	public EmergencyNumber getItemContact1() {
		EmergencyNumber contact=m_itemContact1.get().clone();
		return (contact.getName().isEmpty() && contact.getPhone().isEmpty())? null : contact;
	}

	public EmergencyNumber getItemContact2() {
		EmergencyNumber contact=m_itemContact2.get().clone();
		return (contact.getName().isEmpty() && contact.getPhone().isEmpty())? null : contact;
	}

	public void setItemContactName1(String name) {
		m_itemContact1.get().setName(name);
		m_itemContact1.updateTrackers();
	}

	public void setItemContactName2(String name) {
		m_itemContact2.get().setName(name);
		m_itemContact2.updateTrackers();
	}

	public void setItemContactNumber1(String number) {
		m_itemContact1.get().setPhone(number);
		m_itemContact1.updateTrackers();
	}

	public void setItemContactNumber2(String number) {
		m_itemContact2.get().setPhone(number);
		m_itemContact2.updateTrackers();
	}

	public String getItemContactName1() {
		return m_itemContact1.get().getName();
	}

	public String getItemContactName2() {
		return m_itemContact2.get().getName();
	}

	public String getItemContactNumber1() {
		return m_itemContact1.get().getPhone();
	}

	public String getItemContactNumber2() {
		return m_itemContact2.get().getPhone();
	}

	public int getItemMemberBG() {
		return m_color;
	}

	public int getHeight() {
		return m_height;
	}

	public int getWidth() {
		return m_width;
	}

	public ClearClickListener getClearClickListener() {
		return m_clearListener;
	}

	public OnClickListener getSetForAllClickListener() {
		if (m_setForAllListener != null)
			return m_setForAllListener;
		else 
			return m_reentrySetForAllListener;
	}

	public void setSelected(boolean isSelected) {
		m_isSelected.set(isSelected);
	}

	public boolean getIsSelected() {
		return m_isSelected.get();
	}

	public class ClearClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			setItemContact1(new EmergencyNumber());
			setItemContact2(new EmergencyNumber());
		}
	}
}
