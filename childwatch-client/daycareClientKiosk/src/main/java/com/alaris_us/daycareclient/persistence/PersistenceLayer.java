package com.alaris_us.daycareclient.persistence;

import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Log;

import com.alaris_us.daycareclient.data.DaycareCredential;
import com.alaris_us.daycareclient.fragments.MainScreenFragment;
import com.alaris_us.daycareclient.persistance.commands.AddEmailToEmailQueue;
import com.alaris_us.daycareclient.persistance.commands.AuthenticateByBarcode;
import com.alaris_us.daycareclient.persistance.commands.AuthenticateByBarcodeOrMagCard;
import com.alaris_us.daycareclient.persistance.commands.AuthenticateByPin;
import com.alaris_us.daycareclient.persistance.commands.CheckInChild;
import com.alaris_us.daycareclient.persistance.commands.CheckInParent;
import com.alaris_us.daycareclient.persistance.commands.CheckOutChild;
import com.alaris_us.daycareclient.persistance.commands.CheckOutParent;
import com.alaris_us.daycareclient.persistance.commands.CreateAuthorization;
import com.alaris_us.daycareclient.persistance.commands.CreateSelfCheckIn;
import com.alaris_us.daycareclient.persistance.commands.FetchAMembersFamilies;
import com.alaris_us.daycareclient.persistance.commands.FetchAdmissionByFamily;
import com.alaris_us.daycareclient.persistance.commands.FetchAdmissionOverride;
import com.alaris_us.daycareclient.persistance.commands.FetchAllChildren;
import com.alaris_us.daycareclient.persistance.commands.FetchAllCubbies;
import com.alaris_us.daycareclient.persistance.commands.FetchAllOffers;
import com.alaris_us.daycareclient.persistance.commands.FetchAllPrograms;
import com.alaris_us.daycareclient.persistance.commands.FetchAllRooms;
import com.alaris_us.daycareclient.persistance.commands.FetchAllWorkoutAreas;
import com.alaris_us.daycareclient.persistance.commands.FetchAvailableCubbies;
import com.alaris_us.daycareclient.persistance.commands.FetchAvailablePagers;
import com.alaris_us.daycareclient.persistance.commands.FetchCheckedInMembers;
import com.alaris_us.daycareclient.persistance.commands.FetchCheckedInMembersCount;
import com.alaris_us.daycareclient.persistance.commands.FetchCheckedInMembersMS;
import com.alaris_us.daycareclient.persistance.commands.FetchClockedInStaff;
import com.alaris_us.daycareclient.persistance.commands.FetchClockedInStaffMS;
import com.alaris_us.daycareclient.persistance.commands.FetchFacility;
import com.alaris_us.daycareclient.persistance.commands.FetchFacilityQuestions;
import com.alaris_us.daycareclient.persistance.commands.FetchFamilyCubbies;
import com.alaris_us.daycareclient.persistance.commands.FetchHomeFacility;
import com.alaris_us.daycareclient.persistance.commands.FetchMemberByMemberId;
import com.alaris_us.daycareclient.persistance.commands.FetchMemberFamily;
import com.alaris_us.daycareclient.persistance.commands.FetchMembersCubbies;
import com.alaris_us.daycareclient.persistance.commands.FetchProgramRooms;
import com.alaris_us.daycareclient.persistance.commands.FetchProgramSchedules;
import com.alaris_us.daycareclient.persistance.commands.FetchScheduleRecurrences;
import com.alaris_us.daycareclient.persistance.commands.FetchSelfCheckInMembers;
import com.alaris_us.daycareclient.persistance.commands.FindIncidents;
import com.alaris_us.daycareclient.persistance.commands.IsMembershipValidForFacility;
import com.alaris_us.daycareclient.persistance.commands.Login;
import com.alaris_us.daycareclient.persistance.commands.RegisterAuthChildInDaycare;
import com.alaris_us.daycareclient.persistance.commands.RegisterFamilyInDaycare;
import com.alaris_us.daycareclient.persistance.commands.RegisterMember;
import com.alaris_us.daycareclient.persistance.commands.RegisterUser;
import com.alaris_us.daycareclient.persistance.commands.ReserveCubbies;
import com.alaris_us.daycareclient.persistance.commands.ReservePagers;
import com.alaris_us.daycareclient.persistance.commands.ReturnCubbies;
import com.alaris_us.daycareclient.persistance.commands.SaveConsentRecord;
import com.alaris_us.daycareclient.persistance.commands.SaveDocument;
import com.alaris_us.daycareclient.persistance.commands.SetupAuthChildFamilyAssociationInDaycare;
import com.alaris_us.daycareclient.persistance.commands.SetupFamilyAssociationInDaycare;
import com.alaris_us.daycareclient.persistance.commands.UpdateAllMembers;
import com.alaris_us.daycareclient.persistance.commands.UpdateFamily;
import com.alaris_us.daycareclient.persistance.commands.UpdateIncidents;
import com.alaris_us.daycareclient.persistance.commands.UpdateMember;
import com.alaris_us.daycareclient.persistance.commands.UpdateMemberStatus;
import com.alaris_us.daycareclient.persistance.commands.UpdateUnitMemberStatus;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.Invoker.InvokerStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceData.PersistenceDataListener;
import com.alaris_us.daycareclient.services.Service_Capacity;
import com.alaris_us.daycareclient.services.Service_DateTime;
import com.alaris_us.daycareclient.services.Service_Temperature;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycareclient.yombu.YombuData;
import com.alaris_us.daycareclient.yombu.YombuRESTOperationComplete;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.ParseData;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Document;
import com.alaris_us.daycaredata.to.EmailQueue;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.FamilyAssociation;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Offer;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Schedule;
import com.alaris_us.daycaredata.to.SelfCheckIn;
import com.alaris_us.daycaredata.to.User;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.externaldata.ExternalData;
import com.alaris_us.externaldata.ExternalDataOperationComplete;
import com.alaris_us.externaldata.abc.ABCExternalData;
import com.alaris_us.externaldata.activenet.ActiveNetExternalData;
import com.alaris_us.externaldata.ccc.CCCExternalData;
import com.alaris_us.externaldata.data.Barcode;
import com.alaris_us.externaldata.data.PhoneNumber;
import com.alaris_us.externaldata.daxko.DaxkoExternalData;
import com.alaris_us.externaldata.exception.ExternalDataException;
import com.alaris_us.externaldata.personify.PersonifyExternalData;
import com.alaris_us.externaldata.reclique.RecliqueExternalData;
import com.alaris_us.externaldata.spirit.SpiritExternalData;
import com.alaris_us.externaldata.to.UnitInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class PersistenceLayer {
	public interface PersistenceLayerListener {
		public void updating();

		public void updated();

		public void changed(CHANGE_ID id, Object newValue);

		public void exception(Exception e);
	}

	private final int mDefaultMaxChildAgeInYears;
	private ListenerRelay m_listenerRelay;
	private final PersistenceData m_persistenceData;
	private Invoker m_commandInvoker;
	private String m_androidID;
	private DaycareData m_daycareDataSource;
	private ExternalData m_externalDataSource;
	private Service_DateTime service_dateTime;
	private Service_Temperature service_temperature;
	private Service_Capacity service_capacity;
	private Context context;
	private static String PARSE_USER_PASSWORD;
	private List<PersistenceLayerListener> m_listeners = new CopyOnWriteArrayList<PersistenceLayerListener>();

	class ListenerRelay implements InvokerStatusListener, PersistenceDataListener {

		private int tasks = 0;

		@Override
		public void started() {
			if (tasks == 0) {
				for (PersistenceLayerListener pl : m_listeners) {
					Log.d("PersistenceLayer", "updating -> " + tasks);
					pl.updating();
				}
			}
			Log.d("PersistenceLayer", "tasks -> " + tasks);
			incrementCount();

		}

		@Override
		public void finished() {
			decrementCount();
			Log.d("PersistenceLayer", "tasks -> " + tasks);
			if (tasks == 0) {
				Log.d("PersistenceLayer", "updated -> " + tasks);
				for (PersistenceLayerListener pl : m_listeners) {
					pl.updated();
				}
			}
		}

		@Override
		public void exception(Exception e) {
			for (PersistenceLayerListener pl : m_listeners) {
				pl.exception(e);
			}
		}

		@Override
		public void valueChanged(CHANGE_ID id, Object newValue) {
			for (PersistenceLayerListener pl : m_listeners) {
				pl.changed(id, newValue);
			}
		}

		private synchronized void incrementCount() {
			tasks++;
		}

		private synchronized void decrementCount() {
			tasks--;
		}

		public synchronized void resetCount() {
			tasks = 0;
		}

	}

	public PersistenceLayer(Context context) {
		super();
		this.context = context;
		mDefaultMaxChildAgeInYears = context.getResources().getInteger(R.integer.DEFAULT_MAX_CHILD_YEARS);

		// Parse Keys
		String parseApplicationId = context.getString(R.string.PARSE_APPLICATION_ID);
		String parseClientKey = context.getString(R.string.PARSE_CLIENT_KEY);
		PARSE_USER_PASSWORD = parseClientKey;
		String parseServerURL = context.getString(R.string.PARSE_SERVER_URL);
		// Daxko Keys
		/*String daxkoUserName = context.getString(R.string.DAXKO_USER_NAME);
		String daxkoPassword = context.getString(R.string.DAXKO_PASSWORD);
		String daxkoEndpoint = context.getString(R.string.DAXKO_ENDPOINT);
		String daxkoClientId = context.getString(R.string.DAXKO_CLIENTID);
		boolean daxkoDetailedLog = true;*/
		// Spirit Keys
		/*String spiritUserName = context.getString(R.string.SPIRIT_USER_NAME_PROD);
		String spiritPassword = context.getString(R.string.SPIRIT_PASSWORD_PROD);
		String spiritEndpoint = context.getString(R.string.SPIRIT_ENDPOINT_PROD);*/
		//String daxkoClientId = context.getString(R.string.DAXKO_CLIENTID);
		// Initialize DAOs //TODO Change Databases Here

		m_daycareDataSource = new ParseData(context, parseApplicationId, parseClientKey, parseServerURL, false);
		/*m_externalDataSource = new DaxkoExternalData(daxkoUserName, daxkoPassword, daxkoEndpoint, daxkoClientId,
				daxkoDetailedLog);*/
		//m_externalDataSource = new SpiritExternalData(spiritUserName, spiritPassword, spiritEndpoint);
		//m_externalDataSource = new ActiveNetExternalData(context, parseApplicationId, parseClientKey, parseServerURL);

		// Get Android ID which is used for logging into parse
		m_androidID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		// Initialize internal structure
		m_listenerRelay = new ListenerRelay();
		m_persistenceData = new PersistenceData(m_listenerRelay);
		m_commandInvoker = new PersistenceCommandLoggingInvoker(m_daycareDataSource, m_persistenceData, m_listenerRelay);
		// startServices();
	}

	public void buildExternalDataSource(Facility facility) {

		// Daxko Keys
		String daxkoUserName = "";
		String daxkoPassword = "";
		String daxkoEndpoint = "";
		String daxkoClientId ="";
		boolean daxkoDetailedLog = true;
		
		String dataSource = "";
		JSONObject externalData = new JSONObject();
		externalData = facility.getExternalDataOpts();
		try {
			if (externalData != null) {
				dataSource = externalData.getString("dataSource");
				m_persistenceData.setDataSource(dataSource);

				switch (dataSource) {
				case "Daxko":
					daxkoUserName = context.getString(R.string.DAXKO_USER_NAME);
					daxkoPassword = context.getString(R.string.DAXKO_PASSWORD);
					daxkoEndpoint = context.getString(R.string.DAXKO_ENDPOINT);
					daxkoClientId = externalData.getString("clientId");
					m_externalDataSource = new DaxkoExternalData(daxkoUserName, daxkoPassword, daxkoEndpoint, daxkoClientId, daxkoDetailedLog);
					break;
				case "CCC":
					String cccBaseEndpoint = externalData.getString("domain");
					String cccToken = externalData.getString("token");
					String cccDirVal = externalData.getString("dirval");
					m_externalDataSource = new CCCExternalData(cccBaseEndpoint, cccToken, cccDirVal);
					break;
				case "Spirit":
					String spiritUserName = context.getString(R.string.SPIRIT_USER_NAME_PROD);
					String spiritPassword = context.getString(R.string.SPIRIT_PASSWORD_PROD);
					String spiritEndpoint = context.getString(R.string.SPIRIT_ENDPOINT_PROD);
					m_externalDataSource = new SpiritExternalData(spiritUserName, spiritPassword, spiritEndpoint);
					break;
				case "ABC":
					String abcAppId = context.getString(R.string.ABC_APP_ID);
					String abcAppKey = context.getString(R.string.ABC_APP_KEY);
					String abcEndpoint = context.getString(R.string.ABC_ENDPOINT_PROD);
					String clubNumber = externalData.getString("clubNumber");
					m_externalDataSource = new ABCExternalData(abcEndpoint, abcAppId, abcAppKey, clubNumber);
					break;
				case "ABC_MSF":
					String abcMsfAppId = context.getString(R.string.ABC_APP_ID);
					String abcMsfAppKey = context.getString(R.string.ABC_APP_KEY);
					String abcMsfEndpoint = context.getString(R.string.ABC_ENDPOINT_PROD);
					String msfClubNumber = externalData.getString("clubNumber");
					m_externalDataSource = new ABCExternalData(abcMsfEndpoint, abcMsfAppId, abcMsfAppKey, msfClubNumber);
					break;
				case "Personify_TC":
					String personifyTCUsername = context.getString(R.string.PERSONIFY_TC_USER_NAME);
					String personifyTCPassword = context.getString(R.string.PERSONIFY_TC_PASSWORD);
					String personifyTCEndpoint = externalData.getString("domain");
					m_externalDataSource = new PersonifyExternalData(personifyTCUsername, personifyTCPassword, personifyTCEndpoint,
							"", daxkoDetailedLog);
					break;
				case "Personify_TRI":
					String personifyTRIUsername = context.getString(R.string.PERSONIFY_TRI_USER_NAME);
					String personifyTRIPassword = context.getString(R.string.PERSONIFY_TRI_PASSWORD);
					String personifyTRIEndpoint = externalData.getString("domain");
					m_externalDataSource = new PersonifyExternalData(personifyTRIUsername, personifyTRIPassword, personifyTRIEndpoint,
							"", daxkoDetailedLog);
					break;
				case "Reclique":
					String recliqueUsername = context.getString(R.string.RECLIQUE_USER_NAME);
					String recliquePassword = context.getString(R.string.RECLIQUE_PASSWORD);
					String recliqueEndpoint = externalData.getString("domain");
					m_externalDataSource = new RecliqueExternalData(recliqueUsername, recliquePassword, recliqueEndpoint);
					break;
				case "ActiveNet":
					String parseApplicationId = context.getString(R.string.PARSE_APPLICATION_ID);
					String parseClientKey = context.getString(R.string.PARSE_CLIENT_KEY);
					String parseServerURL = context.getString(R.string.PARSE_SERVER_URL);
					m_externalDataSource = new ActiveNetExternalData(context, parseApplicationId, parseClientKey, parseServerURL);
					break;
				case "ABC_EOS":
					String abcEosAppId = context.getString(R.string.ABC_APP_ID);
					String abcEosAppKey = context.getString(R.string.ABC_APP_KEY);
					String abcEosEndpoint = context.getString(R.string.ABC_ENDPOINT_PROD);
					String eosClubNumber = externalData.getString("clubNumber");
					m_externalDataSource = new ABCExternalData(abcEosEndpoint, abcEosAppId, abcEosAppKey, eosClubNumber);
					break;
				default:
					daxkoUserName = context.getString(R.string.DAXKO_USER_NAME);
					daxkoPassword = context.getString(R.string.DAXKO_PASSWORD);
					daxkoEndpoint = context.getString(R.string.DAXKO_ENDPOINT);
					daxkoClientId = context.getString(R.string.DAXKO_CLIENTID);
					m_externalDataSource = new DaxkoExternalData(daxkoUserName, daxkoPassword, daxkoEndpoint, daxkoClientId, daxkoDetailedLog);
					break;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public PersistenceData getPersistenceData() {
		return m_persistenceData;
	}

	public void getExternalUnitInfo(String unitId, final ExternalDataOperationComplete<UnitInfo> cb) {
		m_listenerRelay.started();
		m_externalDataSource.getUnitInfo(unitId, new ExternalDataOperationComplete<UnitInfo>() {
			@Override
			public void DataReceived(UnitInfo arg0, ExternalDataException arg1) {
				m_listenerRelay.finished();
				cb.DataReceived(arg0, arg1);
			}
		});
	}

	public void getExternalUnitMembers(String familyUnit,
			final ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>> cb) {
		m_listenerRelay.started();
		m_externalDataSource.findMembers(familyUnit,
				new ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>>() {
					@Override
					public void DataReceived(List<com.alaris_us.externaldata.to.Member> arg0, ExternalDataException arg1) {
						m_listenerRelay.finished();
						cb.DataReceived(arg0, arg1);
					}
				});
	}

	public Member getNewEmptyMember() {
		return m_daycareDataSource.getMembersDAO().getEmptyTO();
	}

	public Family getNewEmptyFamily() {
		return m_daycareDataSource.getFamiliesDAO().getEmptyTO();
	}

	public EmailQueue getNewEmptyEmail() {
		return m_daycareDataSource.getEmailQueueDAO().getEmptyTO();
	}

	public Document getNewEmptyDocument() {
		return m_daycareDataSource.getDocumentsDAO().getEmptyTO();
	}

	public FamilyAssociation getNewEmptyFamilyAssociation() {
		return m_daycareDataSource.getFamilyAssociationsDAO().getEmptyTO();
	}
	
	public String getAndroidId() {
		return m_androidID;
	}

	public void addListener(PersistenceLayerListener lis) {
		if (m_listeners.size() == 0) {
			m_listenerRelay.resetCount();
		}
		m_listeners.add(lis);
	}

	public void removeListener(PersistenceLayerListener lis) {
		m_listeners.remove(lis);
	}

	public void login() {
		login(m_androidID, PARSE_USER_PASSWORD);
	}

	public void login(CommandStatusListener listener) {
		login(m_androidID, PARSE_USER_PASSWORD, listener);
	}

	public void login(String username, String password) {
		m_commandInvoker.invoke(new Login(username, password));
	}

	public void login(String username, String password, CommandStatusListener listener) {
		m_commandInvoker.invoke(new Login(username, password), listener);
	}

	public void registerUser() {
		registerUser(m_androidID, PARSE_USER_PASSWORD);
	}

	public void registerUser(CommandStatusListener listener) {
		registerUser(m_androidID, PARSE_USER_PASSWORD, listener);
	}

	public void registerUser(String username, String password) {
		m_commandInvoker.invoke(new RegisterUser(username, password));
	}

	public void registerUser(String username, String password, CommandStatusListener listener) {
		m_commandInvoker.invoke(new RegisterUser(username, password), listener);
	}

	public void fetchFacility() {
		User user = m_persistenceData.getUser();
		fetchFacility(user);
	}

	public void fetchFacility(CommandStatusListener listener) {
		User user = m_persistenceData.getUser();
		fetchFacility(user, listener);
	}

	public void fetchFacility(User user) {
		m_commandInvoker.invoke(new FetchFacility(user));
	}

	public void fetchFacility(User user, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchFacility(user), listener);
	}

	// Microservice fetch command access functions.
	public void fetchInCenterMembersStaff() {
		Facility facility = m_persistenceData.getFacility();
		m_commandInvoker.invoke(new FetchClockedInStaffMS(facility));
		m_commandInvoker.invoke(new FetchCheckedInMembersMS(facility));
		m_commandInvoker.invoke(new FetchAllRooms(facility));
	}

    public void fetchClockedInStaff(CommandStatusListener listener) {
		Facility facility = m_persistenceData.getFacility();
		m_commandInvoker.invoke(new FetchClockedInStaff(facility), listener);
	}

    public void fetchCheckedInMembersCount(CommandStatusListener listener) {
		Facility facility = m_persistenceData.getFacility();
		m_commandInvoker.invoke(new FetchCheckedInMembersCount(facility), listener);
	}
    
    public void fetchCheckedInMembers(CommandStatusListener listener) {
        Facility facility = m_persistenceData.getFacility();
        m_commandInvoker.invoke(new FetchCheckedInMembers(facility), listener);
    }

	public void fetchAllWorkoutLocations() {
		Facility facility = m_persistenceData.getFacility();
		fetchAllWorkoutLocations(facility);
	}

	public void fetchAllWorkoutLocations(CommandStatusListener listener) {
		Facility facility = m_persistenceData.getFacility();
		fetchAllWorkoutLocations(facility, listener);
	}

	public void fetchAllWorkoutLocations(Facility facility) {
		m_commandInvoker.invoke(new FetchAllWorkoutAreas(facility));
	}

	public void fetchAllWorkoutLocations(Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchAllWorkoutAreas(facility), listener);
	}

	public void fetchAllCubbies() {
		Facility facility = m_persistenceData.getFacility();
		fetchAllCubbies(facility);
	}

	public void fetchAllCubbies(CommandStatusListener listener) {
		Facility facility = m_persistenceData.getFacility();
		fetchAllCubbies(facility, listener);
	}

	public void fetchAllCubbies(Facility facility) {
		m_commandInvoker.invoke(new FetchAllCubbies(facility));
	}

	public void fetchAllCubbies(Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchAllCubbies(facility), listener);
	}

	public void fetchAllRooms(CommandStatusListener listener) {
		Facility facility = m_persistenceData.getFacility();
		fetchAllRooms(facility, listener);
	}

	public void fetchAllRooms(Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchAllRooms(facility), listener);
	}

	public void fetchAllPrograms(CommandStatusListener listener) {
		Facility facility = m_persistenceData.getFacility();
		fetchAllPrograms(facility, listener);
	}

	public void fetchAllPrograms(Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchAllPrograms(facility), listener);
	}
	
	public void fetchProgramRooms(Program program, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchProgramRooms(program), listener);
	}
	
	public void fetchProgramSchedules(Program program, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchProgramSchedules(program), listener);
	}
	
	public void fetchScheduleRecurrences(List<Schedule> schedule, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchScheduleRecurrences(schedule), listener);
	}
	
	public void fetchAvailableCubbies(Facility facility) {
		m_commandInvoker.invoke(new FetchAvailableCubbies(facility));
	}

	public void fetchAvailableCubbies(Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchAvailableCubbies(facility), listener);
	}

	public void fetchMembersCubbies(Facility facility, List<Member> members) {
		m_commandInvoker.invoke(new FetchMembersCubbies(facility, members));
	}

	public void fetchAvailablePagers(Facility facility) {
		m_commandInvoker.invoke(new FetchAvailablePagers(facility));
	}

	public void fetchAvailablePagers(Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchAvailablePagers(facility), listener);
	}

    public void fetchFacilityQuestions(Facility facility, CommandStatusListener listener) {
        m_commandInvoker.invoke(new FetchFacilityQuestions(facility), listener);
    }

	public void fetchParentData(Member member, Facility facility, CommandStatusListener listener) {
		fetchAllChildren(member);
		m_commandInvoker.invoke(new FetchMemberFamily(member), listener);
	}

	public void fetchAllChildren(Member member) {
		m_commandInvoker.invoke(new FetchAllChildren(member));
	}

	public void fetchFamilyCubbies(Family family) {
		m_commandInvoker.invoke(new FetchFamilyCubbies(family));
	}

	public void fetchFamilyCubbies(Family family, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchFamilyCubbies(family), listener);
	}

	public void authenticateByPinInDaycareData(String credential) {
		m_commandInvoker.invoke(new AuthenticateByPin(credential));
	}

	public void authenticateByPinInDaycareData(String credential, CommandStatusListener listener) {
		m_commandInvoker.invoke(new AuthenticateByPin(credential), listener);
	}

	public void authenticateByBarcodeInDaycareData(String credential) {
		m_commandInvoker.invoke(new AuthenticateByBarcode(credential));
	}

	public void authenticateByBarcodeInDaycareData(String credential, CommandStatusListener listener) {
		Log.e(PersistenceLayer.class.getSimpleName(), "BARCODE: " + credential);
		Log.e(PersistenceLayer.class.getSimpleName(), "BARCODE LENGTH: " + credential.length());
		m_commandInvoker.invoke(new AuthenticateByBarcode(credential), listener);
	}

	public void authenticateByBarcodeOrMagCardInDaycareData(String credential, CommandStatusListener listener) {
		m_commandInvoker.invoke(new AuthenticateByBarcodeOrMagCard(credential), listener);
	}

	public void fetchSelfCheckInMembers(DaycareCredential credential, Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchSelfCheckInMembers(credential, facility), listener);
	}

	public void authenticateByPinInYMCAData(String credential,
			final ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>> callback) {
		if (credential == null)
			return;
		m_listenerRelay.started();
		PhoneNumber pin = PhoneNumber.getNewInstance(credential.toString());
		m_externalDataSource.findMembers(pin,
				new ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>>() {
					@Override
					public void DataReceived(List<com.alaris_us.externaldata.to.Member> arg0, ExternalDataException arg1) {
						m_listenerRelay.finished();
						callback.DataReceived(arg0, arg1);
					}
				});
	}

	public void authenticateByBarcodeInYMCAData(String credential,
			final ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>> callback) {

		Barcode bc = Barcode.getNewInstance(credential.toString());
		m_listenerRelay.started();
		m_externalDataSource.findMembers(bc,
				new ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>>() {
					@Override
					public void DataReceived(List<com.alaris_us.externaldata.to.Member> arg0, ExternalDataException arg1) {
						m_listenerRelay.finished();
						callback.DataReceived(arg0, arg1);
					}
				});
	}

	public void registerMember(String barcode, CommandStatusListener listener) {
		getPersistenceData().setIsRegistration(true);
		Facility facility = getPersistenceData().getFacility();
		m_commandInvoker.invoke(new RegisterMember(facility, barcode), listener);
	}

	public void fetchAddOns(String clubNumber, String memberId, String addOn, final ExternalDataOperationComplete<String> callback) {
		m_listenerRelay.started();
		m_externalDataSource.findMemberAddOns(clubNumber, memberId, addOn, new ExternalDataOperationComplete<String>() {
			@Override
			public void DataReceived(String isProfitCenterInUse, ExternalDataException e) {
				m_listenerRelay.finished();
				callback.DataReceived(isProfitCenterInUse, e);
			}
		});
	}


	public void bindFragmentToService(MainScreenFragment fragment) {
		if (service_capacity != null && fragment != null)
			service_capacity.bindFragment(fragment);
	}



	public ExternalData getExternalData() {
		return m_externalDataSource;
	}

	public void registerFamilyInDaycare(List<Member> parents, List<Member> children, CommandStatusListener listener) {
		getPersistenceData().setIsRegistration(true);
		Family family = getNewEmptyFamily();
		family.setEmergencySkips(getPersistenceData().getFacility().getEmergencySkips());
		Facility facility = getPersistenceData().getFacility();
		m_commandInvoker.invoke(new RegisterFamilyInDaycare(family, parents, children, facility), listener);
	}
	
	public void registerFamilyInDaycare(List<Member> parents, List<Member> children, Family family, CommandStatusListener listener) {
		getPersistenceData().setIsRegistration(true);
		family.setEmergencySkips(getPersistenceData().getFacility().getEmergencySkips());
		Facility facility = getPersistenceData().getFacility();
		m_commandInvoker.invoke(new RegisterFamilyInDaycare(family, parents, children, facility), listener);
	}
	
	public void registerAuthChildInDaycare(Member child, CommandStatusListener listener) {
		/*Family family = getNewEmptyFamily();
		family.setEmergencySkips(getPersistenceData().getFacility().getEmergencySkips());*/
		Facility facility = getPersistenceData().getFacility();
		m_commandInvoker.invoke(new RegisterAuthChildInDaycare(child, facility), listener);
	}

	public void setupFamilyAssociationInDaycare(List<Member> parents, List<Member> children,
			CommandStatusListener listener) {
		m_commandInvoker.invoke(new SetupFamilyAssociationInDaycare(m_persistenceData.getFamily(), parents, children),
				listener);
	}
	
	public void setupAuthChildFamilyAssociationInDaycare(Member child, CommandStatusListener listener) {
		m_commandInvoker.invoke(new SetupAuthChildFamilyAssociationInDaycare(child.getPFamily(), child),
				listener);
	}
	
	public void setupAuthorization(Member child, Member authorizedBy, CommandStatusListener listener) {
		m_commandInvoker.invoke(new CreateAuthorization(child, authorizedBy), listener);
	}

	private Member updateChildAtCheckIn(Member child, Number dailyTimeLimit, String currentDate) {
		int minutesLeft = dailyTimeLimit.intValue();
		if (m_persistenceData.getIsOverride()){
			child.setMinutesLeft(minutesLeft);
		}
		else if (null == child.getLastCheckIn()) {
			child.setMinutesLeft(minutesLeft);
		} else if (DateGenerator.getFormattedDate(child.getLastCheckIn()).compareTo(currentDate) != 0){
			if (child.isGuest()) {
				if (child.getGuestDaysLeft().intValue() > 0)
					child.setGuestDaysLeft(child.getGuestDaysLeft().intValue() - 1);
				else
					minutesLeft = 0;
			}
			child.setMinutesLeft(minutesLeft);
		}
		return child;
	}

	public void checkInFamily(Facility facility, Family family, List<Member> children, Member parent,
			SignInType signinType, List<Cubby> cubbies, List<Pager> pagers, List<Room> rooms, List<WorkoutArea> selectedAreas,
			List<WorkoutArea> allAreas, String returnTime) {
		String service = getPersistenceData().getSelectedService();
		String language = getPersistenceData().getSelectedLanguage();
		for (Member child : children) {
			m_commandInvoker.invoke(new CheckInChild(facility, child, parent, signinType, selectedAreas, service, language));
		}
		m_commandInvoker.invoke(new CheckInParent(parent, selectedAreas, allAreas, returnTime));
		m_commandInvoker.invoke(new ReserveCubbies(cubbies, children, family, facility));
		if (!pagers.isEmpty()) {
			m_commandInvoker.invoke(new ReservePagers(pagers, children, family, facility, parent));
		}
	}

	public void checkOutFamily(Facility facility, Family family, List<Member> children, Member parent,
			SignInType signinType, List<Cubby> cubbyPairs, List<WorkoutArea> allAreas, List<Room> rooms) {
		m_commandInvoker.invoke(new ReturnCubbies(children));
		for (Member child : children) {
			m_commandInvoker.invoke(new CheckOutChild(facility, child, parent, signinType));
		}
		m_commandInvoker.invoke(new CheckOutParent(parent, allAreas));
		m_commandInvoker.invoke(new UpdateFamily(family));
	}

	public void findIncidents(List<Member> members) {
		m_commandInvoker.invoke(new FindIncidents(members));
	}

	public void findIncidents(List<Member> members, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FindIncidents(members), listener);
	}

	public void updateIncidents(List<Incident> incidents, boolean isAcknowledged) {
		for (Incident incident : incidents)
			m_commandInvoker.invoke(new UpdateIncidents(incident, isAcknowledged));
	}

	public void updateMemberStatus(List<Member> members, Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new UpdateMemberStatus(members, facility), listener);
	}

	public void updateUnitMemberStatus(List<Member> members, Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new UpdateUnitMemberStatus(members, facility), listener);
	}

	public boolean usesCapacityWidget() {
		if(context.getString(R.string.ASSOCIATION_NAME).equals(context.getString(R.string.ASSOCIATION_NAME_EOS)))
			return true;
		else
			return false;
	}

	public void updateCapacityInfo() {
		String updatedCapacityInfo = "";
		try {
			if (usesCapacityWidget() && service_capacity != null) {
				updatedCapacityInfo = service_capacity.getCapacityInfoText();
				m_persistenceData.setCapacityInfo(updatedCapacityInfo);
			}
		} catch(Exception e) {
			Log.e("PersistenceLayer", "Error updating capacity info.");
			Log.i("PersistenceLayer", e.getStackTrace().toString());
		}
	}

	public void startServices() {
		if (service_temperature != null)
			service_temperature.shutdown();
		if (service_capacity != null)
			service_capacity.shutdown();
		service_temperature = new Service_Temperature(this, context.getResources());
		service_dateTime = new Service_DateTime(this, context.getResources());

		if(usesCapacityWidget()) {
			service_capacity = new Service_Capacity(this);
			service_capacity.startService();
		}
	}

	public void stopServices() {
		service_dateTime.shutdown();
		service_temperature.shutdown();
		if (service_capacity != null)
			service_capacity.shutdown();
	}

	public void updateMember(Member member, CommandStatusListener listener) {
		m_commandInvoker.invoke(new UpdateMember(member), listener);
	}

	public void updateAllMembers(List<Member> members, CommandStatusListener listener) {
		m_commandInvoker.invoke(new UpdateAllMembers(members), listener);
	}

	public void addEmailToEmailQueue(Member member, String email, Offer offer) {
		EmailQueue emailQueue = getNewEmptyEmail();
		emailQueue.setToName(member.getFirstName() + " " + member.getLastName());
		emailQueue.setToEmail(email);
		emailQueue.setIsSent(false);
		emailQueue.setRetries(3);
		emailQueue.setPOffer(offer);
		m_commandInvoker.invoke(new AddEmailToEmailQueue(emailQueue));
	}

	public void fetchAllOffers(Facility facility, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchAllOffers(facility), listener);
	}

	public void selectUser(Member member) {
		selectUser(member, null);
	}

	public void selectUser(Member member, CommandStatusListener listener) {
		m_persistenceData.setCurrentMember(member);
		m_commandInvoker.invoke(new FetchAMembersFamilies(member, mDefaultMaxChildAgeInYears), listener);
	}
	
	public void getMemberByMemberId(String memberId, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchMemberByMemberId(memberId), listener);
	}
	
	public void getHomeFacilityByClubNumber(String clubNumber, CommandStatusListener listener) {
		m_commandInvoker.invoke(new FetchHomeFacility(clubNumber), listener);
	}

	public void isMembershipValidForFacility(Facility facility, String membershipType, CommandStatusListener listener) {
		m_commandInvoker.invoke(new IsMembershipValidForFacility(facility, membershipType), listener);
	}

	public Member createMemberFromYMCAData(com.alaris_us.externaldata.to.Member member, String childSeparatorDate, Date defaultDate){
		return createMemberFromYMCAData(member, childSeparatorDate, defaultDate, null, null);
	}
	
	public Member createMemberFromYMCAData(com.alaris_us.externaldata.to.Member member, String childSeparatorDate, Date defaultDate, String membershipType,
										   Facility homeFacility) {
		String dataSource = getPersistenceData().getDataSource();

		switch (dataSource) {
			case "Daxko":
				return createMemberFromDaxkoData(member, childSeparatorDate, defaultDate, membershipType);
			case "CCC":
				return createMemberFromCCCData(member, childSeparatorDate, defaultDate);
			case "Spirit":
				return createMemberFromSpiritData(member, childSeparatorDate, defaultDate, homeFacility);
			case "ABC":
				return createMemberFromABCFinancialData(member, childSeparatorDate, defaultDate, homeFacility);
			case "ABC_MSF":
				return createMemberFromABCFinancialMSFData(member, childSeparatorDate, defaultDate, homeFacility);
			case "ActiveNet":
				return createMemberFromActiveNetData(member, childSeparatorDate, defaultDate);
			case "Personify_TC":
				return createMemberFromPersonifyTCData(member, childSeparatorDate, defaultDate);
			case "Personify_TRI":
				return createMemberFromPersonifyTRIData(member, childSeparatorDate, defaultDate);
			case "Reclique":
				return createMemberFromRecliqueData(member, childSeparatorDate, defaultDate, homeFacility);
			case "ABC_EOS":
				return createMemberFromABCFinancialEOSData(member, childSeparatorDate, defaultDate, homeFacility);
			default:
				return null;
		}
	}
	
	public Member createMemberFromDaxkoData(com.alaris_us.externaldata.to.Member daxkoMember, String childSeparatorDate,
			Date defaultDate, String membershipType) {

		Member newMember = getNewEmptyMember();
		newMember.setFirstName(daxkoMember.getFirstName());
		newMember.setLastName(daxkoMember.getLastName());
		newMember.setMemberID(daxkoMember.getMemberId());
		newMember.setUnitID(daxkoMember.getUnitID());
		newMember.setBirthDate(daxkoMember.getBirthDate());
		newMember.setGender(daxkoMember.getGender());
		newMember.setUsesChildWatch(true);
		if (daxkoMember.getEmail() != null) {
			newMember.setEmail(daxkoMember.getEmail().toString());
		}
		if (daxkoMember.getBarcode() != null) {
			newMember.setBarcode(daxkoMember.getBarcode().toString());
		}

		for (PhoneNumber n : daxkoMember.getPhones()) {
			if (!n.getType().equals("EMERGENCY"))
				newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
			com.alaris_us.daycaredata.PhoneNumber p = new com.alaris_us.daycaredata.PhoneNumber(n.getType(), n.getDashedSeparatedNumberWithAreaCode());
			newMember.addPhoneNumber(p);
		}

		newMember.setGuest(false);
		newMember.setActive(daxkoMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		newMember.setWeeklyMinutesLeft(getPersistenceData().getFacility().getWeeklyTimeLimit().intValue());
		newMember.setUsesSMS(true);
		if (!getPersistenceData().getFacility().getUsesValidMembership())
			newMember.setIsValidMembership(true);
		newMember.setMembershipType(membershipType);
		return newMember;
	}
	
	public Member createMemberFromSpiritData(com.alaris_us.externaldata.to.Member spiritMember, String childSeparatorDate,
			Date defaultDate, Facility homeFacility) {

		Member newMember = getNewEmptyMember();
		newMember.setFirstName(spiritMember.getFirstName());
		newMember.setLastName(spiritMember.getLastName());
		newMember.setMemberID(spiritMember.getMemberId());
		newMember.setUnitID(spiritMember.getUnitID());
		newMember.setBirthDate(spiritMember.getBirthDate());
		newMember.setGender(spiritMember.getGender());
		newMember.setMembershipType(spiritMember.getMembershipType());
		if (spiritMember.getEmail() != null) {
			newMember.setEmail(spiritMember.getEmail().toString());
		}
		if (spiritMember.getBarcode() != null) {
			newMember.setBarcode(spiritMember.getBarcode().toString());
		}

		if (DateGenerator.getFormattedDate(spiritMember.getBirthDate()).compareTo(childSeparatorDate) < 0) {
			for (PhoneNumber n : spiritMember.getPhones()) {
				if (n != null)
					newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
			}
		}

		// create internal emergency numbers list
		List<com.alaris_us.daycaredata.EmergencyNumber> emergencyNumbers = new ArrayList<com.alaris_us.daycaredata.EmergencyNumber>();
		if (spiritMember.getEmergencyNumbers() != null) {
			for (com.alaris_us.externaldata.data.EmergencyNumber n : spiritMember.getEmergencyNumbers()) {
				if (n != null) {
					com.alaris_us.daycaredata.EmergencyNumber parseEmergencyNumber = new com.alaris_us.daycaredata.EmergencyNumber();
					parseEmergencyNumber.setName(n.getName());
					parseEmergencyNumber.setPhone(n.getDashedSeparatedNumberWithAreaCode());
					emergencyNumbers.add(parseEmergencyNumber);
				}

			}
		}
		newMember.setEmergencyNumbers(emergencyNumbers);

		// create phones list
		List<com.alaris_us.daycaredata.PhoneNumber> phoneNumbers = new ArrayList<com.alaris_us.daycaredata.PhoneNumber>();

		for (com.alaris_us.externaldata.data.PhoneNumber n : spiritMember.getPhones()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}
		}
		newMember.setPhoneNumbers(phoneNumbers);

		newMember.setGuest(false);
		newMember.setActive(spiritMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		newMember.setUsesSMS(true);
		newMember.setIsPottyTrained(false);
		if (!getPersistenceData().getFacility().getUsesValidMembership())
			newMember.setIsValidMembership(true);
		newMember.setUsesChildWatch(true);
		newMember.setMedicalConcerns(spiritMember.getMedicalConcerns());
		newMember.setImage(spiritMember.getPhoto());
		newMember.setPHomeFacility(homeFacility);
		return newMember;
	}
	
	public Member createMemberFromCCCData(com.alaris_us.externaldata.to.Member cccMember, String childSeparatorDate,
			 Date defaultDate) {

		Member newMember = getNewEmptyMember();
		newMember.setFirstName(cccMember.getFirstName());
		newMember.setLastName(cccMember.getLastName());
		newMember.setMemberID(cccMember.getMemberId());
		newMember.setUnitID(cccMember.getUnitID());
		newMember.setBirthDate(cccMember.getBirthDate());
		newMember.setGender(cccMember.getGender());
		newMember.setUsesChildWatch(true);
		if (cccMember.getEmail() != null) {
			newMember.setEmail(cccMember.getEmail().toString());
		}
		if (cccMember.getBarcode() != null) {
			newMember.setBarcode(cccMember.getBarcode().toString());
		}

		if (DateGenerator.getFormattedDate(cccMember.getBirthDate()).compareTo(childSeparatorDate) < 0) {
			for (PhoneNumber n : cccMember.getPhones()) {
				if (n != null) {
					if (!n.getType().equals("EMGERGENCY"))
						newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
					com.alaris_us.daycaredata.PhoneNumber p = new com.alaris_us.daycaredata.PhoneNumber(n.getType(), n.getDashedSeparatedNumberWithAreaCode());
					newMember.addPhoneNumber(p);
				}
			}
		}
		
		// create internal emergency numbers
		List<com.alaris_us.daycaredata.EmergencyNumber> emergencyNumbers = new ArrayList<com.alaris_us.daycaredata.EmergencyNumber>();
		for (com.alaris_us.externaldata.data.EmergencyNumber n : cccMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.EmergencyNumber parseEmergencyNumber = new com.alaris_us.daycaredata.EmergencyNumber();
				parseEmergencyNumber.setName(n.getName());
				parseEmergencyNumber.setPhone(n.getDashedSeparatedNumberWithAreaCode());
				emergencyNumbers.add(parseEmergencyNumber);
			}

		}
		newMember.setEmergencyNumbers(emergencyNumbers);

		newMember.setGuest(false);
		newMember.setActive(cccMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		newMember.setWeeklyMinutesLeft(getPersistenceData().getFacility().getWeeklyTimeLimit().intValue());
		newMember.setUsesSMS(true);
		newMember.setIsPottyTrained(false);
		newMember.setMembershipType(cccMember.getMembershipType());
		newMember.setImage(cccMember.getPhoto() == null ? null : cccMember.getPhoto());
		if (!getPersistenceData().getFacility().getUsesValidMembership())
			newMember.setIsValidMembership(true);
		return newMember;
	}

	public Member createMemberFromABCFinancialData(com.alaris_us.externaldata.to.Member abcMember, String childSeparatorDate,
			Date defaultDate, Facility homeFacility) {

		Member newMember = getNewEmptyMember();
		newMember.setFirstName(abcMember.getFirstName());
		newMember.setLastName(abcMember.getLastName());
		newMember.setMemberID(abcMember.getMemberId());
		newMember.setUnitID(abcMember.getUnitID());
		newMember.setBirthDate(abcMember.getBirthDate());
		newMember.setGender(abcMember.getGender());
		newMember.setMembershipType(abcMember.getMembershipType());
		if (abcMember.getEmail() != null) {
			newMember.setEmail(abcMember.getEmail().toString());
		}
		if (abcMember.getBarcode() != null) {
			newMember.setBarcode(abcMember.getBarcode().toString());
		}

		if (DateGenerator.getFormattedDate(abcMember.getBirthDate()).compareTo(childSeparatorDate) < 0) {
			for (PhoneNumber n : abcMember.getPhones()) {
				if (n != null)
					newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
			}
		}

		// create internal emergency numbers list
		List<com.alaris_us.daycaredata.EmergencyNumber> emergencyNumbers = new ArrayList<com.alaris_us.daycaredata.EmergencyNumber>();
		for (com.alaris_us.externaldata.data.EmergencyNumber n : abcMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.EmergencyNumber parseEmergencyNumber = new com.alaris_us.daycaredata.EmergencyNumber();
				parseEmergencyNumber.setName(n.getName());
				parseEmergencyNumber.setPhone(n.getDashedSeparatedNumberWithAreaCode());
				emergencyNumbers.add(parseEmergencyNumber);
			}

		}
		newMember.setEmergencyNumbers(emergencyNumbers);

		// create phones list
		List<com.alaris_us.daycaredata.PhoneNumber> phoneNumbers = new ArrayList<com.alaris_us.daycaredata.PhoneNumber>();

		for (com.alaris_us.externaldata.data.PhoneNumber n : abcMember.getPhones()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}
		}
		for (com.alaris_us.externaldata.data.EmergencyNumber n : abcMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}

		}
		newMember.setPhoneNumbers(phoneNumbers);

		newMember.setGuest(false);
		newMember.setActive(abcMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		newMember.setUsesSMS(true);
		newMember.setIsPottyTrained(false);
		if (!getPersistenceData().getFacility().getUsesValidMembership())
			newMember.setIsValidMembership(true);
		newMember.setUsesChildWatch(true);
		newMember.setMedicalConcerns(abcMember.getMedicalConcerns());
		newMember.setImage(abcMember.getPhoto());
		newMember.setPHomeFacility(homeFacility);
		return newMember;
	}

	public Member createMemberFromABCFinancialMSFData(com.alaris_us.externaldata.to.Member abcMember, String childSeparatorDate,
			Date defaultDate, Facility homeFacility) {

		Member newMember = getNewEmptyMember();
		newMember.setFirstName(abcMember.getFirstName());
		newMember.setLastName(abcMember.getLastName());
		newMember.setMemberID(abcMember.getMemberId());
		newMember.setUnitID(abcMember.getUnitID());
		newMember.setBirthDate(abcMember.getBirthDate());
		newMember.setGender(abcMember.getGender());
		newMember.setMembershipType(abcMember.getMembershipType());
		if (abcMember.getEmail() != null) {
			newMember.setEmail(abcMember.getEmail().toString());
		}
		if (abcMember.getBarcode() != null) {
			newMember.setBarcode(abcMember.getBarcode().toString());
		}

		if (DateGenerator.getFormattedDate(abcMember.getBirthDate()).compareTo(childSeparatorDate) < 0) {
			for (PhoneNumber n : abcMember.getPhones()) {
				if (n != null)
					newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
			}
		}

		// create internal emergency numbers list
		List<com.alaris_us.daycaredata.EmergencyNumber> emergencyNumbers = new ArrayList<com.alaris_us.daycaredata.EmergencyNumber>();
		for (com.alaris_us.externaldata.data.EmergencyNumber n : abcMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.EmergencyNumber parseEmergencyNumber = new com.alaris_us.daycaredata.EmergencyNumber();
				parseEmergencyNumber.setName(n.getName());
				parseEmergencyNumber.setPhone(n.getDashedSeparatedNumberWithAreaCode());
				emergencyNumbers.add(parseEmergencyNumber);
			}

		}
		newMember.setEmergencyNumbers(emergencyNumbers);

		// create phones list
		List<com.alaris_us.daycaredata.PhoneNumber> phoneNumbers = new ArrayList<com.alaris_us.daycaredata.PhoneNumber>();

		for (com.alaris_us.externaldata.data.PhoneNumber n : abcMember.getPhones()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}
		}
		for (com.alaris_us.externaldata.data.EmergencyNumber n : abcMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}

		}
		newMember.setPhoneNumbers(phoneNumbers);

		newMember.setGuest(false);
		newMember.setActive(abcMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		newMember.setUsesSMS(true);
		newMember.setIsPottyTrained(false);
		if (abcMember.getMembershipType().contains("Family")) {
			if (abcMember.isActive())
				newMember.setIsValidMembership(true);
			else
				newMember.setIsValidMembership(false);
		} else {
			if (!abcMember.getRecurringService().isEmpty()) {
				newMember.setService(abcMember.getRecurringService());
				newMember.setIsValidMembership(true);
			} else
				newMember.setIsValidMembership(false);
		}

		newMember.setUsesChildWatch(true);
		newMember.setMedicalConcerns(abcMember.getMedicalConcerns());
		newMember.setImage(abcMember.getPhoto());
		newMember.setPHomeFacility(homeFacility);
		return newMember;
	}

	public Member createMemberFromActiveNetData(com.alaris_us.externaldata.to.Member activeNetMember, String childSeparatorDate,
			 Date defaultDate) {

		Member newMember = getNewEmptyMember();
		newMember.setFirstName(activeNetMember.getFirstName());
		newMember.setLastName(activeNetMember.getLastName());
		newMember.setMemberID(activeNetMember.getMemberId());
		newMember.setUnitID(activeNetMember.getUnitID());
		newMember.setBirthDate(activeNetMember.getBirthDate());
		newMember.setGender(activeNetMember.getGender());
		newMember.setUsesChildWatch(true);
		if (activeNetMember.getEmail() != null) {
			newMember.setEmail(activeNetMember.getEmail().toString());
		}
		if (activeNetMember.getBarcode() != null) {
			newMember.setBarcode(activeNetMember.getBarcode().toString());
		}

		if (DateGenerator.getFormattedDate(activeNetMember.getBirthDate()).compareTo(childSeparatorDate) < 0) {
			for (PhoneNumber n : activeNetMember.getPhones()) {
				if (!n.getType().equals("EMGERGENCY"))
					newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
				com.alaris_us.daycaredata.PhoneNumber p = new com.alaris_us.daycaredata.PhoneNumber(n.getType(), n.getDashedSeparatedNumberWithAreaCode());
				newMember.addPhoneNumber(p);
			}
		}
		
		newMember.setGuest(false);
		newMember.setActive(activeNetMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		newMember.setUsesSMS(true);
		newMember.setIsPottyTrained(false);
		newMember.setIsValidMembership(activeNetMember.isActive() ? true : false);
		newMember.setMembershipType(activeNetMember.getMembershipType());
		newMember.setImage(activeNetMember.getPhoto() == null ? null : activeNetMember.getPhoto());
		return newMember;
	}
	
	public Member createMemberFromPersonifyTCData(com.alaris_us.externaldata.to.Member personifyMember, String childSeparatorDate,
			 Date defaultDate) {
		Member newMember = getNewEmptyMember();
		newMember.setFirstName(personifyMember.getFirstName());
		newMember.setLastName(personifyMember.getLastName());
		newMember.setMemberID(personifyMember.getMemberId());
		newMember.setUnitID(personifyMember.getUnitID());
		newMember.setBirthDate(personifyMember.getBirthDate());
		newMember.setGender(personifyMember.getGender());
		newMember.setMembershipType(personifyMember.getMembershipType());
		if (personifyMember.getEmail() != null) {
			newMember.setEmail(personifyMember.getEmail().toString());
		}
		if (personifyMember.getBarcode() != null) {
			newMember.setBarcode(personifyMember.getBarcode().toString());
		}

		//if (DateGenerator.getFormattedDate(daxkoMember.getBirthDate()).compareTo(childSeparatorDate) < 0) {
			for (PhoneNumber n : personifyMember.getPhones()) {
				if (n != null)
					newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
			}
		//}

		// create internal emergency numbers list
		List<com.alaris_us.daycaredata.EmergencyNumber> emergencyNumbers = new ArrayList<com.alaris_us.daycaredata.EmergencyNumber>();
		for (com.alaris_us.externaldata.data.EmergencyNumber n : personifyMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.EmergencyNumber parseEmergencyNumber = new com.alaris_us.daycaredata.EmergencyNumber();
				parseEmergencyNumber.setName(n.getName());
				parseEmergencyNumber.setPhone(n.getDashedSeparatedNumberWithAreaCode());
				emergencyNumbers.add(parseEmergencyNumber);
			}

		}
		newMember.setEmergencyNumbers(emergencyNumbers);

		// create phones list
		List<com.alaris_us.daycaredata.PhoneNumber> phoneNumbers = new ArrayList<com.alaris_us.daycaredata.PhoneNumber>();

		for (com.alaris_us.externaldata.data.PhoneNumber n : personifyMember.getPhones()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}
		}
		for (com.alaris_us.externaldata.data.EmergencyNumber n : personifyMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}

		}
		newMember.setPhoneNumbers(phoneNumbers);

		newMember.setGuest(false);
		newMember.setActive(personifyMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		newMember.setUsesSMS(false);
		newMember.setIsPottyTrained(false);
		newMember.setIsValidMembership(true);
		newMember.setUsesChildWatch(true);
		newMember.setMedicalConcerns(personifyMember.getMedicalConcerns());
		newMember.setImage(personifyMember.getPhoto());
		return newMember;
	}
	
	public Member createMemberFromPersonifyTRIData(com.alaris_us.externaldata.to.Member personifyMember, String childSeparatorDate,
			Date defaultDate) {

		Member newMember = getNewEmptyMember();
		newMember.setFirstName(personifyMember.getFirstName());
		newMember.setLastName(personifyMember.getLastName());
		newMember.setMemberID(personifyMember.getMemberId());
		newMember.setUnitID(personifyMember.getUnitID());
		newMember.setBirthDate(personifyMember.getBirthDate());
		newMember.setGender(personifyMember.getGender());
		newMember.setMembershipType(personifyMember.getMembershipType());
		if (personifyMember.getEmail() != null) {
			newMember.setEmail(personifyMember.getEmail().toString());
		}
		if (personifyMember.getBarcode() != null) {
			newMember.setBarcode(personifyMember.getBarcode().toString());
		}

		if (DateGenerator.getFormattedDate(personifyMember.getBirthDate()).compareTo(childSeparatorDate) < 0) {
			for (PhoneNumber n : personifyMember.getPhones()) {
				if (n != null)
					newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
			}
		}

		// create internal emergency numbers list
		List<com.alaris_us.daycaredata.EmergencyNumber> emergencyNumbers = new ArrayList<com.alaris_us.daycaredata.EmergencyNumber>();
		for (com.alaris_us.externaldata.data.EmergencyNumber n : personifyMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.EmergencyNumber parseEmergencyNumber = new com.alaris_us.daycaredata.EmergencyNumber();
				parseEmergencyNumber.setName(n.getName());
				parseEmergencyNumber.setPhone(n.getDashedSeparatedNumberWithAreaCode());
				emergencyNumbers.add(parseEmergencyNumber);
			}

		}
		newMember.setEmergencyNumbers(emergencyNumbers);

		// create phones list
		List<com.alaris_us.daycaredata.PhoneNumber> phoneNumbers = new ArrayList<com.alaris_us.daycaredata.PhoneNumber>();

		for (com.alaris_us.externaldata.data.PhoneNumber n : personifyMember.getPhones()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}
		}
		for (com.alaris_us.externaldata.data.EmergencyNumber n : personifyMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}

		}
		newMember.setPhoneNumbers(phoneNumbers);

		newMember.setGuest(false);
		newMember.setActive(personifyMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		boolean hasCellNumber = false;
		for (com.alaris_us.daycaredata.PhoneNumber p : phoneNumbers) {
			if (p.getType().equals("CELL") || p.getType().equals("SMS"))
				hasCellNumber = true;
		}
		if (hasCellNumber)
			newMember.setUsesSMS(true);
		else 
			newMember.setUsesSMS(false);
		newMember.setIsPottyTrained(false);
		
		if (personifyMember.getMembershipType().contains("Not-a-Member"))
			newMember.setIsValidMembership(false);
		else
			newMember.setIsValidMembership(true);
		
		newMember.setUsesChildWatch(true);
		newMember.setMedicalConcerns(personifyMember.getMedicalConcerns());
		newMember.setImage(personifyMember.getPhoto());
		return newMember;
	}

	public Member createMemberFromRecliqueData(com.alaris_us.externaldata.to.Member recliqueMember, String childSeparatorDate,
		   Date defaultDate, Facility homeFacility) {

		Member newMember = getNewEmptyMember();
		newMember.setFirstName(recliqueMember.getFirstName());
		newMember.setLastName(recliqueMember.getLastName());
		newMember.setMemberID(recliqueMember.getMemberId());
		newMember.setUnitID(recliqueMember.getUnitID());
		newMember.setBirthDate(recliqueMember.getBirthDate());
		newMember.setGender(recliqueMember.getGender());
		newMember.setMembershipType(recliqueMember.getMembershipType());
		if (recliqueMember.getEmail() != null) {
			newMember.setEmail(recliqueMember.getEmail().toString());
		}
		if (recliqueMember.getBarcode() != null) {
			newMember.setBarcode(recliqueMember.getBarcode().toString());
		}

		if (DateGenerator.getFormattedDate(recliqueMember.getBirthDate()).compareTo(childSeparatorDate) < 0) {
			for (PhoneNumber n : recliqueMember.getPhones()) {
				if (n != null)
					newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
			}
		}

		// create internal emergency numbers list
		List<com.alaris_us.daycaredata.EmergencyNumber> emergencyNumbers = new ArrayList<com.alaris_us.daycaredata.EmergencyNumber>();
		if (recliqueMember.getEmergencyNumbers() != null) {
			for (com.alaris_us.externaldata.data.EmergencyNumber n : recliqueMember.getEmergencyNumbers()) {
				if (n != null) {
					com.alaris_us.daycaredata.EmergencyNumber parseEmergencyNumber = new com.alaris_us.daycaredata.EmergencyNumber();
					parseEmergencyNumber.setName(n.getName());
					parseEmergencyNumber.setPhone(n.getDashedSeparatedNumberWithAreaCode());
					emergencyNumbers.add(parseEmergencyNumber);
				}

			}
		}
		newMember.setEmergencyNumbers(emergencyNumbers);

		// create phones list
		List<com.alaris_us.daycaredata.PhoneNumber> phoneNumbers = new ArrayList<com.alaris_us.daycaredata.PhoneNumber>();

		for (com.alaris_us.externaldata.data.PhoneNumber n : recliqueMember.getPhones()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}
		}
		newMember.setPhoneNumbers(phoneNumbers);

		newMember.setGuest(false);
		newMember.setActive(recliqueMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		newMember.setUsesSMS(true);
		newMember.setIsPottyTrained(false);
		if (!getPersistenceData().getFacility().getUsesValidMembership())
			newMember.setIsValidMembership(true);
		newMember.setUsesChildWatch(true);
		newMember.setMedicalConcerns(recliqueMember.getMedicalConcerns());
		newMember.setImage(recliqueMember.getPhoto());
		newMember.setPHomeFacility(homeFacility);
		return newMember;
	}

	public Member createMemberFromABCFinancialEOSData(com.alaris_us.externaldata.to.Member abcMember, String childSeparatorDate,
													  Date defaultDate, Facility homeFacility) {

		Member newMember = getNewEmptyMember();
		newMember.setFirstName(abcMember.getFirstName());
		newMember.setLastName(abcMember.getLastName());
		newMember.setMemberID(abcMember.getMemberId());
		newMember.setUnitID(abcMember.getUnitID());
		newMember.setBirthDate(abcMember.getBirthDate());
		newMember.setGender(abcMember.getGender());
		newMember.setMembershipType(abcMember.getMembershipType());
		if (abcMember.getEmail() != null) {
			newMember.setEmail(abcMember.getEmail().toString());
		}
		if (abcMember.getBarcode() != null) {
			newMember.setBarcode(abcMember.getBarcode().toString());
		}

		if (DateGenerator.getFormattedDate(abcMember.getBirthDate()).compareTo(childSeparatorDate) < 0) {
			for (PhoneNumber n : abcMember.getPhones()) {
				if (n != null)
					newMember.addUserPIN(n.getDashedSeparatedNumberWithAreaCode());
			}
		}

		// create internal emergency numbers list
		List<com.alaris_us.daycaredata.EmergencyNumber> emergencyNumbers = new ArrayList<com.alaris_us.daycaredata.EmergencyNumber>();
		for (com.alaris_us.externaldata.data.EmergencyNumber n : abcMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.EmergencyNumber parseEmergencyNumber = new com.alaris_us.daycaredata.EmergencyNumber();
				parseEmergencyNumber.setName(n.getName());
				parseEmergencyNumber.setPhone(n.getDashedSeparatedNumberWithAreaCode());
				emergencyNumbers.add(parseEmergencyNumber);
			}

		}
		newMember.setEmergencyNumbers(emergencyNumbers);

		// create phones list
		List<com.alaris_us.daycaredata.PhoneNumber> phoneNumbers = new ArrayList<com.alaris_us.daycaredata.PhoneNumber>();

		for (com.alaris_us.externaldata.data.PhoneNumber n : abcMember.getPhones()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}
		}
		for (com.alaris_us.externaldata.data.EmergencyNumber n : abcMember.getEmergencyNumbers()) {
			if (n != null) {
				com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber();
				phone.setNumber(n.getDashedSeparatedNumberWithAreaCode());
				phone.setType(n.getType());
				phoneNumbers.add(phone);
			}

		}
		newMember.setPhoneNumbers(phoneNumbers);

		newMember.setGuest(false);
		newMember.setActive(abcMember.isActive());
		newMember.setLastCheckIn(defaultDate);
		newMember.setMinutesLeft(getPersistenceData().getFacility().getTimeLimit().intValue());
		newMember.setUsesSMS(true);
		newMember.setIsPottyTrained(false);

		if (!abcMember.getRecurringService().isEmpty()) {
			newMember.setService(abcMember.getRecurringService());
			newMember.setIsValidMembership(true);
		} else if (getPersistenceData().getProfitCenterAddOn() != null) {
			newMember.setAddOn(getPersistenceData().getProfitCenterAddOn());
			newMember.setIsValidMembership(true);
		} else
			newMember.setIsValidMembership(false);

		newMember.setUsesChildWatch(true);
		newMember.setMedicalConcerns(abcMember.getMedicalConcerns());
		newMember.setImage(abcMember.getPhoto());
		newMember.setPHomeFacility(homeFacility);
		return newMember;
	}

	public void saveConsentRecord(Member parent, Facility facility) {
		m_commandInvoker.invoke(new SaveConsentRecord(parent, facility));
	}

	public void saveDocument(Document doc, CommandStatusListener listener) {
		m_commandInvoker.invoke(new SaveDocument(doc), listener);
	}

	public void pushExternalDataDocument(String memberId, String documentContents, final ExternalDataOperationComplete<String> cb) {
		m_listenerRelay.started();
		HashMap<String, String> requestMap = new HashMap<>();

		requestMap.put("imageType", "Contract");
		requestMap.put("documentType", "PDF");
		requestMap.put("document", documentContents);
		requestMap.put("memberId", memberId);

		m_externalDataSource.saveDocument(memberId, requestMap, new ExternalDataOperationComplete<String>() {
			@Override
			public void DataReceived(String arg0, ExternalDataException arg1) {
				m_listenerRelay.finished();
				cb.DataReceived(arg0, arg1);
			}
		});
	}

	public void fetchWaiver(String documentId, String accessToken, YombuRESTOperationComplete listener) {

		YombuData yombuData = new YombuData(context.getString(R.string.YOMBU_ENDPOINT_PROD));
		yombuData.getYombuCommands().fetchWaiver("document", documentId, accessToken, listener);
	}

	public void createSelfCheckIn(Member member) {
		m_commandInvoker.invoke(new CreateSelfCheckIn(member));
	}

	public void getAdmissionOverride(List<Member> members, Facility facility, CommandStatusListener listener) {
        m_commandInvoker.invoke(new FetchAdmissionOverride(members, facility), listener);
    }
}
