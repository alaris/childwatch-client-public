package com.alaris_us.daycareclient.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.activities.MainActivityScreen;
import com.alaris_us.daycareclient.fragments.RegisterScreenFragment;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.externaldata.ExternalDataOperationComplete;
import com.alaris_us.externaldata.exception.ExternalDataException;

public class RegisterScreenViewModel extends ViewModel {
	public RegisterScreenViewModel(PersistenceLayer persistenceLayer, RegisterScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private RegisterScreenFragment fragment;

	@Override
	protected void onPopulateModelData() {
	}

	@Override
	public void onPersistModel() {
		// Don't need to persist any data here
	}

	public class RegisterClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (getPersistenceLayer().getPersistenceData().getRegistrationUnits() != null) {
				if (getPersistenceLayer().getPersistenceData().getRegistrationUnits().size() == 1) {
					String unitId = getPersistenceLayer().getPersistenceData().getRegistrationUnits().get(0).getUnitId();
					String membershipType = getPersistenceLayer().getPersistenceData().getRegistrationUnits().get(0).getMembershipType();
					getPersistenceLayer().getExternalUnitMembers(unitId, new GetFamilyInYMCADataCommandListener(membershipType));
				} else if (getPersistenceLayer().getPersistenceData().getRegistrationUnits().size() > 1)
					fragment.nextScreen("FAMILYUNIT");
				else
					fragment.nextScreen();
			} else
				fragment.nextScreen();
		}
	}

	public class NotNowClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			((MainActivityScreen) fragment.getActivity()).gotoFirstScreen();
		}
	}
	
	public class NoChildrenDialogClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			fragment.dismissNoChildrenDialog();
			((MainActivityScreen) fragment.getActivity()).gotoFirstScreen();
		}
	}

	private class GetFamilyInYMCADataCommandListener implements ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>> {
		
		private String membershipType;
		
		GetFamilyInYMCADataCommandListener(String type){
			membershipType = type;
		}
		
		@Override
		public void DataReceived(List<com.alaris_us.externaldata.to.Member> data, ExternalDataException arg1) {
			if (data == null) {
				reset();
			} else {
				Date defaultDate = DateGenerator.getDefaultDate();
				String childSeparatorDate = DateGenerator.getFormattedDateAtOffset(DateGenerator.CHILDSEPARATOR);
				String childSelfCheckinSeparatorDate = DateGenerator.getFormattedDateAtOffset(DateGenerator.CHILDSELFCHECKINSEPARATOR);
				List<Member> allMembers = new ArrayList<Member>();
				List<Member> ymcaParents = new ArrayList<Member>();
				List<Member> ymcaChildren = new ArrayList<Member>();
				for (com.alaris_us.externaldata.to.Member mem : data) {
					Member newMember = getPersistenceLayer().createMemberFromYMCAData(mem, childSeparatorDate, defaultDate, membershipType, null);
					String memberDate = DateGenerator.getFormattedDate(newMember.getBirthDate());
					if (memberDate.compareTo(childSeparatorDate) < 0) {
						ymcaParents.add(newMember);
					} else {
						ymcaChildren.add(newMember);
					}
					allMembers.add(newMember);
				}
				if (ymcaChildren.size() > 0) {
					getPersistenceLayer().getPersistenceData().setYMCAFamily(allMembers, ymcaParents, ymcaChildren);
					fragment.nextScreen("FAMILYCONFIRMATION");
				} else {
					// exception(new Exception("NO KIDS UNDER 12"));
					fragment.showNoChildrenDialog();
				}
			}
		}
	}
}
