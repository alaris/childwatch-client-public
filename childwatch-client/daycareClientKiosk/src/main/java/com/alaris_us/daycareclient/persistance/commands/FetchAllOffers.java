package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Offer;

public class FetchAllOffers
	extends Command<List<Offer>> {

	private Facility	m_facility;

	public FetchAllOffers(Facility facility) {

		this(facility, null, null);
	};

	public FetchAllOffers(Facility facility, DaycareData dataSource,
			PersistenceData model) {

		super(dataSource, model);
		m_facility = facility;
	}

	@Override
	public void executeSource() {
		m_dataSource.getOfferDAO().listOffersForFacility(m_facility, this);
	}

	@Override
	public void handleResponse(List<Offer> data) {
		m_model.setAllOffers(data);
	}

}