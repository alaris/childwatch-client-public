package com.alaris_us.daycareclient.utils;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;

import com.alaris_us.daycareclient.app.App;
import com.alaris_us.daycareclient_dev.R;

public class ImageFunctions {
	public static float colorizeMix = GeneralFunctions.getFloatResource(App.getContext(), R.dimen.view_listitem_image_mix);
	public static float colorizeSaturation = GeneralFunctions.getFloatResource(App.getContext(), R.dimen.view_listitem_image_saturation);
	public static float alphaChecked = GeneralFunctions.getFloatResource(App.getContext(), R.dimen.view_listitem_image_alpha_checked);
	public static float alphaUnchecked = GeneralFunctions.getFloatResource(App.getContext(), R.dimen.view_listitem_image_alpha_unchecked);

	public static int HORIZONTAL = 0;
	public static int VERTICAL = 1;

	public static Bitmap createDirectCopy(Bitmap toCopy) {
		
		return createDirectCopy(toCopy, true);
		
	}
	
	public static Bitmap createDirectCopy(Bitmap toCopy, boolean isMutable) {
		
		return toCopy.copy(toCopy.getConfig(), isMutable);
		
	}

	public static void colorizeBitmap(final Bitmap source) {

		float[] colorTransform = { 0, 1f, 0, 0, 0, 0, 0, 0f, 0, 0, 0, 0, 0, 0f, 0, 0, 0, 0, 1f, 0 };

		ColorMatrix colorMatrix = new ColorMatrix();
		colorMatrix.setSaturation(0f); // Remove Colour
		colorMatrix.set(colorTransform); // Apply the Red

		ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
		Paint paint = new Paint();
		paint.setColorFilter(colorFilter);

		Canvas canvas = new Canvas(source);
		canvas.drawBitmap(source, 0, 0, paint);

	}

	public static void colorizeBitmap(final Bitmap source, int value, float mix, float saturation) {

		changeBitmapSaturation(source, mix);

		Paint paint = new Paint();
		paint.setColorFilter(new PorterDuffColorFilter(value, PorterDuff.Mode.OVERLAY));

		Canvas canvas = new Canvas(source);
		canvas.drawBitmap(source, 0, 0, paint);

		changeBitmapSaturation(source, saturation);

	}

	public static void changeBitmapSaturation(final Bitmap source, float value) {

		ColorMatrix grayMatrix = new ColorMatrix();
		grayMatrix.setSaturation(value);

		ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(grayMatrix);
		Paint paint = new Paint();
		paint.setColorFilter(colorFilter);

		Canvas canvas = new Canvas(source);
		canvas.drawBitmap(source, 0, 0, paint);

	}

	public static Bitmap loadBitmapFromURI(Uri uri, Context context) {

		try {
			return MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		}

	}

	public static Bitmap compressAsJPG(Bitmap src, int quality) {

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		src.compress(Bitmap.CompressFormat.JPEG, quality, os);

		byte[] array = os.toByteArray();
		return BitmapFactory.decodeByteArray(array, 0, array.length);

	}

	public static Bitmap squareCropBitmap(Bitmap source) {

		int height = source.getHeight();
		int width = source.getWidth();

		if (width >= source.getHeight()) {

			return Bitmap.createBitmap(source, width / 2 - height / 2, 0, height, height);

		} else {

			return Bitmap.createBitmap(source, 0, height / 2 - width / 2, width, width);
		}

	}

	public static Bitmap mirrorBitmap(Bitmap source, int axis) {
		Matrix m = new Matrix();
		if (axis == HORIZONTAL)
			m.preScale(-1, 1);
		else
			m.preScale(-1, -1);
		Bitmap dst = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), m, false);
		dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
		return dst;
	}
	
	public static Bitmap scaleBitmap(Bitmap source, int newWidth, int newLength) {

		return Bitmap.createScaledBitmap(source, newWidth, newLength, false);

	}

	public static Bitmap drawableToBitmap(Drawable drawable) {

		return ((BitmapDrawable) drawable).getBitmap();

	}

	public static byte[] bitmapToByteArray(Bitmap bitmap) {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

		return stream.toByteArray();

	}

	public static Bitmap byteArrayToBitmap(byte[] byteArray) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inMutable = true;

		return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);

	}

	public static Bitmap crop(Bitmap source, Rect cropRect) {

		int height = source.getHeight();
		int width = source.getWidth();
		Rect sourceRect = new Rect(0, 0, width, height);
		if (!sourceRect.contains(cropRect)) {
			return source;
		}
		return Bitmap.createBitmap(source, cropRect.left, cropRect.top, cropRect.width(), cropRect.height());
	}

	public static Bitmap cropBitmap(Bitmap source, int x, int y, int width, int height) {

		Bitmap bitmap;
		try {
			bitmap = Bitmap.createBitmap(source, x, y, width, height);
		} catch (IllegalArgumentException e) {
			bitmap = source;
		}
		return bitmap;

	}

	public static Bitmap cropBitmap(Bitmap source, Rect cropRect) {

		return cropBitmap(source, cropRect.left, cropRect.top, cropRect.width(), cropRect.height());

	}
	public static Bitmap rotateBitmap(Bitmap source, float degrees) {
		Matrix m = new Matrix();
		m.preRotate(degrees);
		Bitmap dst = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), m, false);
		dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
		return dst;
	}
}
