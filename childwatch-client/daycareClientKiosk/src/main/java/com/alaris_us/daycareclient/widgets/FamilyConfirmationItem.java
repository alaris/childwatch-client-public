package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class FamilyConfirmationItem extends RelativeLayout implements BoundUi<FamilyConfirmationItemModel>{

	private FamilyConfirmationItemModel m_data;
	
	private ImageView m_memberBG;
	private TextView m_memberName;
	
	public FamilyConfirmationItem(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public FamilyConfirmationItem(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		View.inflate(getContext(), R.layout.view_listitem_familyconfirmation, this);
		
		m_memberBG = (ImageView) findViewById(R.id.view_listitem_ymcafamily_image);
		m_memberName = (TextView) findViewById(R.id.view_listitem_ymcafamily_name);
		//findViewById(R.id.view_listitem_ymcamember_checkmark).setVisibility(View.GONE);;
		
	}
	
	public Member getMember() {
		return m_data.getMember();
	}
	
	public FamilyConfirmationItemModel getModel() {
		return m_data; 
	}

	public void setWidth(int width){
		ListViewFunctions.setTileWidth(this, width);
	}
	
	@Override
	public void bind(FamilyConfirmationItemModel dataSource) {
		m_data = dataSource;
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberName, "Text")), m_data, "MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberBG, "BackgroundColor")), m_data, "MemberBG", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Width")), m_data, "Width", BindingMode.ONE_WAY);
	}
}
