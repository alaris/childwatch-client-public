package com.alaris_us.daycareclient.services;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.app.AlertDialog;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.alaris_us.daycareclient.activities.MainActivityScreen;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient_dev.R;

public class Service_DisplayTimeoutDialog implements Runnable {
	MainActivityScreen activity;
	ScheduledThreadPoolExecutor scheduledSwitchActivityService;
	int delay = 45;
	private AlertDialog timeoutDialog;
	private CountDownTimer countdown;
	private boolean showDialog=true;

	public Service_DisplayTimeoutDialog(MainActivityScreen activity, int delay, boolean showDialog) {
		this.activity = activity;
		this.delay = delay;
		this.showDialog=showDialog;
		startService();
	}

	@Override
	public void run() {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				if(showDialog)
					showTimeoutDialog();
				else
					activity.gotoFirstScreen();
			}
		});
	}

	private void startService() {
		scheduledSwitchActivityService = new ScheduledThreadPoolExecutor(1);
		scheduledSwitchActivityService.schedule(Service_DisplayTimeoutDialog.this, delay, TimeUnit.SECONDS);
	}

	public void shutdown() {
		scheduledSwitchActivityService.shutdownNow();
	}
	
	public void showTimeoutDialog() {
		FrameLayout timeoutDialogContainer = new FrameLayout(activity);
		final View timeoutDialogView = activity.getLayoutInflater().inflate(R.layout.view_timeoutdialog_layout, timeoutDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setView(timeoutDialogContainer);
		timeoutDialog = builder.create();
		timeoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		timeoutDialog.setCancelable(false);
		timeoutDialog.show();

		countdown=new CountDownTimer(10000, 1000) { 
			FontTextView message = (FontTextView) timeoutDialogView.findViewById(R.id.view_timeoutdialog_message_text);
			String baseText = message.getText().toString();
			@Override
			public void onTick(long millisUntilFinished) {
				message.setText(baseText + " " + (millisUntilFinished / 1000) + "s");
			}

			@Override
			public void onFinish() {
				dismissTimeoutDialog();
				activity.gotoFirstScreen();
			}
		}.start();
		
		ViewGroup.LayoutParams lp = timeoutDialogView.getLayoutParams();
		lp.width = (int) activity.getResources().getDimension(R.dimen.view_timeoutdialog_width);
		lp.height = (int) activity.getResources().getDimension(R.dimen.view_timeoutdialog_height);
		timeoutDialogView.setLayoutParams(lp);
		((View) timeoutDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		timeoutDialogView.findViewById(R.id.view_timeoutdialog_button).setOnClickListener(new TimeoutDialogClickListener());
	}

	public void dismissTimeoutDialog() {
		if (timeoutDialog != null && timeoutDialog.isShowing())
			timeoutDialog.dismiss();
		if(countdown !=null)
			countdown.cancel();
	}

	public class TimeoutDialogClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissTimeoutDialog();
			startService();

		}
	}
}