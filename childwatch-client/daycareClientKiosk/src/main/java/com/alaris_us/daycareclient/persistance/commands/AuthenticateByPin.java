package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Member;

public class AuthenticateByPin extends Command<List<Member>> {

	private String m_credential;

	public AuthenticateByPin(String credential) {
		this(credential, null, null);
	};

	public AuthenticateByPin(String credential, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_credential = credential;
	}

	@Override
	public void executeSource() {
		m_dataSource.getMembersDAO().listMembersByPin(m_credential, this);
	}

	@Override
	public void handleResponse(List<Member> members) {
		/*if(!members.isEmpty()){
			m_model.setDaycareMembersWithPin(members);
		}*/
	}
	
}