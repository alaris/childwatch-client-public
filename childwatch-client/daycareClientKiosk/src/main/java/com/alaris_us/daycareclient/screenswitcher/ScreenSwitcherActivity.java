package com.alaris_us.daycareclient.screenswitcher;

import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MotionEvent;

import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherAdapter.ScreenManagerListener;
import com.alaris_us.daycareclient_dev.R;

public abstract class ScreenSwitcherActivity extends Activity implements ScreenManagerListener {

	protected ScreenSwitcher m_screenSwitcher;
	protected ScreenSwitcherAdapter m_screenSwitcherAdapter;
	protected ScreenSwitcherFragment m_displayedFragment;	

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean isFinishing() {
		// TODO Auto-generated method stub
		return super.isFinishing();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_layout);
		createScreenSwitcherAdapter();
		if (savedInstanceState != null) {
			Bundle adapter = savedInstanceState.getBundle("screenSwitcherAdapter");
			m_screenSwitcherAdapter.restoreFromBundle(adapter);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBundle("screenSwitcherAdapter", m_screenSwitcherAdapter.getCurrentState());
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if (!m_screenSwitcherAdapter.isScreenSwitching())
			return super.dispatchTouchEvent(ev);
		else
			return true;
	}

	public void createScreenSwitcherAdapter() {
		m_screenSwitcher = getScreenSwitcher();
		m_screenSwitcherAdapter = new ScreenSwitcherAdapter(this, m_screenSwitcher);
	}

	public abstract ScreenSwitcher getScreenSwitcher();

	public ScreenSwitcherFragment getDisplayedFragment() {
		return m_displayedFragment;
	}

	@Override
	public void onBackPressed() {
		prevScreen();
	}

	public void refeshNextScreen() {
		m_screenSwitcherAdapter.refreshNextFragment();
	}

	public void nextScreen(Object... options) {
		m_screenSwitcherAdapter.nextScreen(options);
	}

	public void gotoFirstScreen() {
		m_screenSwitcherAdapter.gotoFirstScreen();
	}

	public void gotoPinScreen() {
		m_screenSwitcherAdapter.gotoPinScreen();
	}

	public void prevScreen() {
		m_screenSwitcherAdapter.previousScreen();
	}

	@Override
	public void onFragmentCreated(ScreenSwitcherFragment fragment) {

	}

	@Override
	public void onFragmentPaused(ScreenSwitcherFragment fragment) {

	}

	@Override
	public void onFragmentResumed(ScreenSwitcherFragment fragment) {

	}

	@Override
	public void onFragmentVisible(ScreenSwitcherFragment fragment) {
		m_displayedFragment = fragment;
	}

	@Override
	public void onLoop() {

	}

	public abstract Map<Class<? extends ScreenSwitcherFragment>, List<ScreenSwitcherFragment>> getFragmentMap();

	public abstract ScreenSwitcherFragment getFirstScreen();
}
