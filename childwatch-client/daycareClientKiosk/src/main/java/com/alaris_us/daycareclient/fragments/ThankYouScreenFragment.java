package com.alaris_us.daycareclient.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.models.ThankYouScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.UiBinder;

public class ThankYouScreenFragment extends ScreenSwitcherFragment {
	private ThankYouScreenViewModel m_model;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.thankyouscreen_layout, container,
				false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void initData() {
		m_model.reset();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		m_model = new ThankYouScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		UiBinder.bind(m_layout, R.id.thankyouscreen_tile_pagers, "Text",
				m_model, "PagersText", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.thankyouscreen_tile_cubbies, "Text",
				m_model, "CubbiesText", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.thankyouscreen_tile_timeleft, "Text",
				m_model, "TimeText", BindingMode.ONE_WAY);
		m_layout.findViewById(R.id.thankyouscreen_layout_container)
				.setOnClickListener(m_model.new NextClickListener());
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		m_model.disconnect();
	}

	@Override
	public void onResumeFragment() {
		m_model = (null == m_model) ? new ThankYouScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this) : m_model;
		m_model.reset();
		super.onResumeFragment();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		m_model.disconnect();
		m_model = null;
	}
}
