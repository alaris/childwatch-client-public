package com.alaris_us.daycareclient.yombu;


import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Map;

public class YombuREST extends AsyncTask<HttpUriRequest, Void, String> {

    private YombuRESTOperationComplete<YombuDataObject> cb;
    private HttpUriRequest mRequest;

    public enum RequestType {
        GET, POST
    }

    public YombuREST(RequestType reqType, String url, JSONObject headers) {
        createRequest(reqType, url);
        setHeaders(getDefaultHeaders());
        if (headers != null)
            setHeaders(headers);
    }

    public YombuREST(RequestType reqType, String url, Map<String, Object> body, JSONObject headers) {
        this(reqType, url, headers);
        //setBody(body);
    }

    public YombuREST(HttpUriRequest request){
        mRequest = request;
    }

    public YombuREST clone(){
        return new YombuREST(mRequest);
    }

    private JSONObject getDefaultHeaders() {
        JSONObject headers = new JSONObject();
        try {
            headers.put("Accept", "application/json");
            headers.put("Content-Type", "application/json");
        } catch (JSONException e) {
            Log.e(this.getClass().getSimpleName(), e.getMessage());
        }
        return headers;
    }

    private void createRequest(RequestType requestType, String url) {
        switch (requestType) {
            case GET:
                mRequest = new HttpGet(url);
                break;
            case POST:
                mRequest = new HttpPost(url);
                break;
        }
    }

    @SuppressWarnings("unchecked")
    private void setHeaders(JSONObject headers) {
        Iterator<String> itr = headers.keys();
        while (itr.hasNext()) {
            try {
                String key = itr.next();
                mRequest.setHeader(key, headers.getString(key));
            } catch (JSONException e) {
                Log.e(this.getClass().getSimpleName(), e.getMessage());
            }
        }
    }

    /*public void setBody(JSONObject body) {
        if (mRequest instanceof HttpPost) {
            String bodyStr = "";
            if (body != null)
                bodyStr = body.toString();
            StringEntity entity = new StringEntity(bodyStr, "UTF-8");
            ((HttpPost) mRequest).setEntity(entity);
        }
    }

    public void setBody(Map<String, Object> body){
        setBody(new JSONObject(body));
    }*/

    public void execute(YombuRESTOperationComplete<YombuDataObject> callback) {
        cb = callback;
        super.execute(mRequest);
    }

    @Override
    protected String doInBackground(HttpUriRequest... params) {
        try {
            HttpUriRequest request = params[0];
            HttpClient mClient = new DefaultHttpClient();
            HttpResponse serverResponse = mClient.execute(request);
            BasicResponseHandler handler = new BasicResponseHandler();
            return handler.handleResponse(serverResponse);
        } catch (Exception e) {
            Log.e(e.getClass().getSimpleName(), e.getMessage());
            cb.operationComplete(null, new YombuRESTException(500, e.getMessage()));
        }
        return "";
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            JSONObject obj = new JSONObject(result);
            if (!obj.isNull("id")) {
                cb.operationComplete(new YombuDataObject(obj), null);
            } else if (!obj.isNull("dev_message")) {
                String error = obj.getString("dev_message");
                cb.operationComplete(null, new YombuRESTException(obj.getInt("code"), error));
            } else {
                throw new JSONException("Empty Data");
            }
        } catch (JSONException e) {
            Log.e("Error Response", result);
            Log.e("JSONException", e.getMessage());
            cb.operationComplete(null, new YombuRESTException(500, e.getMessage()));
        }
    }
}

