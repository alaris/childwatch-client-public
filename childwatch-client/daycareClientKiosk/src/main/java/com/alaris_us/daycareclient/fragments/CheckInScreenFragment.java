package com.alaris_us.daycareclient.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.models.CheckInScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontEditText;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient.widgets.MemberCheckBox;
import com.alaris_us.daycareclient.widgets.SelectSelfCheckInDialogItem;
import com.alaris_us.daycareclient.widgets.SliderQuestionDialogItem;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

public class CheckInScreenFragment extends ScreenSwitcherFragment {
	CheckInScreenViewModel m_model;
	private View m_pictureDialogView;
	private View m_noTimeLeftDialogView;
	private final int pictureDialogWidthRes = R.dimen.view_picturedialog_width;
	private final int pictureDialogHeightRes = R.dimen.view_picturedialog_height;
	private final int noTimeLeftDialogWidthRes = R.dimen.view_notimeleftdialog_width;
	private final int noTimeLeftDialogHeightRes = R.dimen.view_notimeleftdialog_height;
	private int pictureDialogWidth;
	private int pictureDialogHeight;
	private int noTimeLeftDialogWidth;
	private int noTimeLeftDialogHeight;
	private FontTextView timeBreakdownView;
	private FontTextView guestBreakdownView;
	private View m_staffOverrideEntryDialogView;
	private View m_successfulOverrideDialogView;
	private View m_noGuestDaysLeftDialogView;
	private FontEditText overridePIN;
	private AlertDialog m_noChildrenDialog;
    private View m_questionDialogView;
	private View m_roomsAreFullDialogView;
	private FontTextView roomsFullBreakdownView;
	private View m_noKidsForCheckDirectionDialogView;
	private View m_timesAlmostUpDialogView;
	private View m_freeAndPaidTimeDialogView;
	private FontTextView minutesLeftBreakdownView;
	private FontTextView freeAndPaidTimeLeftBreakdownView;
	private HorizontalListView mChildList;
	private FontTextView mSingleChildName;
	private FontTextView mSelectChildTitle;
    private View m_capacityExceededDialogView;
    private FontTextView capacityExceededBreakdownView;
    private FontTextView noTimeTitleView;
    private FontTextView timesAlmostUpTitleView;
	private View m_selectableChildrenLimitDialogView;
	private FontTextView selectableChildrenLimitView;
	private View m_selectSelfCheckInDialogView;
	private View m_sliderQuestionDialogView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.checkinscreen_layout, container, false);
		m_pictureDialogView = inflater.inflate(R.layout.view_picturedialog_layout, container, false);
		m_noTimeLeftDialogView = inflater.inflate(R.layout.view_notimeleftdialog_layout, container, false);
		m_staffOverrideEntryDialogView = inflater.inflate(R.layout.view_staffoverrideentrydialog_layout, container,
				false);
		m_successfulOverrideDialogView = inflater.inflate(R.layout.view_successfuloverridedialog_layout, container,
				false);
		m_noGuestDaysLeftDialogView = inflater.inflate(R.layout.view_noguestdaysleftdialog_layout, container, false);
		m_roomsAreFullDialogView = inflater.inflate(R.layout.view_roomsarefulldialog_layout, container, false);
		m_noKidsForCheckDirectionDialogView = inflater.inflate(R.layout.view_nokidsforcheckdirectiondialog_layout,
				container, false);
        m_questionDialogView = inflater.inflate(R.layout.view_questiondialog_layout, container, false);
		m_timesAlmostUpDialogView = inflater.inflate(R.layout.view_timesalmostupdialog_layout, container, false);
		m_freeAndPaidTimeDialogView = inflater.inflate(R.layout.view_freeandpaidtimedialog_layout, container, false);
        m_capacityExceededDialogView = inflater.inflate(R.layout.view_capacityexceededdialog_layout, container, false);
		m_selectableChildrenLimitDialogView = inflater.inflate(R.layout.view_selectablechildrenlimitdialog_layout, container, false);
		m_selectSelfCheckInDialogView = inflater.inflate(R.layout.view_selectselfcheckindialog_layout, container, false);
		m_sliderQuestionDialogView = inflater.inflate(R.layout.view_sliderquestiondialog_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mChildList = (HorizontalListView) m_layout.findViewById(R.id.checkinscreen_tile_childlist);
		mSingleChildName = (FontTextView) m_layout.findViewById(R.id.checkinscreen_single_child_name);
		mSelectChildTitle = (FontTextView) m_layout.findViewById(R.id.checkinscreen_tile_selectchild_textbox_text);
		m_model = new CheckInScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this);
		timeBreakdownView = (FontTextView) m_noTimeLeftDialogView.findViewById(R.id.view_notimeleftdialog_bottom_text);
		noTimeTitleView = (FontTextView) m_noTimeLeftDialogView.findViewById(R.id.view_notimeleftdialog_title);
		timesAlmostUpTitleView = (FontTextView) m_timesAlmostUpDialogView.findViewById(R.id.view_timesalmostupdialog_title);
		guestBreakdownView = (FontTextView) m_noGuestDaysLeftDialogView
				.findViewById(R.id.view_noguestdaysleftdialog_bottom_text);
		roomsFullBreakdownView = (FontTextView) m_roomsAreFullDialogView
				.findViewById(R.id.view_roomsarefulldialog_bottom_text);
		minutesLeftBreakdownView = (FontTextView) m_timesAlmostUpDialogView
				.findViewById(R.id.view_timesalmostupdialog_message);
		freeAndPaidTimeLeftBreakdownView = (FontTextView) m_freeAndPaidTimeDialogView
				.findViewById(R.id.view_freeandpaidtimedialog_message);
        capacityExceededBreakdownView = (FontTextView) m_capacityExceededDialogView.findViewById(R.id.view_capacityexceededdialog_bottom_text);
		selectableChildrenLimitView = (FontTextView) m_selectableChildrenLimitDialogView
				.findViewById(R.id.view_selectablechildrenlimitdialog_message_text);
        pictureDialogWidth = (int) getResources().getDimension(pictureDialogWidthRes);
		pictureDialogHeight = (int) getResources().getDimension(pictureDialogHeightRes);
		noTimeLeftDialogWidth = (int) getResources().getDimension(noTimeLeftDialogWidthRes);
		noTimeLeftDialogHeight = (int) getResources().getDimension(noTimeLeftDialogHeightRes);
		m_layout.findViewById(R.id.view_navigation_withbuttons_next_container).setId(R.id.checkinscreen_nav_next);
		m_layout.findViewById(R.id.view_navigation_withbuttons_back_container).setId(R.id.checkinscreen_nav_back);
		m_layout.findViewById(R.id.view_navigation_withbuttons_selectall_container)
				.setId(R.id.checkinscreen_nav_selectall);
		m_layout.findViewById(R.id.view_navigation_withbuttons_showproxy_container)
				.setId(R.id.checkinscreen_nav_showproxy);
		UiBinder.bind(m_layout, R.id.checkinscreen_tile_childlist, "Adapter", m_model, "VisibleChildren",
				BindingMode.TWO_WAY, new AdapterConverter(MemberCheckBox.class, true, true));
		UiBinder.bind(m_layout, R.id.checkinscreen_nav_next, "OnClickListener", m_model, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.checkinscreen_nav_next, "Alpha", m_model, "NextAlpha", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.view_navigation_withbuttons_showproxy_textbox, "TextRes", m_model,
				"ProxyButtonText", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.view_navigation_withbuttons_selectall_textbox, "TextRes", m_model,
				"SelectAllButtonText", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.checkinscreen_nav_showproxy, "Visibility", m_model, "ProxyButtonVisibility",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.checkinscreen_nav_selectall, "LayoutParams", m_model, "SelectAllLayoutParams",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.view_navigation_withbuttons_selectall_textbox, "TextSizeRes", m_model,
				"SelectAllButtonTextSize", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.checkinscreen_tile_childlist, "Position", m_model, "ScrollPosition",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.checkinscreen_tile_selectchild_textbox_text, "TextRes", m_model, "TitleTextRes",
				BindingMode.ONE_WAY);
		mChildList.setOnItemClickListener(m_model.new ChildClickListener());
		m_layout.findViewById(R.id.checkinscreen_nav_showproxy)
				.setOnClickListener(m_model.new ShowProxyClickListener());
		m_layout.findViewById(R.id.checkinscreen_nav_selectall)
				.setOnClickListener(m_model.new SelectAllClickListener());

		// UiBinder.bind(m_layout, R.id.checkinscreen_nav_next,
		// "OnTouchListener", m_model, "OnTouchListener",
		// BindingMode.ONE_WAY);
		// UiBinder.bind(m_layout, R.id.checkinscreen_nav_back,
		// "OnTouchListener", m_model, "OnTouchListener",
		// BindingMode.ONE_WAY);
		// UiBinder.bind(m_layout, R.id.checkinscreen_nav_selectall,
		// "OnTouchListener", m_model, "OnTouchListener",
		// BindingMode.ONE_WAY);
		// UiBinder.bind(m_layout, R.id.checkinscreen_nav_showproxy,
		// "OnTouchListener", m_model, "OnTouchListener",
		// BindingMode.ONE_WAY);

		/* PictureDialog */
		UiBinder.bind(m_pictureDialogView, R.id.view_picturedialog_yes_button, "OnClickListener", m_model,
				"PictureDialogYesClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_pictureDialogView, R.id.view_picturedialog_skip_button, "OnClickListener", m_model,
				"PictureDialogSkipClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "PictureDialog"), m_model, "PictureDialog",
				BindingMode.ONE_WAY_TO_SOURCE);

		/* NoTimeLeftDialog */
		UiBinder.bind(m_noTimeLeftDialogView, R.id.view_notimeleftdialog_reselect_button, "OnClickListener", m_model,
				"NoTimeLeftDialogReselectClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_noTimeLeftDialogView, R.id.view_notimeleftdialog_home_button, "OnClickListener", m_model,
				"NoTimeLeftDialogHomeClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_noTimeLeftDialogView, R.id.view_notimeleftdialog_override_button, "OnClickListener", m_model,
				"NoTimeLeftDialogOverrideClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "NoTimeLeftDialog"), m_model, "NoTimeLeftDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "TimeBreakdownText"), m_model, "TimeBreakdownText",
				BindingMode.ONE_WAY);

		/* StaffOverrideEntryDialog */
		UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_enter_button,
				"OnClickListener", m_model, "StaffOverrideEntryDialogEnterClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_cancel_button,
				"OnClickListener", m_model, "StaffOverrideEntryDialogCancelClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "StaffOverrideEntryDialog"), m_model, "StaffOverrideEntryDialog",
				BindingMode.ONE_WAY_TO_SOURCE);

		/* SuccessfulOverrideDialog */
		UiBinder.bind(m_successfulOverrideDialogView, R.id.view_successfuloverridedialog_ok_button, "OnClickListener",
				m_model, "SuccessfulOverrideDialogOkClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "SuccessfulOverrideDialog"), m_model, "SuccessfulOverrideDialog",
				BindingMode.ONE_WAY_TO_SOURCE);

		/* NoGuestDaysLeftDialog */
		UiBinder.bind(m_noGuestDaysLeftDialogView, R.id.view_noguestdaysleftdialog_reselect_button, "OnClickListener",
				m_model, "NoGuestDaysLeftDialogReselectClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_noGuestDaysLeftDialogView, R.id.view_noguestdaysleftdialog_home_button, "OnClickListener",
				m_model, "NoGuestDaysLeftDialogHomeClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_noGuestDaysLeftDialogView, R.id.view_noguestdaysleftdialog_override_button, "OnClickListener",
				m_model, "NoGuestDaysLeftDialogOverrideClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "NoGuestDaysLeftDialog"), m_model, "NoGuestDaysLeftDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "GuestBreakdownText"), m_model, "GuestBreakdownText",
				BindingMode.ONE_WAY);

		/* RoomsAreFullDialog */
		UiBinder.bind(m_roomsAreFullDialogView, R.id.view_roomsarefulldialog_reselect_button, "OnClickListener",
				m_model, "RoomsAreFullDialogReselectClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_roomsAreFullDialogView, R.id.view_roomsarefulldialog_home_button, "OnClickListener", m_model,
				"RoomsAreFullDialogHomeClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_roomsAreFullDialogView, R.id.view_roomsarefulldialog_override_button, "OnClickListener",
				m_model, "RoomsAreFullDialogOverrideClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "RoomsAreFullDialog"), m_model, "RoomsAreFullDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "RoomsFullBreakdownText"), m_model, "RoomsFullBreakdownText",
				BindingMode.ONE_WAY);

		/* TimesAlmostUpDialog */
		UiBinder.bind(m_timesAlmostUpDialogView, R.id.view_timesalmostupdialog_ok_button, "OnClickListener", m_model,
				"TimesAlmostUpDialogOkClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "TimesAlmostUpDialog"), m_model, "TimesAlmostUpDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "MinutesLeftBreakdownText"), m_model, "MinutesLeftBreakdownText",
				BindingMode.ONE_WAY);

		/* FreeAndPaidTimeDialog */
		UiBinder.bind(m_freeAndPaidTimeDialogView, R.id.view_freeandpaidtimedialog_ok_button, "OnClickListener", m_model,
				"FreeAndPaidTImeDialogOkClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "FreeAndPaidTImeDialog"), m_model, "FreeAndPaidTImeDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "FreeAndPaidTimeLeftBreakdownText"), m_model, "FreeAndPaidTimeLeftBreakdownText",
				BindingMode.ONE_WAY);
		
        /* CapacityExceededDialog */
        UiBinder.bind(m_capacityExceededDialogView, R.id.view_capacityexceededdialog_reselect_button, "OnClickListener", m_model,
                "CapacityExceededDialogReselectClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(m_capacityExceededDialogView, R.id.view_capacityexceededdialog_home_button, "OnClickListener", m_model,
                "CapacityExceededDialogHomeClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(m_capacityExceededDialogView, R.id.view_capacityexceededdialog_override_button, "OnClickListener", m_model,
                "CapacityExceededDialogOverrideClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(new ReflectedProperty(this, "CapacityExceededDialog"), m_model, "CapacityExceededDialog",
                BindingMode.ONE_WAY_TO_SOURCE);
        UiBinder.bind(new ReflectedProperty(this, "CapacityExceededBreakdownText"), m_model, "CapacityExceededBreakdownText",
                BindingMode.ONE_WAY);

        /* QuestionDialog */
        UiBinder.bind(m_questionDialogView, R.id.view_questiondialog_accept_button, "OnClickListener", m_model,
                "QuestionDialogAcceptClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(m_questionDialogView, R.id.view_questiondialog_cancel_button, "OnClickListener", m_model,
                "QuestionDialogCancelClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(new ReflectedProperty(this, "QuestionDialog"), m_model, "QuestionDialog",
                BindingMode.ONE_WAY_TO_SOURCE);

		/* SelectableChildrenLimitDialog */
		UiBinder.bind(m_selectableChildrenLimitDialogView, R.id.view_selectablechildrenlimitdialog_reselect_button, "OnClickListener", m_model,
				"SelectableChildrenLimitDialogReselectClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_selectableChildrenLimitDialogView, R.id.view_selectablechildrenlimitdialog_home_button, "OnClickListener", m_model,
				"SelectableChildrenLimitDialogHomeClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_selectableChildrenLimitDialogView, R.id.view_selectablechildrenlimitdialog_override_button, "OnClickListener", m_model,
				"SelectableChildrenLimitDialogOverrideClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "SelectableChildrenLimitDialog"), m_model, "SelectableChildrenLimitDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "SelectableChildrenLimitText"), m_model, "SelectableChildrenLimitText",
				BindingMode.ONE_WAY);

		/* SelectSelfCheckInDialog */
		UiBinder.bind(m_selectSelfCheckInDialogView, R.id.view_selectselfcheckindialog_confirm_button, "OnClickListener", m_model,
				"SelectSelfCheckInDialogConfirmClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_selectSelfCheckInDialogView, R.id.view_selectselfcheckindialog_cancel_button, "OnClickListener", m_model,
				"SelectSelfCheckInDialogCancelClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_selectSelfCheckInDialogView, R.id.view_selectselfcheckindialog_list, "Adapter", m_model, "SelectCanSelfCheckInChildren",
				BindingMode.TWO_WAY, new AdapterConverter(SelectSelfCheckInDialogItem.class, true, true));
		UiBinder.bind(new ReflectedProperty(this, "SelectSelfCheckInDialog"), m_model, "SelectSelfCheckInDialog",
				BindingMode.ONE_WAY_TO_SOURCE);

		/* SliderQuestionDialog */
		UiBinder.bind(m_sliderQuestionDialogView, R.id.view_sliderquestiondialog_confirm_button, "OnClickListener", m_model,
				"SliderQuestionDialogConfirmClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_sliderQuestionDialogView, R.id.view_sliderquestiondialog_cancel_button, "OnClickListener", m_model,
				"SliderQuestionDialogCancelClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_sliderQuestionDialogView, R.id.view_sliderquestiondialog_list, "Adapter", m_model, "SliderQuestionChildren",
				BindingMode.TWO_WAY, new AdapterConverter(SliderQuestionDialogItem.class, true, true));
		UiBinder.bind(new ReflectedProperty(this, "SliderQuestionDialog"), m_model, "SliderQuestionDialog",
				BindingMode.ONE_WAY_TO_SOURCE);

		// Navigation Button(s)
		m_layout.findViewById(R.id.checkinscreen_nav_back).setOnClickListener(new BackClickListener());
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		if (m_model != null)
			m_model.disconnect();
	}

	@Override
	public void onResumeFragment() {
		m_model = (null == m_model)
				? new CheckInScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this)
				: m_model;
		m_model.reset();
		super.onResumeFragment();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		m_model.disconnect();
		m_model = null;
	}

	public AlertDialog getPictureDialog() {
		FrameLayout pictureDialogContainer = new FrameLayout(getActivity());
		pictureDialogContainer.addView(m_pictureDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(pictureDialogContainer);
		AlertDialog pictureDialog = builder.create();
		pictureDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		pictureDialog.setCancelable(false);
		pictureDialog.show();
		pictureDialog.dismiss();
		ViewGroup.LayoutParams lp = m_pictureDialogView.getLayoutParams();
		lp.width = pictureDialogWidth;
		lp.height = pictureDialogHeight;
		m_pictureDialogView.setLayoutParams(lp);
		((View) m_pictureDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return pictureDialog;
	}

	public AlertDialog getNoTimeLeftDialog() {
		FrameLayout noTimeLeftDialogContainer = new FrameLayout(getActivity());
		noTimeLeftDialogContainer.addView(m_noTimeLeftDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noTimeLeftDialogContainer);
		AlertDialog noTimeLeftDialog = builder.create();
		noTimeLeftDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		noTimeLeftDialog.setCancelable(false);
		noTimeLeftDialog.show();
		noTimeLeftDialog.dismiss();
		ViewGroup.LayoutParams lp = m_noTimeLeftDialogView.getLayoutParams();
		lp.width = noTimeLeftDialogWidth;
		lp.height = noTimeLeftDialogHeight;
		m_noTimeLeftDialogView.setLayoutParams(lp);
		((View) m_noTimeLeftDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return noTimeLeftDialog;
	}

	public void setTimeBreakdownText(String breakdown) {
		timeBreakdownView.setText(breakdown);
	}
	
	public void setNoTimeTitleText(String timeType) {
		String defaultText = getResources().getString(R.string.view_notimeleftdialog_title_text);
		defaultText = defaultText.replace("[TIMEPERIOD]", timeType);
		noTimeTitleView.setText(defaultText);
	}

	public AlertDialog getStaffOverrideEntryDialog() {
		FrameLayout staffOverrideEntryDialogContainer = new FrameLayout(getActivity());
		staffOverrideEntryDialogContainer.addView(m_staffOverrideEntryDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(staffOverrideEntryDialogContainer);
		AlertDialog staffOverrideEntryDialog = builder.create();
		staffOverrideEntryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		staffOverrideEntryDialog.setCancelable(false);
		staffOverrideEntryDialog.show();
		staffOverrideEntryDialog.dismiss();
		ViewGroup.LayoutParams lp = m_staffOverrideEntryDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_height);
		m_staffOverrideEntryDialogView.setLayoutParams(lp);
		((View) m_staffOverrideEntryDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return staffOverrideEntryDialog;
	}

	public String getOverridePIN() {
		overridePIN = (FontEditText) m_staffOverrideEntryDialogView
				.findViewById(R.id.view_staffoverrideentrydialog_pin_edittext);
		return overridePIN.getText().toString();
	}

	public void setOverridePIN(String pin) {
		overridePIN.setText(pin);
		overridePIN.setHint("Invalid PIN, Please Retry");
	}

	public void setOverridePINHint(String hint) {
		overridePIN.setHint(hint);
	}

	public AlertDialog getSuccessfulOverrideDialog() {
		FrameLayout successfulOverrideDialogContainer = new FrameLayout(getActivity());
		successfulOverrideDialogContainer.addView(m_successfulOverrideDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(successfulOverrideDialogContainer);
		AlertDialog successfulOverrideDialog = builder.create();
		successfulOverrideDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		successfulOverrideDialog.setCancelable(false);
		successfulOverrideDialog.show();
		successfulOverrideDialog.dismiss();
		ViewGroup.LayoutParams lp = m_successfulOverrideDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_height);
		m_successfulOverrideDialogView.setLayoutParams(lp);
		((View) m_successfulOverrideDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return successfulOverrideDialog;
	}

	public AlertDialog getNoGuestDaysLeftDialog() {
		FrameLayout noGuestDaysLeftDialogContainer = new FrameLayout(getActivity());
		noGuestDaysLeftDialogContainer.addView(m_noGuestDaysLeftDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noGuestDaysLeftDialogContainer);
		AlertDialog noGuestDaysLeftDialog = builder.create();
		noGuestDaysLeftDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		noGuestDaysLeftDialog.setCancelable(false);
		noGuestDaysLeftDialog.show();
		noGuestDaysLeftDialog.dismiss();
		ViewGroup.LayoutParams lp = m_noGuestDaysLeftDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_noguestdaysleftdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_noguestdaysleftdialog_height);
		m_noGuestDaysLeftDialogView.setLayoutParams(lp);
		((View) m_noGuestDaysLeftDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return noGuestDaysLeftDialog;
	}

	public void setGuestBreakdownText(String breakdown) {
		guestBreakdownView.setText(breakdown);
	}

	public AlertDialog getRoomsAreFullDialog() {
		FrameLayout roomsAreFullDialogContainer = new FrameLayout(getActivity());
		roomsAreFullDialogContainer.addView(m_roomsAreFullDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(roomsAreFullDialogContainer);
		AlertDialog noTimeLeftDialog = builder.create();
		noTimeLeftDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		noTimeLeftDialog.setCancelable(false);
		noTimeLeftDialog.show();
		noTimeLeftDialog.dismiss();
		ViewGroup.LayoutParams lp = m_roomsAreFullDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_roomsarefulldialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_roomsarefulldialog_height);
		m_roomsAreFullDialogView.setLayoutParams(lp);
		((View) m_roomsAreFullDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return noTimeLeftDialog;
	}

	public void setRoomsFullBreakdownText(String breakdown) {
		roomsFullBreakdownView.setText(breakdown);
	}

	@Override
	public void initData() {
		m_model.reset();
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}

	public void showNoChildrenDialog() {
		FrameLayout noChildrendialogContainer = new FrameLayout(getActivity());
		View noChildrenDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_nochildrendialog_layout,
				noChildrendialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noChildrendialogContainer);
		m_noChildrenDialog = builder.create();
		m_noChildrenDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_noChildrenDialog.setCancelable(false);
		m_noChildrenDialog.show();
		ViewGroup.LayoutParams lp = noChildrenDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nochildrendialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nochildrendialog_height);
		noChildrenDialogView.setLayoutParams(lp);
		((View) noChildrenDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
	}

	public void dismissNoChildrenDialog() {
		if (m_noChildrenDialog != null && m_noChildrenDialog.isShowing())
			m_noChildrenDialog.dismiss();
	}

    public AlertDialog getQuestionDialog() {
        FrameLayout questionDialogContainer = new FrameLayout(getActivity());
        questionDialogContainer.addView(m_questionDialogView);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(questionDialogContainer);
        AlertDialog questionDialog = builder.create();
        questionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        questionDialog.setCancelable(false);
        questionDialog.show();
        questionDialog.dismiss();
        ViewGroup.LayoutParams lp = m_questionDialogView.getLayoutParams();
        lp.width = (int) getResources().getDimension(R.dimen.view_questiondialog_width);
        lp.height = (int) getResources().getDimension(R.dimen.view_questiondialog_height);
        m_questionDialogView.setLayoutParams(lp);
        ((View) m_questionDialogView.getParent().getParent().getParent())
                .setBackgroundResource(R.color.invis);
        return questionDialog;
    }

	public AlertDialog getNoKidsForCheckDirectionDialog() {
		FrameLayout noKidsForCheckDirectionDialogContainer = new FrameLayout(getActivity());
		noKidsForCheckDirectionDialogContainer.addView(m_noKidsForCheckDirectionDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noKidsForCheckDirectionDialogContainer);
		AlertDialog noKidsForCheckDirectionDialog = builder.create();
		noKidsForCheckDirectionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		noKidsForCheckDirectionDialog.setCancelable(false);
		noKidsForCheckDirectionDialog.show();
		noKidsForCheckDirectionDialog.dismiss();
		ViewGroup.LayoutParams lp = m_noKidsForCheckDirectionDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nokidsforcheckdirectiondialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nokidsforcheckdirectiondialog_height);
		m_noKidsForCheckDirectionDialogView.setLayoutParams(lp);
		((View) m_noKidsForCheckDirectionDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		String dir = "In";
		String otherDir = "Out";
		String title = getResources().getString(R.string.view_nokidsforcheckdirectiondialog_title_text).replace("[DIR]",
				dir);
		String msg = getResources().getString(R.string.view_nokidsforcheckdirectiondialog_message_text).replace("[DIR]",
				otherDir);
		((TextView) m_noKidsForCheckDirectionDialogView.findViewById(R.id.view_nokidsforcheckdirectiondialog_title))
				.setText(title);
		((TextView) m_noKidsForCheckDirectionDialogView.findViewById(R.id.view_nokidsforcheckdirectiondialog_message))
				.setText(msg);
		return noKidsForCheckDirectionDialog;
	}

	public AlertDialog getTimesAlmostUpDialog() {
		FrameLayout timesAlmostUpDialogContainer = new FrameLayout(getActivity());
		timesAlmostUpDialogContainer.addView(m_timesAlmostUpDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(timesAlmostUpDialogContainer);
		AlertDialog timesAlmostUpDialog = builder.create();
		timesAlmostUpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		timesAlmostUpDialog.setCancelable(false);
		timesAlmostUpDialog.show();
		timesAlmostUpDialog.dismiss();
		ViewGroup.LayoutParams lp = m_timesAlmostUpDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_timesalmostupdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_timesalmostupdialog_height);
		m_timesAlmostUpDialogView.setLayoutParams(lp);
		((View) m_timesAlmostUpDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return timesAlmostUpDialog;
	}

	public void setMinutesLeftBreakdownText(String minsLeft) {
		minutesLeftBreakdownView.setText(minsLeft);
	}

	public String getDefaultMinutesLeftText() {
		return getResources().getString(R.string.view_timesalmostupdialog_message_text);
	}
	
	public AlertDialog getFreeAndPaidTImeDialog() {
		FrameLayout freeAndPaidTimeDialogContainer = new FrameLayout(getActivity());
		freeAndPaidTimeDialogContainer.addView(m_freeAndPaidTimeDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(freeAndPaidTimeDialogContainer);
		AlertDialog freeAndPaidTimeDialog = builder.create();
		freeAndPaidTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		freeAndPaidTimeDialog.setCancelable(false);
		freeAndPaidTimeDialog.show();
		freeAndPaidTimeDialog.dismiss();
		ViewGroup.LayoutParams lp = m_freeAndPaidTimeDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_freeandpaidtimedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_freeandpaidtimedialog_height);
		m_freeAndPaidTimeDialogView.setLayoutParams(lp);
		((View) m_freeAndPaidTimeDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return freeAndPaidTimeDialog;
	}
	
	public void setFreeAndPaidTimeLeftBreakdownText(String minsLeft) {
		freeAndPaidTimeLeftBreakdownView.setText(minsLeft);
	}
	
	public String getDefaultFreeAndPaidTimeLeftText() {
		return getResources().getString(R.string.view_freeandpaidtimedialog_message_text);
	}
	
	public void setTimesAlmostUpTitleText(String timeType) {
		String defaultText = getResources().getString(R.string.view_timesalmostupdialog_title_text);
		defaultText = defaultText.replace("[TIMEPERIOD]", timeType);
		timesAlmostUpTitleView.setText(defaultText);
	}

    public AlertDialog getCapacityExceededDialog() {
        FrameLayout capacityExceededDialogContainer = new FrameLayout(getActivity());
        capacityExceededDialogContainer.addView(m_capacityExceededDialogView);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(capacityExceededDialogContainer);
        AlertDialog capacityExceededDialog = builder.create();
        capacityExceededDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        capacityExceededDialog.setCancelable(false);
        capacityExceededDialog.show();
        capacityExceededDialog.dismiss();
        ViewGroup.LayoutParams lp = m_capacityExceededDialogView.getLayoutParams();
        lp.width = (int) getResources().getDimension(R.dimen.view_capacityexceededdialog_width);
        lp.height = (int) getResources().getDimension(R.dimen.view_capacityexceededdialog_height);
        m_capacityExceededDialogView.setLayoutParams(lp);
        ((View) m_capacityExceededDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
        return capacityExceededDialog;
    }
    
    public String getDefaultCapacityExceededText() {
        return getResources().getString(R.string.view_capacityexceededdialog_bottom_text);
    }
    
    public String getDefaultRoomCapacityExceededText() {
		return getResources().getString(R.string.view_capacityexceededdialog_bottom_text_room);
	}
    
    public String getDefaultFacilityAndRoomCapacityExceededText() {
		return getResources().getString(R.string.view_capacityexceededdialog_bottom_text_facility_and_room);
	}
    
    public void setCapacityExceededBreakdownText(String breakdown) {
        capacityExceededBreakdownView.setText(breakdown);
    }

	public HorizontalListView getUserList() {
		return mChildList;
	}

	public FontTextView getSingleUserName() {
		return mSingleChildName;
	}

	public FontTextView getSelectUserTitle() {
		return mSelectChildTitle;
	}
	
    public void setQuestionTitleText(String title) {
        FontTextView tv = (FontTextView) m_questionDialogView.findViewById(R.id.view_questiondialog_title);
        tv.setText(title);
    }
    
    public void setQuestionBodyText(String body) {
        FontTextView tv = (FontTextView) m_questionDialogView.findViewById(R.id.view_questiondialog_body);
        tv.setText(body);
    }

	public void setSliderQuestionTitleText(String title) {
		FontTextView tv = (FontTextView) m_sliderQuestionDialogView.findViewById(R.id.view_sliderquestiondialog_title);
		tv.setText(title);
	}

	public AlertDialog getSelectableChildrenLimitDialog() {
		FrameLayout selectableChildrenLimitDialogContainer = new FrameLayout(getActivity());
		selectableChildrenLimitDialogContainer.addView(m_selectableChildrenLimitDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(selectableChildrenLimitDialogContainer);
		AlertDialog selectableChildrenLimitDialog = builder.create();
		selectableChildrenLimitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		selectableChildrenLimitDialog.setCancelable(false);
		selectableChildrenLimitDialog.show();
		selectableChildrenLimitDialog.dismiss();
		ViewGroup.LayoutParams lp = m_selectableChildrenLimitDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_selectablechildrenlimitdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_selectablechildrenlimitdialog_height);
		m_selectableChildrenLimitDialogView.setLayoutParams(lp);
		((View) m_selectableChildrenLimitDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return selectableChildrenLimitDialog;
	}

	public String getDefaultSelectableChildrenLimitText() {
		return getResources().getString(R.string.view_selectablechildrenlimitdialog_message_text);
	}

	public void setSelectableChildrenLimitText(String message) {
		selectableChildrenLimitView.setText(message);
	}

	public AlertDialog getSelectSelfCheckInDialog() {
		FrameLayout selectSelfCheckInDialogContainer = new FrameLayout(getActivity());
		selectSelfCheckInDialogContainer.addView(m_selectSelfCheckInDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(selectSelfCheckInDialogContainer);
		AlertDialog selectSelfCheckInDialog = builder.create();
		selectSelfCheckInDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		selectSelfCheckInDialog.setCancelable(false);
		selectSelfCheckInDialog.show();
		selectSelfCheckInDialog.dismiss();
		ViewGroup.LayoutParams lp = m_selectSelfCheckInDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_selectselfcheckindialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_selectselfcheckindialog_height);
		m_selectSelfCheckInDialogView.setLayoutParams(lp);
		((View) m_selectSelfCheckInDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);

		return selectSelfCheckInDialog;
	}

	public AlertDialog getSliderQuestionDialog() {
		FrameLayout sliderQuestionDialogContainer = new FrameLayout(getActivity());
		sliderQuestionDialogContainer.addView(m_sliderQuestionDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(sliderQuestionDialogContainer);
		AlertDialog sliderQuestionDialog = builder.create();
		sliderQuestionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		sliderQuestionDialog.setCancelable(false);
		sliderQuestionDialog.show();
		sliderQuestionDialog.dismiss();
		ViewGroup.LayoutParams lp = m_sliderQuestionDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_sliderquestiondialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_sliderquestiondialog_height);
		m_sliderQuestionDialogView.setLayoutParams(lp);
		((View) m_sliderQuestionDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return sliderQuestionDialog;
	}
}
