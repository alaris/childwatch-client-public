package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Family;

public class UpdateFamily extends Command<Family> {
	private final Family m_family;

	public UpdateFamily(Family family) {
		this(family, null, null);
	};

	public UpdateFamily(Family family, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_family=family;
	}

	@Override
	public void executeSource() {
		/*if (m_family.getWasOverriden())
			m_family.setWasOverriden(false);
		else if (m_model.getIsOverride())
			m_family.setWasOverriden(true);*/
		m_dataSource.getFamiliesDAO().update(m_family, this);
	}

	@Override
	public void handleResponse(Family family) {
	}

}