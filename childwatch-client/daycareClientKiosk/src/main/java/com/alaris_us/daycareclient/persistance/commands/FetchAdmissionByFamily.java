package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;

public class FetchAdmissionByFamily extends Command<List<Admission>> {

	private Family m_family;
	private Facility m_facility;

	public FetchAdmissionByFamily(Family family, Facility facility) {
		this(family, facility, null, null);
	}

	public FetchAdmissionByFamily(Family family, Facility facility, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_family = family;
		m_facility = facility;
	}

	@Override
	public void executeSource() {
		if (m_facility.getUsesAssocWideAdmissions())
			m_dataSource.getAdmissionsDAO().fetchFamilyAdmissions(m_family, this);
		else
			m_dataSource.getAdmissionsDAO().fetchFacilityFamilyAdmissions(m_family, m_facility, this);
	}

	public void handleResponse(List<Admission> admission) {

	}
}
