package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Document;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;

public class SaveDocument extends Command<Document> {

	private final Document m_document;

	public SaveDocument(Document document) {
		this(document, null, null);
	};

	public SaveDocument(Document document, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_document = document;
	}

	@Override
	public void executeSource() {
		m_dataSource.getDocumentsDAO().createDocument(m_document, this);
	}

	@Override
	public void handleResponse(Document document) {
		
	}
}