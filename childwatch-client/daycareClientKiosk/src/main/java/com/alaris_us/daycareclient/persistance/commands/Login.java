package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.User;

public class Login
	extends Command<User> {

	private String	m_username, m_password;

	public Login(String username, String password) {

		this(username, password, null, null);
	};

	public Login(String username, String password, DaycareData dataSource,
			PersistenceData model) {

		super(dataSource, model);
		m_username = username;
		m_password = password;
	}

	@Override
	public void executeSource() {

		m_dataSource.login(m_username, m_password, this);

	}

	@Override
	public void handleResponse(
			User data) {

		m_model.setUser(data);

	}

}