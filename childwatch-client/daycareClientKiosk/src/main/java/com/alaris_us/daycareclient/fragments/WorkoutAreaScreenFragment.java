package com.alaris_us.daycareclient.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.models.WorkoutAreaScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher.ScrollDir;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient.widgets.WorkoutAreaCheckBox;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class WorkoutAreaScreenFragment extends ScreenSwitcherFragment {
	WorkoutAreaScreenViewModel m_model;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.workoutareascreen_layout,
				container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		HorizontalListView areaList = (HorizontalListView) m_layout
				.findViewById(R.id.workoutareascreen_tile_arealist_container);
		m_model = new WorkoutAreaScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		m_layout.findViewById(R.id.view_navigation_next_container).setId(
				R.id.workoutareascreen_nav_next);
		m_layout.findViewById(R.id.view_navigation_back_container).setId(
				R.id.workoutareascreen_nav_back);
		UiBinder.bind(m_layout, R.id.workoutareascreen_tile_arealist_container,
				"Adapter", m_model, "WorkoutAreas", BindingMode.TWO_WAY,
				new AdapterConverter(WorkoutAreaCheckBox.class, true, true));
		UiBinder.bind(m_layout, R.id.workoutareascreen_nav_next,
				"OnClickListener", m_model, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.workoutareascreen_nav_next, "Alpha",
				m_model, "NextAlpha", BindingMode.ONE_WAY);
		areaList.setOnItemClickListener(m_model.new WorkoutAreaClickListener());
		// Navigation Button(s)
		m_layout.findViewById(R.id.workoutareascreen_nav_back)
				.setOnClickListener(new BackClickListener());
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		m_model.disconnect();
	}

	@Override
	public void onResumeFragment() {
		m_model = (null == m_model) ? new WorkoutAreaScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this) : m_model;
		m_model.reset();
		super.onResumeFragment();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		m_model.disconnect();
		m_model = null;
	}

	@Override
	public void initData() {
		setCanScroll(ScrollDir.BACKWARD, false);
		m_model.reset();
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}
}
