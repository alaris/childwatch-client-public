package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public class TileHolder extends FrameLayout{
	
	public TileHolder(Context context, AttributeSet attr) {
		super(context, attr);
		
	}

	public TileHolder(Context context) {
		super(context);
	}

	public void setView(View v){
		removeAllViews();
		addView(v);
	}
	
}
