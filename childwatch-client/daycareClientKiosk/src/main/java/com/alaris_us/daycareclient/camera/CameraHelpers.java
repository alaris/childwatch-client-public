package com.alaris_us.daycareclient.camera;

import android.app.Activity;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Area;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.view.OrientationEventListener;
import android.view.Surface;

import java.util.ArrayList;
import java.util.List;

public class CameraHelpers {
	public static int IMAGE_FORMAT_NV21 = ImageFormat.NV21;
	public static String FOCUS_MODE_CONTINUOUS_PICTURE = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;
	public static String FOCUS_MODE_AUTO = Camera.Parameters.FOCUS_MODE_AUTO;

	public static int NO_CAMERA = -1;

	public enum CameraDirection {
		BACK(Camera.CameraInfo.CAMERA_FACING_BACK), FRONT(Camera.CameraInfo.CAMERA_FACING_FRONT), NOTSPECIFIED(
				NO_CAMERA);

		private int direction;

		CameraDirection(int direction) {
			this.direction = direction;
		}

		public int getDirection() {
			return direction;
		}
	}

	public static int findCamera(CameraDirection direction) {

		int numberOfCameras = Camera.getNumberOfCameras();
		CameraInfo cameraInfo = new CameraInfo();
		for (int i = 0; i < numberOfCameras; i++) {
			Camera.getCameraInfo(i, cameraInfo);
			if (cameraInfo.facing == direction.getDirection()) {
				return i;
			}
		}

		return NO_CAMERA;

	}

	public static Camera openCamera(CameraDirection direction) {

		int cameraID = findCamera(direction);

		if (NO_CAMERA == cameraID) {
			return null;
		}

		try {
			return Camera.open(cameraID);
		} catch (RuntimeException e) {
			e.printStackTrace();
			return null;
		}

	}

	public static void releaseCamera(Camera toRelease) {
		if (toRelease != null) {
			toRelease.release(); // release the camera for other applications
			toRelease = null;
		}
	}

	public static void setOptimalPictureSize(Camera camera, int width, int height) {
		if (camera == null)
			return;
		Camera.Parameters params = camera.getParameters();
		List<Size> sizes = params.getSupportedPictureSizes();
		Camera.Size size = params.getSupportedPictureSizes().get(0);
		int diffWidth = Math.abs(width - size.width);
		int diffHeight;
		for (int i = 0; i < sizes.size(); i++) {
			int newDiffWidth = Math.abs(width - sizes.get(i).width);
			if (newDiffWidth == diffWidth) {
				diffHeight = Math.abs(height - size.height);
				int newDiffHeight = Math.abs(height - sizes.get(i).height);
				if (newDiffHeight < diffHeight) {
					size = sizes.get(i);
					diffHeight = newDiffHeight;
				}
			} else if (newDiffWidth < diffWidth) {
				size = sizes.get(i);
				diffWidth = newDiffWidth;
			}
		}
		params.setPictureSize(size.width, size.height);
		camera.setParameters(params);
	}

	public static void setFocusArea(Camera camera, Rect focusArea) {
		if (camera == null || focusArea == null)
			return;
		Camera.Parameters params = camera.getParameters();
		List<Area> areas = new ArrayList<Area>();
		areas.add(new Area(focusArea, 1000));
		if (params.getMaxNumFocusAreas() > 0)
			params.setFocusAreas(areas);
		if (params.getMaxNumMeteringAreas() > 0)
			params.setMeteringAreas(areas);
		camera.setParameters(params);
	}

	public static void setPictureOptions(Camera camera, int imageFormat, String focusMode) {
		if (camera == null)
			return;
		Camera.Parameters params = camera.getParameters();
		params.setPreviewFormat(ImageFormat.NV21);
		if (params.getSupportedFocusModes().contains(focusMode))
			params.setFocusMode(focusMode);
		camera.setParameters(params);
	}

	public static void startFaceDetection(Camera camera) {
		// Try starting Face Detection
		Camera.Parameters params = camera.getParameters();

		// start face detection only *after* preview has started
		if (params.getMaxNumDetectedFaces() > 0) {
			// camera supports face detection, so can start it:
			camera.startFaceDetection();
		}
	}

	public Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.1;
		double targetRatio = (double) w / h;
		if (sizes == null)
			return null;

		Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		// Try to find an size match aspect ratio and size
		for (Size size : sizes) {
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}

		// Cannot find the one match the aspect ratio, ignore the requirement
		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}

	// Orientation hysteresis amount used in rounding, in degrees
	private static final int ORIENTATION_HYSTERESIS = 5;

	public static int getDisplayRotation(Activity activity) {
		int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
		switch (rotation) {
		case Surface.ROTATION_0:
			return 0;
		case Surface.ROTATION_90:
			return 90;
		case Surface.ROTATION_180:
			return 180;
		case Surface.ROTATION_270:
			return 270;
		}
		return 0;
	}

	public static int getDisplayOrientation(int degrees, int cameraId) {
		// See android.hardware.Camera.setDisplayOrientation for
		// documentation.
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(cameraId, info);
		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		return result;
	}

	public static void prepareMatrix(Matrix matrix, boolean mirror, int viewWidth, int viewHeight) {
		// Need mirror for front camera.
		matrix.setScale(mirror ? -1 : 1, 1);
		// Camera driver coordinates range from (-1000, -1000) to (1000, 1000).
		// UI coordinates range from (0, 0) to (width, height).
		matrix.postRotate(180f);
		matrix.postScale(viewWidth / 2000f, viewHeight / 2000f);
		matrix.postTranslate(viewWidth / 2f, viewHeight / 2f);
	}

	public static int roundOrientation(int orientation, int orientationHistory) {
		boolean changeOrientation = false;
		if (orientationHistory == OrientationEventListener.ORIENTATION_UNKNOWN) {
			changeOrientation = true;
		} else {
			int dist = Math.abs(orientation - orientationHistory);
			dist = Math.min(dist, 360 - dist);
			changeOrientation = (dist >= 45 + ORIENTATION_HYSTERESIS);
		}
		if (changeOrientation) {
			return ((orientation + 45) / 90 * 90) % 360;
		}
		return orientationHistory;
	}
}
