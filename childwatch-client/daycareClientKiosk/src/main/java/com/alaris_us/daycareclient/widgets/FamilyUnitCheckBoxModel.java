package com.alaris_us.daycareclient.widgets;

import android.view.View;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.externaldata.to.UnitInfo;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

public class FamilyUnitCheckBoxModel {

	private TrackableField<UnitInfo> m_unit;
	private TrackableBoolean m_isSelected;
	private TrackableInt m_width;
	private Integer m_color;
	//private int checkedRes, unCheckedRes;

	public FamilyUnitCheckBoxModel() {
		this(null, null, null);
	}

	public FamilyUnitCheckBoxModel(UnitInfo unit, Integer width, Integer color) {
		m_unit = new TrackableField<UnitInfo>(unit);
		m_isSelected = new TrackableBoolean(false);
		m_width= new TrackableInt(width);
		m_color = color;
		
		//checkedRes=R.drawable.checkmark;
		//unCheckedRes=R.drawable.dashed_circle;
	}
	
	public UnitInfo getUnit() {
		return m_unit.get();
	}
	
	public String getFamilyUnitName() {
		return m_unit.get().getUnitName();
	}
	
	public String getFamilyUnitStatus() {
		return m_unit.get().getStatus();
	}
	
	public String getFamilyUnitMembershipType() {
		return m_unit.get().getMembershipType();
	}
	
	public String getFamilyUnitJoinDate() {
		return m_unit.get().getJoinDate().toString();
	}

	public int getBackgroundColor(){
		return m_color;
	}

	public int getWidth() {
		return m_width.get();
	}
	
	public void setWidth(int width){
		m_width.set(width);
	}
	
	public float getAlpha(){
		return ListViewFunctions.getTileAlpha(m_isSelected.get());
	}
	
	public boolean getIsSelected(){
		return m_isSelected.get();
	}
	
	public void setIsSelected(boolean isSelected){
		m_isSelected.set(isSelected);
	}
	
	public void toggleIsSelected(){
		setIsSelected(!getIsSelected());
	}
	
	public int getCheckBoxRes(){
		return m_isSelected.get()? View.VISIBLE : View.INVISIBLE;
	}

}
