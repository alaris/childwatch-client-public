package com.alaris_us.daycareclient.yombu;

public class YombuRESTException extends Exception {
    private static final long serialVersionUID = 1L;
    private int mCode;

    public YombuRESTException(int code, String message) {
        super(message);
        mCode = code;
    }

    public YombuRESTException(Throwable throwable) {
        super(throwable);
        mCode = -1;
    }

    public YombuRESTException(int code, String message, Throwable throwable) {
        super(message, throwable);
        mCode = code;
    }

    public int getCode() {
        return mCode;
    }

}

