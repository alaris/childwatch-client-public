package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.data.DaycareCredential;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;

import java.util.List;

public class FetchSelfCheckInMembers extends Command<List<Member>> {

	private DaycareCredential m_credential;
	private Facility m_facility;

	public FetchSelfCheckInMembers(DaycareCredential credential, Facility facility) {

		this(credential, facility, null, null);
	};

	public FetchSelfCheckInMembers(DaycareCredential credential, Facility facility, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_credential = credential;
		m_facility = facility;
	}

	@Override
	public void executeSource() {

		m_dataSource.getMembersDAO().getSelfCheckInMembers(m_credential.getText(), m_credential.getForm().toString(), m_facility.getObjectId(), this);

	}

	@Override
	public void handleResponse(List<Member> response) {
	}

}