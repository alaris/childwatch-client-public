package com.alaris_us.daycareclient.models;

import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.fragments.EmergencyScreenFragment;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.EmergencyContactItem;
import com.alaris_us.daycareclient.widgets.EmergencyContactItemModel;
import com.alaris_us.daycaredata.EmergencyNumber;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class EmergencyScreenViewModel extends ViewModel {
	public EmergencyScreenViewModel(PersistenceLayer persistenceLayer, EmergencyScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private EmergencyScreenFragment											fragment;
	private TrackableField<TrackableCollection<EmergencyContactItemModel>>	m_memberModels;
	private TrackableField<TrackableCollection<Member>>						mMembers;
	private TrackableField<TrackableCollection<Member>>						mParents;

	public TrackableCollection<EmergencyContactItemModel> getMembers() {
		return m_memberModels.get();
	}

	@Override
	protected void onPopulateModelData() {
		dismissAllDialogs();
		mMembers.get().clear();
		mMembers.get().addAll(getPersistenceLayer().getPersistenceData().getAllYMCAChildren());
		mParents.get().clear();
		mParents.get().addAll(getPersistenceLayer().getPersistenceData().getAllYMCAParents());
		m_memberModels.get().clear();
		int childNum = 0;
		for (Member member : mMembers.get()) {
			if (member.getUsesChildWatch()) {
				childNum++;
			}
		}
		int width = ListViewFunctions.calculateTileWidth(childNum, 2.25);
		SetForAllClickListener setForAllListener = new SetForAllClickListener();
		int pos = 0;
		for (Member member : mMembers.get()) {
			if (member.getUsesChildWatch()) {
				EmergencyContactItemModel model = new EmergencyContactItemModel(member, width,
						ListViewFunctions.getColorAt(pos++), mParents.get(), setForAllListener);
				m_memberModels.get().add(model);
			}
		}
	}

	private void updateChildren() {
		for (EmergencyContactItemModel model : m_memberModels.get()) {
			Member cur = model.getMember();
			for (Member m : mMembers.get()) {
				if (m.equals(cur)) {
					List<EmergencyNumber> numbers = new ArrayList<EmergencyNumber>();
					EmergencyNumber contact1 = model.getItemContact1();
					if (contact1 != null)
						numbers.add(contact1);
					EmergencyNumber contact2 = model.getItemContact2();
					if (contact2 != null)
						numbers.add(contact2);
					m.setEmergencyNumbers(numbers);
				}
			}
		}
	}

	public class MemberClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			EmergencyContactItemModel model = ((EmergencyContactItem) arg1).getModel();
			for (EmergencyContactItemModel m : m_memberModels.get())
				m.setSelected(m.equals(model));
		}
	}

	public class SetForAllClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			EmergencyContactItemModel currModel = (EmergencyContactItemModel) v.getTag();
			EmergencyNumber contact1 = currModel.getItemContact1();
			EmergencyNumber contact2 = currModel.getItemContact2();
			for (EmergencyContactItemModel model : m_memberModels.get()) {
				model.setSelected(false);
				if (!model.equals(currModel)) {
					model.setItemContact1(contact1);
					model.setItemContact2(contact2);
				}
			}
		}
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			updateChildren();
			getPersistenceLayer().getPersistenceData().setSelectedYMCAChildren(mMembers.get());
			fragment.nextScreen();
		}
	}

	@Override
	public void onInitialize() {
		m_memberModels = new TrackableField<TrackableCollection<EmergencyContactItemModel>>(
				new TrackableCollection<EmergencyContactItemModel>());
		mMembers = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		mParents = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		addWatch(CHANGE_ID.SELECTEDYMCACHILDREN);
	}

	public void dismissAllDialogs() {
		for (EmergencyContactItemModel model : m_memberModels.get())
			model.setSelected(false);
	}
}
