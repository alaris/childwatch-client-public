package com.alaris_us.daycareclient.models;

import java.util.List;

import com.alaris_us.daycareclient.fragments.MainScreenFragment;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;
import com.bindroid.trackable.TrackableField;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import org.json.JSONException;

public class MainScreenViewModel extends ViewModel {
	public MainScreenViewModel(PersistenceLayer persistenceLayer, MainScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	@Override
	protected void onInitialize() {
		m_temperature = new TrackableField<String>();
		m_temperatureUnit = new TrackableField<String>();
		m_dateTime = new TrackableField<CharSequence>();
		m_capacityInfo = new TrackableField<String>();
		mCityStateText = new TrackableField<CharSequence>();
		mClubNameText = new TrackableField<CharSequence>();
		m_locationImage = new TrackableField<Bitmap>();
        m_fullCapacityDialog = new TrackableField<AlertDialog>();
        m_staffOverrideEntryDialog = new TrackableField<AlertDialog>();
        m_successfulOverrideDialog = new TrackableField<AlertDialog>();
	}

	@Override
	protected void onReset() {
		addWatch(CHANGE_ID.TEMPERATURE);
		addWatch(CHANGE_ID.DATETIME);
		addWatch(CHANGE_ID.FACILITY);
		addWatch(CHANGE_ID.CAPACITY);
	}

	@Override
	protected void onPopulateModelData() {
		PersistenceData pd = getPersistenceLayer().getPersistenceData();
		m_locationImage.set(pd.getFacilityImage());
		mCityStateText.set(pd.getFacilityCity() + ", " + pd.getFacilityState());
		mClubNameText.set(pd.getYmcaName());
		m_temperature.set(pd.getCurrentTemperature());
		m_temperatureUnit.set(pd.getCurrentTemperatureUnit());
		m_dateTime.set(pd.getCurrentDateTime());
		m_capacityInfo.set(pd.getCapacityInfo());
		this.bindServiceToFragment();
	}


	private TrackableField<String>			m_temperature;
	private TrackableField<String>			m_temperatureUnit;

	private TrackableField<String> 			m_capacityInfo;

	private TrackableField<CharSequence>	m_dateTime;
	private TrackableField<CharSequence>	mCityStateText;
	private TrackableField<CharSequence>	mClubNameText;
	private TrackableField<Bitmap>			m_locationImage;
	private MainScreenFragment fragment;
	private Integer m_checkedInMembersCount;
    private TrackableField<AlertDialog> m_fullCapacityDialog;
    private TrackableField<AlertDialog> m_staffOverrideEntryDialog;
    private TrackableField<AlertDialog> m_successfulOverrideDialog;
    private int m_clockedInStaffNum;


	public String getTemperature() {
		return this.m_temperature.get();
	}

	public String getTemperatureUnit() {
		return this.m_temperatureUnit.get();
	}

	public CharSequence getDateTime() {
		return this.m_dateTime.get();
	}

	public CharSequence getCityStateText() {
		return mCityStateText.get();
	}

	public CharSequence getClubNameText() {
		return mClubNameText.get();
	}

	public void setCapacityInfo(String capacityText){
		Log.e("MainScreenView", "Setting capacity text to " + capacityText);
		getPersistenceLayer().getPersistenceData().setCapacityInfo(capacityText);
		m_capacityInfo.set(capacityText);
	}

	public String getCapacityInfo() {
		return this.m_capacityInfo.get();
	}

	public Bitmap getLocationImage() {
		return this.m_locationImage.get();
	}


	private void bindServiceToFragment() {
		getPersistenceLayer().bindFragmentToService(fragment);
	}

	public String getLanguageOne() {
		String start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text1);
		try {
			String language = getPersistenceLayer().getPersistenceData().getFacility().getLanguages().getString(0);
			switch (language) {
				case "en":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text1);
					break;
				case "es":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text2);
					break;
				case "zh":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text3);
					break;
				case "ar":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text4);
					break;
				case "so":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text5);
					break;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return start;
	}

	public String getLanguageTwo() {
		String start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text1);
		try {
			String language = getPersistenceLayer().getPersistenceData().getFacility().getLanguages().getString(1);
			switch (language) {
				case "en":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text1);
					break;
				case "es":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text2);
					break;
				case "zh":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text3);
					break;
				case "ar":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text4);
					break;
				case "so":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text5);
					break;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return start;
	}

	public String getLanguageThree() {
		String start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text1);
		try {
			String language = getPersistenceLayer().getPersistenceData().getFacility().getLanguages().getString(2);
			switch (language) {
				case "en":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text1);
					break;
				case "es":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text2);
					break;
				case "zh":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text3);
					break;
				case "ar":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text4);
					break;
				case "so":
					start = fragment.getContext().getResources().getString(R.string.mainscreen_tile_start_text5);
					break;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return start;
	}

	public void checkCapacity() {
		getPersistenceLayer().fetchCheckedInMembersCount(new FetchCheckedInMembersCountListener(true));
	}

	private class FetchCheckedInMembersCountListener implements CommandStatusListener {

		FetchCheckedInMembersCountListener(boolean checkCapacity) {

		}
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			m_checkedInMembersCount = (Integer) command.getResult();
			getPersistenceLayer().fetchClockedInStaff(new FetchClockedInStaffListener());
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class FetchClockedInStaffListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void finished(Command<?> command) {
			List<Staff> clockedInStaff = (List<Staff>) command.getResult();
			m_clockedInStaffNum = clockedInStaff.size();
			Facility facility = getPersistenceLayer().getPersistenceData().getFacility();
			Integer facilityRatio = (Integer) facility.getRatio(); //TODO add handling for room ratios here


			List<Room> rooms = getPersistenceLayer().getPersistenceData().getAllRooms();

			if (facility.getRatioType() == null) {
				fragment.setLanguage();
				fragment.nextScreen();
			} else if (facility.getRatioType().equals("FACILITYANDROOM")) {
				// TODO Handle facility that uses both facility and room ratio
				getPersistenceLayer().fetchCheckedInMembers(new FetchCheckedInMembersListener());
			} else if (facilityRatio != 0) {
				int capacityToCompare = 0;
				int maxCapacity = facility.getMaxCapacity().intValue();
				if (maxCapacity != 0 && facility.getMaxCapacityType().equals("FACILITY")) {
					capacityToCompare = ((m_clockedInStaffNum * facilityRatio) > maxCapacity) ? maxCapacity
							: m_clockedInStaffNum * facilityRatio;
				} else
					capacityToCompare = m_clockedInStaffNum * facilityRatio;

				if (m_checkedInMembersCount < capacityToCompare) {
					fragment.setLanguage();
					fragment.nextScreen();
				} else {
					showFullCapacityDialog();
				}
			} else if (facility.getMaxCapacityType() != null) {
				if (facility.getMaxCapacityType().equals("FACILITY")) {
					if (m_checkedInMembersCount < facility.getMaxCapacity().intValue()) {
						fragment.setLanguage();
						fragment.nextScreen();
					} else {
						showFullCapacityDialog();
					}
				} else if (facility.getMaxCapacityType().equals("PROGRAM") || facility.getMaxCapacityType().equals("ROOM")) {

					fragment.setLanguage();
					fragment.nextScreen();
				}
			} else {
				int capacity = 0;

				for (Room r : rooms) {

					Log.i("MainScreenView", "ROOM CAP ===== " + r.getName() + ": " + r.getCurrentCapacity());
					for (Staff s : clockedInStaff) {
						if (s.getPRoom().equals(r))
							capacity += r.getRatio().intValue();
					}
				}
				if (m_checkedInMembersCount < capacity) {
					fragment.setLanguage();
					fragment.nextScreen();
				} else {
					showFullCapacityDialog();
				}
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class FetchCheckedInMembersListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {
		}

		 @Override
		public void finished(Command<?> command) {
			List<Room> allRooms = getPersistenceLayer().getPersistenceData().getAllRooms();
			int maxCapacity = getPersistenceLayer().getPersistenceData().getFacility().getMaxCapacity().intValue();
			int currentMaxCapacity = getPersistenceLayer().getPersistenceData().getFacility().getRatio().intValue() * m_clockedInStaffNum;
			double availableCapacity = 0;

			if (getPersistenceLayer().getPersistenceData().getFacility().getMaxCapacityType() != null)
				if (getPersistenceLayer().getPersistenceData().getFacility().getMaxCapacityType().equals("FACILITY") && currentMaxCapacity > maxCapacity) {
					availableCapacity = maxCapacity;
				} else {
					availableCapacity = currentMaxCapacity;
				}
			else
				availableCapacity = currentMaxCapacity;
			List<Member> checkedInMembers = (List<Member>) command.getResult();
			for (Room r : allRooms) {
				int checkedIntoRoomCount = 0;
				for (Member m : checkedInMembers) {
					if (m.getPRoom() != null && m.getPRoom().equals(r)) {
						checkedIntoRoomCount++;
					}
				}
				availableCapacity = availableCapacity - (checkedIntoRoomCount * r.getCapacityMultiplier().doubleValue());
			}
			availableCapacity = Math.floor(availableCapacity);
			if (availableCapacity <= 0) {
				showFullCapacityDialog();
			} else {
				fragment.setLanguage();
				fragment.nextScreen();
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
			Log.d("MainScreenViewModel", "Check in all members.");

		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	public void setFullCapacityDialog(AlertDialog alertDialog) {
		m_fullCapacityDialog.set(alertDialog);
	}

	private void showFullCapacityDialog() {
		if (m_fullCapacityDialog.get() != null) {
			m_fullCapacityDialog.get().show();
		}
	}

	private void dismissFullCapacityDialog() {
		if (m_fullCapacityDialog.get() != null && m_fullCapacityDialog.get().isShowing())
			m_fullCapacityDialog.get().dismiss();
	}

	public OnClickListener getFullCapacityDialogOkClickListener() {
		return new FullCapacityDialogOkClickListener();
	}

	public OnClickListener getFullCapacityDialogOverrideClickListener() {
		return new FullCapacityDialogOverrideClickListener();
	}

	public class FullCapacityDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissFullCapacityDialog();
		}
	}

	public class FullCapacityDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissFullCapacityDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public void setStaffOverrideEntryDialog(AlertDialog alertDialog) {
		m_staffOverrideEntryDialog.set(alertDialog);
	}

	private void showStaffOverrideEntryDialog() {
		if (m_staffOverrideEntryDialog.get() != null) {
			m_staffOverrideEntryDialog.get().show();
		}
	}

	public void dismissStaffOverrideEntryDialog() {
		if (m_staffOverrideEntryDialog != null && m_staffOverrideEntryDialog.get().isShowing())
			m_staffOverrideEntryDialog.get().dismiss();
	}

	public void setSuccessfulOverrideDialog(AlertDialog alertDialog) {
		m_successfulOverrideDialog.set(alertDialog);
	}

	private void showSuccessfulOverrideDialog() {
		if (m_successfulOverrideDialog.get() != null) {
			m_successfulOverrideDialog.get().show();
		}
	}

	public void dismissSuccessfulOverrideDialog() {
		if (m_successfulOverrideDialog != null && m_successfulOverrideDialog.get().isShowing())
			m_successfulOverrideDialog.get().dismiss();
	}

	public OnClickListener getStaffOverrideEntryDialogEnterClickListener() {
		return new StaffOverrideEntryDialogEnterClickListener();
	}

	public OnClickListener getStaffOverrideEntryDialogCancelClickListener() {
		return new StaffOverrideEntryDialogCancelClickListener();
	}

	public class StaffOverrideEntryDialogEnterClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			Facility facility = getPersistenceLayer().getPersistenceData().getFacility();
			if (facility.getOverridePIN().equals(fragment.getOverridePIN())) {
				dismissStaffOverrideEntryDialog();
				showSuccessfulOverrideDialog();
			} else {
				fragment.setOverridePIN("");
				fragment.setOverridePINHint("Invalid PIN, Please Retry");
			}
		}
	}

	public class StaffOverrideEntryDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissStaffOverrideEntryDialog();
		}
	}

	public OnClickListener getSuccessfulOverrideDialogOkClickListener() {
		return new SuccessfulOverrideDialogOkClickListener();
	}

	public class SuccessfulOverrideDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getPersistenceLayer().getPersistenceData().setIsOverride(true);
			dismissSuccessfulOverrideDialog();
			fragment.setLanguage();
			fragment.nextScreen();
		}
	}
}
