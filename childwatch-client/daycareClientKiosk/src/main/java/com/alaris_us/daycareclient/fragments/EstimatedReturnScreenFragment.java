package com.alaris_us.daycareclient.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.models.EstimatedReturnScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.EstimatedReturnCheckBox;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class EstimatedReturnScreenFragment extends ScreenSwitcherFragment {
	EstimatedReturnScreenViewModel m_model;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.estimatedreturnscreen_layout,
				container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		HorizontalListView timeList = (HorizontalListView) m_layout
				.findViewById(R.id.estimatedreturnscreen_tile_timelist);
		m_model = new EstimatedReturnScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		m_layout.findViewById(R.id.view_navigation_next_container).setId(
				R.id.estimatedreturnscreen_nav_next);
		m_layout.findViewById(R.id.view_navigation_back_container).setId(
				R.id.estimatedreturnscreen_nav_back);
		UiBinder.bind(this.m_layout, R.id.estimatedreturnscreen_tile_timelist,
				"Adapter", m_model, "Times", BindingMode.TWO_WAY,
				new AdapterConverter(EstimatedReturnCheckBox.class, true, true));
		UiBinder.bind(m_layout, R.id.estimatedreturnscreen_nav_next,
				"OnClickListener", m_model, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.estimatedreturnscreen_nav_back,
				"OnClickListener", m_model, "BackClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.estimatedreturnscreen_nav_next, "Alpha",
				m_model, "NextAlpha", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout,
				R.id.view_navigation_withbuttons_selectall_textbox,
				"TextSizeRes", m_model, "SelectAllButtonTextSize",
				BindingMode.ONE_WAY);
		timeList.setOnItemClickListener(m_model.new TimeClickListener());
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		m_model.disconnect();
	}

	@Override
	public void onResumeFragment() {
		m_model = (null == m_model) ? new EstimatedReturnScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this) : m_model;
		m_model.reset();
		super.onResumeFragment();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		m_model.disconnect();
		m_model = null;
	}

	@Override
	public void initData() {
		m_model.reset();
	}
}
