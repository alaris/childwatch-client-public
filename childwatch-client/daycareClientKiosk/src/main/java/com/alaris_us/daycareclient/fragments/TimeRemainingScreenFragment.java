package com.alaris_us.daycareclient.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient_dev.R;

public class TimeRemainingScreenFragment extends ScreenSwitcherFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.timeremainingscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		m_layout.findViewById(R.id.view_ok_navigation_container).setOnClickListener(new ContinueClickListener());
	}

	private class ContinueClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.view_ok_navigation_container) {
				m_activity.gotoFirstScreen();
			}
		}
	}

	@Override
	public void initData() {
	}
}
