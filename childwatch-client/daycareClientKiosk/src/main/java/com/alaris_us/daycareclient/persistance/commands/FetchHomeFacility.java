package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;

public class FetchHomeFacility extends Command<Facility> {

	private String m_clubNumber;

	public FetchHomeFacility(String clubNumber) {

		this(clubNumber, null, null);
	}

	protected FetchHomeFacility(String clubNumber, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_clubNumber = clubNumber;
	}

	@Override
	public void executeSource() {
		m_dataSource.getFacilitiesDAO().findFacilityByClubNumber(m_clubNumber, this);
	}

	@Override
	public void handleResponse(Facility data) {
	}
}