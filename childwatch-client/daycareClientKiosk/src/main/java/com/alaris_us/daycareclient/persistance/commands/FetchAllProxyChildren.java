package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Member;

public class FetchAllProxyChildren extends Command<List<Member>> {

	Member m_member;

	public FetchAllProxyChildren(Member member) {

		this(member, null, null);
	}

	protected FetchAllProxyChildren(Member member, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_member = member;
	}

	@Override
	public void executeSource() {

		m_dataSource.getMembersDAO().listProxyChildren(m_member, this);

	}

	@Override
	public void handleResponse(List<Member> data) {

		//m_model.setAllProxyChildren(data);

	}

}