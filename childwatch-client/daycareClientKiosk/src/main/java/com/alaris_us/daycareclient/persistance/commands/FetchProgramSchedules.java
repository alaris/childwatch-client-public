package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Recurrence;
import com.alaris_us.daycaredata.to.Schedule;

public class FetchProgramSchedules extends Command<List<Schedule>> {

	Program	m_program;
	List<Schedule> m_schedules;

	public FetchProgramSchedules(Program program) {

		this(program, null, null);
	}

	protected FetchProgramSchedules(Program program, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_program = program;
	}

	@Override
	public void executeSource() {

		m_dataSource.getProgramsDAO().listProgramSchedules(m_program, this);

	}

	@Override
	public void handleResponse(List<Schedule> data) {
		List<Schedule> r = data;
	}
	
	private class ScheduleCallback implements DaycareOperationComplete<List<Schedule>> {
		FetchProgramSchedules m_listener;

		public ScheduleCallback(FetchProgramSchedules listener) {
			m_listener = listener;
		}

		@Override
		public void operationComplete(List<Schedule> arg0, DaycareDataException arg1) {
			List<Recurrence> m_Recurrences = new ArrayList<Recurrence>();
			if (arg0 != null) {
				m_schedules = arg0;
				for (Schedule s : arg0)
					m_Recurrences.add(s.getPRecurrence());
				m_dataSource.getRecurrencesDAO().fetchAllObjectsIfNeeded(m_Recurrences, new RecurrenceCallback(m_listener));
			}
		}
	}
	
	private class RecurrenceCallback implements DaycareOperationComplete<List<Recurrence>> {
		FetchProgramSchedules m_listener;

		public RecurrenceCallback(FetchProgramSchedules listener) {
			m_listener = listener;
		}

		@Override
		public void operationComplete(List<Recurrence> arg0, DaycareDataException arg1) {
			m_listener.handleResponse(m_schedules);
		}
	}
}