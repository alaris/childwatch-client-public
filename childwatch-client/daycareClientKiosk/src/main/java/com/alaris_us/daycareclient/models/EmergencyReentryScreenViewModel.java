package com.alaris_us.daycareclient.models;

import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.fragments.EmergencyReentryScreenFragment;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.EmergencyContactItem;
import com.alaris_us.daycareclient.widgets.EmergencyContactItemModel;
import com.alaris_us.daycaredata.EmergencyNumber;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class EmergencyReentryScreenViewModel extends ViewModel {
	public EmergencyReentryScreenViewModel(PersistenceLayer persistenceLayer, EmergencyReentryScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private EmergencyReentryScreenFragment									fragment;
	private TrackableField<TrackableCollection<EmergencyContactItemModel>>	m_memberModels;
	private TrackableBoolean												m_isCorrectlyFormatted;

	public TrackableCollection<EmergencyContactItemModel> getMembers() {
		return m_memberModels.get();
	}

	@Override
	protected void onPopulateModelData() {
		dismissAllDialogs();
		List<Member> members;
		members = getPersistenceLayer().getPersistenceData().getSelectedDaycareChildren();
		m_memberModels.get().clear();
		m_isCorrectlyFormatted.set(false);
		int width = ListViewFunctions.calculateTileWidth(members.size(), 2.25);
		ReentrySetForAllClickListener reentrySetForAllListener = new ReentrySetForAllClickListener();
		int pos = 0;
		for (Member member : members) {
			EmergencyContactItemModel model = new EmergencyContactItemModel(member, width,
					ListViewFunctions.getColorAt(pos++), reentrySetForAllListener);
			m_memberModels.get().add(model);
		}
	}

	private List<Member> createChildrenToSave() {
		List<Member> members = new ArrayList<Member>();
		for (EmergencyContactItemModel model : m_memberModels.get()) {
			Member member = model.getMember();
			List<EmergencyNumber> numbers = new ArrayList<EmergencyNumber>();
			EmergencyNumber contact1 = model.getItemContact1();
			if (contact1 != null)
				numbers.add(contact1);
			EmergencyNumber contact2 = model.getItemContact2();
			if (contact2 != null)
				numbers.add(contact2);
			member.setEmergencyNumbers(numbers);
			members.add(member);
		}
		return members;
	}

	public class MemberClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			EmergencyContactItemModel model = ((EmergencyContactItem) arg1).getModel();
			for (EmergencyContactItemModel m : m_memberModels.get())
				m.setSelected(m.equals(model));
		}
	}

	public class ReentrySetForAllClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			EmergencyContactItemModel currModel = (EmergencyContactItemModel) v.getTag();
			EmergencyNumber contact1 = currModel.getItemContact1();
			EmergencyNumber contact2 = currModel.getItemContact2();
			for (EmergencyContactItemModel model : m_memberModels.get()) {
				model.setSelected(false);
				if (!model.equals(currModel)) {
					model.setItemContact1(contact1);
					model.setItemContact2(contact2);
				}
			}
		}
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			// m_persistenceLayer.updateEmergencyContacts(createChildrenToSave());
			getPersistenceLayer().getPersistenceData().setSelectedChildren(createChildrenToSave());
			fragment.nextScreen();
		}
	}

	public OnClickListener getNextClickListener() {
		List<EmergencyContactItemModel> models = new ArrayList<EmergencyContactItemModel>();
		models = m_memberModels.get();
		for (int i = 0; i < models.size(); i++) {
			if (models.get(i).getItemContact1().getName().isEmpty()
					|| models.get(i).getItemContact1().getPhone().isEmpty()) {
				m_isCorrectlyFormatted.set(false);
				break;
			}
			if (i == models.size() - 1)
				m_isCorrectlyFormatted.set(true);
		}
		if (m_isCorrectlyFormatted.get())
			return new NextClickListener();
		else
			return null;
	}

	public float getNextAlpha() {
		return ListViewFunctions.getTileAlpha(m_isCorrectlyFormatted.get());
	}

	@Override
	public void onInitialize() {
		m_memberModels = new TrackableField<TrackableCollection<EmergencyContactItemModel>>(
				new TrackableCollection<EmergencyContactItemModel>());
		m_isCorrectlyFormatted = new TrackableBoolean(false);
		addWatch(CHANGE_ID.SELECTEDDAYCARECHILDREN);
	}

	public void dismissAllDialogs() {
		for (EmergencyContactItemModel model : m_memberModels.get())
			model.setSelected(false);
	}
}
