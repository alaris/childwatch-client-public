package com.alaris_us.daycareclient.models;

import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.fragments.CubbyScreenFragment;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class CubbyScreenViewModel extends ViewModel {
	public CubbyScreenViewModel(PersistenceLayer persistenceLayer, CubbyScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private CubbyScreenFragment							fragment;
	private TrackableField<TrackableCollection<Cubby>>	m_allCubbies;
	private TrackableField<TrackableCollection<Cubby>>	m_availableCubbies;
	private TrackableField<TrackableCollection<Cubby>>	m_familyCubbies;
	private TrackableField<Facility>					m_facility;

	@Override
	protected void onPopulateModelData() {
		m_allCubbies.get().clear();
		m_availableCubbies.get().clear();
		m_familyCubbies.get().clear();
		m_allCubbies.get().addAll(getPersistenceLayer().getPersistenceData().getAllCubbies());
		m_availableCubbies.get().addAll(getPersistenceLayer().getPersistenceData().getAvailableCubbies());
		m_familyCubbies.get().addAll(getPersistenceLayer().getPersistenceData().getFamilyCubbies());
		m_facility.set(getPersistenceLayer().getPersistenceData().getFacility());
	}

	@Override
	public void onPersistModel() {
		// Don't need to persist any data here
	}

	public class YesClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (!m_availableCubbies.get().isEmpty() && m_familyCubbies.get().isEmpty())
				m_familyCubbies.get().add(m_availableCubbies.get().remove(0));
			getPersistenceLayer().getPersistenceData().setFamilyCubbies(new ArrayList<Cubby>(m_familyCubbies.get()));
			List<Member> children = getPersistenceLayer().getPersistenceData().getSelectedDaycareChildren();
			if (!getPersistenceLayer().getPersistenceData().getMultiRoomChildren().isEmpty())
				fragment.nextScreen("ROOMS");
			else if (children.size() == 1
					&& getPersistenceLayer().getPersistenceData().getCurrentMember().getObjectId().equals(children.get(0).getObjectId()))
				fragment.nextScreen("CONFIRM");
			else
				fragment.nextScreen("WORKOUT");
			}	
	}

	public class NoClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			List<Member> children = getPersistenceLayer().getPersistenceData().getSelectedDaycareChildren();
			if (!getPersistenceLayer().getPersistenceData().getMultiRoomChildren().isEmpty())
				fragment.nextScreen("ROOMS");
			else if (children.size() == 1
					&& getPersistenceLayer().getPersistenceData().getCurrentMember().getObjectId().equals(children.get(0).getObjectId()))
				fragment.nextScreen("CONFIRM");
			else
				fragment.nextScreen("WORKOUT");
		}
	}

	@Override
	public void onInitialize() {
		m_allCubbies = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		m_availableCubbies = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		m_familyCubbies = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		m_facility = new TrackableField<Facility>();
		addWatch(CHANGE_ID.ALLCUBBIES);
		addWatch(CHANGE_ID.AVAILABLECUBBIES);
	}
}
