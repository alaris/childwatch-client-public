package com.alaris_us.daycareclient.models;

import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

import java.util.List;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.WorkoutAreaCheckBox;
import com.alaris_us.daycareclient.widgets.WorkoutAreaCheckBoxModel;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class WorkoutAreaScreenViewModel extends ViewModel {
	private State	m_currentState;

	public WorkoutAreaScreenViewModel(PersistenceLayer persistenceLayer, ScreenSwitcherFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
		reset();
	}

	private ScreenSwitcherFragment											fragment;
	private TrackableField<TrackableCollection<WorkoutAreaCheckBoxModel>>	m_areaModels;
	private TrackableField<TrackableCollection<WorkoutArea>>				m_selectedAreas;

	public TrackableCollection<WorkoutAreaCheckBoxModel> getWorkoutAreas() {
		return m_areaModels.get();
	}

	public void onReset() {
		/*
		 * m_selectedAreas.get().clear(); for (WorkoutAreaCheckBoxModel model :
		 * m_areaModels.get()) { model.setIsSelected(false); }
		 */
		m_currentState = new AwaitingSelectionState();
		m_currentState.enterState();
	}

	@Override
	protected void onPopulateModelData() {
		if (m_currentState instanceof AwaitingSelectionState) {
			List<WorkoutArea> workoutAreas = getPersistenceLayer().getPersistenceData().getAllWorkoutAreas();
			m_areaModels.get().clear();
			m_selectedAreas.get().clear();
			int width = ListViewFunctions.calculateTileWidth(workoutAreas.size(), 7.5);
			int pos = 0;
			for (WorkoutArea area : workoutAreas) {
				WorkoutAreaCheckBoxModel model = new WorkoutAreaCheckBoxModel(area, width,
						ListViewFunctions.getColorAt(pos++));
				m_areaModels.get().add(model);
			}
		}
	}

	public OnClickListener getNextClickListener() {
		if (!m_selectedAreas.get().isEmpty())
			return new NextClickListener();
		else
			return null;
	}

	public float getNextAlpha() {
		return ListViewFunctions.getTileAlpha(!m_selectedAreas.get().isEmpty());
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			Log.w("NEXTCLICK", "CLICKED");
			if (m_currentState instanceof AwaitingSelectionState) {
				Log.w("NEXTCLICK", "PASSED");
				m_currentState.exitState();
			}
		}
	}

	public class WorkoutAreaClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> parent, View view, int position,
				long id) {
			if (m_currentState instanceof AwaitingSelectionState) {
				WorkoutArea area = ((WorkoutAreaCheckBox) view).getArea();
				WorkoutAreaCheckBoxModel workoutAreaModel = ((WorkoutAreaCheckBox) view).getModel();
				workoutAreaModel.toggleIsSelected();
				if (workoutAreaModel.getIsSelected())
					m_selectedAreas.get().add(area);
				else
					m_selectedAreas.get().remove(area);
				// fragment.setCanScroll(ScrollDir.FORWARD,
				// !m_selectedAreas.get().isEmpty());
			}
		}
	}

	@Override
	public void onInitialize() {
		m_areaModels = new TrackableField<TrackableCollection<WorkoutAreaCheckBoxModel>>(
				new TrackableCollection<WorkoutAreaCheckBoxModel>());
		m_selectedAreas = new TrackableField<TrackableCollection<WorkoutArea>>(new TrackableCollection<WorkoutArea>());
		addWatch(CHANGE_ID.ALLWORKOUTAREAS);
	}

	abstract class State {
		public abstract void enterState();

		public void exitState() {
			m_currentState = null;
		}

		public void exception(Exception e) {
			m_currentState = null;
		}
	}

	private class AwaitingSelectionState extends State {
		@Override
		public void enterState() {
			Log.w("AwaitingSelectionState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("AwaitingSelectionState", "ExitState");
			List<WorkoutArea> areas = m_selectedAreas.get();
			if (!areas.isEmpty()) {
				m_currentState = new FinishedState(areas);
				m_currentState.enterState();
			} else
				exception(new Exception("No Member Selected"));
		}

		@Override
		public void exception(Exception e) {
			Log.w("AwaitingSelectionState", "Exception: " + e.getMessage());
			reset();
		}
	}

	private class FinishedState extends State {
		private List<WorkoutArea>	m_selectedAreas;

		public FinishedState(List<WorkoutArea> areas) {
			m_selectedAreas = areas;
		}

		@Override
		public void enterState() {
			Log.w("FinishedState", "EnterState");
			getPersistenceLayer().getPersistenceData().setSelectedWorkoutAreas(m_selectedAreas);
            if (getPersistenceLayer().getPersistenceData().getFacility().getUsesEstimatedTime())
                fragment.nextScreen();
            else
            	fragment.nextScreen("CONFIRM");
			exitState();
		}

		@Override
		public void exitState() {
			Log.w("FinishedState", "ExitState");
		}

		@Override
		public void exception(Exception e) {
			Log.w("FinishedState", "Exception: " + e.getMessage());
		}
	}
}
