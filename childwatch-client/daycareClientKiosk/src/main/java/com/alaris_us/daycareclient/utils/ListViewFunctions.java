package com.alaris_us.daycareclient.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.res.TypedArray;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.app.App;
import com.alaris_us.daycareclient_dev.R;

public class ListViewFunctions {
	private final static double TILEDIV = 3.5;

	public static int calculateTileHeight(int numTiles) {
		int lineHeight = (int) App.getContext().getResources().getDimension(R.dimen.default_lineheight);
		double div = (numTiles > 0 && numTiles < TILEDIV) ? numTiles : TILEDIV;
		double tileHeightMod;
		if(numTiles == 1)
			tileHeightMod = GeneralFunctions.SCREENHEIGHT * (.333) + 10;
		else
			tileHeightMod = (lineHeight * (1 - (1 / div)));
		return (int) ((GeneralFunctions.SCREENHEIGHT / div) - tileHeightMod);
	}
	
	public static int calculateTileHeight(int numTiles, double numTilesPerSCreen) {
		int lineHeight = (int) App.getContext().getResources().getDimension(R.dimen.default_lineheight);
		double div = (numTiles > 0 && numTiles < numTilesPerSCreen) ? numTiles : numTilesPerSCreen;
		double tileHeightMod;
		if(div == 1)
			tileHeightMod = GeneralFunctions.SCREENHEIGHT * (.32);
		else
			tileHeightMod = GeneralFunctions.SCREENHEIGHT * (.32) +lineHeight*(div-1);
		return (int) ((GeneralFunctions.SCREENHEIGHT - tileHeightMod) / div);
	}
	
	public static int calculateTileWidth(int numTiles) {
		int lineHeight = (int) App.getContext().getResources().getDimension(R.dimen.default_lineheight);
		double div = (numTiles > 0 && numTiles < TILEDIV) ? numTiles : TILEDIV;
		double tileHeightMod = (lineHeight * (1 - (1 / div)));
		return (int) ((GeneralFunctions.SCREENWIDTH / div) - tileHeightMod);
	}
	
	public static int calculateTileWidthForSelectMember(int numTiles) {
		int lineHeight = (int) App.getContext().getResources().getDimension(R.dimen.default_lineheight);
		double div;
		if (numTiles == 1)
			div = 2;
		else
			div = (numTiles > 0 && numTiles < TILEDIV) ? numTiles : TILEDIV;
		double tileHeightMod = (lineHeight * (1 - (1 / div)));
		return (int) ((GeneralFunctions.SCREENWIDTH / div) - tileHeightMod);
	}

	public static int calculateTileWidth(int numTiles, double numTilesPerScreen) {
		int lineHeight = (int) App.getContext().getResources().getDimension(R.dimen.default_lineheight);
		double div = (numTiles > 0 && numTiles < numTilesPerScreen) ? numTiles : numTilesPerScreen;

		double tileHeightMod = (lineHeight * (1 - (1 / div)));
		return (int) ((GeneralFunctions.SCREENWIDTH / div) - tileHeightMod);
	}

	public static List<Integer> getColors() {
		List<Integer> colorBGs = new ArrayList<Integer>();
		TypedArray itemColors = App.getContext().getResources().obtainTypedArray(R.array.listColors);
		for (int i = 0; i < itemColors.length(); i++)
			colorBGs.add(itemColors.getColor(i, -1));
		itemColors.recycle();
		return colorBGs;
	}

	public static int getColorAt(int index) {
		List<Integer> colors = getColors();
		return colors.get(index % (colors.size()));
	}

	public static void setTileHeight(ViewGroup view, int height) {
		ViewGroup.LayoutParams lp = view.getLayoutParams();
		if (lp == null)
			lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
		else
			lp.height = height;
		view.setLayoutParams(lp);
	}
	
	public static void setTileWidth(ViewGroup view, int width) {
		ViewGroup.LayoutParams lp = view.getLayoutParams();
		if (lp == null)
			lp = new ViewGroup.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT);
		else
			lp.width = width;
		view.setLayoutParams(lp);
	}

	public static float getTileAlpha(boolean isSelected) {
		return isSelected ? ImageFunctions.alphaChecked : ImageFunctions.alphaUnchecked;
	}
}
