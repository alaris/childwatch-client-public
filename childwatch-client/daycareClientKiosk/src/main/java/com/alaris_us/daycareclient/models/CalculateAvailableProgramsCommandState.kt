package com.alaris_us.daycareclient.models

import android.util.Log
import com.alaris_us.daycaredata.to.Program
import java.lang.Exception

class CalculateAvailableProgramsCommandState : State {
    private lateinit var m_availablePrograms: List<Program>
    private lateinit var m_allPrograms: List<Program>
    private lateinit var m_programSize: List<Program>
    private var m_programsIterated: Int = 0

    constructor(availablePrograms: List<Program>) : super() {

    }


    override fun enterState() {
        Log.w("CalculateAvailableProgramsCommandState", "EnterState")

    }

    override fun exitState() {
        TODO("Not yet implemented")
    }

    override fun exception(e: Exception?) {
        TODO("Not yet implemented")
    }
}