package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.EmailQueue;

public class AddEmailToEmailQueue
	extends Command<EmailQueue> {

	private EmailQueue m_emailQueue;
	
	public AddEmailToEmailQueue(EmailQueue emailQueue) {

		this(emailQueue, null, null);
	};

	public AddEmailToEmailQueue(EmailQueue emailQueue, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_emailQueue = emailQueue;
	}

	@Override
	public void executeSource() {

		m_dataSource.getEmailQueueDAO().create(m_emailQueue, this);
	}

	@Override
	public void handleResponse(EmailQueue data) {
	}

}