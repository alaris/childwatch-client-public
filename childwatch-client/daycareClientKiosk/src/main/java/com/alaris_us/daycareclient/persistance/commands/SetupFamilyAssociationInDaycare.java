package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.dao.FamilyAssociations.FamilyType;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.FamilyAssociation;
import com.alaris_us.daycaredata.to.Member;

public class SetupFamilyAssociationInDaycare extends Command<List<FamilyAssociation>> {
	private Family m_family;
	private List<Member> m_parents;
	private List<Member> m_children;

	public SetupFamilyAssociationInDaycare(Family family, List<Member> parents, List<Member> children) {
		this(family, parents, children, null, null);
	};

	public SetupFamilyAssociationInDaycare(Family family, List<Member> parents, List<Member> children, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_family = family;
		m_parents = parents;
		m_children = children;
	}

	@Override
	public void executeSource() {
		List<FamilyAssociation> familyAssociations = new ArrayList<FamilyAssociation>();
		List<Member> members = new ArrayList<Member>();
		members.addAll(m_parents);
		members.addAll(m_children);
		for (Member member : members) {
			FamilyAssociation fa = m_dataSource.getFamilyAssociationsDAO().getEmptyTO();
			fa.setPMember(member);
			fa.setPFamily(m_family);
			fa.setFamilyType(FamilyType.PRIMARY);
			familyAssociations.add(fa);
		}
		
		m_dataSource.getFamilyAssociationsDAO().createAll(familyAssociations, this);
	}

	@Override
	public void handleResponse(List<FamilyAssociation> familyAssociation) {

	}

}