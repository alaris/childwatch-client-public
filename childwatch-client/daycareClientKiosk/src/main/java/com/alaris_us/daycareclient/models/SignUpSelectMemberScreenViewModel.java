package com.alaris_us.daycareclient.models;

import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.activities.MainActivityScreen;
import com.alaris_us.daycareclient.fragments.SignUpSelectKidScreenFragment;
import com.alaris_us.daycareclient.fragments.SignUpSelectUserScreenFragment;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher.ScrollDir;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.SignUpSelectMemberItem;
import com.alaris_us.daycareclient.widgets.SignUpSelectMemberItemModel;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class SignUpSelectMemberScreenViewModel extends ViewModel {
	public SignUpSelectMemberScreenViewModel(PersistenceLayer persistenceLayer, ScreenSwitcherFragment fragment) {
		super(persistenceLayer);
		// this.fragment = fragment; userFragment
		if (fragment.getClass() == SignUpSelectUserScreenFragment.class)
			userFragment = (SignUpSelectUserScreenFragment) fragment;
		else
			kidFragment = (SignUpSelectKidScreenFragment) fragment;
		isSelectingParents = fragment.getClass() == SignUpSelectUserScreenFragment.class;
	}

	@Override
	protected void onInitialize() {
		m_memberModels = new TrackableField<TrackableCollection<SignUpSelectMemberItemModel>>(
				new TrackableCollection<SignUpSelectMemberItemModel>());
		m_selectedMembers = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		mInvalidMembers = new TrackableCollection<Member>();
		m_invalidMemberSelectedDialog = new TrackableField<AlertDialog>();
		m_staffOverrideEntryDialog = new TrackableField<AlertDialog>();
		m_successfulOverrideDialog = new TrackableField<AlertDialog>();
	}

	@Override
	protected void onPopulateModelData() {
		PersistenceData pd = getPersistenceLayer().getPersistenceData();
		List<Member> members;
		mFacility = pd.getFacility();
		mInvalidMemberSelectedOverride = pd.getIsInvalidMemberSelectedOverride();
		if (isSelectingParents) {
			members = pd.getAllYMCAParents();
		} else {
			members = pd.getAllYMCAChildren();
		}
		int width = ListViewFunctions.calculateTileWidth(members.size(), 2.75);
		int pos = 0;
		m_memberModels.get().clear();
		for (Member member : members) {
			SignUpSelectMemberItemModel model = new SignUpSelectMemberItemModel(member, width,
					ListViewFunctions.getColorAt(pos++));
			m_memberModels.get().add(model);
		}
	}

	@Override
	protected void onReset() {
		addWatch(CHANGE_ID.ALLYMCAPARENTSANDCHILDREN);
		addWatch(CHANGE_ID.FACILITY);
		addWatch(CHANGE_ID.ISINVALIDMEMBERSELECTEDOVERRIDE);
		m_selectedMembers.get().clear();
		for (SignUpSelectMemberItemModel member : m_memberModels.get()) {
			member.setIsSelected(false);
		}
	}

	private boolean																isSelectingParents	= true;
	private ScreenSwitcherFragment												fragment;
	private SignUpSelectKidScreenFragment										kidFragment;
	private SignUpSelectUserScreenFragment										userFragment;
	private TrackableField<TrackableCollection<SignUpSelectMemberItemModel>>	m_memberModels;
	private TrackableField<TrackableCollection<Member>>							m_selectedMembers;
	private TrackableCollection<Member>											mInvalidMembers;
	private TrackableField<AlertDialog>											m_invalidMemberSelectedDialog;
	private TrackableField<AlertDialog>											m_staffOverrideEntryDialog;
	private TrackableField<AlertDialog>											m_successfulOverrideDialog;
	private Facility															mFacility;
	private boolean																mInvalidMemberSelectedOverride;

	public TrackableCollection<SignUpSelectMemberItemModel> getMembers() {
		return m_memberModels.get();
	}

	public boolean hasMembersSelected() {
		return !m_selectedMembers.get().isEmpty();
	}

	public OnClickListener getNextClickListener() {
		if (hasMembersSelected())
			return new NextClickListener();
		else
			return null;
	}

	public float getNextAlpha() {
		return ListViewFunctions.getTileAlpha(hasMembersSelected());
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			// Nothing selected. Do not allow progression
			if (!hasMembersSelected())
				return;
			// Locate invalid members in family
			mInvalidMembers.clear();
			for (Member member : m_selectedMembers.get()) {
				if (!member.isActive()) {
					mInvalidMembers.add(member);
				}
			}
			if (isSelectingParents) {
				if (!mInvalidMemberSelectedOverride && !mInvalidMembers.isEmpty()) {
					showInvalidMemberSelectedDialog();
				} else {
					populateDaycareStatus();
				}
			} else {
				if (!mInvalidMemberSelectedOverride && !mInvalidMembers.isEmpty()) {
					showInvalidMemberSelectedDialog();
				} else {
					populateDaycareStatus();
				}
			}
		}
	}

	public class MemberClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			Member member = ((SignUpSelectMemberItem) arg1).getMember();
			SignUpSelectMemberItemModel model = ((SignUpSelectMemberItem) arg1).getModel();
			model.toggleIsSelected();
			if (model.getIsSelected())
				m_selectedMembers.get().add(member);
			else
				m_selectedMembers.get().remove(member);
			if (isSelectingParents)
				userFragment.setCanScroll(ScrollDir.FORWARD, !m_selectedMembers.get().isEmpty());
			else
				kidFragment.setCanScroll(ScrollDir.FORWARD, !m_selectedMembers.get().isEmpty());
		}
	}

	private void populateDaycareStatus() {
		dismissSuccessfulOverrideDialog();
		List<Member> mems = new ArrayList<Member>();
		for (SignUpSelectMemberItemModel m : m_memberModels.get()) {
			Member toMod = m.getMember();
			toMod.setUsesChildWatch(m.getIsSelected());
			toMod.setIsValidMembership(true);
			mems.add(toMod);
		}
		if (isSelectingParents) {
			getPersistenceLayer().getPersistenceData().setSelectedYMCAParents(mems);
			((MainActivityScreen) userFragment.getActivity()).nextScreen();
		} else {
			getPersistenceLayer().getPersistenceData().setSelectedYMCAChildren(mems);
			((MainActivityScreen) kidFragment.getActivity()).nextScreen();
		}
	}

	/************ InvalidMemberSelectedDialog ***********/
	public void setInvalidMemberSelectedDialog(AlertDialog alertDialog) {
		m_invalidMemberSelectedDialog.set(alertDialog);
	}

	private void showInvalidMemberSelectedDialog() {
		if (m_invalidMemberSelectedDialog.get() != null) {
			m_invalidMemberSelectedDialog.get().show();
		}
	}

	private void dismissInvalidMemberSelectedDialog() {
		if (m_invalidMemberSelectedDialog.get() != null && m_invalidMemberSelectedDialog.get().isShowing())
			m_invalidMemberSelectedDialog.get().dismiss();
	}

	public OnClickListener getInvalidMemberSelectedDialogReselectClickListener() {
		return new InvalidMemberSelectedDialogReselectClickListener();
	}

	public OnClickListener getInvalidMemberSelectedDialogHomeClickListener() {
		return new InvalidMemberSelectedDialogHomeClickListener();
	}

	public OnClickListener getInvalidMemberSelectedDialogOverrideClickListener() {
		return new InvalidMemberSelectedDialogOverrideClickListener();
	}

	public class InvalidMemberSelectedDialogReselectClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissInvalidMemberSelectedDialog();
		}
	}

	public class InvalidMemberSelectedDialogHomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissInvalidMemberSelectedDialog();
			fragment.firstScreen();
		}
	}

	public class InvalidMemberSelectedDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissInvalidMemberSelectedDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public String getInvalidBreakdownText() {
		String breakdownText = "";
		for (Member child : mInvalidMembers)
			breakdownText = breakdownText + child.getFirstName() + " " + child.getLastName() + "\n";
		return breakdownText;
	}

	/************ StaffOverrideEntryDialog ***********/
	public void setStaffOverrideEntryDialog(AlertDialog alertDialog) {
		m_staffOverrideEntryDialog.set(alertDialog);
	}

	private void showStaffOverrideEntryDialog() {
		if (m_staffOverrideEntryDialog.get() != null) {
			m_staffOverrideEntryDialog.get().show();
		}
	}

	public void dismissStaffOverrideEntryDialog() {
		if (m_staffOverrideEntryDialog != null && m_staffOverrideEntryDialog.get().isShowing())
			m_staffOverrideEntryDialog.get().dismiss();
	}

	public OnClickListener getStaffOverrideEntryDialogEnterClickListener() {
		return new StaffOverrideEntryDialogEnterClickListener();
	}

	public OnClickListener getStaffOverrideEntryDialogCancelClickListener() {
		return new StaffOverrideEntryDialogCancelClickListener();
	}

	public class StaffOverrideEntryDialogEnterClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (isSelectingParents) {
				if (mFacility.getOverridePIN().equals(userFragment.getOverridePIN())) {
					dismissStaffOverrideEntryDialog();
					showSuccessfulOverrideDialog();
				} else {
					userFragment.setOverridePIN("");
					userFragment.setOverridePINHint("Invalid PIN, Please Retry");
				}
			} else {
				if (mFacility.getOverridePIN().equals(userFragment.getOverridePIN())) { // TODO
																						// use
																						// kidFragment
																						// if
																						// needed
																						// here
					dismissStaffOverrideEntryDialog();
					showSuccessfulOverrideDialog();
				} else {
					userFragment.setOverridePIN(""); // TODO use kidFragment if
														// needed here
					userFragment.setOverridePINHint("Invalid PIN, Please Retry"); // TODO
																					// use
																					// kidFragment
																					// if
																					// needed
																					// here
				}
			}
		}
	}

	public class StaffOverrideEntryDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissStaffOverrideEntryDialog();
		}
	}

	/************ SuccessfulOverrideDialog ***********/
	public void setSuccessfulOverrideDialog(AlertDialog alertDialog) {
		m_successfulOverrideDialog.set(alertDialog);
	}

	private void showSuccessfulOverrideDialog() {
		if (m_successfulOverrideDialog.get() != null) {
			m_successfulOverrideDialog.get().show();
		}
	}

	public void dismissSuccessfulOverrideDialog() {
		if (m_successfulOverrideDialog != null && m_successfulOverrideDialog.get() != null
				&& m_successfulOverrideDialog.get().isShowing())
			m_successfulOverrideDialog.get().dismiss();
	}

	public OnClickListener getSuccessfulOverrideDialogOkClickListener() {
		return new SuccessfulOverrideDialogOkClickListener();
	}

	public class SuccessfulOverrideDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (isSelectingParents) {
				getPersistenceLayer().getPersistenceData().setIsInvalidMemberSelectedOverride(true);
				for (SignUpSelectMemberItemModel parentModel : m_memberModels.get()) {
					if (parentModel.getIsSelected()) {
						Member parent = parentModel.getMember();
						if (!parent.isActive()) {
							parent.setMembershipType("Guest");
							parent.setGuestDaysLeft(1);
						}
					}
				}
			}
			populateDaycareStatus();
		}
	}
}
