package com.alaris_us.daycareclient.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.activities.MainActivityScreen;
import com.alaris_us.daycareclient.data.DaycareCredential.FormOfCredential;
import com.alaris_us.daycareclient.fragments.ConfirmationScreenFragment;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceData.InOrOut;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.printing.MsgDialog;
import com.alaris_us.daycareclient.printing.MsgHandle;
import com.alaris_us.daycareclient.printing.TemplatePrint;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.ConfirmationItemModel;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Schedule;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class ConfirmationScreenViewModel extends ViewModel {
	public ConfirmationScreenViewModel(PersistenceLayer persistenceLayer, ConfirmationScreenFragment fragment, View summaryViewIn,
			View summaryViewOut) {
		super(persistenceLayer);
		this.mViewIn = summaryViewIn;
		this.mViewOut = summaryViewOut;
		this.fragment = fragment;

		List<Integer> labelKeys = new ArrayList<Integer>();
		labelKeys.add(fragment.getResources().getInteger(R.integer.CHILDTEMPLATE));
		labelKeys.add(fragment.getResources().getInteger(R.integer.SICKCHILDTEMPLATE));
		labelKeys.add(fragment.getResources().getInteger(R.integer.GUARDIANTEMPLATE));
		
		String printerIP = getPersistenceLayer().getPersistenceData().getPrinterIP();
		String printerModel = getPersistenceLayer().getPersistenceData().getPrinterModel();
		String printerMacAddress = getPersistenceLayer().getPersistenceData().getPrinterMacAddress();

		JSONObject labelOpts = persistenceLayer.getPersistenceData().getFacility().getLabelPrintingOpts();
		
		if ((null != printerIP && printerIP.trim().length() > 0) || (null != printerMacAddress && printerMacAddress.trim().length() > 0)) {
			mDialog = new MsgDialog(fragment.getActivity());
			mHandle = new MsgHandle(fragment.getActivity(), mDialog);
			mPrint = new TemplatePrint(fragment.getActivity(), mHandle, mDialog, printerIP, labelKeys, labelOpts, printerModel, printerMacAddress);
		}
	}

	@Override
	protected void onInitialize() {
		m_inOrOut = new TrackableField<InOrOut>();
		m_areaModels = new TrackableField<TrackableCollection<ConfirmationItemModel>>(new TrackableCollection<ConfirmationItemModel>());
		m_childModels = new TrackableField<TrackableCollection<ConfirmationItemModel>>(new TrackableCollection<ConfirmationItemModel>());
		m_selectedChildren = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		m_selectedAreas = new TrackableField<TrackableCollection<WorkoutArea>>(new TrackableCollection<WorkoutArea>());
		m_allAreas = new TrackableField<TrackableCollection<WorkoutArea>>(new TrackableCollection<WorkoutArea>());
		m_parent = new TrackableField<Member>();
		m_signInType = new TrackableField<SignInType>();
		m_facility = new TrackableField<Facility>();
		m_family = new TrackableField<Family>();
		m_familyCubbies = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		m_incidentDialog = new TrackableField<AlertDialog>();
		m_incidentsList = new TrackableField<TrackableCollection<Incident>>(new TrackableCollection<Incident>());
		m_summaryView = new TrackableField<View>();
		m_assignedRooms = new TrackableField<TrackableCollection<Room>>(new TrackableCollection<Room>());
        m_familyPagers = new TrackableCollection<Pager>();
		addWatch(CHANGE_ID.INOROUT);
	}

	@Override
	protected void onReset() {
		addWatch(CHANGE_ID.SELECTEDWORKOUTAREAS);
		addWatch(CHANGE_ID.ALLWORKOUTAREAS);
		addWatch(CHANGE_ID.SELECTEDDAYCARECHILDREN);
		addWatch(CHANGE_ID.INOROUT);
		addWatch(CHANGE_ID.FACILITY);
		addWatch(CHANGE_ID.CURRENTMEMBER);
		addWatch(CHANGE_ID.SIGNINTYPE);
		addWatch(CHANGE_ID.FAMILYCUBBIES);
		addWatch(CHANGE_ID.FAMILY);
		addWatch(CHANGE_ID.INCIDENTSLIST);
	}

	@Override
	protected void onPersistModel() {
		if (getInOrOut().equals(InOrOut.IN)) {
			if (getPersistenceLayer().getPersistenceData().getFacility().getUsesEstimatedTime() && !getPersistenceLayer().getPersistenceData().getLoginResult().equals(AuthenticationScreenViewModel.LoginResult.CHILDUSER)) {
				DateTimeFormatter dtf = new DateTimeFormatterBuilder().appendPattern("h:mm a").toFormatter();
				String formattedTime = getPersistenceLayer().getPersistenceData().getReturnTime().toString(dtf);
				m_returnTime = formattedTime;
			} else {
				m_returnTime = calculateReturnTime();
			}

			printLabels();

			SignInType signInType = null;
			if (m_parent.get().getMembershipType() != null && m_parent.get().getMembershipType().equals("STAFF")) {
				signInType = SignInType.STAFF;
			} else {
				FormOfCredential form = getPersistenceLayer().getPersistenceData().getCredential().getForm();
				if (form == FormOfCredential.BARCODE)
					signInType = SignInType.BARCODE;
				else if (form == FormOfCredential.PHONE)
					signInType = SignInType.PINPAD;
				else if (form == FormOfCredential.ON_REGISTER)
					signInType = SignInType.ON_REGISTER;
				if (getPersistenceLayer().getPersistenceData().getIsOverride())
					signInType = SignInType.OVERRIDE;
				getPersistenceLayer().checkInFamily(m_facility.get(), m_family.get(), m_selectedChildren.get(), m_parent.get(), signInType,
						m_familyCubbies.get(), m_familyPagers, m_assignedRooms.get(), m_selectedAreas.get(), m_allAreas.get(), m_returnTime);

				if (getPersistenceLayer().getPersistenceData().getAllowedSelfCheckInMembers() != null)
					for (Member selfCheckIn : getPersistenceLayer().getPersistenceData().getAllowedSelfCheckInMembers()) {
						getPersistenceLayer().createSelfCheckIn(selfCheckIn);
					}
			}
			((MainActivityScreen) fragment.getActivity()).nextScreen();
		} else {
			getPersistenceLayer().fetchAllOffers(m_facility.get(), new FetchAllOffersCommandListener());
		}
	}

	private void printLabels() {

		if (null == mPrint)
			return;

		JSONObject printingOpts = new JSONObject();
		printingOpts = m_facility.get().getLabelPrintingOpts();
		// Print labels
		mPrint.clearPrintData();
		Member guardian = m_parent.get();
		String guardianName = "";
		try {
			if (printingOpts != null && printingOpts.getBoolean("usesFirstNameLastInitial"))
				guardianName = guardian.getFirstName() + " " + guardian.getLastName().substring(0, 1) + ".";
			else
				guardianName = guardian.getFirstName() + " " + guardian.getLastName();
		} catch (JSONException e2) {
			e2.printStackTrace();
		}
		List<String> guardianAreas = new ArrayList<String>();
		List<String> childNames = new ArrayList<String>();
		List<String> childAges = new ArrayList<String>();
		String guardianPhone = " ";
		String familyCubbiesString = " ";
		for (int i = 0; i < m_familyCubbies.get().size(); i++) {
			familyCubbiesString = familyCubbiesString + m_familyCubbies.get().get(i).getName() + ", ";
			if (i == m_familyCubbies.get().size() - 1)
				familyCubbiesString = familyCubbiesString.substring(0, familyCubbiesString.length() - 2);
		}
		if (guardian.getPhones() != null) {
			JSONArray guardianPhones = guardian.getPhones();
			for (int i = 0; i < guardianPhones.length(); i++) {
				try {
					JSONObject phone = guardianPhones.getJSONObject(i);
					if (phone.get("type").toString().contains("SMS") || phone.get("type").toString().contains("CELL")) {
						guardianPhone = phone.getString("number");
						break;
					}
				} catch (JSONException e) {
					e.printStackTrace();
					guardianPhone = " ";
				}
			}
		} else
			guardianPhone = " ";
		String guardianBarcode = "";
		if (guardian.getBarcode() == null || guardian.getBarcode().isEmpty())
			guardianBarcode = guardian.getObjectId();
		else
			guardianBarcode = guardian.getBarcode();

		for (WorkoutArea w : m_selectedAreas.get()) {
			guardianAreas.add(w.getWorkoutName());
		}

		String program = getPersistenceLayer().getPersistenceData().getSelectedProgram().getName();

		try {
			if (printingOpts != null && printingOpts.getBoolean("usesGuardianLabels")) {
				for (Member m : m_selectedChildren.get()) {
					String childName = m.getFirstName() + " " + m.getLastName();
					childNames.add(childName);
					childAges.add(getAge(m));
				}
				mPrint.addGuardianPrint(childNames, childAges, guardianName, new Date(), guardianBarcode, m_returnTime, familyCubbiesString);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String childName = "";
		try {
			if (printingOpts != null && printingOpts.getBoolean("usesChildLabels")) {
				for (Member m : m_selectedChildren.get()) {
					if (printingOpts != null && printingOpts.getBoolean("usesFirstNameLastInitial"))
						childName = m.getFirstName() + " " + m.getLastName().substring(0, 1) + ".";
					else
						childName = m.getFirstName() + " " + m.getLastName();
					String diaper = " ";
					String feeding = " ";
					String potty = " ";
					if (m.getAllowsDiaperChange() != null)
						if (m.getAllowsDiaperChange())
							diaper = "D";
					if (m.getAllowsFeeding() != null)
						if (m.getAllowsFeeding())
							feeding = "F";
					if (m.getIsPottyTrained())
						potty = "P";

					boolean ableToPrint = true;
					try {
						JSONArray noPrintRooms = printingOpts.getJSONArray("noPrintRooms");
						if (noPrintRooms.length() != 0) {
							for (int i = 0; i < noPrintRooms.length(); i++) {
								if (m.getPRoom().getObjectId().equals(noPrintRooms.get(i)))
									ableToPrint = false;
							}
						}
						if (ableToPrint)
							mPrint.addChildPrint(childName, m.getBirthDate(), guardianBarcode, guardianName, guardianAreas, guardianPhone,
									m.getMedicalConcerns(), new Date(), m_returnTime, familyCubbiesString, diaper, feeding, potty, program);
					} catch (JSONException e) {
						mPrint.addChildPrint(childName, m.getBirthDate(), guardianBarcode, guardianName, guardianAreas, guardianPhone,
								m.getMedicalConcerns(), new Date(), m_returnTime, familyCubbiesString, diaper, feeding, potty, program);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// call function to print
		mPrint.print();

	}

	@Override
	protected void onPopulateModelData() {
		PersistenceData data = getPersistenceLayer().getPersistenceData();
		if (data.getInOrOut() == null || data.getCurrentMember() == null || data.getSelectedDaycareChildren() == null
				|| data.getSelectedDaycareChildren().isEmpty())
			return;
		m_inOrOut.set(data.getInOrOut());
		m_family.set(data.getPrimaryFamily());
		m_parent.set(data.getCurrentMember());
		m_signInType.set(data.getSignInType());
		m_facility.set(data.getFacility());
		m_selectedChildren.set(new TrackableCollection<Member>(data.getSelectedDaycareChildren()));
		m_selectedAreas.set(new TrackableCollection<WorkoutArea>(data.getSelectedWorkoutAreas()));
		m_allAreas.set(new TrackableCollection<WorkoutArea>(data.getAllWorkoutAreas()));
		m_familyCubbies.set(new TrackableCollection<Cubby>(data.getFamilyCubbies()));
		m_incidentsList.set(new TrackableCollection<Incident>(data.getIncidentsList()));
		m_assignedRooms.set(new TrackableCollection<Room>(data.getAssignedRooms()));
        m_familyPagers.addAll(data.getFamilyPagers());
		m_areaModels.get().clear();
		m_childModels.get().clear();
		setSummaryViews(mViewIn, mViewOut);
		int heightarea = ListViewFunctions.calculateTileHeight(m_selectedAreas.get().size(), 2.5);
		int pos = 0, widthchild = 0, heightchild = 0;
		if (m_inOrOut.get().equals(InOrOut.IN)
				&& data.getCurrentMember().getObjectId().equals(data.getSelectedDaycareChildren().get(0).getObjectId()))
			widthchild = ListViewFunctions.calculateTileWidth(m_selectedChildren.get().size());
		else
			heightchild = ListViewFunctions.calculateTileHeight(m_selectedChildren.get().size(), 2.5);
		for (Member member : m_selectedChildren.get()) {
			m_childModels.get().add(new ConfirmationItemModel(member, widthchild, heightchild, ListViewFunctions.getColorAt(pos++)));
		}
		pos = 10;
		for (WorkoutArea area : m_selectedAreas.get()) {
			m_areaModels.get().add(new ConfirmationItemModel(area, 0, heightarea, ListViewFunctions.getColorAt(pos--)));
		}
	}

	public String getAge(Member child) {

		LocalDate bDate = LocalDate.fromDateFields(child.getBirthDate());
		Period age = new Period(bDate, LocalDate.now());

		int difference = age.getYears();
		String unit = YEAR_UNIT;

		if (difference < 1) {
			difference = age.getMonths();
			unit = MONTH_UNIT;
		}

		return String.format(Locale.getDefault(), "%d%s", difference, unit);
	}

	public String calculateReturnTime() {
		String timeText = "";
		updateChildrenTimes();

		if (m_inOrOut.get().equals(InOrOut.IN)) {
			DateTime returnTime = null;
			int minTimeLeft = Integer.MAX_VALUE;

			for (Member child : m_selectedChildren.get()) {
				int timeLeft = child.getMinutesLeft().intValue();
				if (timeLeft < minTimeLeft)
					minTimeLeft = timeLeft;

				/*
				 * DateTime closeTime =
				 * DaycareHelpers.getScheduleClosingTime(child.getPRoom().
				 * getPShedule()); if (returnTime == null ||
				 * closeTime.isBefore(returnTime)) returnTime = closeTime;
				 */
			}
			HashMap<Program, Schedule> ps = getPersistenceLayer().getPersistenceData().getAvailableProgramSchedules();
			Iterator it = ps.entrySet().iterator();
			Schedule schedule = null;
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (pair.getKey().equals(getPersistenceLayer().getPersistenceData().getSelectedProgram()))
					schedule = (Schedule) pair.getValue();
			}
			DateTime closeTime = DaycareHelpers.getProgramScheduleClosingTime(schedule);
			if (returnTime == null || closeTime.isBefore(returnTime))
				returnTime = closeTime;

			DateTime time = (new DateTime()).plusMinutes(minTimeLeft);
			if (returnTime == null || time.isBefore(returnTime))
				returnTime = time;

			DateTimeFormatter formatter = DateTimeFormat.forPattern("h:mm a");
			timeText = formatter.print(returnTime);

			getPersistenceLayer().getPersistenceData().setReturnTime(returnTime);
		}
		return timeText;
	}

	public void updateChildrenTimes() {
		String currentDate = DateGenerator.getFormattedDate(DateGenerator.getCurrentDate());
		Number facilityTimeLimit = m_facility.get().getTimeLimit();
		Number facilityWeeklyTimeLimit = m_facility.get().getWeeklyTimeLimit();
		for (Member child : m_selectedChildren.get()) {
			if (child.getInProgram() != null) {
				Number programTimeLimit = child.getInProgram().getTimeLimit().intValue();
				if (programTimeLimit.intValue() == 0) {
					Number roomTimeLimit = child.getPRoom().getTimeLimit();
					child = updateChildAtCheckIn(child, roomTimeLimit, facilityWeeklyTimeLimit, currentDate);
				} else
					child = updateChildAtCheckIn(child, programTimeLimit, facilityWeeklyTimeLimit, currentDate);
			} else {
				child = updateChildAtCheckIn(child, facilityTimeLimit, facilityWeeklyTimeLimit, currentDate);
			}
		}
	}

	public Member updateChildAtCheckIn(Member child, Number timeLimit, Number weeklyTimeLimit, String currentDate) {
		DateTime lastSunday = DaycareHelpers.getLastSunday();
		int minutesLeft = timeLimit.intValue();
		if (getPersistenceLayer().getPersistenceData().getIsOverride()) {
			child.setMinutesLeft(minutesLeft);
		} else if (null == child.getLastCheckIn()) {
			child.setMinutesLeft(minutesLeft);
		} else if (DateGenerator.getFormattedDate(child.getLastCheckIn()).compareTo(currentDate) != 0) {
			if (child.isGuest())
				if (child.getGuestDaysLeft().intValue() > 0)
					child.setGuestDaysLeft(child.getGuestDaysLeft().intValue() - 1);
				else
					minutesLeft = 0;
			DateTime lastCheckInDate = new DateTime(child.getLastCheckIn());
			if (lastCheckInDate.isBefore(lastSunday))
				child.setWeeklyMinutesLeft(weeklyTimeLimit);
			child.setMinutesLeft(minutesLeft);
		} else if (DateGenerator.getFormattedDate(child.getLastCheckIn()).compareTo(currentDate) == 0 && !m_facility.get().getUsesTimeTracking()) {
			child.setMinutesLeft(minutesLeft);
		}
		return child;
	}

	// Fields used for UI
	// Dependencies
	private final ConfirmationScreenFragment fragment;
	private final View mViewIn;
	private final View mViewOut;
	private MsgDialog mDialog;
	private MsgHandle mHandle;
	private TemplatePrint mPrint;
	// Persistent read
	// Persistent modify
	// Persistent write
	// System state
	// Fields

	private TrackableField<TrackableCollection<ConfirmationItemModel>> m_areaModels;
	private TrackableField<TrackableCollection<ConfirmationItemModel>> m_childModels;
	private TrackableField<TrackableCollection<Member>> m_selectedChildren;
	private TrackableField<TrackableCollection<WorkoutArea>> m_selectedAreas;
	private TrackableField<TrackableCollection<WorkoutArea>> m_allAreas;
	private TrackableField<Member> m_parent;
	private TrackableField<SignInType> m_signInType;
	private TrackableField<Facility> m_facility;
	private TrackableField<InOrOut> m_inOrOut;
	private TrackableField<TrackableCollection<Cubby>> m_familyCubbies;
	private TrackableField<Family> m_family;
	private TrackableField<AlertDialog> m_incidentDialog;
	private TrackableField<TrackableCollection<Incident>> m_incidentsList;
	private TrackableField<TrackableCollection<Room>> m_assignedRooms;
	private TrackableField<View> m_summaryView;
	private final int checkInMessageRes = R.string.confirmationscreen_tile_summary_message_children_in,
			checkOutMessageRes = R.string.confirmationscreen_tile_summary_message_children_out;
	private final int checkInMessageSizeRes = R.dimen.confirmationscreen_tile_message_children_in_textsize,
			checkOutMessageSizeRes = R.dimen.confirmationscreen_tile_message_children_out_textsize;
	private final static String YEAR_UNIT = "yr";
	private final static String MONTH_UNIT = "mo";
	private String m_returnTime = " ";
	private TrackableCollection<Pager> m_familyPagers;

	public TrackableCollection<ConfirmationItemModel> getChildren() {
		return m_childModels.get();
	}

	public TrackableCollection<ConfirmationItemModel> getAreas() {
		return m_areaModels.get();
	}

	public View getTile() {
		return m_summaryView.get();
	}

	public void setSummaryViews(View in, View out) {
		if (m_inOrOut.get().equals(InOrOut.IN)) {
			if (m_parent.get().getMembershipType() == "STAFF") {

			} else if (!m_parent.get().getObjectId().equals(m_selectedChildren.get().get(0).getObjectId())) {
				m_summaryView.set(in);
			} else
				m_summaryView.set(out);
		} else {
			m_summaryView.set(out);
		}
	}

	public InOrOut getInOrOut() {
		return m_inOrOut.get();
	}

	public int getInOrOutMessage() {
		return getInOrOut().equals(InOrOut.IN) ? checkInMessageRes : checkOutMessageRes;
	}

	public int getInOrOutMessageTextSize() {
		return getInOrOut().equals(InOrOut.IN) ? checkInMessageSizeRes : checkOutMessageSizeRes;
	}

	public class ConfirmClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			persistModel();
			// ((MainActivityScreen) fragment.getActivity()).nextScreen();
		}
	}

	public OnClickListener getIncidentDialogOkClickListener() {
		return new IncidentDialogOkClickListener();
	}

	public class IncidentDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissIncidentDialog();
			getPersistenceLayer().updateIncidents(m_incidentsList.get(), true);
			/*
			 * for (Member child : m_selectedChildren.get())
			 * child.setPIncident(null);
			 */
			getPersistenceLayer().checkOutFamily(m_facility.get(), m_family.get(), m_selectedChildren.get(), m_parent.get(), m_signInType.get(),
					m_familyCubbies.get(), m_allAreas.get(), m_assignedRooms.get());
			if (getPersistenceLayer().getPersistenceData().getOffersList().size() > 0)
				((MainActivityScreen) fragment.getActivity()).nextScreen("OFFER");
			else
				((MainActivityScreen) fragment.getActivity()).nextScreen();
		}
	}

	public void setIncidentDialog(AlertDialog alertDialog) {
		m_incidentDialog.set(alertDialog);
	}

	private void showIncidentDialog() {
		if (m_incidentDialog.get() != null) {
			m_incidentDialog.get().show();
		}
	}

	private void dismissIncidentDialog() {
		if (m_incidentDialog.get() != null && m_incidentDialog.get().isShowing())
			m_incidentDialog.get().dismiss();
	}

	private class FetchIncidentsCommandListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void finished(Command<?> command) {
			List<Incident> incidents = (List<Incident>) command.getResult();
			SignInType signInType = null;
			if (!incidents.isEmpty())
				showIncidentDialog();
			else {
				FormOfCredential form = getPersistenceLayer().getPersistenceData().getCredential().getForm();
				if (form == FormOfCredential.BARCODE)
					signInType = SignInType.BARCODE;
				else if (form == FormOfCredential.PHONE)
					signInType = SignInType.PINPAD;
				if (getPersistenceLayer().getPersistenceData().getIsOverride())
					signInType = SignInType.OVERRIDE;
				getPersistenceLayer().checkOutFamily(m_facility.get(), m_family.get(), m_selectedChildren.get(), m_parent.get(), signInType,
						m_familyCubbies.get(), m_allAreas.get(), m_assignedRooms.get());
				if (getPersistenceLayer().getPersistenceData().getOffersList().size() > 0)
					((MainActivityScreen) fragment.getActivity()).nextScreen("OFFER");
				else
					((MainActivityScreen) fragment.getActivity()).nextScreen();
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class FetchAllOffersCommandListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			getPersistenceLayer().findIncidents(m_selectedChildren.get(), new FetchIncidentsCommandListener());
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
}
