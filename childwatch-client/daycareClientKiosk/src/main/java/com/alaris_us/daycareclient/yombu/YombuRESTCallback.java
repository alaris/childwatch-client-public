package com.alaris_us.daycareclient.yombu;

import org.json.JSONObject;

public interface YombuRESTCallback {

    public void success(JSONObject result);

    public void failure(YombuRESTException error);

}