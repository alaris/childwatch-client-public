package com.alaris_us.daycareclient.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.models.CheckOutScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontButton;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient.widgets.MemberCheckBox;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

public class CheckOutScreenFragment extends ScreenSwitcherFragment {
	CheckOutScreenViewModel m_model;
	private View m_emergencyDialogView;
	private final int skipTextLabelRes = R.string.view_emergencydialog_skip_button_text;
	private final int cancelTextLabelRes = R.string.view_emergencydialog_cancel_button_text;
	private final int emergencyDialogWidthRes = R.dimen.view_emergencydialog_width;
	private final int emergencyDialogHeightRes = R.dimen.view_emergencydialog_height;
	private String skipTextLabel;
	private int emergencyDialogWidth;
	private int emergencyDialogHeight;
	private FontButton skipButton;
	private View m_noKidsForCheckDirectionDialogView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.checkoutscreen_layout, container,
				false);
		m_emergencyDialogView = inflater.inflate(
				R.layout.view_emergencydialog_layout, container, false);
		m_noKidsForCheckDirectionDialogView = inflater.inflate(
				R.layout.view_nokidsforcheckdirectiondialog_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		HorizontalListView childList = (HorizontalListView) m_layout
				.findViewById(R.id.checkoutscreen_tile_childlist);
		m_model = new CheckOutScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		skipTextLabel = getResources().getString(skipTextLabelRes);
		emergencyDialogWidth = (int) getResources().getDimension(
				emergencyDialogWidthRes);
		emergencyDialogHeight = (int) getResources().getDimension(
				emergencyDialogHeightRes);
		skipButton = (FontButton) m_emergencyDialogView
				.findViewById(R.id.view_emergencydialog_skip_button);
		m_layout.findViewById(R.id.view_navigation_withbuttons_next_container)
				.setId(R.id.checkoutscreen_nav_next);
		m_layout.findViewById(R.id.view_navigation_withbuttons_back_container)
				.setId(R.id.checkoutscreen_nav_back);
		m_layout.findViewById(
				R.id.view_navigation_withbuttons_selectall_container).setId(
				R.id.checkoutscreen_nav_selectall);
		m_layout.findViewById(
				R.id.view_navigation_withbuttons_showproxy_container).setId(
				R.id.checkoutscreen_nav_showproxy);
		UiBinder.bind(this.m_layout, R.id.checkoutscreen_tile_childlist,
				"Adapter", m_model, "VisibleChildren", BindingMode.TWO_WAY,
				new AdapterConverter(MemberCheckBox.class, true, true));
		UiBinder.bind(m_layout, R.id.checkoutscreen_nav_next,
				"OnClickListener", m_model, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.checkoutscreen_nav_next, "Alpha", m_model,
				"NextAlpha", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout,
				R.id.view_navigation_withbuttons_showproxy_textbox, "TextRes",
				m_model, "ProxyButtonText", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout,
				R.id.view_navigation_withbuttons_selectall_textbox, "TextRes",
				m_model, "SelectAllButtonText", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.checkoutscreen_nav_showproxy,
				"Visibility", m_model, "ProxyButtonVisibility",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.checkoutscreen_nav_selectall,
				"LayoutParams", m_model, "SelectAllLayoutParams",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout,
				R.id.view_navigation_withbuttons_selectall_textbox,
				"TextSizeRes", m_model, "SelectAllButtonTextSize",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.checkoutscreen_tile_childlist, "Position",
				m_model, "ScrollPosition", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "SkipButtonText"), m_model,
				"NumOfSkips", BindingMode.ONE_WAY);
		UiBinder.bind(m_emergencyDialogView,
				R.id.view_emergencydialog_yes_button, "OnClickListener",
				m_model, "EmergencyDialogYesClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_emergencyDialogView,
				R.id.view_emergencydialog_skip_button, "OnClickListener",
				m_model, "EmergencyDialogSkipClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "EmergencyDialog"), m_model,
				"EmergencyDialog", BindingMode.ONE_WAY_TO_SOURCE);
		childList.setOnItemClickListener(m_model.new ChildClickListener());
		m_layout.findViewById(R.id.checkoutscreen_nav_showproxy)
				.setOnClickListener(m_model.new ShowProxyClickListener());
		m_layout.findViewById(R.id.checkoutscreen_nav_selectall)
				.setOnClickListener(m_model.new SelectAllClickListener());
		
		// Navigation Button(s)
		m_layout.findViewById(R.id.checkoutscreen_nav_back).setOnClickListener(
				new BackClickListener());
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		m_model.disconnect();
	}

	@Override
	public void onResumeFragment() {
		m_model = (null == m_model) ? new CheckOutScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this) : m_model;
		m_model.reset();
		super.onResumeFragment();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		m_model.disconnect();
		m_model = null;
	}

	public void setSkipButtonText(int skips) {
		if (skips <= 0)
			skipButton.setTextRes(cancelTextLabelRes);
		else
			skipButton.setText(skipTextLabel + " (" + skips + " Left)");
	}

	public AlertDialog getEmergencyDialog() {
		FrameLayout emergencyDialogContainer = new FrameLayout(getActivity());
		emergencyDialogContainer.addView(m_emergencyDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(emergencyDialogContainer);
		AlertDialog emergencyDialog = builder.create();
		emergencyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		emergencyDialog.setCancelable(false);
		emergencyDialog.show();
		emergencyDialog.dismiss();
		ViewGroup.LayoutParams lp = m_emergencyDialogView.getLayoutParams();
		lp.width = emergencyDialogWidth;
		lp.height = emergencyDialogHeight;
		m_emergencyDialogView.setLayoutParams(lp);
		((View) m_emergencyDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return emergencyDialog;
	}

	@Override
	public void initData() {
		m_model.reset();
	}

	public class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}
	
	public AlertDialog getNoKidsForCheckDirectionDialog() {
		FrameLayout noKidsForCheckDirectionDialogContainer = new FrameLayout(getActivity());
		noKidsForCheckDirectionDialogContainer.addView(m_noKidsForCheckDirectionDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noKidsForCheckDirectionDialogContainer);
		AlertDialog noKidsForCheckDirectionDialog = builder.create();
		noKidsForCheckDirectionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		noKidsForCheckDirectionDialog.setCancelable(false);
		noKidsForCheckDirectionDialog.show();
		noKidsForCheckDirectionDialog.dismiss();
		ViewGroup.LayoutParams lp = m_noKidsForCheckDirectionDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nokidsforcheckdirectiondialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nokidsforcheckdirectiondialog_height);
		m_noKidsForCheckDirectionDialogView.setLayoutParams(lp);
		((View) m_noKidsForCheckDirectionDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		String dir = "Out";
		String otherDir = "In";
		String title = getResources().getString(R.string.view_nokidsforcheckdirectiondialog_title_text).replace("[DIR]", dir);
		String msg = getResources().getString(R.string.view_nokidsforcheckdirectiondialog_message_text).replace("[DIR]", otherDir);
		((TextView) m_noKidsForCheckDirectionDialogView.findViewById(R.id.view_nokidsforcheckdirectiondialog_title)).setText(title);
		((TextView) m_noKidsForCheckDirectionDialogView.findViewById(R.id.view_nokidsforcheckdirectiondialog_message)).setText(msg);
		return noKidsForCheckDirectionDialog;
	}
}
