package com.alaris_us.daycareclient.services;

import android.util.Log;

import com.alaris_us.daycareclient.fragments.MainScreenFragment;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Service_Capacity implements Runnable {
    ScheduledThreadPoolExecutor scheduledCapacityService;
    PersistenceLayer persistenceLayer;
    String ratioType;
    List<Room> rooms;
    List<String> capacityLines;
    Facility fac;
    StringBuilder fullCapacityText;

    private MainScreenFragment mainscreen;

    /**
     *
     * @param persistenceLayer
     */
    public Service_Capacity(PersistenceLayer persistenceLayer) {
        this.persistenceLayer = persistenceLayer;
        this.fullCapacityText = new StringBuilder();
        this.capacityLines = new ArrayList();
        mainscreen = null;
    }

    /**
     * This method can be called to make MainScreenFragment visible to Service_Capacity.
     * @param fragment
     */
    public void bindFragment(MainScreenFragment fragment) {
        this.mainscreen = fragment;
    }

    public void run() {
        do {
            try {
                refreshCapacityInfo();
                Thread.sleep(5000);
            } catch (InterruptedException ie) {
                Log.e("Capacity_Service", "Error sleeping Capacity_Service thread");
            }
        } while (true);
    }


    public void refreshCapacityInfo() {
        try {
            fetchStaffMemberAndRooms();
        } catch(Exception e) {
            Log.e("Service_Capacity", "Exception occurred while updating the capacity info.");
            Log.d("Service_Capacity", e.getStackTrace().toString());
        }
    }

    /**
     * Returns capacity info, formatted for display, using the most recently fetched
     * capacity data.  Call to this method DOES NOT cause a fetch of data to occur.
     */
    public String getCapacityInfoText() {
        double adjustedInCenter = 0;
        double openSpots;
        double calculatedMax = 0;

        List<Member> checkedInMembers = persistenceLayer.getPersistenceData().getCheckedInMembers();
        List<Staff> clockedInStaff = persistenceLayer.getPersistenceData().getClockedInStaff();

        capacityLines = new ArrayList();
        String templine;

        try {
            if (fac.getRatioType().equalsIgnoreCase("FACILITYANDROOM")) {

                if (clockedInStaff != null) {
                    calculatedMax = clockedInStaff.size() * fac.getRatio().intValue();
                }
                for (Member m : checkedInMembers) {
                    if(m.getPRoom() != null)
                        adjustedInCenter += m.getPRoom().getCapacityMultiplier().doubleValue();
                }
                for (Room r : rooms) {
                    openSpots = calculatedMax - adjustedInCenter;
                    if (r.getCapacityMultiplier() != null && r.getCapacityMultiplier().doubleValue() > 0) {
                        openSpots = (openSpots / r.getCapacityMultiplier().doubleValue());
                    }

                    templine = r.getName() + ": " + (int) Math.floor(openSpots) + " open spots";
                    capacityLines.add(templine);
                }
            } else if (fac.getRatioType().equalsIgnoreCase("ROOM")) {
                for (Room r : rooms) {
                    adjustedInCenter = 0;
                    if (checkedInMembers != null) {
                        for (Member m : checkedInMembers) {
                            if (r != null && m.getPRoom() != null && m.getPRoom().equals(r))
                                adjustedInCenter++;
                        }
                    }
                    openSpots = r.getRatio().intValue() - adjustedInCenter;
                    templine = r.getName() + ": " + (int) Math.floor(openSpots) + " open spots";
                    capacityLines.add(templine);
                }
            }
            return getFormattedCapacityText(capacityLines);

        } catch(Exception e) {
            Log.e("CapacityInfo", "There was an error computing room multiplier.");
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Refreshes and sets the clocked in staff, checked in members, and rooms in persistence data.
     * Once done, sets the capacity info in persistence data
     */
    private void fetchStaffMemberAndRooms() {
        Log.d("Capacity_Service", "FETCH BEGIN");
        persistenceLayer.fetchClockedInStaff(new Command.CommandStatusListener() {
            @Override
            public void starting(Command<?> command) {
            }

            @Override
            public void finished(Command<?> command) {
                persistenceLayer.fetchCheckedInMembers(new Command.CommandStatusListener() {
                    @Override
                    public void starting(Command<?> command) {
                    }

                    @Override
                    public void finished(Command<?> command) {
                        persistenceLayer.fetchAllRooms(new Command.CommandStatusListener() {
                            @Override
                            public void starting(Command<?> command) {
                            }

                            @Override
                            public void finished(Command<?> command) {
                                Log.d("Capacity_Service", "FETCH END");

                                if(mainscreen != null) {
                                    persistenceLayer.getPersistenceData().setCapacityInfo(getCapacityInfoText());
                                }

                            }

                            @Override
                            public void valueChanged(Command<?> command) {
                            }

                            @Override
                            public void exception(Command<?> command, Exception e) {
                            }
                        });
                    }

                    @Override
                    public void valueChanged(Command<?> command) {
                    }

                    @Override
                    public void exception(Command<?> command, Exception e) {
                    }
                });
            }

            @Override
            public void valueChanged(Command<?> command) {
            }

            @Override
            public void exception(Command<?> command, Exception e) {
            }
        });
    }

    /**
     * Builds a single string representing all Rooms capacities
     * without refreshing the member and staff data.
     *
     * @return String formatted to display capacity text.
     */
    private static String getFormattedCapacityText(List<String> lineList) {
        StringBuilder fullCapacityText = new StringBuilder();
        try {
            for (String line : lineList) {
                fullCapacityText.append(line + "\n\n");
            }
        } catch(Exception e) {
            Log.e("Service_Capacity", "Exception encountered formatting capacity text. + " + e.getMessage());
        }
        return fullCapacityText.toString();
    }

    /**
     * Starts this service with a periodic delay (hardcoded currently).  Fetches staff and member
     * data from Parse and builds string for Widget.
     *
     */
    public void startService() {
        try {
            this.fac = persistenceLayer.getPersistenceData().getFacility();
            this.ratioType = persistenceLayer.getPersistenceData().getFacility().getRatioType();
            this.rooms = persistenceLayer.getPersistenceData().getAllRooms();
            getCapacityInfoText();
            scheduledCapacityService = new ScheduledThreadPoolExecutor(1);
            scheduledCapacityService.scheduleWithFixedDelay(
                    Service_Capacity.this, 1, 5, TimeUnit.SECONDS); //Not sure why initialDelay is needed to keep "Fetching Data" popup to appear
        } catch(Exception e) {
            Log.e("Capacity_Service", "Error attempting to start capacity service.");
        }
    }

    public void shutdown() {
        scheduledCapacityService.shutdown();
    }

}
