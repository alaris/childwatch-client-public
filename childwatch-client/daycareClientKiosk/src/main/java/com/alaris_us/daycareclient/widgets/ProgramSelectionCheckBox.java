package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class ProgramSelectionCheckBox extends RelativeLayout implements BoundUi<ProgramSelectionCheckBoxModel>{

	private ProgramSelectionCheckBoxModel m_data;
	
	private ImageView m_checkBox;
	private ImageView m_programImage;
	private TextView m_programText;
	
	public ProgramSelectionCheckBox(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public ProgramSelectionCheckBox(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		View.inflate(getContext(), R.layout.view_programlistitem_layout, this);
		
		m_programImage = (ImageView) findViewById(R.id.view_programlistitem_image);
		m_programText = (TextView) findViewById(R.id.view_programlistitem_name);
		m_checkBox = (ImageView) findViewById(R.id.view_programlistitem_checkmark);
	}
	
	public ProgramSelectionCheckBoxModel getModel() {
		return m_data; 
	}

	public void setHeight(int height){
		ListViewFunctions.setTileHeight(this, height);
	}
	
	public void setWidth(int width){
		ListViewFunctions.setTileWidth(this, width);
	}
	
	@Override
	public void bind(ProgramSelectionCheckBoxModel dataSource) {
		m_data = dataSource;
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_programText, "Text")), m_data, "ProgramText", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_programImage, "BackgroundColor")), m_data, "BackgroundColor", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Width")), m_data, "Width", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_programImage, "Alpha")), m_data, "Alpha", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_checkBox, "Visibility")), m_data, "CheckBoxRes", BindingMode.ONE_WAY);
	}
}
