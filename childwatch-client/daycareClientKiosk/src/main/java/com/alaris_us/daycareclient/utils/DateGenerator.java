package com.alaris_us.daycareclient.utils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateGenerator {
	public final static int CHILDSEPARATOR = -16; // TODO make sure -13
	public final static int CHILDSELFCHECKINSEPARATOR = -8;
	
	public final static SimpleDateFormat DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	
    @SuppressWarnings("deprecation")
	public static Date getRandomDate() {

        Date date = new Date();
        date.setYear(randBetween(1900, 2010));
        date.setDate(randBetween(1, 28));
        date.setMonth(randBetween(1,12));

        return date;
    }

    public static long randBetween(long start, long end) {
        return start + Math.round(Math.random() * (end - start));
    }
 
    public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }
    
    public static Date getDefaultDate(){
    	Calendar cal = Calendar.getInstance();
		cal.set(2000, Calendar.JANUARY, 1);
		return cal.getTime();
    }
    
    public static Date getDateAtOffset(int years){
    	Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, years); 
		return cal.getTime();
    }
    
    public static Date getCurrentDate(){
    	return getDateAtOffset(0);
    }
    
    public static String getFormattedDate(Date date){
		return DATEFORMAT.format(date);
    }
    
    public static String getFormattedDateAtOffset(int years){
    	return getFormattedDate(getDateAtOffset(years));
    }
    
    public static long getMinutesSince(Date date){
    	Calendar now = Calendar.getInstance();
		long difference = now.getTimeInMillis() - date.getTime();
		return difference / (60 * 1000) % 60;
    }

}
