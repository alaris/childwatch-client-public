package com.alaris_us.daycareclient.fragments;

import android.app.AlertDialog;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.alaris_us.daycareclient.models.MainScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontEditText;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Facility;
import com.bindroid.BindingMode;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

import java.util.Locale;

public class MainScreenFragment extends ScreenSwitcherFragment {
	private MainScreenViewModel	mModel;
    private View m_fullCapacityDialogView;
    private View m_selectedLanguageView;
    private View m_staffOverrideEntryDialogView;
    private View m_successfulOverrideDialogView;
    private FontEditText overridePIN;
    private View m_capacityView;
    private View m_temperatureView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		m_layout = inflater.inflate(R.layout.mainscreen_layout, container, false);
        m_fullCapacityDialogView = inflater.inflate(R.layout.view_fullcapacitydialog_layout, container, false);
        m_staffOverrideEntryDialogView = inflater.inflate(R.layout.view_staffoverrideentrydialog_layout, container,
                false);
        m_successfulOverrideDialogView = inflater.inflate(R.layout.view_successfuloverridedialog_layout, container,
                false);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mModel = new MainScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this);
		StartLanguageListener startLanguageListener = new StartLanguageListener();
		m_layout.findViewById(R.id.mainscreen_tile_start_textview1_container).setOnClickListener(startLanguageListener);
		m_layout.findViewById(R.id.mainscreen_tile_start_textview2_container).setOnClickListener(startLanguageListener);
		m_layout.findViewById(R.id.mainscreen_tile_start_textview3_container).setOnClickListener(startLanguageListener);
		m_capacityView = m_layout.findViewById(R.id.mainscreen_tile_capacity);
		m_temperatureView = m_layout.findViewById(R.id.mainscreen_tile_temperature);

        UiBinder.bind(m_layout, R.id.mainscreen_tile_start_textview1, "Text", mModel, "LanguageOne",
                BindingMode.ONE_WAY);
        UiBinder.bind(m_layout, R.id.mainscreen_tile_start_textview2, "Text", mModel, "LanguageTwo",
                BindingMode.ONE_WAY);
        UiBinder.bind(m_layout, R.id.mainscreen_tile_start_textview3, "Text", mModel, "LanguageThree",
                BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.mainscreen_tile_location_citystate_textview, "Text", mModel, "CityStateText",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.mainscreen_tile_location_clubname_textview, "Text", mModel, "ClubNameText",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.mainscreen_tile_datetime_textview, "Text", mModel, "DateTime", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.mainscreen_tile_temperature_textview_unit, "Text", mModel, "TemperatureUnit",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.mainscreen_tile_temperature_textview_value, "Text", mModel, "Temperature",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.mainscreen_tile_location_imageview, "ImageBitmap", mModel, "LocationImage",
				BindingMode.ONE_WAY);

		/* Capacity tile */
		UiBinder.bind(m_layout, R.id.mainscreen_tile_capacity_label1_textview, "Text", mModel, "CapacityInfo",
				BindingMode.ONE_WAY);
		if (((PersistanceLayerHost) getFragmentActivity()).getPersistanceLayer().usesCapacityWidget())
			m_capacityView.setVisibility(View.VISIBLE);
		else
			m_capacityView.setVisibility(View.GONE);

		/* Temperature tile */
		if (((PersistanceLayerHost) getFragmentActivity()).getPersistanceLayer().usesCapacityWidget())
			m_temperatureView.setVisibility(View.GONE);
		else
			m_temperatureView.setVisibility(View.VISIBLE);
		
		/* FullCapacityDialog */
		UiBinder.bind(m_fullCapacityDialogView, R.id.view_fullcapacitydialog_ok_button, "OnClickListener",
				mModel, "FullCapacityDialogOkClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_fullCapacityDialogView, R.id.view_fullcapacitydialog_override_button, "OnClickListener",
				mModel, "FullCapacityDialogOverrideClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "FullCapacityDialog"), mModel, "FullCapacityDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
		
        /* StaffOverrideEntryDialog */
        UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_enter_button,
                "OnClickListener", mModel, "StaffOverrideEntryDialogEnterClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_cancel_button,
                "OnClickListener", mModel, "StaffOverrideEntryDialogCancelClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(new ReflectedProperty(this, "StaffOverrideEntryDialog"), mModel, "StaffOverrideEntryDialog",
                BindingMode.ONE_WAY_TO_SOURCE);

        /* SuccessfulOverrideDialog */
        UiBinder.bind(m_successfulOverrideDialogView, R.id.view_successfuloverridedialog_ok_button, "OnClickListener",
                mModel, "SuccessfulOverrideDialogOkClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(new ReflectedProperty(this, "SuccessfulOverrideDialog"), mModel, "SuccessfulOverrideDialog",
                BindingMode.ONE_WAY_TO_SOURCE);

	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
        if (mModel != null)
            mModel.disconnect();
	}

	@Override
	public void onResumeFragment() {
		super.onResumeFragment();
		mModel = (null == mModel) ? new MainScreenViewModel(
				((PersistanceLayerHost) getFragmentActivity()).getPersistanceLayer(), this) : mModel;
		mModel.reset();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mModel.disconnect();
		mModel = null;
	}

	public class StartLanguageListener implements OnClickListener {
		@Override
		public void onClick(View v) {
            m_selectedLanguageView = v;
            mModel.checkCapacity();
		}
	}

    public void setLanguage() {
		Facility facility = ((PersistanceLayerHost) getFragmentActivity()).getPersistanceLayer().getPersistenceData().getFacility();
        String lang = "";
        try {
			if (m_selectedLanguageView.getId() == R.id.mainscreen_tile_start_textview1_container)
				lang = facility.getLanguages().getString(0);
			else if (m_selectedLanguageView.getId() == R.id.mainscreen_tile_start_textview2_container)
				lang = facility.getLanguages().getString(1);
			else if (m_selectedLanguageView.getId() == R.id.mainscreen_tile_start_textview3_container)
				lang = facility.getLanguages().getString(2);

			Locale myLocale = new Locale(lang);
			String langName = myLocale.getDisplayLanguage(Locale.ENGLISH);
			((PersistanceLayerHost) getActivity()).getPersistanceLayer().getPersistenceData().setSelectedLanguage(langName);
			Resources res = getResources();
			DisplayMetrics dm = res.getDisplayMetrics();
			Configuration conf = res.getConfiguration();
			conf.locale = myLocale;
			res.updateConfiguration(conf, dm);
			refreshNextScreen();
		} catch(Exception e) {
        	Log.e("MainScreenFragment", "setLanguage() - Error trying to set language.");
        	Log.i("MainScreenFragment", e.getMessage());
		}

    }

    public AlertDialog getFullCapacityDialog() {
		FrameLayout FullCapacityDialogContainer = new FrameLayout(getActivity());
		FullCapacityDialogContainer.addView(m_fullCapacityDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(FullCapacityDialogContainer);
		AlertDialog fullCapacityDialog = builder.create();
		fullCapacityDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		fullCapacityDialog.setCancelable(false);
		fullCapacityDialog.show();
		fullCapacityDialog.dismiss();
		ViewGroup.LayoutParams lp = m_fullCapacityDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_fullcapacitydialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_fullcapacitydialog_height);
		m_fullCapacityDialogView.setLayoutParams(lp);
		((View) m_fullCapacityDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return fullCapacityDialog;
	}
	
	public AlertDialog getStaffOverrideEntryDialog() {
		FrameLayout staffOverrideEntryDialogContainer = new FrameLayout(getActivity());
		staffOverrideEntryDialogContainer.addView(m_staffOverrideEntryDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(staffOverrideEntryDialogContainer);
		AlertDialog staffOverrideEntryDialog = builder.create();
		staffOverrideEntryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		staffOverrideEntryDialog.setCancelable(false);
		staffOverrideEntryDialog.show();
		staffOverrideEntryDialog.dismiss();
		ViewGroup.LayoutParams lp = m_staffOverrideEntryDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_height);
		m_staffOverrideEntryDialogView.setLayoutParams(lp);
		((View) m_staffOverrideEntryDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return staffOverrideEntryDialog;
	}

	public String getOverridePIN() {
		overridePIN = (FontEditText) m_staffOverrideEntryDialogView
				.findViewById(R.id.view_staffoverrideentrydialog_pin_edittext);
		return overridePIN.getText().toString();
	}

	public void setOverridePIN(String pin) {
		overridePIN.setText(pin);
		overridePIN.setHint("Invalid PIN, Please Retry");
	}

	public void setOverridePINHint(String hint) {
		overridePIN.setHint(hint);
	}

	public AlertDialog getSuccessfulOverrideDialog() {
		FrameLayout successfulOverrideDialogContainer = new FrameLayout(getActivity());
		successfulOverrideDialogContainer.addView(m_successfulOverrideDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(successfulOverrideDialogContainer);
		AlertDialog successfulOverrideDialog = builder.create();
		successfulOverrideDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		successfulOverrideDialog.setCancelable(false);
		successfulOverrideDialog.show();
		successfulOverrideDialog.dismiss();
		ViewGroup.LayoutParams lp = m_successfulOverrideDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_height);
		m_successfulOverrideDialogView.setLayoutParams(lp);
		((View) m_successfulOverrideDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return successfulOverrideDialog;
	}
}
