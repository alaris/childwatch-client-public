/**
 * BasePrint for printing
 * 
 * @author Brother Industries, Ltd.
 * @version 2.2
 */

package com.alaris_us.daycareclient.printing;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Message;

import com.alaris_us.daycareclient_dev.R;
import com.brother.ptouch.sdk.LabelInfo;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterInfo.ErrorCode;
import com.brother.ptouch.sdk.PrinterInfo.Model;
import com.brother.ptouch.sdk.PrinterInfo.Port;
import com.brother.ptouch.sdk.PrinterStatus;

public abstract class BasePrint {

	protected String customSetting;
	public static Printer mPrinter;
	protected PrinterStatus mPrintResult;
	protected PrinterInfo mPrinterInfo;
	protected Context mContext;
	protected MsgHandle mHandle;
	protected MsgDialog mDialog;
	protected LabelInfo mLabelInfo;
	protected final String mPrinterIP;
    protected final String mPrinterModel;
    protected final String mPrinterMacAddress;
    

	protected abstract void doPrint();

    public BasePrint(Context context, MsgHandle handle, MsgDialog dialog, String ip) {
        this(context, handle, dialog, ip, "", "");
    }

    public BasePrint(Context context, MsgHandle handle, MsgDialog dialog, String ip, String model, String macAddress) {

		mContext = context;
		mDialog = dialog;
		mHandle = handle;
		mDialog.setHandle(mHandle);
		mPrinterIP = ip;
        mPrinterModel = model;
        mPrinterMacAddress = macAddress;

		// initialization for print
		mPrinterInfo = new PrinterInfo();
		mPrinter = new Printer();
		mLabelInfo = new LabelInfo();
		mPrinterInfo = mPrinter.getPrinterInfo();
		mPrinter.setMessageHandle(mHandle, Common.MSG_SDK_EVENT);
	}

	public static void cancel() {
		if (mPrinter != null)
			mPrinter.cancel();
	}

	/** set PrinterInfo */
	public void setPrinterInfo() {

		getPreferences();
		mPrinter.setPrinterInfo(mPrinterInfo);
		mPrinter.setLabelInfo(mLabelInfo);
		if (mPrinterInfo.port == PrinterInfo.Port.USB) {
			while (true) {
				if (Common.mUsbRequest != 0)
					break;
			}
			if (Common.mUsbRequest != 1)
				return;
		}
	}

	/** get Printer */
	public Printer getPrinter() {

		return mPrinter;
	}

	public void setBluetoothAdapter(BluetoothAdapter bluetoothAdapter) {

		mPrinter.setBluetooth(bluetoothAdapter);
	}

	@TargetApi(12)
	public UsbDevice getUsbDevice(UsbManager usbManager) {
		return mPrinter.getUsbDevice(usbManager);
	}

	/** get the printer settings from the SharedPreferences */
	private void getPreferences() {
        if (mPrinterModel == null) {
            mPrinterInfo.printerModel = PrinterInfo.Model.QL_720NW;
        } else if (mPrinterModel.equals("QL-820NWB")) {
			mPrinterInfo.printerModel = PrinterInfo.Model.QL_820NWB;
		} else if (mPrinterModel.equals("QL-810W")) {
            mPrinterInfo.printerModel = PrinterInfo.Model.QL_810W;
        } else 
            mPrinterInfo.printerModel = PrinterInfo.Model.QL_720NW;

            mPrinterInfo.port = PrinterInfo.Port.NET;
            if (mPrinterIP != null)
            	if (mPrinterIP.length() > 0)
            		mPrinterInfo.ipAddress = mPrinterIP;
            if (mPrinterMacAddress != null)
            	if (mPrinterMacAddress.length() > 0)
            		  mPrinterInfo.macAddress = mPrinterMacAddress;
            mPrinterInfo.paperSize = PrinterInfo.PaperSize.CUSTOM;
            mPrinterInfo.labelNameIndex = LabelInfo.QL700.W62.ordinal();
            mPrinterInfo.isAutoCut = false;
            mPrinterInfo.isCutAtEnd = false;
            mPrinterInfo.isHalfCut = false;
            /*mLabelInfo.labelNameIndex = LabelInfo.QL700.W62H100.ordinal();
            mLabelInfo.isAutoCut = false;
            mLabelInfo.isEndCut = false;
            mLabelInfo.isHalfCut = false;*/
    
            //mPrinterInfo.orientation = PrinterInfo.Orientation.PORTRAIT;
            //mPrinterInfo.numberOfCopies = 1;
            //mPrinterInfo.halftone = PrinterInfo.Halftone.THRESHOLD;
            //mPrinterInfo.printMode = PrinterInfo.PrintMode.ORIGINAL;

	}

	/** Launch the thread to print */
	public void print() {

		PrinterThread printTread = new PrinterThread();
		printTread.start();
	}

	/** Thread for printing */
	protected class PrinterThread extends Thread {
		@Override
		public void run() {

			// set info. for printing
			setPrinterInfo();

			// start message
			Message msg = mHandle.obtainMessage(Common.MSG_PRINT_START);
			mHandle.sendMessage(msg);

			mPrintResult = new PrinterStatus();

			mPrinter.startCommunication();

			doPrint();

			mPrinter.endCommunication();

			// end message
			mHandle.setResult(showResult());
			mHandle.setBattery(getBattery());
			msg = mHandle.obtainMessage(Common.MSG_PRINT_END);
			mHandle.sendMessage(msg);
		}
	}

	/** Launch the thread to get the printer's status */
	public void getPrinterStatus() {

		getStatusThread getTread = new getStatusThread();
		getTread.start();
	}

	/** Thread for getting the printer's status */
	class getStatusThread extends Thread {
		@Override
		public void run() {

			// set info. for printing
			setPrinterInfo();

			// start message
			Message msg = mHandle.obtainMessage(Common.MSG_PRINT_START);
			mHandle.sendMessage(msg);

			mPrintResult = new PrinterStatus();
			mPrintResult = mPrinter.getPrinterStatus();

			// end message
			mHandle.setResult(showResult());
			mHandle.setBattery(getBattery());
			msg = mHandle.obtainMessage(Common.MSG_PRINT_END);
			mHandle.sendMessage(msg);

		}
	}

	/** get the end message of print */
	protected String showResult() {

		String result = "";
		if (mPrintResult.errorCode == ErrorCode.ERROR_NONE) {
			result = mContext.getString(R.string.ErrorMessage_ERROR_NONE);
		} else {
			result = mPrintResult.errorCode.toString();
		}

		return result;
	}

	/** show information of battery */
	protected String getBattery() {

		String battery = "";
		if (mPrinterInfo.printerModel == PrinterInfo.Model.MW_260) {
			if (mPrintResult.batteryLevel > 80) {
				battery = mContext.getString(R.string.battery_full);
			} else if (30 <= mPrintResult.batteryLevel && mPrintResult.batteryLevel <= 80) {
				battery = mContext.getString(R.string.battery_middle);
			} else if (0 <= mPrintResult.batteryLevel && mPrintResult.batteryLevel < 30) {
				battery = mContext.getString(R.string.battery_weak);
			}
		} else if (mPrinterInfo.printerModel == PrinterInfo.Model.RJ_4030
				|| mPrinterInfo.printerModel == PrinterInfo.Model.RJ_4040
				|| mPrinterInfo.printerModel == PrinterInfo.Model.RJ_3050
				|| mPrinterInfo.printerModel == PrinterInfo.Model.RJ_3150

				|| mPrinterInfo.printerModel == PrinterInfo.Model.TD_2020
				|| mPrinterInfo.printerModel == PrinterInfo.Model.TD_2120N
				|| mPrinterInfo.printerModel == PrinterInfo.Model.TD_2130N
				|| mPrinterInfo.printerModel == PrinterInfo.Model.PT_E550W
				|| mPrinterInfo.printerModel == PrinterInfo.Model.PT_P750W) {
			switch (mPrintResult.batteryLevel) {
			case 0:
				battery = mContext.getString(R.string.battery_full);
				break;
			case 1:
				battery = mContext.getString(R.string.battery_middle);
				break;
			case 2:
				battery = mContext.getString(R.string.battery_weak);
				break;
			case 3:
				battery = mContext.getString(R.string.battery_charge);
				break;
			case 4:
				battery = mContext.getString(R.string.ac_adapter);
				break;
			default:
				break;
			}
		} else {
			switch (mPrintResult.batteryLevel) {
			case 0:
				battery = mContext.getString(R.string.ac_adapter);
				break;
			case 1:
				battery = mContext.getString(R.string.battery_weak);
				break;
			case 2:
				battery = mContext.getString(R.string.battery_middle);
				break;
			case 3:
				battery = mContext.getString(R.string.battery_full);
				break;
			default:
				break;
			}
		}
		if (mPrintResult.errorCode != ErrorCode.ERROR_NONE)
			battery = "";
		return battery;
	}

}
