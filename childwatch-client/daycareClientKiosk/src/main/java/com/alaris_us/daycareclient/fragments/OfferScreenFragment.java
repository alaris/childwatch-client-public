package com.alaris_us.daycareclient.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.alaris_us.daycareclient.models.OfferScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontCheckBox;
import com.alaris_us.daycareclient.widgets.FontEditText;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

public class OfferScreenFragment extends ScreenSwitcherFragment {
	private OfferScreenViewModel m_model;
	private View m_emailDialogView;
	private final int emailDialogWidthRes = R.dimen.view_emaildialog_width;
	private final int emailDialogHeightRes = R.dimen.view_emaildialog_height;
	private int emailDialogWidth;
	private int emailDialogHeight;
	private FontEditText m_editText;
	private FontCheckBox m_checkBox;
	private View m_verifyEmailDialogView;
	private final int verifyEmailDialogWidthRes = R.dimen.view_verifyemaildialog_width;
	private final int verifyEmailDialogHeightRes = R.dimen.view_verifyemaildialog_height;
	private int verifyEmailDialogWidth;
	private int verifyEmailDialogHeight;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.offerscreen_layout, container,
				false);
		m_emailDialogView = inflater.inflate(R.layout.view_emaildialog_layout,
				container, false);
		m_verifyEmailDialogView = inflater.inflate(
				R.layout.view_verifyemaildialog_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void initData() {
		m_model.reset();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		m_model = new OfferScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		emailDialogWidth = (int) getResources().getDimension(
				verifyEmailDialogWidthRes);
		emailDialogHeight = (int) getResources().getDimension(
				verifyEmailDialogHeightRes);
		verifyEmailDialogWidth = (int) getResources().getDimension(
				emailDialogWidthRes);
		verifyEmailDialogHeight = (int) getResources().getDimension(
				emailDialogHeightRes);
		m_editText = (FontEditText) m_emailDialogView
				.findViewById(R.id.view_emaildialog_bottom_edittext);
		m_checkBox = (FontCheckBox) m_emailDialogView
				.findViewById(R.id.view_emaildialog_setasdefault_checkbox);
		UiBinder.bind(m_layout,
				R.id.view_offerscreen_navigation_emailme_container,
				"OnClickListener", m_model, "EmailMeClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout,
				R.id.view_offerscreen_navigation_nothanks_container,
				"OnClickListener", m_model, "NoThanksClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.offerscreen_tile_offer_heading, "Text",
				m_model, "HeaderText", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.offerscreen_tile_offer_displaytext,
				"Text", m_model, "DisplayText", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.offerscreen_tile_offer_imageview,
				"ImageBitmap", m_model, "DisplayImage", BindingMode.ONE_WAY);
		UiBinder.bind(m_emailDialogView, R.id.view_emaildialog_ok_button,
				"OnClickListener", m_model, "EmailDialogOkClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_emailDialogView, R.id.view_emaildialog_cancel_button,
				"OnClickListener", m_model, "EmailDialogCancelClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "EmailDialog"), m_model,
				"EmailDialog", BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(m_verifyEmailDialogView,
				R.id.view_verifyemaildialog_send_button, "OnClickListener",
				m_model, "VerifyEmailDialogSendClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_verifyEmailDialogView,
				R.id.view_verifyemaildialog_cancel_button, "OnClickListener",
				m_model, "VerifyEmailDialogCancelClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "VerifyEmailDialog"),
				m_model, "VerifyEmailDialog", BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(m_verifyEmailDialogView,
				R.id.view_verifyemaildialog_bottom_text, "Text", m_model,
				"EmailAddress", BindingMode.ONE_WAY);
		UiBinder.bind(m_verifyEmailDialogView,
				R.id.view_verifyemaildialog_other_button, "Visibility",
				m_model, "IsEmailOnFile", BindingMode.ONE_WAY);
		UiBinder.bind(m_verifyEmailDialogView,
				R.id.view_verifyemaildialog_other_button, "OnClickListener",
				m_model, "VerifyEmailDialogOtherClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_verifyEmailDialogView,
				R.id.view_verifyemaildialog_send_button, "LayoutParams",
				m_model, "LayoutParams", BindingMode.ONE_WAY);
		UiBinder.bind(m_verifyEmailDialogView,
				R.id.view_verifyemaildialog_cancel_button, "LayoutParams",
				m_model, "LayoutParams", BindingMode.ONE_WAY);
		/*
		 * View v = m_verifyEmailDialogView.findViewById(R.id.
		 * view_verifyemaildialog_cancel_button); v.setLayoutParams(params);
		 */
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		m_model.disconnect();
	}

	@Override
	public void onResumeFragment() {
		super.onResumeFragment();
		m_model = (null == m_model) ? new OfferScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this) : m_model;
		m_model.reset();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		m_model.disconnect();
		m_model = null;
	}

	public AlertDialog getEmailDialog() {
		FrameLayout emailDialogContainer = new FrameLayout(getActivity());
		emailDialogContainer.addView(m_emailDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(emailDialogContainer);
		AlertDialog emailDialog = builder.create();
		emailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		emailDialog.setCancelable(false);
		emailDialog.show();
		emailDialog.dismiss();
		ViewGroup.LayoutParams lp = m_emailDialogView.getLayoutParams();
		lp.width = emailDialogWidth;
		lp.height = emailDialogHeight;
		m_emailDialogView.setLayoutParams(lp);
		((View) m_emailDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return emailDialog;
	}

	public AlertDialog getVerifyEmailDialog() {
		FrameLayout verifyEmailDialogContainer = new FrameLayout(getActivity());
		verifyEmailDialogContainer.addView(m_verifyEmailDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(verifyEmailDialogContainer);
		AlertDialog verifyEmailDialog = builder.create();
		verifyEmailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		verifyEmailDialog.setCancelable(false);
		verifyEmailDialog.show();
		verifyEmailDialog.dismiss();
		ViewGroup.LayoutParams lp = m_verifyEmailDialogView.getLayoutParams();
		lp.width = verifyEmailDialogWidth;
		lp.height = verifyEmailDialogHeight;
		m_verifyEmailDialogView.setLayoutParams(lp);
		((View) m_verifyEmailDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return verifyEmailDialog;
	}

	public String getNewEmail() {
		return m_editText.getText().toString();
	}

	public boolean getIsSetAsDefault() {
		return m_checkBox.isChecked();
	}
}
