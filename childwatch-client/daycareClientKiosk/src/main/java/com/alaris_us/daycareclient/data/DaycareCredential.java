package com.alaris_us.daycareclient.data;

public final class DaycareCredential {
	public enum FormOfCredential {
		PHONE, BARCODE, ON_REGISTER
	};

	private final FormOfCredential	mForm;
	private final String			mText;

	public static DaycareCredential getNewInstance(String text, FormOfCredential form) {
		if (text == null || form == null)
			return null;
		if (form == FormOfCredential.PHONE && text.length() != 10) {
			return null;
		}
		return new DaycareCredential(text, form);
		// Assign fields
		// // Check phone number pattern
		// if (!Patterns.PHONE.matcher(phoneNumber).matches()) return null;
		// // Strip phone number
		// phoneNumber = PhoneNumberUtils.stripSeparators(phoneNumber);
		// // Check number length
		// if (phoneNumber.length() != 10) return null;
		// // Valid format. Populate fields
		// return new PhoneNumber(phoneNumber.substring(0, 3),
		// phoneNumber.substring(3, 6),
		// phoneNumber.substring(6, 10));
	}

	public FormOfCredential getForm() {
		return mForm;
	}

	public String getRawText() {
		return mText;
	}

	public String getText() {
		if (mForm == FormOfCredential.PHONE) {
			return mText.substring(0, 3) + "-" + mText.substring(3, 6) + "-" + mText.substring(6);
		} else {
			return mText;
		}
	}

	private DaycareCredential(String text, FormOfCredential form) {
		mText = text;
		mForm = form;
	}
}
