package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Room;

public class FetchAllRooms extends Command<List<Room>> {

	Facility	m_facility;

	public FetchAllRooms(Facility facility) {

		this(facility, null, null);
	}

	protected FetchAllRooms(Facility facility, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_facility = facility;
	}

	@Override
	public void executeSource() {

		m_dataSource.getRoomsDAO().listAllFacilityRooms(m_facility, this);

	}

	@Override
	public void handleResponse(List<Room> data) {
		m_model.setAllRooms(data);
	}
}