package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.alaris_us.daycareclient_dev.R;

public class FontRadioButton extends AppCompatRadioButton {

	private static final String FONT_DIRECTORY = "fonts/";

	public FontRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init(attrs);

	}

	public FontRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(attrs);

	}

	public FontRadioButton(Context context) {
		super(context);

		init(null);
	}

	private void init(AttributeSet attrs) {

		if (attrs!=null) {

			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FontTextView);

			String fontName = a
					.getString(R.styleable.FontTextView_fontName);

			if (fontName != null) {
				try {
					Typeface myTypeface = Typeface.createFromAsset(getContext()
							.getAssets(), FONT_DIRECTORY + fontName);
					setTypeface(myTypeface);
				} catch (Exception e) {
				}
			}
			a.recycle();

		}

	}
	public void setText(String text){
		super.setText(text);
	}

	public void setTextRes(int resId){
		super.setText(resId);
	}

	public void setTextSizeRes(int resId){
		super.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(resId));
	}


}
