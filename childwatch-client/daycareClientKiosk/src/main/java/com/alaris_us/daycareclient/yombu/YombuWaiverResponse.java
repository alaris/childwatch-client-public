package com.alaris_us.daycareclient.yombu;


import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class YombuWaiverResponse extends YombuDataObject {

    public YombuWaiverResponse(JSONObject json) {
        super(json);
    }

    public YombuWaiverResponse(YombuDataObject obj) {
        super(obj);
    }

    public String getId() {
        String id = "";
        try {
            id = getData().getString("id");
        } catch (JSONException e) {
            Log.e(this.getClass().getSimpleName(), e.getMessage());
        }
        return id;
    }

    public String getCreatedAt(){
        String createdAt = "";
        try {
            createdAt = getData().getString("created_at");
        } catch (JSONException e) {
            Log.e(this.getClass().getSimpleName(), e.getMessage());
        }
        return createdAt;
    }

    public String getContents() {
        String contents = "";
        try {
            contents = getData().getString("contents");
        } catch (JSONException e) {
            Log.e(this.getClass().getSimpleName(), e.getMessage());
        }
        return contents;
    }

}
