package com.alaris_us.daycareclient.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.models.CheckInOrOutScreenViewModel;
import com.alaris_us.daycareclient.models.QuestionScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.UiBinder;

public class QuestionScreenFragment extends ScreenSwitcherFragment {
	protected QuestionScreenViewModel	mModel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.questionscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		mModel.disconnect();
	}

	@Override
	public void onResumeFragment() {
		super.onResumeFragment();
		mModel = (null == mModel) ? new QuestionScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this) : mModel;
		mModel.reset();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mModel.disconnect();
		mModel = null;
	}

	@Override
	public void initData() {
		mModel.reset();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mModel = new QuestionScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this);
		m_layout.findViewById(R.id.questionscreen_tile_yes_container).setOnClickListener(mModel.new YesClickListener());
		m_layout.findViewById(R.id.questionscreen_tile_no_container).setOnClickListener(mModel.new NoClickListener());
		UiBinder.bind(m_layout, R.id.questionscreen_tile_questionmessage_textbox, "Text", mModel, "TitleText", BindingMode.ONE_WAY);
		// Navigation Button(s)
		m_layout.findViewById(R.id.questionscreen_tile_back_halfarrow).setOnClickListener(new BackClickListener());
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}
}