package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Room;

public class CheckIntoRoom extends Command<Room> {

	private final Room m_assignedRoom;

	public CheckIntoRoom(Room assignedRooms) { 
		this(assignedRooms, null, null);
	};

	public CheckIntoRoom(Room assignedRoom, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_assignedRoom=assignedRoom;
	}

	@Override
	public void executeSource() {
		m_dataSource.getRoomsDAO().update(m_assignedRoom, this);
	}

	@Override
	public void handleResponse(Room data) {
		
	}

}