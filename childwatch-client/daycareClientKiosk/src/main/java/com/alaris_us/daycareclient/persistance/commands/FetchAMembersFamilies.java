package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.FamilyAssociations.FamilyType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Authorization;
import com.alaris_us.daycaredata.to.FamilyAssociation;
import com.alaris_us.daycaredata.to.Member;

public class FetchAMembersFamilies extends Command<List<Authorization>> {

	private final Member mMember;
	private final int mDefaultMaxChildAgeInYears;
	private List<FamilyAssociation> mFamilyAssociations;

	public FetchAMembersFamilies(Member member, int defaultMaxChildAgeInYears) {

		this(member, defaultMaxChildAgeInYears, null, null);
	}

	protected FetchAMembersFamilies(Member member, int defaultMaxChildAgeInYears, DaycareData dataSource,
			PersistenceData model) {
		super(dataSource, model);
		mMember = member;
		mDefaultMaxChildAgeInYears = defaultMaxChildAgeInYears;
	}

	@Override
	public void executeSource() {

		m_dataSource.getFamilyAssociationsDAO().fetchMemberAssociations(mMember.getPFamily(), new FetchAuthorizedChildren(this));

	}

	@Override
	public void handleResponse(List<Authorization> data) {

		List<Member> related = new ArrayList<Member>();
		List<Member> proxies = new ArrayList<Member>();
		List<Member> validRelated = new ArrayList<Member>();
		List<Member> validProxies = new ArrayList<Member>();

		Double minAge = m_model.getMinRoomAge();
		Double maxAge = m_model.getMaxRoomAge();

		// Determine if room age is used
		minAge = (minAge == null) ? 0 : minAge;
		maxAge = (maxAge == null) ? mDefaultMaxChildAgeInYears : maxAge;

		for (FamilyAssociation association : mFamilyAssociations) {
			Member inQuestion = association.getPMember();

			if (null != inQuestion) {
				if (DaycareHelpers.getIsAgeBetween(inQuestion.getBirthDate(), minAge, maxAge)){

					if (FamilyType.PRIMARY.equals(association.getFamilyType())) {
						related.add(inQuestion);
						m_model.setPrimaryFamily(inQuestion.getPFamily());
						if (inQuestion.getIsValidMembership()) {
							validRelated.add(inQuestion);
						}
					}
				}
			}
		}
		
		for (Authorization auth : data) {
			Member inQuestion = auth.getAuthorizedFor();

			if (null != inQuestion) {
				int age = DaycareHelpers.getAgeInYears(inQuestion.getBirthDate());
				if (age >= minAge && age < maxAge) {
					proxies.add(inQuestion);
					if (inQuestion.getIsValidMembership()) {
						validProxies.add(inQuestion);
					}
				}
			}
		}

		// Store results
		m_model.setAllDaycareChildren(related, proxies);
		m_model.setValidRelatedChildren(validRelated);
		m_model.setValidProxyChildren(validProxies);

		// TODO remove this. Override logic.
		if (m_model.getIsOverride())
			m_model.setAllValidDaycareChildren(related, proxies);
		else
			m_model.setAllValidDaycareChildren(validRelated, validProxies);

	}

	private class FetchAuthorizedChildren implements DaycareOperationComplete<List<FamilyAssociation>> {
		FetchAMembersFamilies mListener;

		public FetchAuthorizedChildren(FetchAMembersFamilies listener) {
			mListener = listener;
		}
		@Override
		public void operationComplete(List<FamilyAssociation> data, DaycareDataException e) {
			mFamilyAssociations = data;
			m_dataSource.getAuthorizationsDAO().fetchAuthorizedChildren(mMember, mListener);
		}
		
	}
}
