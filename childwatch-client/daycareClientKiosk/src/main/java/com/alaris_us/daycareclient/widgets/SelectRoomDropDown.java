package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class SelectRoomDropDown extends TextView implements BoundUi<String> {

	private TrackableField<String> mData;
	private static final String FONT_DIRECTORY = "fonts/";

	public String getData() {

		return mData.get();
	}

	public float getTextSize() {

		return getResources().getDimension(R.dimen.selectroomscreen_spinner_textsize);
	}

	public int getGravity() {

		return Gravity.CENTER;
	}

	public int getTextColor() {
		return getResources().getColor(R.color.white);
	}

	public SelectRoomDropDown(Context context, AttributeSet attrs, int defStyle) {

		super(context, attrs, defStyle);
		init(attrs);
	}

	public SelectRoomDropDown(Context context, AttributeSet attrs) {

		this(context, attrs, R.attr.selectRoomDropDown);
	}

	public SelectRoomDropDown(Context context) {

		this(context, null);
	}

	private void init(AttributeSet attrs) {

		if (attrs != null) {

			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FontTextView);

			String fontName = a.getString(R.styleable.FontTextView_fontName);

			if (fontName != null) {
				try {
					Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), FONT_DIRECTORY + fontName);
					setTypeface(myTypeface);
				} catch (Exception e) {
				}
			}
			a.recycle();

		}
	}

	@Override
	public void bind(String dataSource) {

		mData = new TrackableField<String>(dataSource);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Text")), this, "Data", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "TextSize")), this, "TextSize", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Gravity")), this, "Gravity", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "TextColor")), this, "TextColor", BindingMode.ONE_WAY);
	}
}
