package com.alaris_us.daycareclient.screenswitcher;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.alaris_us.daycareclient_dev.R;

public class ScreenSwitcher extends ViewPager {
	ScreenSwitcherAdapter adapter;
	int scrollThreshhold;

	public enum ScrollDir {
		FORWARD, BACKWARD
	}

	public ScreenSwitcher(Context context) {
		super(context);
		init();
	}

	public ScreenSwitcher(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		scrollThreshhold = (int) getResources().getDimension(R.dimen.viewpager_scrollthreshhold);
		setPageMargin(10);
		setPageMarginDrawable(R.color.dividerLine);
		setOffscreenPageLimit(0);
	}

	/*@Override
	protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
		if (dx > scrollThreshhold)
			return !adapter.getFragmentAtIndex(getCurrentItem()).canScroll(ScrollDir.BACKWARD);
		else if (dx < -scrollThreshhold)
			return !adapter.getFragmentAtIndex(getCurrentItem()).canScroll(ScrollDir.FORWARD);
		else
			return true;
	}*/
	
	@Override
	public void setAdapter(PagerAdapter arg0) {
		super.setAdapter(arg0);
		adapter = (ScreenSwitcherAdapter) arg0;
	}

	@Override
	public void setCurrentItem(int item) {
        setCurrentItem(item, false);

	}

    private int currItem = 0;
	@Override
	public void setCurrentItem(int item, boolean smoothScroll) {
        currItem = item;
		super.setCurrentItem(item, smoothScroll);
	}

	public ScreenSwitcherFragment getCurrentFragment() {
		return adapter.getFragmentAtIndex(getCurrentItem());
	}

	@Override
	protected void onPageScrolled(int arg0, float arg1, int arg2) {
		super.onPageScrolled(arg0, arg1, arg2);
	}

	@Override
	public void setOnPageChangeListener(final OnPageChangeListener listener) {
		super.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int arg0) {
				listener.onPageSelected(arg0);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				listener.onPageScrolled(currItem, arg1, arg2);
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				listener.onPageScrollStateChanged(arg0);
			}
		});
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		//Disable swiping
		this.requestDisallowInterceptTouchEvent(true);
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		//To Disable Swiping
		return false;
	}
	
}
