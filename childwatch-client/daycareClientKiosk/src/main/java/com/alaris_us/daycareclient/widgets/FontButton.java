package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.alaris_us.daycareclient_dev.R;

public class FontButton extends Button {

	private static final String FONT_DIRECTORY = "fonts/";

	public FontButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init(attrs);

	}

	public FontButton(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(attrs);

	}

	public FontButton(Context context) {
		super(context);

		init(null);
	}

	private void init(AttributeSet attrs) {

		if (attrs!=null) {

			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FontButton);

			String fontName = a
					.getString(R.styleable.FontButton_fontName);

			if (fontName != null) {
				try {
					Typeface myTypeface = Typeface.createFromAsset(getContext()
							.getAssets(), FONT_DIRECTORY + fontName);
					setTypeface(myTypeface);
				} catch (Exception e) {
				}
			}
			a.recycle();

		}

	}
	
	public void setTextRes(int resId){
		super.setText(resId);
	}

}
