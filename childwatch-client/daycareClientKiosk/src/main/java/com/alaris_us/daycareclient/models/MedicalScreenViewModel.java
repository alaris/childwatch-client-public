package com.alaris_us.daycareclient.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.alaris_us.daycareclient.data.DaycareCredential;
import com.alaris_us.daycareclient.fragments.MedicalScreenFragment;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.MedicalScreenItem;
import com.alaris_us.daycareclient.widgets.MedicalScreenItemModel;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.ValidMembership;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

import android.view.View;
import android.view.View.OnClickListener;
import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

public class MedicalScreenViewModel extends ViewModel {
	public MedicalScreenViewModel(PersistenceLayer persistenceLayer, MedicalScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private MedicalScreenFragment										fragment;
	private TrackableField<TrackableCollection<MedicalScreenItemModel>>	m_memberModels;
	private TrackableCollection<Member>					m_parentsToSave;
	private TrackableInt												m_selectedMember;
	private TrackableField<String>										m_medicalMessage;
	private TrackableField<DaycareCredential>							mCredential;
	private TrackableCollection<Member>				m_childrenToSave;
	private TrackableCollection<Member> m_allMembers;
	private int m_loopIndex;
	private int m_lastIndex;
	private int m_updatedMemberships;
	private TrackableCollection<Member> m_existingMembers;

	public TrackableCollection<MedicalScreenItemModel> getMembers() {
		return m_memberModels.get();
	}

	@Override
	protected void onPopulateModelData() {
		dismissAllDialogs();
		int childNum = 0;
		mCredential.set(getPersistenceLayer().getPersistenceData().getCredential());
		m_childrenToSave.clear();
		m_childrenToSave.addAll(getPersistenceLayer().getPersistenceData().getAllYMCAChildren());
		m_parentsToSave.addAll(getPersistenceLayer().getPersistenceData().getAllYMCAParents());
		m_medicalMessage.set("");
		m_selectedMember.set(0);
		m_memberModels.get().clear();
		for (Member member : m_childrenToSave) {
			if (member.getUsesChildWatch()) {
				childNum++;
			}
		}
		int width = ListViewFunctions.calculateTileWidth(childNum, 2.25);
		SetForAllClickListener setForAllListener = new SetForAllClickListener();
		int pos = 2;
		for (Member member : m_childrenToSave) {
			if (member.getUsesChildWatch()) {
				MedicalScreenItemModel model = new MedicalScreenItemModel(member, width,
						ListViewFunctions.getColorAt(pos++), setForAllListener);
				m_memberModels.get().add(model);
			}
		}
	}

	public String getMedicalMessage() {
		return m_medicalMessage.get();
	}

	public void setMedicalMessage(String msg) {
		m_medicalMessage.set(msg);
	}

	public String getSelectedMemberName() {
		return m_memberModels.get().get(m_selectedMember.get()).getMemberName();
	}

	@Override
	public void onPersistModel() {
		fragment.showRegistrationMemberDialog();
		createChildrenToSave();
		m_existingMembers.clear();
		m_allMembers.clear();
		m_allMembers.addAll(m_parentsToSave);
		m_allMembers.addAll(m_childrenToSave);
		m_lastIndex = m_allMembers.size();
		getPersistenceLayer().updateAllMembers(m_allMembers, new FinishedStatusListener());
	}

	public void createChildrenToSave() {
		for (MedicalScreenItemModel model : m_memberModels.get()) {
			Member cur = model.getMember();
			for (Member m : m_childrenToSave) {
				if (cur.equals(m)) {
					m.setMedicalConcerns(model.getItemMedicalMessage());
				}
			}
		}
	}

	public void dismissAllDialogs() {
		for (MedicalScreenItemModel model : m_memberModels.get())
			model.setSelected(false);
	}

	public class MemberClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			MedicalScreenItemModel model = ((MedicalScreenItem) arg1).getModel();
			for (MedicalScreenItemModel m : m_memberModels.get())
				m.setSelected(m.equals(model));
		}
	}

	public class SetForAllClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			MedicalScreenItemModel currModel = (MedicalScreenItemModel) v.getTag();
			String medMsg = currModel.getItemMedicalMessage();
			for (MedicalScreenItemModel model : m_memberModels.get()) {
				model.setSelected(false);
				if (!model.equals(currModel)) {
					model.setItemMedicalMessage(medMsg);
				}
			}
		}
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			persistModel();
		}
	}

	private class RegisterFamilyStatusListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			List<Member> allMembersInFamily = new ArrayList<Member>();
			allMembersInFamily.addAll(m_parentsToSave);
			allMembersInFamily.addAll(m_childrenToSave);
			if (!getPersistenceLayer().getPersistenceData().getIsRegOverride()
					&& !getPersistenceLayer().getPersistenceData().getIsInvalidMemberSelectedOverride())
				if (getPersistenceLayer().getPersistenceData().getFacility().getUsesValidMembership()) {
                    for (Member m : allMembersInFamily) {
						getPersistenceLayer().isMembershipValidForFacility(getPersistenceLayer().getPersistenceData().getFacility(),
								m.getMembershipType(), new IsMembershipValidForFacilityListener(m));
					}
				} else {
					getPersistenceLayer().setupFamilyAssociationInDaycare(m_parentsToSave, m_childrenToSave,
							new FinishedStatusListener());
				}
			else if (getPersistenceLayer().getPersistenceData().getIsInvalidMemberSelectedOverride() || getPersistenceLayer().getPersistenceData().getIsRegOverride()) {
				List<Member> activeMembers = new ArrayList<Member>();
				for (Member member : allMembersInFamily) {
					if (!member.isActive()) {
						member.setIsValidMembership(true);
						member.setMembershipType("Guest");
						member.setGuestDaysLeft(1);
					} else {
						activeMembers.add(member);
					}
				}
				if (getPersistenceLayer().getPersistenceData().getFacility().getUsesValidMembership()) {
					for (Member m : allMembersInFamily) {
						getPersistenceLayer().isMembershipValidForFacility(getPersistenceLayer().getPersistenceData().getFacility(),
								m.getMembershipType(), new IsMembershipValidForFacilityListener(m));
					}
				} else {
					getPersistenceLayer().setupFamilyAssociationInDaycare(m_parentsToSave, m_childrenToSave,
							new FinishedStatusListener());
				}
			} else {
				if (getPersistenceLayer().getPersistenceData().getFacility().getUsesValidMembership()) {
					for (Member m : allMembersInFamily) {
						getPersistenceLayer().isMembershipValidForFacility(getPersistenceLayer().getPersistenceData().getFacility(),
								m.getMembershipType(), new IsMembershipValidForFacilityListener(m));
					}
				} else {
					getPersistenceLayer().setupFamilyAssociationInDaycare(m_parentsToSave, m_childrenToSave,
							new FinishedStatusListener());
				}
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class IsMembershipValidForFacilityListener implements CommandStatusListener{

		private Member member;

		public IsMembershipValidForFacilityListener(Member member) {
			this.member = member;
		}

		@Override
		public void starting(Command<?> command) {

		}

		@Override
		public void finished(Command<?> command) {
			m_updatedMemberships++;
			List<ValidMembership> result = (List<ValidMembership>) command.getResult();
			if (result.isEmpty())
				member.setIsValidMembership(false);
			else
				member.setIsValidMembership(true);
			if (m_updatedMemberships == m_lastIndex)
				getPersistenceLayer().setupFamilyAssociationInDaycare(m_parentsToSave, m_childrenToSave,
						new FinishedStatusListener());
		}

		@Override
		public void valueChanged(Command<?> command) {

		}

		@Override
		public void exception(Command<?> command, Exception e) {

		}
	}

	private class UpdateFamilyStatusListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			getPersistenceLayer().setupFamilyAssociationInDaycare(m_parentsToSave, m_childrenToSave,
					new FinishedStatusListener());
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class FinishedStatusListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			fragment.dismissRegistrationDialog();
			fragment.nextScreen();
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class GetMemberByMemberIdCommandListener implements CommandStatusListener{

		private Member member;

		public GetMemberByMemberIdCommandListener(Member member){
			this.member = member;
		}

		@Override
		public void starting(Command<?> command) {

		}

		@Override
		public void finished(Command<?> command) {
			Member result = (Member) command.getResult();
			if (result != null){
				m_existingMembers.add(result);
				for (Iterator<Member> iterator = m_childrenToSave.iterator(); iterator.hasNext();) {
				    if (iterator.next().getMemberID().equals(member.getMemberID())) {
				        // Remove the current element from the iterator and the list.
				        iterator.remove();
				    }
				}
				for (Iterator<Member> iterator = m_parentsToSave.iterator(); iterator.hasNext();) {
				    if (iterator.next().getMemberID().equals(member.getMemberID())) {
				        // Remove the current element from the iterator and the list.
				        iterator.remove();
				    }
				}
			}
			m_loopIndex++;
			if(m_lastIndex == m_loopIndex){
				if (!m_existingMembers.isEmpty())
					fragment.showMemberAlreadyExistsDialog();
				else if (m_childrenToSave.size() > 0) {
					getPersistenceLayer().registerFamilyInDaycare(m_parentsToSave, m_childrenToSave,
							new RegisterFamilyStatusListener());
				} else {
					new Exception("NO KIDS UNDER 12");
					//TODO add error prompt here
				}
			}
		}

		@Override
		public void valueChanged(Command<?> command) {

		}

		@Override
		public void exception(Command<?> command, Exception e) {

		}
	}

	@Override
	public void onInitialize() {
		m_memberModels = new TrackableField<TrackableCollection<MedicalScreenItemModel>>(
				new TrackableCollection<MedicalScreenItemModel>());
		m_parentsToSave = new TrackableCollection<Member>();
		m_selectedMember = new TrackableInt();
		m_medicalMessage = new TrackableField<String>();
		mCredential = new TrackableField<DaycareCredential>();
		m_childrenToSave = new TrackableCollection<Member>();
		m_allMembers = new TrackableCollection<Member>();
		m_existingMembers = new TrackableCollection<Member>();
		m_loopIndex = 0;
		m_updatedMemberships = 0;
		addWatch(CHANGE_ID.SELECTEDYMCACHILDREN);
		addWatch(CHANGE_ID.SELECTEDYMCAPARENTS);
		addWatch(CHANGE_ID.CREDENTIAL);
		//addWatch(CHANGE_ID.FAMILY);
	}
}
