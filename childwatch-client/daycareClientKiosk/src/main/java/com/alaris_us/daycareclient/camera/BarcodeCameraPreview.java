package com.alaris_us.daycareclient.camera;

import java.util.Collection;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.alaris_us.daycareclient_dev.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;

public class BarcodeCameraPreview extends CameraPreview implements Camera.PreviewCallback {

	private AutoFocusManager mAutoFocusManager;
	private Handler mDecodeHandler;
	private BarcodeCameraPreviewHandler mPreviewHandler;
	private int mPreviewMessage;
	private Collection<BarcodeFormat> decodeFormats;
	private Map<DecodeHintType, ?> decodeHints;
	private String characterSet;
	private BarcodeCameraPreviewListener mListener;

	public interface BarcodeCameraPreviewListener {
		public void read(String barcodeText);
		
		public void onReady();
		
		public void onStopped();
		
	}

	public BarcodeCameraPreview(Context context, CameraManager manager) {
		super(context, manager);
	}

	public void setDecodeListener(BarcodeCameraPreviewListener listener) {
		mListener = listener;

		if (mListener == null) {
			// stopDecode();
		} else {
			// restartPreviewAfterDelay(200L);
		}
	}

	void setHandler(Handler previewHandler, int previewMessage) {
		mDecodeHandler = previewHandler;
		mPreviewMessage = previewMessage;
	}

	Handler getUIHandler() {
		return mPreviewHandler;
	}

	protected void startPreview() {
		Camera theCamera = getCamera();
		if (null == theCamera)
			return;
		theCamera.setDisplayOrientation(180);
		Camera.Parameters p = theCamera.getParameters();
		// p.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
		// p.setPreviewSize(640, 480);
		theCamera.setParameters(p);

		theCamera.startPreview();
		// theCamera.autoFocus(null);
		theCamera.setPreviewCallback(this);
		mAutoFocusManager = new AutoFocusManager(theCamera);
		// if (mPreviewHandler == null) {
		mPreviewHandler = new BarcodeCameraPreviewHandler(this, decodeFormats, decodeHints, characterSet);
		// }
		restartPreview();
		
		if (null != mListener) {
			mListener.onReady();
		}

	}

	@Override
	protected void stopPreview() {
		
		if (null != mListener) {
			mListener.onStopped();
		}
		
		if (mAutoFocusManager != null) {
			mAutoFocusManager.stop();
			mAutoFocusManager = null;
		}
		Camera theCamera = getCamera();
		if (null == theCamera)
			return;

		try {
			theCamera.autoFocus(null);
			theCamera.stopPreview();
			theCamera.setPreviewCallback(null);
			mCManager.closeCamera();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}

		if (mPreviewHandler != null) {
			mPreviewHandler.quitSynchronously();
			mPreviewHandler = null;
		}
		setHandler(null, 0);
	}

	@Override
	protected void pictureTaken() {
		// Not required. Super method.
	}

	public synchronized void requestPreviewFrame(Handler handler, int message) {
		Camera theCamera = getCamera();
		if (theCamera != null) {
			setHandler(handler, message);
			theCamera.setOneShotPreviewCallback(this);
		}
	}

	public void setViewFinder(View viewFinder) {

		// TODO Temporary hardcoded numbers
		// mDecoderService.setViewFinderRect(new Rect(360, 202, 1200 + 360, 675
		// + 202));
		// mDecoderService.setViewFinderRect(new Rect(0, 0, 640, 480));

	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		Size cameraResolution = camera.getParameters().getPreviewSize();

		Handler thePreviewHandler = mDecodeHandler;
		if (cameraResolution != null && thePreviewHandler != null) {
			Message message = thePreviewHandler.obtainMessage(mPreviewMessage, cameraResolution.width,
					cameraResolution.height, data);
			message.sendToTarget();
			mDecodeHandler = null;
		} else {
			Log.d("BarcodeCameraPreview", "Got preview callback, but no handler or resolution available");
		}
	}

	public PlanarYUVLuminanceSource buildLuminanceSource(byte[] data, int width, int height) {
		Rect rect = new Rect(0, 0, width, height);

		// Go ahead and assume it's YUV rather than die.
		return new PlanarYUVLuminanceSource(data, width, height, rect.left, rect.top, rect.width(), rect.height(),
				false);
	}

	public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {

		if (mListener != null) {
			mListener.read(rawResult.getText());
		}
		restartPreviewAfterDelay(200L);

	}

	public void stopDecode() {
		if (mPreviewHandler != null) {
			mPreviewHandler.sendEmptyMessage(R.id.quit);

		}
	}

	public void restartPreview() {
		if (mPreviewHandler != null) {
			mPreviewHandler.sendEmptyMessage(R.id.restart_preview);
		}

	}

	public void restartPreviewAfterDelay(long delayMS) {
		if (mPreviewHandler != null) {
			mPreviewHandler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
		}
	}
}