package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Pager;

public class FetchAvailablePagers extends Command<List<Pager>> {

	Facility m_facility;

	public FetchAvailablePagers(Facility facility) {

		this(facility, null, null);
	}

	protected FetchAvailablePagers(Facility facility, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_facility = facility;
	}

	@Override
	public void executeSource() {

		m_dataSource.getPagersDAO().listAvailablePagers(m_facility, this);

	}

	@Override
	public void handleResponse(List<Pager> data) {
		m_model.setAvailablePagers(data);
	}
}