package com.alaris_us.daycareclient.persistence;

import com.alaris_us.daycaredata.DaycareData;


public class PersistenceCommandInvoker
	extends Invoker {

	protected PersistenceCommandInvoker(DaycareData dataSource, PersistenceData model,
			InvokerStatusListener listener) {

		super(dataSource, model, listener);
	}

}
