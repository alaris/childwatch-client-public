package com.alaris_us.daycareclient.persistence;

import java.util.ArrayList;
import java.util.List;

import org.acra.ACRA;

import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycaredata.DaycareData;

public abstract class Invoker implements CommandStatusListener {
	public interface InvokerStatusListener {
		public void started();

		public void finished();

		public void exception(Exception e);
	}

	protected DaycareData			m_dataSource;
	protected PersistenceData		m_model;
	protected List<Command<?>>		m_commandlist	= new ArrayList<Command<?>>();
	protected InvokerStatusListener	m_listener;

	protected Invoker(DaycareData dataSource, PersistenceData model, InvokerStatusListener listener) {
		m_model = model;
		m_dataSource = dataSource;
		m_listener = listener;
	}

	public final void invoke(Command<?> command) {
		command.addListener(this);
		command.setDataSource(m_dataSource);
		command.setPersistentModel(m_model);
		m_commandlist.add(command);
		command.execute();
	}

	public void invoke(Command<?> command, CommandStatusListener listener) {
		command.addListener(listener);
		invoke(command);
	}

	public void starting(Command<?> command) {
//		if (m_listener != null)
			m_listener.started();
	}

	public void finished(Command<?> command) {
		m_commandlist.remove(command);
//		if (m_commandlist.size() > 0)
//			return;
		if (m_listener != null)
			m_listener.finished();
	}

	@Override
	public void valueChanged(Command<?> command) {
		// Placeholder
	}

	public void exception(Command<?> command, Exception e) {
		try {
			ACRA.getErrorReporter().handleException(e);
		} catch (IllegalStateException ex) {
		}
		if (m_listener != null)
			m_listener.exception(e);
	}
}