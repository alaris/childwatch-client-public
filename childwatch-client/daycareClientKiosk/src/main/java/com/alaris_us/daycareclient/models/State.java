package com.alaris_us.daycareclient.models;

public abstract class State {

    public abstract void enterState();

    public abstract void exitState();

    public abstract void exception(Exception e);
}