package com.alaris_us.daycareclient.yombu;

import com.alaris_us.daycaredata.util.ConnectivityUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class YombuCommand extends YombuDAO {

    public YombuCommand(String yombuBaseUrl, ConnectivityUtility connectivityUtility) {
        super(yombuBaseUrl, connectivityUtility);
    }

    protected void createJob(String urlPath, Map<String, Object> body, JSONObject headers,
                             final YombuRESTOperationComplete<YombuWaiverResponse> callback) {
        runYombuRESTTask(YombuREST.RequestType.GET, urlPath, body, headers, new YombuRESTOperationComplete<YombuDataObject>() {
            @Override
            public void operationComplete(YombuDataObject data, YombuRESTException e) {
                if (e != null)
                    callback.operationComplete(null, e);
                else
                    callback.operationComplete(new YombuWaiverResponse(data), null);
            }
        });
    }

    protected void generateAccessToken(String urlPath, String id, String apiKey,
                             final YombuRESTOperationComplete<YombuWaiverResponse> callback) {
        runYombuRESTTask(YombuREST.RequestType.GET, urlPath, null, null, new YombuRESTOperationComplete<YombuDataObject>() {
            @Override
            public void operationComplete(YombuDataObject data, YombuRESTException e) {
                if (e != null)
                    callback.operationComplete(null, e);
                else
                    callback.operationComplete(new YombuWaiverResponse(data), null);
            }
        });
    }

    public void fetchWaiver(String urlPath, String documentId, String accessToken,
                                   final YombuRESTOperationComplete<YombuWaiverResponse> callback) {

        JSONObject headers = new JSONObject();
        try {
            headers.put("Access-Token", accessToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        createJob(urlPath + "/" + documentId, null, headers, new YombuRESTOperationComplete<YombuWaiverResponse>() {
            @Override
            public void operationComplete(YombuWaiverResponse data, YombuRESTException e) {
                if (data == null)
                    callback.operationComplete(null, e);
                else {
                    callback.operationComplete(data, null);
                }
            }
        });
    }
}
