package com.alaris_us.daycareclient.services;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.alaris_us.daycareclient.activities.MainActivityScreen;

public class Service_GoToFirstScreen implements Runnable {
	MainActivityScreen activity;
	ScheduledThreadPoolExecutor scheduledSwitchActivityService;
	int delay = 45;

	public Service_GoToFirstScreen(MainActivityScreen activity, int delay) {
		this.activity = activity;
		this.delay = delay;
		startService();
	}

	@Override
	public void run() {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				activity.gotoFirstScreen();
			}
		});
	}

	private void startService() {
		scheduledSwitchActivityService = new ScheduledThreadPoolExecutor(1);
		scheduledSwitchActivityService.schedule(Service_GoToFirstScreen.this, delay, TimeUnit.SECONDS);
	}

	public void shutdown() {
		scheduledSwitchActivityService.shutdownNow();
	}

}