package com.alaris_us.daycareclient.fragments;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.AlertDialog;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.alaris_us.daycareclient.camera.BarcodeCameraPreview;
import com.alaris_us.daycareclient.camera.BarcodeCameraPreview.BarcodeCameraPreviewListener;
import com.alaris_us.daycareclient.camera.CameraManager;
import com.alaris_us.daycareclient.models.AuthenticationScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontEditText;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.UiBinder;

import java.util.Locale;

public class AuthenticationScreenFragment extends ScreenSwitcherFragment implements PopupFragment, BarcodeCameraPreviewListener {
	private AuthenticationScreenViewModel mModel;
	private AlertDialog noChildrenDialog, invalidMemberDialog, invalidCheckDirectionDialog, staffOverrideEntryDialog, successfulOverrideDialog,
			resolveAccountConflictDialog, registrationDialog, invalidMemberSignUpDialog, noBirthdateDialog, registrationErrorDialog;
	private View staffOverrideEntryDialogView;
	private FontEditText overridePIN;
	private AnimatorSet mErrorAnimations;
	private BarcodeCameraPreview mPreview;
	private FrameLayout					mCameraDisplay;
	private View mViewFinder;
	private View mBusyView;
	private Button mInvalidMemberSignUpOkButton;
	private boolean mIsServiceSelected, mIsChildcareNumSelected;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		m_layout = inflater.inflate(R.layout.authenticationscreen_layout, container, false);
		

		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mModel = new AuthenticationScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this, getResources().getString(
				R.string.authenticationscreen_tile_enterpin), getResources().getString(R.string.authenticationscreen_tile_invalidpin));
		// Bind text views
		UiBinder.bind(m_layout, R.id.authenticationscreen_tile_enterpin_textbox, "Text", mModel, "EnterPinText", BindingMode.ONE_WAY);
		// Configure pinpad listeners
		m_layout.findViewById(R.id.numpad_button_0).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_1).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_2).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_3).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_4).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_5).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_6).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_7).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_8).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_9).setOnClickListener(new NumpadClickListener());
		m_layout.findViewById(R.id.numpad_button_delete).setOnClickListener(new DeleteClickListener());
		m_layout.findViewById(R.id.numpad_button_enter).setOnClickListener(new EnterClickListener());
        m_layout.findViewById(R.id.authenticationscreen_tile_scancard_textbox).setOnTouchListener(new ManualBarcodeEntryTouchListener());
        m_layout.findViewById(R.id.authenticationscreen_tile_scancard_textbox).setOnFocusChangeListener(new ManualBarcodeEntryFocusChangeListener());
		// Navigation Button(s)
		m_layout.findViewById(R.id.authenticationscreen_tile_back_halfarrow).setOnClickListener(new BackClickListener());

		// Setup Camera
		mCameraDisplay = (FrameLayout) m_layout.findViewById(R.id.authenticationscreen_tile_camerapreview);
		mPreview = new BarcodeCameraPreview(getActivity(), new CameraManager());
		mPreview.setBackground(getResources().getDrawable(R.color.invis));

		mBusyView = m_layout.findViewById(R.id.authenticationscreen_tile_camerabusy);
		

		// Initialize error animations
		Animator colorChange = AnimatorInflater.loadAnimator(getActivity(), R.animator.animator_authenticationscreen_enterpin_container_invalidpin);
		colorChange.setTarget(m_layout.findViewById(R.id.authenticationscreen_tile_enterpin_container));
		Animator translateScreen = AnimatorInflater.loadAnimator(getActivity(), R.animator.animator_authentication_layout_invalidpin);
		translateScreen.setTarget(m_layout);
		mErrorAnimations = new AnimatorSet();
		mErrorAnimations.playTogether(colorChange, translateScreen);
		mErrorAnimations.addListener(new InvalidPinAnimationComplete());
		m_layout.setFocusable(true);
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		mPreview.setDecodeListener(null);
		mCameraDisplay.removeAllViews();
		
		if (mModel != null)
			mModel.disconnect();
	}

	@Override
	public void onResumeFragment() {
		super.onResumeFragment();
		mModel.reset();
		
		mCameraDisplay.removeAllViews();
		if (mPreview != null) {
			ViewGroup parent = (ViewGroup) mPreview.getParent();
			if (parent != null)
				parent.removeView(mPreview);
		}
		mCameraDisplay.addView(mPreview);
		mPreview.setViewFinder(mViewFinder);

		mPreview.setDecodeListener(this);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mModel.disconnect();
		mModel = null;
	}

	public void showInvalidMemberDialog() {
		FrameLayout invalidMemberDialogContainer = new FrameLayout(getActivity());
		View invalidMemberDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_invalidmemberdialog_layout, invalidMemberDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(invalidMemberDialogContainer);
		invalidMemberDialog = builder.create();
		invalidMemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		invalidMemberDialog.setCancelable(false);
		invalidMemberDialog.show();
		ViewGroup.LayoutParams lp = invalidMemberDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_invalidmemberdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_invalidmemberdialog_height);
		invalidMemberDialogView.setLayoutParams(lp);
		((View) invalidMemberDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		invalidMemberDialogView.findViewById(R.id.view_invalidmemberdialog_cancel_button).setOnClickListener(new InvalidMemberDialogCancelClickListener());
		invalidMemberDialogView.findViewById(R.id.view_invalidmemberdialog_override_button).setOnClickListener(new InvalidMemberDialogOverrideClickListener());
	}

	public void showInvalidMemberSignUpDialog() {
		FrameLayout invalidMemberSignUpDialogContainer = new FrameLayout(getActivity());
		View invalidMemberSignUpDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_invalidmembersignupdialog_layout, invalidMemberSignUpDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(invalidMemberSignUpDialogContainer);
		invalidMemberSignUpDialog = builder.create();
		invalidMemberSignUpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		invalidMemberSignUpDialog.setCancelable(false);
		invalidMemberSignUpDialog.show();
		ViewGroup.LayoutParams lp = invalidMemberSignUpDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_invalidmembersignupdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_invalidmembersignupdialog_height);
		invalidMemberSignUpDialogView.setLayoutParams(lp);
		((View) invalidMemberSignUpDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		invalidMemberSignUpDialogView.findViewById(R.id.view_invalidmembersignupdialog_cancel_button).setOnClickListener(new InvalidMemberSignUpDialogCancelClickListener());
		invalidMemberSignUpDialogView.findViewById(R.id.view_invalidmembersignupdialog_override_button).setOnClickListener(new InvalidMemberSignUpDialogOverrideClickListener());
		invalidMemberSignUpDialogView.findViewById(R.id.view_invalidmembersignupdialog_ok_button).setOnClickListener(new InvalidMemberSignUpDialogOkClickListener());
		invalidMemberSignUpDialogView.findViewById(R.id.view_invalidmembersignupdialog_ok_button).setEnabled(false);
		mInvalidMemberSignUpOkButton = (Button) invalidMemberSignUpDialogView.findViewById(R.id.view_invalidmembersignupdialog_ok_button);
		final RadioGroup radioGrpService = (RadioGroup) invalidMemberSignUpDialogView.findViewById(R.id.Radio_Group_Service);
		String[] serviceOptionsArray = new String[0];
		serviceOptionsArray = m_layout.getResources().getStringArray(R.array.signup_service_array);

		// create radio buttons
		for (int i = 0; i < serviceOptionsArray.length; i++) {
			RadioButton radioButton = new RadioButton(invalidMemberSignUpDialogView.getContext());
			radioButton.setText(serviceOptionsArray[i]);
			radioButton.setTextColor(m_layout.getResources().getColor(R.color.white));
			radioButton.setTextSize(m_layout.getResources().getDimension(R.dimen.view_invalidmembersignupdialog_radiobutton_textsize));
			Typeface face = Typeface.createFromAsset(m_activity.getAssets(), "fonts/gotham_medium.ttf");
			radioButton.setTypeface(face);
			radioGrpService.addView(radioButton);
		}

		final RadioGroup radioGrpNumber = (RadioGroup) invalidMemberSignUpDialogView.findViewById(R.id.Radio_Group_NumOfKids);
		String[] numberOptionsArray = new String[0];
		numberOptionsArray = m_layout.getResources().getStringArray(R.array.signup_num_of_kids_array);

		// create radio buttons
		for (int i = 0; i < numberOptionsArray.length; i++) {
			RadioButton radioButton = new RadioButton(invalidMemberSignUpDialogView.getContext());
			radioButton.setText(numberOptionsArray[i]);
			radioButton.setId(i);
			radioButton.setTextColor(m_layout.getResources().getColor(R.color.white));
			radioButton.setTextSize(m_layout.getResources().getDimension(R.dimen.view_invalidmembersignupdialog_radiobutton_textsize));
			Typeface face = Typeface.createFromAsset(m_activity.getAssets(), "fonts/gotham_medium.ttf");
			radioButton.setTypeface(face);
			radioGrpNumber.addView(radioButton);

		}
		UiBinder.bind(invalidMemberSignUpDialogView, R.id.Radio_Group_Service, "OnCheckedChangeListener", this, "RadioGroupServiceCheckedChangeListener");
		UiBinder.bind(invalidMemberSignUpDialogView, R.id.Radio_Group_NumOfKids, "OnCheckedChangeListener", this, "RadioGroupNumCheckedChangeListener");
		TextView serviceTV = (TextView) invalidMemberSignUpDialogView.findViewById(R.id.service_header);
		serviceTV.setPaintFlags(serviceTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		TextView numOfKidsTV = (TextView) invalidMemberSignUpDialogView.findViewById(R.id.service_numofkids);
		numOfKidsTV.setPaintFlags(numOfKidsTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
	}


	public void showInvalidCheckDirection(String dir) {
		dir = dir.toLowerCase(Locale.US).equals("in") ? "In" : "Out";
		String otherDir = dir.equals("In") ? "Out" : "In";
		FrameLayout invalidCheckDirectiondialogContainer = new FrameLayout(getActivity());
		View invalidCheckDirectiondialogView = m_activity.getLayoutInflater().inflate(R.layout.view_invalidcheckdirectiondialog_layout,
				invalidCheckDirectiondialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(invalidCheckDirectiondialogContainer);
		invalidCheckDirectionDialog = builder.create();
		invalidCheckDirectionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		invalidCheckDirectionDialog.setCancelable(false);
		invalidCheckDirectionDialog.show();
		ViewGroup.LayoutParams lp = invalidCheckDirectiondialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nochildrendialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nochildrendialog_height);
		invalidCheckDirectiondialogView.setLayoutParams(lp);
		String title = getResources().getString(R.string.view_invalidcheckdirectiondialog_title_text).replace("[DIR]", dir);
		String msg = getResources().getString(R.string.view_invalidcheckdirectiondialog_message_text).replace("[DIR]", otherDir);
		((TextView) invalidCheckDirectiondialogView.findViewById(R.id.view_invalidcheckdirectiondialog_title)).setText(title);
		((TextView) invalidCheckDirectiondialogView.findViewById(R.id.view_invalidcheckdirectiondialog_message)).setText(msg);
		((View) invalidCheckDirectiondialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		invalidCheckDirectiondialogView.findViewById(R.id.view_invalidcheckdirectiondialog_button).setOnClickListener(
				new InvalidCheckDirectionDialogClickListener());
	}

	public void showNoChildrenDialog() {
		FrameLayout noChildrendialogContainer = new FrameLayout(getActivity());
		View noChildrenDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_nochildrendialog_layout, noChildrendialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noChildrendialogContainer);
		noChildrenDialog = builder.create();
		noChildrenDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		noChildrenDialog.setCancelable(false);
		noChildrenDialog.show();
		ViewGroup.LayoutParams lp = noChildrenDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nochildrendialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nochildrendialog_height);
		noChildrenDialogView.setLayoutParams(lp);
		((View) noChildrenDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		noChildrenDialogView.findViewById(R.id.view_nochildrendialog_button).setOnClickListener(new NoChildrenDialogClickListener());
	}

	public void showResolveAccountConflictDialog() {
		FrameLayout resolveAccountConflictDialogContainer = new FrameLayout(getActivity());
		View resolveAccountConflictDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_resolveaccountconflictdialog_layout,
				resolveAccountConflictDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(resolveAccountConflictDialogContainer);
		resolveAccountConflictDialog = builder.create();
		resolveAccountConflictDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		resolveAccountConflictDialog.setCancelable(false);
		resolveAccountConflictDialog.show();
		ViewGroup.LayoutParams lp = resolveAccountConflictDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_resolveaccountconflictdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_resolveaccountconflictdialog_height);
		resolveAccountConflictDialogView.setLayoutParams(lp);
		((View) resolveAccountConflictDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		resolveAccountConflictDialogView.findViewById(R.id.view_resolveaccountconflictdialog_ok_button).setOnClickListener(
				new ResolveAccountConflictDialogOkClickListener());
	}

	private void dismissInvalidMemberSignUpDialog() {
		if (invalidMemberSignUpDialog != null && invalidMemberSignUpDialog.isShowing())
			invalidMemberSignUpDialog.dismiss();
	}

	private class InvalidMemberSignUpDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissInvalidMemberSignUpDialog();
			mIsServiceSelected = false;
			mIsChildcareNumSelected = false;
			mModel.overrideSuccess();
		}
	}

	private class InvalidMemberSignUpDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mModel.setSelectedService(null);
			mModel.setSelectedChildcareNum(null);
			dismissInvalidMemberSignUpDialog();
		}
	}

	private class InvalidMemberSignUpDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mModel.setSelectedService(null);
			mModel.setSelectedChildcareNum(null);
			dismissInvalidMemberSignUpDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public android.widget.RadioGroup.OnCheckedChangeListener getRadioGroupServiceCheckedChangeListener() {
		return new RadioGroupServiceCheckedChangeListener();
	}

	private class RadioGroupServiceCheckedChangeListener implements android.widget.RadioGroup.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			mIsServiceSelected = true;
			RadioButton rb = (RadioButton) group.findViewById(checkedId);
			mModel.setSelectedService(rb.getText().toString());
			if (mIsServiceSelected && mIsChildcareNumSelected)
				mInvalidMemberSignUpOkButton.setEnabled(true);
		}
	}

	public android.widget.RadioGroup.OnCheckedChangeListener getRadioGroupNumCheckedChangeListener() {
		return new RadioGroupNumCheckedChangeListener();
	}

	private class RadioGroupNumCheckedChangeListener implements android.widget.RadioGroup.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			mIsChildcareNumSelected = true;
			RadioButton rb = (RadioButton) group.findViewById(checkedId);
			mModel.setSelectedChildcareNum("CHILDCARE " + rb.getText().toString());
			if (mIsServiceSelected && mIsChildcareNumSelected)
				mInvalidMemberSignUpOkButton.setEnabled(true);
		}
	}

	private void showStaffOverrideEntryDialog() {
		FrameLayout staffOverrideEntryDialogContainer = new FrameLayout(getActivity());
		staffOverrideEntryDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_staffoverrideentrydialog_layout, staffOverrideEntryDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(staffOverrideEntryDialogContainer);
		staffOverrideEntryDialog = builder.create();
		staffOverrideEntryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		staffOverrideEntryDialog.setCancelable(false);
		staffOverrideEntryDialog.show();
		ViewGroup.LayoutParams lp = staffOverrideEntryDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_height);
		staffOverrideEntryDialogView.setLayoutParams(lp);
		((View) staffOverrideEntryDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		staffOverrideEntryDialogView.findViewById(R.id.view_staffoverrideentrydialog_enter_button).setOnClickListener(
				new StaffOverrideEntryDialogEnterClickListener());
		staffOverrideEntryDialogView.findViewById(R.id.view_staffoverrideentrydialog_cancel_button).setOnClickListener(
				new StaffOverrideEntryDialogCancelClickListener());
	}

	private void showSuccessfulOverrideDialog() {
		FrameLayout successfulOverrideDialogContainer = new FrameLayout(getActivity());
		View successfulOverrideDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_successfuloverridedialog_layout,
				successfulOverrideDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(successfulOverrideDialogContainer);
		successfulOverrideDialog = builder.create();
		successfulOverrideDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		successfulOverrideDialog.setCancelable(false);
		successfulOverrideDialog.show();
		ViewGroup.LayoutParams lp = successfulOverrideDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_height);
		successfulOverrideDialogView.setLayoutParams(lp);
		((View) successfulOverrideDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		successfulOverrideDialogView.findViewById(R.id.view_successfuloverridedialog_ok_button).setOnClickListener(
				new SuccessfulOverrideDialogOkClickListener());
	}

	private void dismissNoChildrenDialog() {
		if (noChildrenDialog != null && noChildrenDialog.isShowing())
			noChildrenDialog.dismiss();
	}

	private void dismissInvalidCheckDirectionDialog() {
		if (invalidCheckDirectionDialog != null && invalidCheckDirectionDialog.isShowing())
			invalidCheckDirectionDialog.dismiss();
	}

	private void dismissInvalidMemberDialog() {
		if (invalidMemberDialog != null && invalidMemberDialog.isShowing())
			invalidMemberDialog.dismiss();
	}

	private void dismissStaffOverrideEntryDialog() {
		if (staffOverrideEntryDialog != null && staffOverrideEntryDialog.isShowing())
			staffOverrideEntryDialog.dismiss();
	}
	
	private String getOverridePIN() {
		overridePIN = (FontEditText) staffOverrideEntryDialogView.findViewById(R.id.view_staffoverrideentrydialog_pin_edittext);
		return overridePIN.getText().toString();
	}

	private class InvalidPinAnimationComplete implements AnimatorListener {
		@Override
		public void onAnimationCancel(Animator animation) {
		}

		@Override
		public void onAnimationEnd(Animator animation) {
			mModel.onErrorAnimationEnd();
		}

		@Override
		public void onAnimationRepeat(Animator animation) {
		}

		@Override
		public void onAnimationStart(Animator animation) {
			mModel.onErrorAnimationStart();
		}
	}

	private class NoChildrenDialogClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoChildrenDialog();
			firstScreen();
		}
	}

	private class InvalidCheckDirectionDialogClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissInvalidCheckDirectionDialog();
			firstScreen();
		}
	}

	private class InvalidMemberDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissInvalidMemberDialog();
			firstScreen();
		}
	}

	private class InvalidMemberDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissInvalidMemberDialog();
			showStaffOverrideEntryDialog();
		}
	}

	private class StaffOverrideEntryDialogEnterClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (mModel.isValidOverridePIN(getOverridePIN())) {
				dismissStaffOverrideEntryDialog();
				showSuccessfulOverrideDialog();
			} else {
				overridePIN.setText("");
				overridePIN.setHint("Invalid PIN, Please Retry");
			}
		}
	}

	private class StaffOverrideEntryDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissStaffOverrideEntryDialog();
			showInvalidMemberDialog();
		}
	}

	private class SuccessfulOverrideDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (successfulOverrideDialog != null && successfulOverrideDialog.isShowing())
				successfulOverrideDialog.dismiss();
			mModel.overrideSuccess();
		}
	}

	private class ResolveAccountConflictDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (resolveAccountConflictDialog != null && resolveAccountConflictDialog.isShowing())
				resolveAccountConflictDialog.dismiss();
			mModel.resolveAccountConflict();
		}
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}

	private class NumpadClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mModel.pressNumpadKey(((Button) v).getText().toString());
		}
	}

	private class DeleteClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mModel.pressNumpadDelete();
		}
	}

	private class EnterClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mModel.pressNumpadEnter();
		}
	}

    private class ManualBarcodeEntryTouchListener implements OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mModel.setInputStateToManualBarcode();
            return false;
        }
    }
    
    private class ManualBarcodeEntryFocusChangeListener implements OnFocusChangeListener {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
        	if (!hasFocus && mModel != null) {
	        	FontEditText et = (FontEditText) m_layout.findViewById(R.id.authenticationscreen_tile_scancard_textbox);
	    		et.setText(null);
	        	mModel.setInputStateToPinPadInput();
	        }
        }
    }

    public String getScanCardText() {
        FontEditText et = (FontEditText) m_layout.findViewById(R.id.authenticationscreen_tile_scancard_textbox);
        String toReturn = et.getText().toString();
        return toReturn;
    }

	public void setScanCardText(String text) {
		FontEditText et = (FontEditText) m_layout.findViewById(R.id.authenticationscreen_tile_scancard_textbox);
		et.setText(text);
	}

	@Override
	public void onKeyPress(char digit) {
		mModel.pressKey(String.valueOf(digit));
	}

	@Override
	public void onEnterKeyPress() {
		mModel.pressEnterKey();
        FontEditText et = (FontEditText) m_layout.findViewById(R.id.authenticationscreen_tile_scancard_textbox);
        et.setText(null);
	}

	public void runErrorAnimation() {
		mErrorAnimations.start();
	}

	@Override
	public void read(String barcodeText) {
		mModel.pressKey(barcodeText);
		mModel.pressEnterKey();		
	}

	@Override
	public void onReady() {
		if (null != mBusyView) {
			mBusyView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onStopped() {
		if (null != mBusyView) {
			mBusyView.setVisibility(View.VISIBLE);
		}
	}
	
	public void showRegistrationMemberDialog() {
		FrameLayout registrationDialogContainer = new FrameLayout(getActivity());
		View invalidMemberDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_registrationdialog_layout,
				registrationDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(registrationDialogContainer);
		registrationDialog = builder.create();
		registrationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		registrationDialog.setCancelable(false);
		registrationDialog.show();
		ViewGroup.LayoutParams lp = invalidMemberDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_registrationdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_registrationdialog_height);
		invalidMemberDialogView.setLayoutParams(lp);
		((View) invalidMemberDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
	}

	public void dismissRegistrationDialog() {
		if (registrationDialog != null && registrationDialog.isShowing())
			registrationDialog.dismiss();
	}

	public void showNoBirthDateDialog() {
		FrameLayout noBirthdatedialogContainer = new FrameLayout(getActivity());
		View noBirthDateDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_nobirthdatedialog_layout, noBirthdatedialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noBirthdatedialogContainer);
		noBirthdateDialog = builder.create();
		noBirthdateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		noBirthdateDialog.setCancelable(false);
		noBirthdateDialog.show();
		ViewGroup.LayoutParams lp = noBirthDateDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nobirthdatedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nobirthdatedialog_height);
		noBirthDateDialogView.setLayoutParams(lp);
		((View) noBirthDateDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		noBirthDateDialogView.findViewById(R.id.view_nobirthdatedialog_button).setOnClickListener(new NoBirthDateDialogClickListener());
	}

	public void dismissNoBirthdateDialog() {
		if (noBirthdateDialog != null && noBirthdateDialog.isShowing())
			noBirthdateDialog.dismiss();
	}

	private class NoBirthDateDialogClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoBirthdateDialog();
			firstScreen();
		}
	}

	public void showRegistrationErrorDialog() {
		FrameLayout registrationErrorDialogContainer = new FrameLayout(getActivity());
		View registrationErrorDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_registrationerrordialog_layout, registrationErrorDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(registrationErrorDialogContainer);
		registrationErrorDialog = builder.create();
		registrationErrorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		registrationErrorDialog.setCancelable(false);
		registrationErrorDialog.show();
		ViewGroup.LayoutParams lp = registrationErrorDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_registrationerrordialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_registrationerrordialog_height);
		registrationErrorDialogView.setLayoutParams(lp);
		((View) registrationErrorDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		registrationErrorDialogView.findViewById(R.id.view_registrationerrordialog_button).setOnClickListener(new RegistrationErrorDialogClickListener());
	}

	public void dismissRegistrationErrorDialog() {
		if (registrationErrorDialog != null && registrationErrorDialog.isShowing())
			registrationErrorDialog.dismiss();
	}

	private class RegistrationErrorDialogClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissRegistrationErrorDialog();
			firstScreen();
		}
	}
}
