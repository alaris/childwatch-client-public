package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.WorkoutArea;

public class CheckInParent extends Command<Member> {

	private final Member m_parent;
	private final List<WorkoutArea> m_selectedAreas;
	private final List<WorkoutArea> m_allAreas;
	private final String m_returnTime;

	public CheckInParent(Member parent, List<WorkoutArea> selectedAreas, List<WorkoutArea> allAreas, String returnTime) { 
		this(parent, selectedAreas, allAreas, returnTime, null, null);
	};

	public CheckInParent(Member parent, List<WorkoutArea> selectedAreas, List<WorkoutArea> allAreas, String returnTime,
			DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_parent = parent;
		m_selectedAreas = selectedAreas;
		m_allAreas = allAreas;
		m_returnTime = returnTime;

	}

	@Override
	public void executeSource() {
		// Added because of concurrent list violation
		List<WorkoutArea> toRemove = new ArrayList<WorkoutArea>();
		for (WorkoutArea w : m_allAreas) {
			if (!m_selectedAreas.contains(w))
				toRemove.add(w);
		}
		
		m_dataSource.getMembersDAO().removeWorkoutAreasNoPersist(m_parent, toRemove);
		m_dataSource.getMembersDAO().addWorkoutAreasNoPersist(m_parent, m_selectedAreas);
		if(m_model.getIsOverride())
			m_parent.setWasOverridden(true);
		m_parent.setEstimatedPickupTime(m_returnTime);
		
		m_dataSource.getMembersDAO().update(m_parent, this);
	}

	@Override
	public void handleResponse(Member member) {
	}

}