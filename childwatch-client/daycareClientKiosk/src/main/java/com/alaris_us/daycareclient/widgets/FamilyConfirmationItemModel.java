package com.alaris_us.daycareclient.widgets;

import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableField;

public class FamilyConfirmationItemModel {

	private TrackableField<Member> m_member;
	private Integer m_width;
	private Integer m_color;

	public FamilyConfirmationItemModel() {
		this(null, null, null);
	}

	public FamilyConfirmationItemModel(Member member, Integer width, Integer color) {
		m_member = new TrackableField<Member>(member);
		m_width = width;
		m_color = color;
	}
	
	public Member getMember() {
		return m_member.get();
	}

	public void setMember(Member member) {
		m_member.set(member);
	}

	public String getMemberName() {
		return m_member.get().getFirstName() + " " + m_member.get().getLastName();
	}

	public int getMemberBG() {
		return m_color;
	}

	public int getWidth() {
		return m_width;
	}
}
