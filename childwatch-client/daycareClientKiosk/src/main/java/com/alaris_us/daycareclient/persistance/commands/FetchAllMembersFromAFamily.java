package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.FamilyAssociation;
import com.alaris_us.daycaredata.to.Member;

public class FetchAllMembersFromAFamily extends Command<List<Member>> {

	private Family m_family;
	List<Member> m_familyMembers;
	List<Member> m_proxyMembers;

	public FetchAllMembersFromAFamily(Family family) {

		this(family, null, null);
	}

	protected FetchAllMembersFromAFamily(Family family, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_family = family;
	}

	@Override
	public void executeSource() {
		m_dataSource.getFamilyAssociationsDAO().fetchAllMembersFromAFamily(m_family, new ListChildrenComplete(this));
	}

	@Override
	public void handleResponse(List<Member> data) {
		List<Member> related = filterMembersByAge(m_familyMembers);
		List<Member> proxies = filterMembersByAge(m_proxyMembers);
		List<Member> validRelated = getValidChildren(related);
		List<Member> validProxies = getValidChildren(proxies);

		m_model.setAllDaycareChildren(related, proxies);
		m_model.setValidRelatedChildren(validRelated);
		m_model.setValidProxyChildren(validProxies);
		if (m_model.getIsOverride())
			m_model.setAllValidDaycareChildren(related, proxies);
		else
			m_model.setAllValidDaycareChildren(validRelated, validProxies);
		/*//if (!data.isEmpty()) {
			m_proxyMembers = new ArrayList<Member>();
			for (FamilyAssociation familyAssociation : data)
				m_proxyMembers.add(familyAssociation.getPMember());
		//}
		
		List<Member> allMembers = new ArrayList<Member>();
		allMembers.addAll(m_familyMembers);
		allMembers.addAll(m_proxyMembers);
		m_dataSource.getMembersDAO().fetchAllObjectsIfNeeded(allMembers, new MembersCallback());*/
		
		/*List<Member> related = filterMembersByAge(m_familyMembers);
		if (data != null) {
			m_proxyMembers = new ArrayList<Member>();
			for (FamilyAssociation familyAssociation : data)
				m_proxyMembers.add(familyAssociation.getPMember());
		}

		List<Member> proxies = filterMembersByAge(m_proxyMembers);
		List<Member> validRelated = getValidChildren(related);
		List<Member> validProxies = getValidChildren(proxies);

		m_model.setAllDaycareChildren(related, proxies);
		m_model.setValidRelatedChildren(validRelated);
		m_model.setValidProxyChildren(validProxies);
		m_model.setAllValidDaycareChildren(validRelated, validProxies);*/
	}

	private class ListChildrenComplete implements DaycareOperationComplete<List<FamilyAssociation>> {
		FetchAllMembersFromAFamily m_listener;

		public ListChildrenComplete(FetchAllMembersFromAFamily listener) {
			m_listener = listener;
		}

		@Override
		public void operationComplete(List<FamilyAssociation> arg0, DaycareDataException arg1) {
			
			m_familyMembers = new ArrayList<Member>();
			if (arg0 != null)
				for (FamilyAssociation familyAssociation : arg0){
					if (familyAssociation.getPMember() != null)
						m_familyMembers.add(familyAssociation.getPMember());
				}
			m_dataSource.getFamilyAssociationsDAO().fetchAllMembersFromAFamily(m_model.getSecondaryFamily(), new ProxyMembersCallback(m_listener));
		}
	}
	
	private class ProxyMembersCallback implements DaycareOperationComplete<List<FamilyAssociation>> {
		FetchAllMembersFromAFamily m_listener;

		public ProxyMembersCallback(FetchAllMembersFromAFamily listener) {
			m_listener = listener;
		}

		@Override
		public void operationComplete(List<FamilyAssociation> arg0, DaycareDataException arg1) {
			
			m_proxyMembers = new ArrayList<Member>();
			if (arg0 != null)
				for (FamilyAssociation familyAssociation : arg0)
					m_proxyMembers.add(familyAssociation.getPMember());
		
		List<Member> allMembers = new ArrayList<Member>();
		allMembers.addAll(m_familyMembers);
		allMembers.addAll(m_proxyMembers);
		m_dataSource.getMembersDAO().fetchAllObjectsIfNeeded(allMembers, m_listener);
		}
	}

	private List<Member> filterMembersByAge(List<Member> familyMembers) {
		List<Member> children = new ArrayList<Member>();
		
		if (m_model.getMinRoomAge() != null && m_model.getMaxRoomAge() != null){
			for (Member member : familyMembers) {
				if (DaycareHelpers.getAgeInYears(member.getBirthDate()) >= m_model.getMinRoomAge())
					if (DaycareHelpers.getAgeInYears(member.getBirthDate()) < m_model.getMaxRoomAge())
						children.add(member);
			}
		}else{
			String formattedDate = DateGenerator.getFormattedDateAtOffset(DateGenerator.CHILDSEPARATOR);
			for (Member member : familyMembers) {
				String memberDate = DateGenerator.getFormattedDate(member.getBirthDate());
				if (memberDate.compareTo(formattedDate) >= 0) {
					children.add(member);
				}
			}
		}
		return children;
	}

	private List<Member> getValidChildren(List<Member> children) {
		List<Member> validChildren = new ArrayList<Member>();
		for (Member child : children) {
			if (child.getIsValidMembership())
				validChildren.add(child);
		}
		return validChildren;
	}
}