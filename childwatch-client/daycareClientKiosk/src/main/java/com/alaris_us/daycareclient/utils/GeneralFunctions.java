package com.alaris_us.daycareclient.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.util.Pair;
import android.util.TypedValue;

import com.alaris_us.daycareclient.app.App;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.DaycareDataTO;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.WorkoutArea;

public class GeneralFunctions {
	public final static double SCREENWIDTH=App.getContext().getResources().getDisplayMetrics().widthPixels;
	public final static double SCREENHEIGHT=App.getContext().getResources().getDisplayMetrics().heightPixels;
	
	public static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
		return iterable == null ? Collections.<T> emptyList() : iterable;
	}

	public static float getFloatResource(Context context, int resource) {
		TypedValue outValue = new TypedValue();
		context.getResources().getValue(resource, outValue, true);
		return outValue.getFloat();
	}
	public static void sortTOList(List<? extends DaycareDataTO> data) {
		if (data != null && !data.isEmpty()) 
			Collections.sort(data, ItemNameComparator);
	}
	
	public static void sortPairList(List<? extends Pair<? extends DaycareDataTO, ?>> data) {
		if (data != null && !data.isEmpty()){
			Collections.sort(data, PairComparator);
		}
	}
	
	public static List<? extends DaycareDataTO> extractTOFromPairs(List<? extends Pair<? extends DaycareDataTO, ?>> pairs){
		List<DaycareDataTO> dataTO = new ArrayList<DaycareDataTO>();
		for(Pair<? extends DaycareDataTO, ?> pair : pairs)
			dataTO.add(pair.first);
		return dataTO;
	}
	
	private static Comparator<Pair<? extends DaycareDataTO, ?>> PairComparator= new Comparator<Pair<? extends DaycareDataTO,?>>() {
		@Override
		public int compare(Pair<? extends DaycareDataTO, ?> item1, Pair<? extends DaycareDataTO, ?> item2) {
			String name1= getName(item1.first);
			String name2 = getName(item2.first);
			return name1.compareToIgnoreCase(name2);
		}
	};

	private static Comparator<DaycareDataTO> ItemNameComparator = new Comparator<DaycareDataTO>() {
		@Override
		public int compare(DaycareDataTO item1, DaycareDataTO item2) {
			String name1 = getName(item1);
			String name2 = getName(item2);
			return name1.compareToIgnoreCase(name2);
		}
	};
	
	private static String getName(DaycareDataTO item){
		String name="";
		if (item instanceof WorkoutArea)
			name = ((WorkoutArea) item).getWorkoutName();
		else if (item instanceof Member)
			name = ((Member) item).getFirstName() + " " + ((Member) item).getLastName();
		else if (item instanceof Cubby)
			name = ((Cubby) item).getName();
		else if (item instanceof Room)
			name = ((Room) item).getName();
		else if (item instanceof Program)
			name = ((Program) item).getName();
		return name;
	}
}