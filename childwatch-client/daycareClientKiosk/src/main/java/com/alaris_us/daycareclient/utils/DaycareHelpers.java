package com.alaris_us.daycareclient.utils;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Duration;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.json.JSONException;

import com.alaris_us.daycaredata.to.Recurrence;
import com.alaris_us.daycaredata.to.Schedule;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class DaycareHelpers {
	public static int LOWER_AGE_CUTTOFF = 18; //inclusive
	public static int UPPER_AGE_CUTTOFF = 18; //inclusive

	public static boolean isAdult(Date ofBirth, int adultAge) {

		return (getAgeInYears(ofBirth) > adultAge);
	}

	public static int getAgeInYears(Date birth) {

		LocalDate localBirth = LocalDate.fromDateFields(birth);
		Period fromNow = new Period(localBirth, LocalDate.now());
		return fromNow.getYears();
	}

	public static int getAgeInMonths(Date birth) {

		LocalDate localBirth = LocalDate.fromDateFields(birth);
		Period fromNow = new Period(localBirth, LocalDate.now());
		return fromNow.getMonths();
	}
	
	public static boolean getIsAgeBetween(Date birth, double lowerBound, double upperBound){
		double age = DaycareHelpers.getAgeInYears(birth);
		int monthsInAge = DaycareHelpers.getAgeInMonths(birth);
		age = age + ((double) monthsInAge / 12);
		if (age >= lowerBound && age < upperBound)
			return true;
		else 
			return false;
	}

	public static int getAgeInYears(DateTime birth) {

		LocalDate localBirth = birth.toLocalDate();
		Period fromNow = new Period(localBirth, LocalDate.now());
		return fromNow.getYears();
	}

	public static int getAgeInMonths(DateTime birth) {

		LocalDate localBirth = birth.toLocalDate();
		Period fromNow = new Period(localBirth, LocalDate.now());
		return fromNow.getMonths();
	}

	public static int getMinutesToNow(Date beforeNow) {

		DateTime beforeNowDT = new DateTime(beforeNow);
		return getMinutesToNow(beforeNowDT);
	}

	public static int getMinutesToNow(DateTime beforeNowDT) {

		DateTime now = new DateTime();
		return Minutes.minutesBetween(beforeNowDT, now).getMinutes();
	}

	public static int getMinutesBetween(DateTime firstDate, DateTime secondDate) {
		
		firstDate = firstDate.withSecondOfMinute(0).withMillisOfSecond(0);
		secondDate = secondDate.withSecondOfMinute(0).withMillisOfSecond(0);
		Duration duration = new Duration(firstDate, secondDate);
		return (int) duration.getStandardMinutes();
	}

	public interface YesNoDialogCallback {

		public void yesClicked();

		public void noClicked();
	}

	public static void createYesNoDialog(Context context, String message,
			final YesNoDialogCallback callback) {
		createYesNoDialog(context, message, "Yes", "No", callback);
	}
	
	public static void createYesNoDialog(Context context, String message, String posText, String negText,
			final YesNoDialogCallback callback) {

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (callback != null) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						callback.yesClicked();
						return;
					case DialogInterface.BUTTON_NEGATIVE:
						callback.noClicked();
						return;
					}
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false)
				.setPositiveButton(posText, dialogClickListener)
				.setNegativeButton(negText, dialogClickListener).show();
	}

	public interface OKDialogCallback {

		public void okClicked();
	}

	public static void createOKDialog(Context context, String message) {

		createOKDialog(context, message, null);
	}

	public static void createOKDialog(Context context, String message,
			final OKDialogCallback callback) {

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (callback != null) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						callback.okClicked();
						return;
					}
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", dialogClickListener).show();
	}
	
	public static String formatName(String string) {
		  char[] chars = string.toLowerCase().toCharArray();
		  boolean found = false;
		  for (int i = 0; i < chars.length; i++) {
		    if (!found && Character.isLetter(chars[i])) {
		      chars[i] = Character.toUpperCase(chars[i]);
		      found = true;
		    } else if (Character.isWhitespace(chars[i]) || chars[i]=='.' || chars[i]=='\'') { // You can add other chars here
		      found = false;
		    }
		  }
		  return String.valueOf(chars);
		}
	
	public static DateTime getScheduleClosingTime(Schedule schedule) {
        DateTime now = new DateTime();
        String dayOfWeek = (now.dayOfWeek()).getAsText();
        boolean isAm = now.get(DateTimeFieldType.halfdayOfDay()) == 0;

        Number close = 0;
        switch (dayOfWeek) {
        case "Monday":
        	if (!isAm && schedule.getMondayAmClose().intValue() > 720 && now.get(DateTimeFieldType.minuteOfDay()) < schedule.getMondayAmClose().intValue())
        		close = schedule.getMondayAmClose();
        	else
        		close = isAm ? schedule.getMondayAmClose() : schedule.getMondayPmClose();
            break;
        case "Tuesday":
        	if (!isAm && schedule.getTuesdayAmClose().intValue() > 720 && now.get(DateTimeFieldType.minuteOfDay()) < schedule.getTuesdayAmClose().intValue())
        		close = schedule.getTuesdayAmClose();
        	else
        		close = isAm ? schedule.getTuesdayAmClose() : schedule.getTuesdayPmClose();
            break;
        case "Wednesday":
        	if (!isAm && schedule.getWednesdayAmClose().intValue() > 720 && now.get(DateTimeFieldType.minuteOfDay()) < schedule.getWednesdayAmClose().intValue())
        		close = schedule.getWednesdayAmClose();
        	else
        		close = isAm ? schedule.getWednesdayAmClose() : schedule.getWednesdayPmClose();
            break;
        case "Thursday":
        	if (!isAm && schedule.getThursdayAmClose().intValue() > 720 && now.get(DateTimeFieldType.minuteOfDay()) < schedule.getThursdayAmClose().intValue())
        		close = schedule.getThursdayAmClose();
        	else
        		close = isAm ? schedule.getThursdayAmClose() : schedule.getThursdayPmClose();
            break;
        case "Friday":
        	if (!isAm && schedule.getFridayAmClose().intValue() > 720 && now.get(DateTimeFieldType.minuteOfDay()) < schedule.getFridayAmClose().intValue())
        		close = schedule.getFridayAmClose();
        	else
        		close = isAm ? schedule.getFridayAmClose() : schedule.getFridayPmClose();
            break;
        case "Saturday":
        	if (!isAm && schedule.getSaturdayAmClose().intValue() > 720 && now.get(DateTimeFieldType.minuteOfDay()) < schedule.getSaturdayAmClose().intValue())
        		close = schedule.getSaturdayAmClose();
        	else
        		close = isAm ? schedule.getSaturdayAmClose() : schedule.getSaturdayPmClose();
            break;
        case "Sunday":
        	if (!isAm && schedule.getSundayAmClose().intValue() > 720 && now.get(DateTimeFieldType.minuteOfDay()) < schedule.getSundayAmClose().intValue())
        		close = schedule.getSundayAmClose();
        	else
        		close = isAm ? schedule.getSundayAmClose() : schedule.getSundayPmClose();
            break;
        }

        DateTime closeTime = now.withHourOfDay(close.intValue() / 60);
        closeTime = closeTime.withMinuteOfHour(close.intValue() % 60);

        return closeTime;
    }
	
	public static DateTime getProgramScheduleClosingTime(Schedule schedule) {
		 LocalTime now = new LocalTime();
		 DateTimeFormatter parseFormat = new DateTimeFormatterBuilder().appendPattern("h:mma").toFormatter();
		 DateTime dateTimeToReturn = new DateTime();
		 dateTimeToReturn = dateTimeToReturn.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
		 LocalTime closingTime;
		try {
			closingTime = LocalTime.parse(schedule.getTimes().getJSONObject(0).getString("close"), parseFormat);
			if (now.isBefore(closingTime)) {
				dateTimeToReturn = closingTime.toDateTimeToday();
			} else {
				closingTime = LocalTime.parse(schedule.getTimes().getJSONObject(1).getString("close"), parseFormat);
				if (now.isBefore(closingTime)) {
					dateTimeToReturn = closingTime.toDateTimeToday();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return dateTimeToReturn;
	}
	
	public static DateTime getLastSunday(){
		DateTime today = new LocalDate().toDateTimeAtStartOfDay();
		//If today's Sunday, return today
		if (today.getDayOfWeek() == 7)
			return today;
		else {
			DateTime lastMonday = today.withDayOfWeek(DateTimeConstants.MONDAY);
			//Subtract one day becasue Joda Time uses ISO weeks with Monday as the start
			DateTime lastSunday = lastMonday.minusDays(1);
			return lastSunday;
		}
	}
	
	public static boolean isProgramAvailable(Schedule schedule, Recurrence recurrence) {
		LocalDate today = new LocalDate();
		if (recurrence.getType().equals("WEEKLY")) {
			LocalDate startDate = new LocalDate(schedule.getStartDate());
			if (startDate.getDayOfWeek() >= recurrence.getDayOfWeek().intValue()) {
				startDate = startDate.plusWeeks(1);
		    }
		    LocalDate startDateOnDayOfWeek = startDate.withDayOfWeek(recurrence.getDayOfWeek().intValue());
		    
		    while (startDateOnDayOfWeek.isBefore(today)) {
		    	startDateOnDayOfWeek = startDateOnDayOfWeek.plusWeeks(1);
		    }
		    if (startDateOnDayOfWeek.isEqual(today)) {
		    	DateTimeFormatter parseFormat = new DateTimeFormatterBuilder().appendPattern("h:mma").toFormatter();
		    	LocalTime now = new LocalTime();
		    	try {
		    		LocalTime openingTime = LocalTime.parse(schedule.getTimes().getJSONObject(0).getString("open"), parseFormat);
					LocalTime closingTime = LocalTime.parse(schedule.getTimes().getJSONObject(0).getString("close"), parseFormat);
					if (now.isAfter(openingTime) && now.isBefore(closingTime))
						return true;
					else {
						if (schedule.getTimes().getJSONObject(1) != null) {
							openingTime = LocalTime.parse(schedule.getTimes().getJSONObject(1).getString("open"), parseFormat);
							closingTime = LocalTime.parse(schedule.getTimes().getJSONObject(1).getString("close"), parseFormat);
							if (now.isAfter(openingTime) && now.isBefore(closingTime))
								return true;
							else
								return false;
						} else {
							return false;
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
		    } else if (startDateOnDayOfWeek.isAfter(today))
		    	return false;
		} else {
			return false;
		}
		return false;
	}
}
