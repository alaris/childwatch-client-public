package com.alaris_us.daycareclient.persistance.commands;

import android.util.Log;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Member;

public class AuthenticateByBarcode extends Command<Member> {

	private String m_barcode;

	public AuthenticateByBarcode(String barcode) {
		this(barcode, null, null);
	};

	public AuthenticateByBarcode(String credential, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_barcode = credential;
	}

	@Override
	public void executeSource() {
		Log.e(AuthenticateByBarcode.class.getSimpleName(), "BARCODE: "+m_barcode);
		Log.e(AuthenticateByBarcode.class.getSimpleName(), "BARCODE LENGTH: "+m_barcode.length());
		m_dataSource.getMembersDAO().getMemberByBarcode(m_barcode, this);
	}

	@Override
	public void handleResponse(Member member) {

	}

}