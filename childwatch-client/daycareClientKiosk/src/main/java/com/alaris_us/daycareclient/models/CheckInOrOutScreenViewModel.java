package com.alaris_us.daycareclient.models;

import com.alaris_us.daycareclient.persistence.PersistenceData.InOrOut;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;

public class CheckInOrOutScreenViewModel extends ViewModel {
	@Override
	protected void onReset() {
		mInOrOut = null;
	}

	@Override
	protected void onPersistModel() {
		getPersistenceLayer().getPersistenceData().setInOrOut(mInOrOut);
	}

	private InOrOut		mInOrOut;

	public CheckInOrOutScreenViewModel(PersistenceLayer persistenceLayer) {
		super(persistenceLayer);
	}

	private void setDirection(InOrOut direction) {
		mInOrOut = direction;
		persistModel();
	}

	public String checkOut() {
		setDirection(InOrOut.OUT);
			return "PAGERS";
	}

	public String checkIn() {
		setDirection(InOrOut.IN);
		return "AUTH";
	}
}
