package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.alaris_us.daycareclient_dev.R;

public class FontCheckBox extends CheckBox {

	private static final String FONT_DIRECTORY = "fonts/";

	public FontCheckBox(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init(attrs);

	}

	public FontCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(attrs);

	}

	public FontCheckBox(Context context) {
		super(context);

		init(null);
	}

	private void init(AttributeSet attrs) {

		if (attrs!=null) {

			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FontCheckBox);

			String fontName = a
					.getString(R.styleable.FontCheckBox_fontName);

			if (fontName != null) {
				try {
					Typeface myTypeface = Typeface.createFromAsset(getContext()
							.getAssets(), FONT_DIRECTORY + fontName);
					setTypeface(myTypeface);
				} catch (Exception e) {
				}
			}
			a.recycle();

		}

	}

}
