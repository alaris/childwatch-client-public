package com.alaris_us.daycareclient.models;

import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

import java.util.List;

import android.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;

import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.MemberCheckBox;
import com.alaris_us.daycareclient.widgets.MemberCheckBoxModel;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class CheckOutScreenViewModel extends ViewModel {
	private State m_currentState;

	public CheckOutScreenViewModel(PersistenceLayer persistenceLayer, ScreenSwitcherFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
		reset();
	}

	private final int showProxyRes = R.string.view_navigation_withbuttons_showproxy,
			hideProxyRes = R.string.view_navigation_withbuttons_hideproxy,
			selectAllRes = R.string.view_navigation_withbuttons_selectall,
			selectNoneRes = R.string.view_navigation_withbuttons_selectnone;
	private final int selectAllWithProxyTextSize = R.dimen.view_navigation_withbuttons_selectall_textsize,
			selectAllWithoutProxyTextSize = R.dimen.view_navigation_withbuttons_selectall_withoutproxy_textsize;
	private LayoutParams layoutParams = new LayoutParams(0, LayoutParams.MATCH_PARENT);
	private ScreenSwitcherFragment fragment;
	private TrackableBoolean isShowingProxyChildren;
	private TrackableBoolean isProxyButtonVisible;
	private TrackableBoolean isProxyButtonInlayout;
	private TrackableBoolean isAllSelected;
	private TrackableField<TrackableCollection<MemberCheckBoxModel>> m_visibleChildrenModels;
	private TrackableField<TrackableCollection<MemberCheckBoxModel>> m_relatedChildrenModels;
	private TrackableField<TrackableCollection<MemberCheckBoxModel>> m_proxyChildrenModels;
	private TrackableField<TrackableCollection<Member>> m_selectedChildren;
	private TrackableField<Facility> m_facility;
	private TrackableField<AlertDialog> m_emergencyDialog;
	private TrackableField<Family> m_family;
	private TrackableField<AlertDialog> m_noKidsForCheckDirectionDialog;

	public TrackableCollection<MemberCheckBoxModel> getVisibleChildren() {
		return m_visibleChildrenModels.get();
	}

	public void onReset() {
		m_currentState = new AwaitingSelectionState();
		m_currentState.enterState();
	}

	@Override
	protected void onPopulateModelData() {
		if (m_currentState instanceof AwaitingSelectionState) {
			List<Member> relatedChildren, proxyChildren;
			isShowingProxyChildren.set(false);
			isAllSelected.set(false);
			m_facility.set(getPersistenceLayer().getPersistenceData().getFacility());
			relatedChildren = getPersistenceLayer().getPersistenceData().getRelatedOutChildren();
			proxyChildren = getPersistenceLayer().getPersistenceData().getProxyOutChildren();
			m_family.set(getPersistenceLayer().getPersistenceData().getPrimaryFamily());
			m_visibleChildrenModels.get().clear();
			m_relatedChildrenModels.get().clear();
			m_proxyChildrenModels.get().clear();
			m_selectedChildren.get().clear();
			int width;
			if (relatedChildren == null || relatedChildren.isEmpty())
				width = ListViewFunctions.calculateTileWidth(proxyChildren.size());
			else
				width = ListViewFunctions.calculateTileWidth(relatedChildren.size());
			int pos = 0;
			for (Member child : relatedChildren) {
				MemberCheckBoxModel model = new MemberCheckBoxModel(child, width, ListViewFunctions.getColorAt(pos++));
				m_relatedChildrenModels.get().add(model);
			}
			for (Member child : proxyChildren) {
				MemberCheckBoxModel model = new MemberCheckBoxModel(child, width, ListViewFunctions.getColorAt(pos++));
				m_proxyChildrenModels.get().add(model);
			}
			if (m_relatedChildrenModels.get().isEmpty()) {
				m_visibleChildrenModels.get().addAll(m_proxyChildrenModels.get());
				isProxyButtonVisible.set(false);
			} else {
				m_visibleChildrenModels.get().addAll(m_relatedChildrenModels.get());
				isProxyButtonVisible.set(!m_proxyChildrenModels.get().isEmpty());
			}
			if (m_visibleChildrenModels.get().isEmpty()) // TODO
				showNoKidsForCheckDirectionDialog();
			else
				dismissNoKidsForCheckDirectionDialog();
		}
	}

	public OnClickListener getNextClickListener() {
		if (!m_selectedChildren.get().isEmpty()) {
			return new NextClickListener();
		} else
			return null;
	}

	public float getNextAlpha() {
		return ListViewFunctions.getTileAlpha(!m_selectedChildren.get().isEmpty());
	}

	public int getProxyButtonText() {
		return isShowingProxyChildren.get() ? hideProxyRes : showProxyRes;
	}

	public int getSelectAllButtonText() {
		return isAllSelected.get() ? selectNoneRes : selectAllRes;
	}

	public int getSelectAllButtonTextSize() {
		return isProxyButtonVisible.get() ? selectAllWithProxyTextSize : selectAllWithoutProxyTextSize;
	}

	public int getProxyButtonVisibility() {
		isProxyButtonInlayout.set(isProxyButtonVisible.get());
		if (isProxyButtonVisible.get())
			return View.VISIBLE;
		else
			return View.GONE;
	}

	public int getScrollPosition() {
		return isShowingProxyChildren.get() ? m_visibleChildrenModels.get().size() - 1 : 0;
	}

	public LayoutParams getSelectAllLayoutParams() {
		layoutParams.weight = isProxyButtonInlayout.get() ? 1.5f : 3;
		return layoutParams;
	}

	public int getNumOfSkips() {
		return m_family.get().getEmergencySkips().intValue();
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof AwaitingSelectionState)
				m_currentState.exitState();
		}
	}

	public class SelectAllClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof AwaitingSelectionState) {
				isAllSelected.set(!isAllSelected.get());
				m_selectedChildren.get().clear();
				for (MemberCheckBoxModel memberModel : m_visibleChildrenModels.get()) {
					if (isAllSelected.get())
						m_selectedChildren.get().add(memberModel.getMember());
					memberModel.setIsSelected(isAllSelected.get());
				}
			}
		}
	}

	public class ShowProxyClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof AwaitingSelectionState) {
				boolean isShowingProxies = !isShowingProxyChildren.get();
				isShowingProxyChildren.set(isShowingProxies);
				if (isShowingProxies) {
					m_visibleChildrenModels.get().addAll(m_proxyChildrenModels.get());
				} else {
					m_visibleChildrenModels.get().removeAll(m_proxyChildrenModels.get());
					for (MemberCheckBoxModel memberModel : m_proxyChildrenModels.get()) {
						memberModel.setIsSelected(false);
						m_selectedChildren.get().remove(memberModel.getMember());
					}
				}
				for (MemberCheckBoxModel memberModel : m_visibleChildrenModels.get())
					memberModel.setWidth(ListViewFunctions.calculateTileWidth(m_visibleChildrenModels.get().size()));
			}
		}
	}

	public class ChildClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> parent, View view, int position,
				long id) {
			if (m_currentState instanceof AwaitingSelectionState) {
				Member member = ((MemberCheckBox) view).getMember();
				MemberCheckBoxModel model = ((MemberCheckBox) view).getModel();
				model.toggleIsSelected();
				if (model.getIsSelected())
					m_selectedChildren.get().add(member);
				else
					m_selectedChildren.get().remove(member);
			}
		}
	}

	public int getEmergencyDialogContainerBG() {
		return android.R.color.transparent;
	}

	public void setEmergencyDialog(AlertDialog alertDialog) {
		m_emergencyDialog.set(alertDialog);
	}

	private void showProgressDialog() {
		if (m_emergencyDialog.get() != null) {
			m_emergencyDialog.get().show();
		}
	}

	private void dismissEmergencyDialog() {
		if (m_emergencyDialog.get() != null && m_emergencyDialog.get().isShowing())
			m_emergencyDialog.get().dismiss();
	}

	public OnClickListener getEmergencyDialogYesClickListener() {
		return new EmergencyDialogYesClickListener();
	}

	public OnClickListener getEmergencyDialogSkipClickListener() {
		return new EmergencyDialogSkipClickListener();
	}

	public class EmergencyDialogYesClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof ShowingDialogState) {
				((ShowingDialogState) m_currentState).exitState(true);
			}
		}
	}

	public class EmergencyDialogSkipClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof ShowingDialogState) {
				((ShowingDialogState) m_currentState).exitState(false);
			}
		}
	}

	public void setNoKidsForCheckDirectionDialog(AlertDialog alertDialog) { // TODO
		m_noKidsForCheckDirectionDialog.set(alertDialog);
	}

	private void showNoKidsForCheckDirectionDialog() {
		if (m_noKidsForCheckDirectionDialog.get() != null) {
			m_noKidsForCheckDirectionDialog.get().show();
		}
	}

	private void dismissNoKidsForCheckDirectionDialog() {
		if (m_noKidsForCheckDirectionDialog.get() != null && m_noKidsForCheckDirectionDialog.get().isShowing())
			m_noKidsForCheckDirectionDialog.get().dismiss();
	}

	public OnClickListener getNoKidsForCheckDirectionClickListener() {
		return new NoKidsForCheckDirectionClickListener();
	}

	public class NoKidsForCheckDirectionClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoKidsForCheckDirectionDialog();
			// fragment.getFragmentActivity().gotoPinScreen();
		}
	}

	@Override
	public void onInitialize() {
		isShowingProxyChildren = new TrackableBoolean();
		isProxyButtonVisible = new TrackableBoolean();
		isProxyButtonInlayout = new TrackableBoolean();
		isAllSelected = new TrackableBoolean();
		m_emergencyDialog = new TrackableField<AlertDialog>();
		m_visibleChildrenModels = new TrackableField<TrackableCollection<MemberCheckBoxModel>>(
				new TrackableCollection<MemberCheckBoxModel>());
		m_relatedChildrenModels = new TrackableField<TrackableCollection<MemberCheckBoxModel>>(
				new TrackableCollection<MemberCheckBoxModel>());
		m_proxyChildrenModels = new TrackableField<TrackableCollection<MemberCheckBoxModel>>(
				new TrackableCollection<MemberCheckBoxModel>());
		m_selectedChildren = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		m_facility = new TrackableField<Facility>();
		m_family = new TrackableField<Family>();
		m_noKidsForCheckDirectionDialog = new TrackableField<AlertDialog>();
		addWatch(CHANGE_ID.RELATEDINOUTCHILDREN);
		addWatch(CHANGE_ID.VALIDPROXYINOUTCHILDREN);
		addWatch(CHANGE_ID.FACILITY);
		addWatch(CHANGE_ID.FAMILY);
	}

	abstract class State {
		public abstract void enterState();

		public void exitState() {
			m_currentState = null;
		}

		public void exception(Exception e) {
			m_currentState = null;
		}
	}

	private class AwaitingSelectionState extends State {
		@Override
		public void enterState() {
			Log.w("AwaitingSelectionState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("AwaitingSelectionState", "ExitState");
			if (!m_selectedChildren.get().isEmpty()) {
				List<Member> children = m_selectedChildren.get();
				getPersistenceLayer().getPersistenceData().setSelectedChildren(children);
				for (int i = 0; i < children.size(); i++) {
					if (children.get(i).listEmergencyNumbers() == null
							|| children.get(i).listEmergencyNumbers().isEmpty()) {
						m_currentState = new ShowingDialogState(m_selectedChildren.get(), m_facility.get());
						break;
					}
					if ((children.get(i).listEmergencyNumbers().get(0).getName() == null || children.get(i)
							.listEmergencyNumbers().get(0).getName().isEmpty())
							|| (children.get(i).listEmergencyNumbers().get(0).getPhone() == null || children.get(i)
									.listEmergencyNumbers().get(0).getPhone().isEmpty())) {
						m_currentState = new ShowingDialogState(m_selectedChildren.get(), m_facility.get());
						break;
					}
					if ((children.get(i).listEmergencyNumbers().get(0).getName() != null || !children.get(i)
							.listEmergencyNumbers().get(0).getName().isEmpty())
							&& (children.get(i).listEmergencyNumbers().get(0).getPhone() != null || !children.get(i)
									.listEmergencyNumbers().get(0).getPhone().isEmpty()) && (i == children.size() - 1)) {
						m_currentState = new FetchCubbiesState(children, m_facility.get());
					}
				}
				m_currentState.enterState();
			} else {
				m_currentState.exception(new Exception("No Children Selected"));
			}
		}

		@Override
		public void exception(Exception e) {
			Log.w("AwaitingSelectionState", "Exception: " + e.getMessage());
			reset();
		}
	}

	private class ShowingDialogState extends State {
		private List<Member> m_children;
		private Facility m_facility;

		public ShowingDialogState(List<Member> children, Facility facilty) {
			m_children = children;
			m_facility = facilty;
		}

		@Override
		public void enterState() {
			Log.w("ShowingDialogState", "EnterState");
			showProgressDialog();
		}

		public void exitState(boolean isReentry) {
			Log.w("ShowingDialogState", "ExitState");
			dismissEmergencyDialog();
			if (isReentry) {
				fragment.nextScreen("REENTRY");
				m_currentState = new FinishedState();
			} else {
				int skips = m_family.get().getEmergencySkips().intValue();
				if (skips <= 0) {
					fragment.firstScreen();
					m_currentState = new FinishedState();
				} else {
					m_family.get().setEmergencySkips(skips - 1);
					m_currentState = new FetchCubbiesState(m_children, m_facility);
				}
			}
			m_currentState.enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("ShowingDialogState", "Exception: " + e.getMessage());
		}
	}

	private class FetchCubbiesState extends State {
		private List<Member> m_children;
		private Facility m_facility;

		public FetchCubbiesState(List<Member> children, Facility facility) {
			m_children = children;
			m_facility = facility;
		}

		@Override
		public void enterState() {
			Log.w("FetchingCubbiesState", "EnterState");
			getPersistenceLayer().fetchMembersCubbies(m_facility, m_children);
			exitState();
		}

		public void exitState() {
			Log.w("FetchingCubbiesState", "ExitState");
			fragment.nextScreen("CONFIRM");
			m_currentState = new FinishedState();
			m_currentState.enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("FetchingCubbiesState", "Exception: " + e.getMessage());
			reset();
		}
	}

	private class FinishedState extends State {
		@Override
		public void enterState() {
			Log.w("FinishedState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("FinishedState", "ExitState");
		}

		@Override
		public void exception(Exception e) {
			Log.w("FinishedState", "Exception: " + e.getMessage());
		}
	}
}
