package com.alaris_us.daycareclient.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.content.res.Resources;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;

import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient_dev.R;

public class Service_DateTime implements Runnable {
	ScheduledThreadPoolExecutor scheduledDateTimeService;
	PersistenceLayer persistenceLayer;
	String time="";
	int textSizeWeekday, textSizeDate, textSizeTime, textSizeAmPm;
	
	public Service_DateTime(PersistenceLayer persistanceLayer, Resources resources){
		this.persistenceLayer=persistanceLayer;
		textSizeWeekday=(int) resources.getDimension(R.dimen.mainscreen_tile_datetime_weekday_textsize);
		textSizeDate=(int) resources.getDimension(R.dimen.mainscreen_tile_datetime_date_textsize);
		textSizeTime=(int) resources.getDimension(R.dimen.mainscreen_tile_datetime_time_textsize);
		textSizeAmPm=(int) resources.getDimension(R.dimen.mainscreen_tile_datetime_ampm_textsize);
		startService();
	}
	@Override
	public void run() {
		time=getCurrentTime();
		persistenceLayer.getPersistenceData().setCurrentDateTime(getFormattedDateTimeString(time));
		//persistenceLayer.notifyListeners();
	}
	private void startService(){
		ScheduledThreadPoolExecutor initialDateTimeService = new ScheduledThreadPoolExecutor(1);
		initialDateTimeService.schedule(this, 0, TimeUnit.SECONDS);
		
		scheduledDateTimeService = new ScheduledThreadPoolExecutor(1);
		scheduledDateTimeService.scheduleWithFixedDelay(Service_DateTime.this, 60-getCurrentSeconds(), 60, TimeUnit.SECONDS);
	}
	public static String getCurrentTime(){
		String cTime="";
		String format="EEEE\nMMMM dd\nh:mm\t\taa";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(format,Locale.US);
		cTime=sdf.format(now);
		return cTime.toUpperCase(Locale.ENGLISH);
	}
	public static int getCurrentSeconds(){
		String cTime="";
		String format="ss";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(format,Locale.US);
		cTime=sdf.format(now);
		return Integer.valueOf(cTime);
	}
	private CharSequence getFormattedDateTimeString(String dateTime) {
		if(dateTime==null)
			return "";
		int posWeekday = dateTime.indexOf("\n", 0);
		int posDate = dateTime.indexOf("\n", posWeekday + 1);
		int posTime = dateTime.indexOf("\t", posDate);
		SpannableString str = new SpannableString(dateTime);

		str.setSpan(new TextAppearanceSpan(null, 0, (int) (textSizeWeekday), null, null), 0,
				posWeekday, 0);
		str.setSpan(new TextAppearanceSpan(null, 0, (int) (textSizeDate), null, null), posWeekday,
				posDate, 0);
		str.setSpan(new TextAppearanceSpan(null, 0, (int) (textSizeTime), null, null), posDate,
				posTime, 0);
		str.setSpan(new TextAppearanceSpan(null, 0, (int) (textSizeAmPm), null, null), posTime,
				dateTime.length(), 0);
		return str;
	}
	public void shutdown(){
		scheduledDateTimeService.shutdownNow();
	}
}