package com.alaris_us.daycareclient.models;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.alaris_us.daycareclient.app.App;
import com.alaris_us.daycareclient.fragments.ThankYouScreenFragment;
import com.alaris_us.daycareclient.persistence.PersistenceData.InOrOut;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Room;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

import android.view.View;
import android.view.View.OnClickListener;

public class ThankYouScreenViewModel extends ViewModel {
	public ThankYouScreenViewModel(PersistenceLayer persistenceLayer, ThankYouScreenFragment fragment) {
		super(persistenceLayer);
		this.m_fragment = fragment;
		reset();
	}

	private ThankYouScreenFragment m_fragment;
	private TrackableField<TrackableCollection<Cubby>> m_familyCubbies;
	private String m_singleCubbyLabel = App.getContext().getString(R.string.thankyouscreen_tile_singlecubby);
	private String m_severalCubbiesLabel = App.getContext().getString(R.string.thankyouscreen_tile_severalcubbies);
	private String m_timeLeftLabel = App.getContext().getString(R.string.thankyouscreen_tile_timeleft_text);
	private TrackableField<TrackableCollection<Member>> m_selectedChildren;
	private TrackableField<InOrOut> m_inOrOut;
	private TrackableField<TrackableCollection<Room>> m_facilityRooms;
	private TrackableField<String> m_timeText;
	private TrackableField<TrackableCollection<Pager>> m_familyPagers;
	private String m_singlePagerLabel = App.getContext().getString(R.string.thankyouscreen_tile_singlepager);
	private String m_severalPagersLabel = App.getContext().getString(R.string.thankyouscreen_tile_severalpagers);

	@Override
	protected void onPopulateModelData() {
		m_selectedChildren.get().clear();
		m_selectedChildren.get().addAll(getPersistenceLayer().getPersistenceData().getSelectedDaycareChildren());
		m_inOrOut.set(getPersistenceLayer().getPersistenceData().getInOrOut());
		m_familyCubbies.set(new TrackableCollection<Cubby>(getPersistenceLayer().getPersistenceData()
				.getFamilyCubbies()));
		m_familyPagers.set(new TrackableCollection<Pager>(getPersistenceLayer().getPersistenceData()
				.getFamilyPagers()));
		m_facilityRooms.set(new TrackableCollection<Room>(getPersistenceLayer().getPersistenceData().getAllRooms()));
		m_timeText.set("");
		calculateTimeText();
	}

	public String getCubbiesText() {
		return createCubbiesText(m_familyCubbies.get());
	}

	private String createCubbiesText(List<Cubby> cubbies) {
		String cubbiesText = "";
		if (cubbies != null && !cubbies.isEmpty()) {
			if (cubbies.size() == 1) {
				cubbiesText = m_singleCubbyLabel + " " + m_familyCubbies.get().get(0).getName();
			} else {
				for (Cubby cubby : cubbies)
					cubbiesText += cubby.getName() + ", ";
				cubbiesText = m_severalCubbiesLabel + " " + cubbiesText.substring(0, cubbiesText.length() - 2);
			}
		}
		return cubbiesText;
	}

	public String getPagersText() {
		return createPagersText(m_familyPagers.get());
	}

	private String createPagersText(List<Pager> pagers) {
		String pagersText = "";
		if (pagers != null && !pagers.isEmpty()) {
			if (pagers.size() == 1) {
				pagersText = m_singlePagerLabel + " " + m_familyPagers.get().get(0).getName();
			} else {
				for (Pager pager: pagers)
					pagersText += pager.getName() + ", ";
				pagersText = m_severalPagersLabel + " " + pagersText.substring(0, pagersText.length() - 2);
			}
		}
		return pagersText;
	}

	public void calculateTimeText() {
		String timeText = "";
		if (m_inOrOut.get().equals(InOrOut.IN)) {
			DateTimeFormatter formatter = DateTimeFormat.forPattern("hh:mm a");
			timeText = m_timeLeftLabel + " " + formatter.print(getPersistenceLayer().getPersistenceData().getReturnTime());
			m_timeText.set(timeText);
		}
	}
	
	public String getTimeText() {
		return m_timeText.get();
	}

	@Override
	public void onInitialize() {
		m_familyCubbies = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		m_selectedChildren = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		m_inOrOut = new TrackableField<InOrOut>();
		m_facilityRooms = new TrackableField<TrackableCollection<Room>>(new TrackableCollection<Room>());
		m_familyPagers = new TrackableField<TrackableCollection<Pager>>(new TrackableCollection<Pager>());
		m_timeText = new TrackableField<String>("");
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_fragment.nextScreen();
		}
	}
}
