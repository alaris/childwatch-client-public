package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class EmergencyContactItem extends RelativeLayout implements BoundUi<EmergencyContactItemModel>{

	private EmergencyContactItemModel m_data;
	
	private View m_itemContainer;
	private TextView m_itemMemberName;
	private TextView m_itemContactName1;
	private TextView m_itemContactName2;
	private TextView m_itemContactNumber1;
	private TextView m_itemContactNumber2;
	
	private View m_formContainer;
	private TextView m_formMemberName;
	private EditText m_formContactName1;
	private EditText m_formContactName2;
	private EditText m_formContactNumber1;
	private EditText m_formContactNumber2;
	private Button m_formClear;
	private Button m_formSetForAll;
	
	private View m_itemView;
	private ListItemDialog m_formDialog;
	
	public EmergencyContactItem(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public EmergencyContactItem(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		m_itemView=View.inflate(context, R.layout.view_listitem_emergencylistadapter, this);
		m_itemContainer = m_itemView.findViewById(R.id.view_emergencylistitem_container);
		m_itemMemberName = (TextView) m_itemView.findViewById(R.id.view_emergencyitem_name);
		m_itemContactName1 = (TextView) m_itemView.findViewById(R.id.view_emergencyitem_contact1);
		m_itemContactName2 = (TextView) m_itemView.findViewById(R.id.view_emergencyitem_contact2);
		m_itemContactNumber1 = (TextView) m_itemView.findViewById(R.id.view_emergencyitem_phone1);
		m_itemContactNumber2 = (TextView) m_itemView.findViewById(R.id.view_emergencyitem_phone2);
		
		m_formDialog=new ListItemDialog(context, R.layout.emergencyscreen_tile_emergencyform);
		m_formContainer = m_formDialog.findViewById(R.id.emergencyscreen_tile_emergencyform_container);
		m_formMemberName = (TextView) m_formDialog.findViewById(R.id.emergencyscreen_tile_emergencyform_childname);
		m_formContactName1 = (FontEditText) m_formDialog.findViewById(R.id.emergencyscreen_tile_emergency_textinputs_contact1_name);
		m_formContactName2 = (FontEditText) m_formDialog.findViewById(R.id.emergencyscreen_tile_emergency_textinputs_contact2_name);
		m_formContactNumber1 = (FontEditText) m_formDialog.findViewById(R.id.emergencyscreen_tile_emergency_textinputs_contact1_number);
		m_formContactNumber2 = (FontEditText) m_formDialog.findViewById(R.id.emergencyscreen_tile_emergency_textinputs_contact2_number);
		m_formClear = (Button) m_formDialog.findViewById(R.id.emergencyscreen_tile_emergencyform_clear);
		m_formSetForAll = (Button) m_formDialog.findViewById(R.id.emergencyscreen_tile_emergencyform_setforall);
		
		m_formContactNumber1.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		m_formContactNumber2.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		m_formContactNumber2.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
	}
	
	public Member getMember() {
		return m_data.getMember();
	}
	
	public EmergencyContactItemModel getModel() {
		return m_data; 
	}

	public void setHeight(int height){
		ListViewFunctions.setTileHeight(this, height);
	}
	public void setWidth(int width){
		ListViewFunctions.setTileWidth(this, width);
	}
	
	public void setDisplayedView(boolean isSelected){
		if(isSelected){
			int[] pos=new int[2];
			m_itemView.getLocationOnScreen(pos);
			m_formDialog.setBounds(pos[0], pos[1], m_itemView.getWidth(), m_itemView.getHeight());
			m_formDialog.show();
		}else
			if(m_formDialog!=null)
				m_formDialog.dismiss();
	}
		
	@Override
	public void bind(EmergencyContactItemModel dataSource) {
		m_data = dataSource;
		m_formSetForAll.setTag(m_data);

		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemMemberName, "Text")), m_data, "ItemMemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemContactName1, "Text")), m_data, "ItemContactName1", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemContactName2, "Text")), m_data, "ItemContactName2", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemContactNumber1, "Text")), m_data, "ItemContactNumber1", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemContactNumber2, "Text")), m_data, "ItemContactNumber2", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemContainer, "BackgroundColor")), m_data, "ItemMemberBG", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Width")), m_data, "Width", BindingMode.ONE_WAY);
		
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_formMemberName, "Text")), m_data, "ItemMemberName", BindingMode.ONE_WAY);
		UiBinder.bind(new EditTextTextProperty(m_formContactName1), m_data, "ItemContactName1", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty(m_formContactName2), m_data, "ItemContactName2", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty(m_formContactNumber1), m_data, "ItemContactNumber1", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty(m_formContactNumber2), m_data, "ItemContactNumber2", BindingMode.TWO_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_formContainer, "BackgroundColor")), m_data, "ItemMemberBG", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_formClear, "OnClickListener")), m_data, "ClearClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_formSetForAll, "OnClickListener")), m_data, "SetForAllClickListener", BindingMode.ONE_WAY);
		
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "DisplayedView")), m_data, "IsSelected", BindingMode.ONE_WAY);
	}
}
