package com.alaris_us.daycareclient.persistance.commands;

import android.util.Log;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Member;

public class AuthenticateByBarcodeOrMagCard extends Command<Member> {

	private String m_barcode;

	public AuthenticateByBarcodeOrMagCard(String barcode) {
		this(barcode, null, null);
	};

	public AuthenticateByBarcodeOrMagCard(String credential, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_barcode = credential;
	}

	@Override
	public void executeSource() {
		m_dataSource.getMembersDAO().getMemberByBarcodeOrMagCard(m_barcode, this);
	}

	@Override
	public void handleResponse(Member member) {

	}

}