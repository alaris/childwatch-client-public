package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class MedicalScreenItem extends RelativeLayout implements BoundUi<MedicalScreenItemModel>{

	private MedicalScreenItemModel m_data;
	
	private View m_itemContainer;
	private TextView m_memberName;
	private TextView m_medicalMessage;
	
	private View m_formContainer;
	private TextView m_formMemberName;
	private FontEditText m_formMedicalMessage;
	private Button m_formClear, m_formSetForAll;
	
	private View m_itemView;
	private ListItemDialog m_formDialog;
	
	public MedicalScreenItem(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public MedicalScreenItem(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		m_itemView=View.inflate(context, R.layout.view_listitem_medicallistadapter, this);
		
		m_itemContainer = m_itemView.findViewById(R.id.view_medicallistitem_container);
		m_memberName = (TextView) m_itemView.findViewById(R.id.view_medicalitem_name);
		m_medicalMessage = (TextView) m_itemView.findViewById(R.id.view_medicalitem_medMessage);
		
		m_formDialog=new ListItemDialog(context, R.layout.medicalscreen_tile_medicalform); 
		m_formContainer = m_formDialog.findViewById(R.id.medicalscreen_tile_medicalform_container);
		m_formMemberName = (TextView) m_formDialog.findViewById(R.id.medicalscreen_tile_medicalform_childname);
		m_formMedicalMessage = (FontEditText) m_formDialog.findViewById(R.id.medicalscreen_tile_medicalform_textfield);
		m_formClear = (Button) m_formDialog.findViewById(R.id.medicalscreen_tile_medicalform_clear);
		m_formSetForAll = (Button) m_formDialog.findViewById(R.id.medicalscreen_tile_medicalform_setforall);
	}
	
	public Member getMember() {
		return m_data.getMember();
	}
	
	public MedicalScreenItemModel getModel() {
		return m_data; 
	}

	public void setHeight(int height){
		ListViewFunctions.setTileHeight(this, height);
	}
	
	public void setWidth(int width){
		ListViewFunctions.setTileWidth(this, width);
	}
	
	public void setDisplayedView(boolean isSelected){
		if(isSelected){
			int[] pos=new int[2];
			m_itemView.getLocationOnScreen(pos);
			m_formDialog.setBounds(pos[0], pos[1], m_itemView.getWidth(), m_itemView.getHeight());
			m_formDialog.show();
		}else
			if(m_formDialog!=null)
				m_formDialog.dismiss();
	}
	
	@Override
	public void bind(MedicalScreenItemModel dataSource) {
		m_data = dataSource;
		m_formSetForAll.setTag(m_data);
		
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberName, "Text")), m_data, "MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_medicalMessage, "Text")), m_data, "ItemMedicalMessage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemContainer, "BackgroundColor")), m_data, "MemberBG", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Width")), m_data, "Width", BindingMode.ONE_WAY);
		
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "DisplayedView")), m_data, "IsSelected", BindingMode.ONE_WAY);
		
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_formMemberName, "Text")), m_data, "MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(new EditTextTextProperty(m_formMedicalMessage), m_data, "ItemMedicalMessage", BindingMode.TWO_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_formContainer, "BackgroundColor")), m_data, "MemberBG", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_formClear, "OnClickListener")), m_data, "ClearClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_formSetForAll, "OnClickListener")), m_data, "SetForAllClickListener", BindingMode.ONE_WAY);
	}
}
