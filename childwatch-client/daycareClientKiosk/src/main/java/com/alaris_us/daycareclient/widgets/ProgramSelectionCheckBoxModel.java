package com.alaris_us.daycareclient.widgets;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycaredata.to.Program;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableInt;

import android.view.View;

public class ProgramSelectionCheckBoxModel {

	private Program	m_program;
	private TrackableBoolean		m_isSelected;
	private TrackableInt			m_width;
	private Integer					m_color;

	public ProgramSelectionCheckBoxModel() {
		this(null, null, null);
	}

	public ProgramSelectionCheckBoxModel(Program program, Integer width, Integer color) {
		m_program = program;
		m_isSelected = new TrackableBoolean(false);
		m_width = new TrackableInt(width);
		m_color = color;
	}

	public String getProgramText() {
		return m_program.getName();
	}

	public Program getProgram() {
		return m_program;
	}
	
	public void setProgram(Program program) {
		m_program = program;
	}

	public int getBackgroundColor() {
		return m_color;
	}

	public int getWidth() {
		return m_width.get();
	}

	public void setWidth(int width) {
		m_width.set(width);
	}

	public float getAlpha() {
		return ListViewFunctions.getTileAlpha(m_isSelected.get());
	}

	public boolean getIsSelected() {
		return m_isSelected.get();
	}

	public void setIsSelected(boolean isSelected) {
		m_isSelected.set(isSelected);
	}

	public void toggleIsSelected() {
		setIsSelected(!getIsSelected());
	}

	public int getCheckBoxRes() {
		return m_isSelected.get() ? View.VISIBLE : View.GONE;
	}

}
