package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.externaldata.to.UnitInfo;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class FamilyUnitCheckBox extends RelativeLayout implements BoundUi<FamilyUnitCheckBoxModel>{

	private FamilyUnitCheckBoxModel m_data;
	
	private ImageView m_checkBox;
	private ImageView m_bgImage;
	private TextView m_familyUnitName;
	private TextView m_familyUnitStatus;
	private TextView m_familyUnitMembershipType;
	private TextView m_familyUnitJoinDate;
	
	public FamilyUnitCheckBox(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public FamilyUnitCheckBox(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		View.inflate(getContext(), R.layout.view_familyunititem_layout, this);
		
		m_familyUnitName = (TextView) findViewById(R.id.view_familyunititem_unitname);
		m_familyUnitStatus = (TextView) findViewById(R.id.view_familyunititem_status);
		m_familyUnitMembershipType = (TextView) findViewById(R.id.view_familyunititem_membershiptype);
		m_familyUnitJoinDate = (TextView) findViewById(R.id.view_familyunititem_joindate);
		m_checkBox = (ImageView) findViewById(R.id.view_familyunititem_checkmark);
		m_bgImage = (ImageView) findViewById(R.id.view_familyunititem_bgimage);
	}
	
	public FamilyUnitCheckBoxModel getModel() {
		return m_data; 
	}
	
	public UnitInfo getUnit() {
		return m_data.getUnit();
	}

	public void setHeight(int height){
		ListViewFunctions.setTileHeight(this, height);
	}
	
	public void setWidth(int width){
		ListViewFunctions.setTileWidth(this, width);
	}
	
	@Override
	public void bind(FamilyUnitCheckBoxModel dataSource) {
		m_data = dataSource;
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_familyUnitName, "Text")), m_data, "FamilyUnitName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_familyUnitStatus, "Text")), m_data, "FamilyUnitStatus", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_familyUnitMembershipType, "Text")), m_data, "FamilyUnitMembershipType", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_familyUnitJoinDate, "Text")), m_data, "FamilyUnitJoinDate", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Width")), m_data, "Width", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_checkBox, "Visibility")), m_data, "CheckBoxRes", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_bgImage, "Alpha")), m_data, "Alpha", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_bgImage, "BackgroundColor")), m_data, "BackgroundColor", BindingMode.ONE_WAY);
	}
}
