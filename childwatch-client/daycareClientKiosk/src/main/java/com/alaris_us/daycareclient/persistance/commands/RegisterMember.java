package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.User;

public class RegisterMember extends Command<String> {

	private Facility m_facility;
	private String m_barcode;

	public RegisterMember(Facility facility, String barcode) {

		this(facility, barcode, null, null);
	};

	public RegisterMember(Facility facility, String barcode, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_facility = facility;
		m_barcode = barcode;
	}

	@Override
	public void executeSource() {

		m_dataSource.getMembersDAO().registerMember(m_facility, m_barcode, this);

	}

	@Override
	public void handleResponse(String response) {
	}

}