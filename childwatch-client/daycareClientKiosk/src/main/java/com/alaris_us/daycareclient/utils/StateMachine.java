package com.alaris_us.daycareclient.utils;

import static com.alaris_us.daycareclient.utils.LogUtils.LOGD;

public class StateMachine {
	private State	mCurrentState;
	private String	mStateTag;

	public <T extends State> StateMachine(T initial) {
		mCurrentState = initial;
        mStateTag = LogUtils.makeLogTag(mCurrentState.getClass().getSimpleName());
		enterState();
	}

	public <T extends State> T changeState(T nextState) {
		exitState();
		mCurrentState = nextState;
        mStateTag = LogUtils.makeLogTag(mCurrentState.getClass().getSimpleName());
		enterState();
		return nextState;
	}

	public void stopMachine() {
		exitState();
	}

	public State getCurrentState() {
		return mCurrentState;
	}

	private void enterState() {
		LOGD(mStateTag, "enterState()");
		mCurrentState.onEnter();
	}

	private void exitState() {
		LOGD(mStateTag, "exitState()");
		mCurrentState.onExit();
	}
}
