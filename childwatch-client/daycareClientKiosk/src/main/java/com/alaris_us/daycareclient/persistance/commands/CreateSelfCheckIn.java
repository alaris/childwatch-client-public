package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Authorization;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.SelfCheckIn;

public class CreateSelfCheckIn extends Command<SelfCheckIn> {
	private Member m_member;

	public CreateSelfCheckIn(Member member) {
		this(member, null, null);
	};

	public CreateSelfCheckIn(Member member, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_member = member;
	}

	@Override
	public void executeSource() {
		SelfCheckIn selfCheckIn = m_dataSource.getSelfCheckInsDAO().getEmptyTO();
		selfCheckIn.setPMember(m_member);
		
		m_dataSource.getSelfCheckInsDAO().create(selfCheckIn, this);
	}

	@Override
	public void handleResponse(SelfCheckIn selfCheckIn) {

	}

}