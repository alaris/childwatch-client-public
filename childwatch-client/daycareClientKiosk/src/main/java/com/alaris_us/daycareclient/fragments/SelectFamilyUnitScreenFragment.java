package com.alaris_us.daycareclient.fragments;

import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.models.SelectFamilyUnitScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher.ScrollDir;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FamilyUnitCheckBox;
import com.alaris_us.daycareclient.widgets.FamilyUnitCheckBoxModel;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.externaldata.to.UnitInfo;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class SelectFamilyUnitScreenFragment extends ScreenSwitcherFragment implements PopupFragment {
	SelectFamilyUnitScreenViewModel	m_model;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.selectfamilyunitscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		HorizontalListView userList = (HorizontalListView) m_layout.findViewById(R.id.selectfamilyunitscreen_tile_userlist_container);
		m_model = new SelectFamilyUnitScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		m_layout.findViewById(R.id.view_navigation_next_container).setId(R.id.selectfamilyunitscreen_nav_next);
		m_layout.findViewById(R.id.view_navigation_back_container).setId(R.id.selectfamilyunitscreen_nav_back);
		UiBinder.bind(m_layout, R.id.selectfamilyunitscreen_tile_userlist_container, "Adapter", m_model, "Members",
				BindingMode.TWO_WAY, new AdapterConverter(FamilyUnitCheckBox.class, true, true));
		UiBinder.bind(m_layout, R.id.selectfamilyunitscreen_nav_next, "Alpha", m_model, "NextAlpha",
				BindingMode.ONE_WAY);
		userList.setOnItemClickListener(new MemberClickListener());
		// Navigation Button(s)
		m_layout.findViewById(R.id.selectfamilyunitscreen_nav_back).setOnClickListener(new BackClickListener());
		m_layout.findViewById(R.id.selectfamilyunitscreen_nav_next).setOnClickListener(new NextClickListener());
	}

	@Override
	public void initData() {
		setCanScroll(ScrollDir.BACKWARD, false);
		m_model.reset();
	}

	private class MemberClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			FamilyUnitCheckBox fuc = (FamilyUnitCheckBox) view;
			UnitInfo unit = fuc.getUnit();
			FamilyUnitCheckBoxModel memberModel = fuc.getModel();
			m_model.selectMember(unit, memberModel);
		}
	}

	private class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_model.next();
		}
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}
}
