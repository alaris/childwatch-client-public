package com.alaris_us.daycareclient.screenswitcher;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher.ScrollDir;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherAdapter.FragmentLifecycle;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherAdapter.ScreenManagerListener;

public abstract class ScreenSwitcherFragment extends Fragment implements FragmentLifecycle {
	protected ScreenSwitcherActivity		m_activity;
	protected View							m_layout;
	boolean									m_canScrollF, m_canScrollB, m_isViewCreated, m_isFragmentResumed,
			m_isFragmentPaused, m_isFragmentVisible;
	protected List<ScreenManagerListener>	m_lifecycleListeners	= new ArrayList<ScreenManagerListener>();

	public void onEnterKeyPress() {
		// Stub
	}

	public void onKeyPress(char digit) {
		// Stub
	}
	
	public void onDeleteKeyPress() {
		// Stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_canScrollB = true;
		m_canScrollF = false;
		m_isViewCreated = false;
		m_isFragmentResumed = false;
		m_isFragmentPaused = false;
		m_isFragmentVisible = false;
		m_activity = (ScreenSwitcherActivity) getActivity();
		m_isViewCreated = true;
		return m_layout;
	}

	@Override
	public void onResume() {
		super.onResume();
		for (ScreenManagerListener listener : m_lifecycleListeners)
			listener.onFragmentCreated(this);
	}

	public ScreenSwitcherActivity getFragmentActivity() {
		return m_activity;
	}

	public boolean canScroll(ScrollDir dir) {
		switch (dir) {
		case FORWARD:
			return m_canScrollF;
		case BACKWARD:
			return m_canScrollB;
		default:
			return false;
		}
	}

	public void setCanScroll(ScrollDir dir, boolean value) {
		switch (dir) {
		case FORWARD:
			m_canScrollF = value;
			break;
		case BACKWARD:
			m_canScrollB = value;
			break;
		}
	}

	public boolean isViewCreated() {
		return m_isViewCreated;
	}

	public boolean isFragmentResumed() {
		return m_isFragmentResumed;
	}

	public boolean isFragmentPaused() {
		return m_isFragmentPaused;
	}

	public boolean isFragmentVisible() {
		return m_isFragmentVisible;
	}

	public void initData(){
		
	};

	@Override
	public void onPauseFragment() {
		for (ScreenManagerListener listener : m_lifecycleListeners)
			listener.onFragmentPaused(this);
		m_isFragmentVisible = false;
		m_isFragmentResumed = false;
		m_isFragmentPaused = true;
	}

	@Override
	public void onResumeFragment() {
		// setCanScroll(ScrollDir.BACKWARD, true);
		// setCanScroll(ScrollDir.FORWARD, false);
		initData();
		m_isFragmentResumed = true;
		m_isFragmentPaused = false;
		for (ScreenManagerListener listener : m_lifecycleListeners)
			listener.onFragmentResumed(this);
	}

	@Override
	public void onFragmentVisible() {
		m_isFragmentVisible = true;
		for (ScreenManagerListener listener : m_lifecycleListeners)
			listener.onFragmentVisible(this);
	}

	public void addScreenManagerListener(ScreenManagerListener listener) {
		if (m_lifecycleListeners == null)
			m_lifecycleListeners = new ArrayList<ScreenManagerListener>();
		m_lifecycleListeners.add(listener);
	}

	public void nextScreen(Object... options) {
		m_activity.nextScreen(options);
	}

	public void prevScreen() {
		m_activity.prevScreen();
	}

	public void firstScreen() {
		m_activity.gotoFirstScreen();
	}

	public void refreshNextScreen() {
		m_activity.refeshNextScreen();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (!(activity instanceof PersistanceLayerHost)) {
			throw new ClassCastException(activity.toString() + " must implement PersistentInterfaceHost");
		}
	}

	public interface PersistanceLayerHost {
		PersistenceLayer getPersistanceLayer();
	}
}
