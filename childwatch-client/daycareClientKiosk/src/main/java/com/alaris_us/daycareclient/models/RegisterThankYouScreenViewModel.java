package com.alaris_us.daycareclient.models;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.app.App;
import com.alaris_us.daycareclient.data.DaycareCredential;
import com.alaris_us.daycareclient.data.DaycareCredential.FormOfCredential;
import com.alaris_us.daycareclient.fragments.RegisterThankYouScreenFragment;
import com.alaris_us.daycareclient.models.AuthenticationScreenViewModel.LoginResult;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.externaldata.exception.ExternalDataException;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class RegisterThankYouScreenViewModel extends ViewModel {
	public RegisterThankYouScreenViewModel(PersistenceLayer persistenceLayer, RegisterThankYouScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
		// reset();
	}

	private State m_currentState;
	private TrackableField<String> m_credential;
	private TrackableInt m_pinBG;
	private TrackableField<SignInType> m_signInType;
	private TrackableInt m_dialogMessageRes;
	private TrackableInt m_dialogTitleRes;
	private TrackableField<Facility> m_facility;
	private RegisterThankYouScreenFragment fragment;
	private TrackableField<DaycareCredential> mCredential;
	private String m_pinTextLabel = App.getContext().getString(R.string.registerthankyouscreen_tile_yourpin);

	public void onReset() {
		m_currentState = new AcceptingInputState();
		m_currentState.enterState();
	}

	public void setCredential(String credential, SignInType SignInType) {
		if (m_currentState instanceof AcceptingInputState) {
			this.m_credential.set(credential);
			this.m_signInType.set(SignInType);
			m_currentState.exitState();
		}
	}

	public String getCredential() {
		return this.m_credential.get();
	}

	public int getPinBGColor() {
		return m_pinBG.get();
	}

	public int getDialogMessage() {
		return m_dialogMessageRes.get();
	}

	public int getDialogTitle() {
		return m_dialogTitleRes.get();
	}

	@Override
	protected void onPopulateModelData() {
		if (m_currentState instanceof AcceptingInputState) {
			mCredential.set(getPersistenceLayer().getPersistenceData().getCredential());
			m_facility.set(getPersistenceLayer().getPersistenceData().getFacility());
		} 
	}

	@Override
	public void onPersistModel() {
	}

	public class CheckInNowClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof AcceptingInputState) {
				m_currentState.exitState();
			}
		}
	}

	public class HomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			fragment.getFragmentActivity().gotoFirstScreen();
		}
	}

	@Override
	public void onInitialize() {
		m_facility = new TrackableField<Facility>();
		m_pinBG = new TrackableInt();
		m_signInType = new TrackableField<SignInType>();
		m_dialogMessageRes = new TrackableInt();
		m_dialogTitleRes = new TrackableInt();
		// Need getText and setText for invalid pin animation
		m_credential = new TrackableField<String>() {
			@SuppressWarnings("unused")
			public void setText(String text) {
				set(text);
			}

			@SuppressWarnings("unused")
			public String getText() {
				return get();
			}
		};
		mCredential = new TrackableField<DaycareCredential>();
		addWatch(CHANGE_ID.CREDENTIAL);
		addWatch(CHANGE_ID.FACILITY);
		addWatch(CHANGE_ID.INOROUT);
	}

	private class AuthenticateByCredInDaycareDataCommandListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void valueChanged(Command<?> command) {
			List<Member> members = new ArrayList<Member>();
			Object result = command.getResult();
			if (result instanceof Member) {
				members = new ArrayList<Member>();
				members.add((Member) result);
			} else {
				members = (List<Member>) (List<?>) command.getResult();
			}
			if (members.isEmpty())
				exception(command, new DaycareDataException("EMPTY MEMBER LIST"));
			else if (m_currentState instanceof AuthByCredentialInDaycareState)
				((AuthByCredentialInDaycareState) m_currentState).exitState(members);
			else
				m_currentState.exception(new ExternalDataException("INVALID CURRENT STATE"));
		}

		@Override
		public void exception(Command<?> command, Exception e) {
			m_currentState.exception(e);
		}
	}

	abstract class State {
		public abstract void enterState();

		public void exitState() {
			m_currentState = null;
		}

		public void exception(Exception e) {
			m_currentState = null;
		}
	}

	private class AcceptingInputState extends State {
		@Override
		public void enterState() {
			Log.w("AcceptingInputState", "EnterState");
			mCredential.set(getPersistenceLayer().getPersistenceData().getCredential());
			// m_signInType.set(SignInType.ON_REGISTER);
		}

		@Override
		public void exitState() {
			Log.w("AcceptingInputState", "ExitState");
			m_currentState = new AuthByCredentialInDaycareState();
			m_currentState.enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("AcceptingInputState", "Exception: " + e.getMessage());
		}
	}

	private class AuthByCredentialInDaycareState extends State {
		private String m_pinOrBarcode;

		public AuthByCredentialInDaycareState() {
			m_pinOrBarcode = mCredential.get().getText();
		}

		@Override
		public void enterState() {
			Log.w("AuthByPinInDaycareState", "EnterState");
			if (mCredential.get().getForm() == FormOfCredential.PHONE)
				getPersistenceLayer().authenticateByPinInDaycareData(m_pinOrBarcode,
						new AuthenticateByCredInDaycareDataCommandListener());
			else if (mCredential.get().getForm() == FormOfCredential.BARCODE)
				getPersistenceLayer().authenticateByBarcodeInDaycareData(m_pinOrBarcode,
						new AuthenticateByCredInDaycareDataCommandListener());
			getPersistenceLayer().getPersistenceData().setCredential(
					DaycareCredential.getNewInstance(mCredential.get().getText(), FormOfCredential.ON_REGISTER));
		}

		public void exitState(List<Member> members) {
			Log.w("AuthByPinInDaycareState", "ExitState");
			/*if (members.size() == 1)
				m_currentState = new DaycareMemberFoundState(members.get(0));
			else*/
				m_currentState = new MultipleDaycareMembersFoundState(members);
			m_currentState.enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("AuthByPinInDaycareState", "Exception: " + e.getMessage());
		}
	}

	private class DaycareMemberFoundState extends State {
		private Member m_member;

		public DaycareMemberFoundState(Member member) {
			m_member = member;
		}

		@Override
		public void enterState() {
			Log.w("DaycareMemberFoundState", "EnterState");
			exitState();
		}

		public void exitState() {
			Log.w("DaycareMemberFoundState", "ExitState");
			getPersistenceLayer().selectUser(m_member, new SelectUserCommandListener());
		}

		@Override
		public void exception(Exception e) {
			Log.w("DaycareMemberFoundState", "Exception: " + e.getMessage());
		}
	}

	private class MultipleDaycareMembersFoundState extends State {
		private List<Member> m_members;

		public MultipleDaycareMembersFoundState(List<Member> members) {
			m_members = members;
		}

		@Override
		public void enterState() {
			Log.w("MultipleDaycareMembersFoundState", "EnterState");
			exitState();
		}

		@Override
		public void exitState() {
			Log.w("MultipleDaycareMembersFoundState", "ExitState");
			m_currentState = new FinishedState();
			m_currentState.enterState();
			getPersistenceLayer().getPersistenceData().setValidDaycareMembersWithPin(m_members);
			fragment.nextScreen(LoginResult.MULTIUSER);
		}

		@Override
		public void exception(Exception e) {
			Log.w("MultipleDaycareMembersFoundState", "Exception: " + e.getMessage());
			reset();
		}
	}

	private class FinishedState extends State {
		@Override
		public void enterState() {
			Log.w("FinishedState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("FinishedState", "ExitState");
		}

		@Override
		public void exception(Exception e) {
			Log.w("FinishedState", "Exception: " + e.getMessage());
		}
	}

	public String getPinText() {
		List<String> allPins = new ArrayList<String>();
		List<Member> parents = new ArrayList<Member>();
		for (Member m: getPersistenceLayer().getPersistenceData().getAllYMCAParents()){
			if (m.isActive())
				parents.add(m);
		}
		for (Member parent : parents){
			List<String> parentPins = parent.listUserPINs();
			for (int i = 0; i < parentPins.size(); i++)
				if (!allPins.contains(parentPins.get(i)))
					allPins.add((String) parentPins.get(i));
		}
		StringBuilder pinsText = new StringBuilder();
		for (int j=0; j < allPins.size(); j++){
			pinsText.append(allPins.get(j));
			pinsText.append(", ");
		}
		return m_pinTextLabel + " " + pinsText.substring(0, pinsText.length() - 2);
	}
	
	public class SelectUserCommandListener implements CommandStatusListener{

		@Override
		public void starting(Command<?> command) {
			// TODO Auto-generated method stub
		}

		@Override
		public void finished(Command<?> command) {
			if (getPersistenceLayer().getPersistenceData().getRelatedInChildren().isEmpty()){
				fragment.showNoChildrenOfAgeDialog();
			} else {
				m_currentState = new FinishedState();
				m_currentState.enterState();
				fragment.nextScreen(LoginResult.SINGLEUSER);
			}
			
		}

		@Override
		public void valueChanged(Command<?> command) {
			// TODO Auto-generated method stub
		}

		@Override
		public void exception(Command<?> command, Exception e) {
			// TODO Auto-generated method stub
		}
	}
	
}
