package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Member;

public class UpdateMember extends Command<Member> {

	private final Member m_parent;

	public UpdateMember(Member parent) {
		this(parent, null, null);
	};

	public UpdateMember(Member parent, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_parent = parent;
	}

	@Override
	public void executeSource() {
		m_dataSource.getMembersDAO().update(m_parent, this);
	}

	@Override
	public void handleResponse(Member member) {
	}

}