package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Member;

public class FetchAllChildren extends Command<List<Member>> {
	Member m_member;
	List<Member> m_familyMembers;

	public FetchAllChildren(Member member) {

		this(member, null, null);
	}

	protected FetchAllChildren(Member member, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_member = member;
	}

	@Override
	public void executeSource() {

		m_dataSource.getFamiliesDAO().listFamilyMembers(m_member, new ListChildrenComplete(this));

	}

	@Override
	public void handleResponse(List<Member> data) {
		List<Member> related=filterMembersByAge(m_familyMembers);
		List<Member> proxies=filterMembersByAge(data);
		List<Member> validRelated = getValidChildren(related);
		List<Member> validProxies = getValidChildren(proxies);
		
		m_model.setAllDaycareChildren(related, proxies);
		m_model.setValidRelatedChildren(validRelated);
		m_model.setValidProxyChildren(validProxies);
		m_model.setAllValidDaycareChildren(validRelated, validProxies);

	}
	
	private List<Member> filterMembersByAge(List<Member> familyMembers){
		List<Member> children=new ArrayList<Member>();
		String formattedDate = DateGenerator.getFormattedDateAtOffset(DateGenerator.CHILDSEPARATOR);

		for(Member member: familyMembers){
			String memberDate = DateGenerator.getFormattedDate(member.getBirthDate());
			if (memberDate.compareTo(formattedDate) >= 0) {
				children.add(member);
			}
		}
		return children;
	}
	
	private List<Member>getValidChildren(List<Member> children){
		List<Member> validChildren = new ArrayList<Member>();
		for(Member child: children){
			if (child.getIsValidMembership())
				validChildren.add(child);
		}
		return validChildren;
	}
	
	private class ListChildrenComplete implements DaycareOperationComplete<List<Member>>{
		FetchAllChildren m_listener;
		public ListChildrenComplete(FetchAllChildren listener){
			m_listener=listener;
		}
		@Override
		public void operationComplete(List<Member> arg0, DaycareDataException arg1) {
			m_familyMembers=arg0;
			m_dataSource.getMembersDAO().listProxyChildren(m_member, m_listener);
		}
	}

}