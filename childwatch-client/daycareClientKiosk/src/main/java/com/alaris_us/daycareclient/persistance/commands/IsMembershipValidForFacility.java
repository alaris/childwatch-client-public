package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.ValidMembership;

import java.util.List;

public class IsMembershipValidForFacility extends Command<List<ValidMembership>> {

	Facility m_facility;
	String m_membershipType;

	public IsMembershipValidForFacility(Facility facility, String membershipType) {

		this(facility, membershipType, null, null);
	}

	protected IsMembershipValidForFacility(Facility facility, String membershipType, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_facility = facility;
		m_membershipType = membershipType;
	}

	@Override
	public void executeSource() {

		m_dataSource.getValidMembershipsDAO().fetchMembershipForFacility(m_facility, m_membershipType, this);

	}

	@Override
	public void handleResponse(List<ValidMembership> data) {
	}
}