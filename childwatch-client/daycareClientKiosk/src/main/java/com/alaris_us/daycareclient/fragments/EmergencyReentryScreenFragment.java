package com.alaris_us.daycareclient.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.models.EmergencyReentryScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.EmergencyContactItem;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class EmergencyReentryScreenFragment extends ScreenSwitcherFragment {
	EmergencyReentryScreenViewModel	m_model;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.emergencyreentryscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		HorizontalListView emergencyList = (HorizontalListView) m_layout.findViewById(R.id.emergencyscreen_tile_emergencylist_container);
		m_model = new EmergencyReentryScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		UiBinder.bind(m_layout, R.id.emergencyscreen_tile_emergencylist_container, "Adapter", m_model, "Members",
				BindingMode.TWO_WAY, new AdapterConverter(EmergencyContactItem.class, true, true));
		UiBinder.bind(m_layout, R.id.view_navigation_next_container, "OnClickListener", m_model, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.view_navigation_next_container, "Alpha", m_model, "NextAlpha", BindingMode.ONE_WAY);
		emergencyList.setOnItemClickListener(m_model.new MemberClickListener());
		emergencyList.setOnTouchListener(new ItemTouchListener());

		// Navigation Button(s)
		m_layout.findViewById(R.id.view_navigation_back_container).setOnClickListener(new BackClickListener());
	}

	@Override
	public void initData() {
		m_model.reset();
	}

	@Override
	public void onPauseFragment() {
		m_model.dismissAllDialogs();
		super.onPauseFragment();
	}

	private class ItemTouchListener implements OnTouchListener {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) 
				m_model.dismissAllDialogs();
			else if (MotionEvent.ACTION_UP == event.getAction())
				return v.performClick();
			return false;
		}
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}
}
