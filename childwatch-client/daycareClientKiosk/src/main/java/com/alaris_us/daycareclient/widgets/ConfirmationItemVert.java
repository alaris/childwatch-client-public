package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.DaycareDataTO;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class ConfirmationItemVert extends RelativeLayout implements BoundUi<ConfirmationItemModel>{

	private ConfirmationItemModel m_data;
	
	private ImageView m_itemImage;
	private TextView m_itemName;
	private TextView m_itemRoom;
	
	public ConfirmationItemVert(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public ConfirmationItemVert(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		View.inflate(getContext(), R.layout.view_listitem_summarylistadapter, this);
		
		m_itemImage = (ImageView) findViewById(R.id.view_listitem_summarylistadapter_bgimage);
		m_itemName = (TextView) findViewById(R.id.view_listitem_summarylistadapter_name);
		m_itemRoom = (TextView) findViewById(R.id.view_listitem_summarylistadapter_room);
	}
	
	public DaycareDataTO getItem() {
		return m_data.getItem();
	}
	
	public ConfirmationItemModel getModel() {
		return m_data; 
	}

	public void setHeight(int height){
		ListViewFunctions.setTileHeight(this, height);
	}
	
	@Override
	public void bind(ConfirmationItemModel dataSource) {
		m_data = dataSource;
		
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemName, "Text")), m_data, "ItemName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemImage, "ImageBitmap")), m_data, "ItemImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemImage, "BackgroundColor")), m_data, "BackgroundColor", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Height")), m_data, "Height", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_itemRoom, "Text")), m_data, "ItemRoom", BindingMode.ONE_WAY);
	}
}
