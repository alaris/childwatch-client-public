package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;

public class FetchAllCubbies extends Command<List<Cubby>> {

	Facility m_facility;

	public FetchAllCubbies(Facility facility) {

		this(facility, null, null);
	}

	protected FetchAllCubbies(Facility facility, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_facility = facility;
	}

	@Override
	public void executeSource() {

		m_dataSource.getCubbiesDAO().listAllFacilityCubbies(m_facility, this);

	}

	@Override
	public void handleResponse(List<Cubby> data) {
		m_model.setAllCubbies(data);
	}
}