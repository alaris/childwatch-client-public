package com.alaris_us.daycareclient.widgets;

import android.graphics.Bitmap;
import android.view.View;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;

public class WorkoutAreaCheckBoxModel {

	private TrackableField<WorkoutArea>	m_area;
	private TrackableBoolean			m_isSelected;
	private Integer						m_width;
	private Integer						m_color;

	public WorkoutAreaCheckBoxModel() {
		this(null, null, null);
	}

	public WorkoutAreaCheckBoxModel(WorkoutArea area, Integer width, Integer color) {
		m_area = new TrackableField<WorkoutArea>(area);
		m_isSelected = new TrackableBoolean(false);
		m_width = width;
		m_color = color;

	}

	public WorkoutArea getArea() {
		return m_area.get();
	}

	public void setArea(WorkoutArea area) {
		m_area.set(area);
	}

	public String getAreaName() {
		return m_area.get().getWorkoutName().replaceAll(".(?!$)", "$0\n");
	}

	public Bitmap getAreaImage() {
		return m_area.get().getWorkoutImage();
	}

	public int getBackgroundColor() {
		return m_color;
	}

	public int getWidth() {
		return m_width;
	}

	public float getAlpha() {
		return ListViewFunctions.getTileAlpha(m_isSelected.get());
	}

	public boolean getIsSelected() {
		return m_isSelected.get();
	}

	public void setIsSelected(boolean isSelected) {
		m_isSelected.set(isSelected);
	}

	public void toggleIsSelected() {
		setIsSelected(!getIsSelected());
	}

	public int getCheckBoxRes() {
		return m_isSelected.get() ? View.VISIBLE : View.GONE;
	}

}
