package com.alaris_us.daycareclient.screenswitcher;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import androidx.legacy.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.fragments.AuthenticationScreenFragment;
import com.alaris_us.daycareclient.fragments.CheckInOrOutScreenFragment;
import com.alaris_us.daycareclient.fragments.CheckInScreenFragment;
import com.alaris_us.daycareclient.fragments.CheckOutScreenFragment;
import com.alaris_us.daycareclient.fragments.ConfirmationScreenFragment;
import com.alaris_us.daycareclient.fragments.CubbyScreenFragment;
import com.alaris_us.daycareclient.fragments.QuestionScreenFragment;
import com.alaris_us.daycareclient.fragments.RegisterScreenFragment;
import com.alaris_us.daycareclient.fragments.RegisterThankYouScreenFragment;
import com.alaris_us.daycareclient.fragments.SelectProgramScreenFragment;
import com.alaris_us.daycareclient.fragments.SelectRoomScreenFragment;
import com.alaris_us.daycareclient.fragments.SelectUserScreenFragment;
import com.alaris_us.daycareclient.fragments.WorkoutAreaScreenFragment;
import com.alaris_us.daycareclient.models.AuthenticationScreenViewModel.LoginResult;
import com.alaris_us.daycareclient.persistence.PersistenceData.InOrOut;

public class ScreenSwitcherAdapter extends FragmentStatePagerAdapter {
	private SparseArray<ScreenSwitcherFragment> m_fragments;
	private int m_currFragPos;
	private int m_prevFragPos;
	private ScreenSwitcherFragment m_firstFragment;
	private int NUMSCREENS = 0;
	private ScreenSwitcherActivity m_activity;
	private ScreenSwitcher m_screenSwitcher;
	private boolean m_isScreenSwitching;
	/*
	 * Hashmap that maps ScreenFragment class variables to Instantiated
	 * ScreenFragment variables Description: Decides which is the next fragment
	 * based on current fragments class Key: Fragment class variable Value:
	 * ArrayList of instantiated fragments that the key class can go to next
	 */
	private Map<Class<? extends ScreenSwitcherFragment>, List<ScreenSwitcherFragment>> m_fragmentMap;

	public ScreenSwitcherAdapter(ScreenSwitcherActivity activity, ScreenSwitcher screenSwitcher) {
		super(activity.getFragmentManager());
		m_activity = activity;
		m_screenSwitcher = screenSwitcher;
		screenSwitcher.setAdapter(this);
		screenSwitcher.setOnPageChangeListener(new FragmentVisibleListener());
		m_fragmentMap = m_activity.getFragmentMap();
		m_firstFragment = m_activity.getFirstScreen();
		recalculateNumScreens(0, m_firstFragment);
		m_fragments = new SparseArray<ScreenSwitcherFragment>();
		m_firstFragment.addScreenManagerListener(m_activity);
		m_fragments.put(0, m_firstFragment);
		m_currFragPos = 0;
		m_isScreenSwitching = true;
	}

	@Override
	public Fragment getItem(int position) {
		ScreenSwitcherFragment nextFrag, currFrag;
		nextFrag = m_fragments.get(position);
		if (nextFrag == null) {
			currFrag = m_fragments.get(position - 1);
			nextFrag = nextFragment(position, currFrag);
			nextFrag.addScreenManagerListener(m_activity);
			m_fragments.put(position, nextFrag);
		}
		return nextFrag;
	}

	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
		super.setPrimaryItem(container, position, object);
		if (m_currFragPos != position) {
			m_prevFragPos = m_currFragPos;
			m_currFragPos = position;
		}
		ScreenSwitcherFragment currFrag = (ScreenSwitcherFragment) object;
		ScreenSwitcherFragment prevFrag = (ScreenSwitcherFragment) m_fragments.get(m_prevFragPos);
		if (prevFrag != null && prevFrag.isViewCreated() && !prevFrag.isFragmentPaused()) {
			prevFrag.onPauseFragment();
		}
		if (currFrag != null && currFrag.isViewCreated() && !currFrag.isFragmentResumed()) {
			currFrag.onResumeFragment();
		}
	}

	@Override
	public int getItemPosition(Object item) {
		ScreenSwitcherFragment fragment = (ScreenSwitcherFragment) item;
		int position = m_fragments.indexOfValue(fragment);
		if (position >= 0) {
			return position;
		} else {
			return POSITION_NONE;
		}
	}

	@Override
	public int getCount() {
		return NUMSCREENS;
	}

	private ScreenSwitcherFragment getNextFragment(int index, ScreenSwitcherFragment context, int choice) {
		List<ScreenSwitcherFragment> paths = m_fragmentMap.get(context.getClass());
		if (paths == null)
			return null;
		ScreenSwitcherFragment frag = paths.get(choice);
		if (choice != 0)
			recalculateNumScreens(index, frag);
		return frag;
	}

	public void recalculateNumScreens(int index, ScreenSwitcherFragment frag) {
		NUMSCREENS = index;
		ScreenSwitcherFragment nextFrag = frag;
		while (nextFrag != null) {
			NUMSCREENS++;
			List<ScreenSwitcherFragment> choices = m_fragmentMap.get(nextFrag.getClass());
			if (choices != null)
				nextFrag = choices.get(0);
			else
				nextFrag = null;
		}
		notifyDataSetChanged();
	}

	/*
	 * private SparseArray<ScreenSwitcherFragment> m_fragments; private int
	 * m_currFragPos; private int m_prevFragPos; private ScreenSwitcherFragment
	 * m_firstFragment; private int NUMSCREENS = 0; private
	 * ScreenSwitcherActivity m_activity; private ScreenSwitcher
	 * m_screenSwitcher; private boolean m_isScreenSwitching;
	 */
	public Bundle getCurrentState() {
		Bundle state = new Bundle();
		String[] frags = new String[m_fragments.size()];
		for (int i = 0; i < m_fragments.size(); i++) {
			frags[i] = m_fragments.valueAt(i).getClass().getName();
		}
		state.putStringArray("fragments", frags);
		// state.putSparseParcelableArray("fragments",
		// ((SparseArray<Parcelable>) ((SparseArray<?>)m_fragments)));
		state.putInt("currFragPos", m_currFragPos);
		state.putInt("prevFragPos", m_prevFragPos);
		state.putInt("NUMSCREENS", NUMSCREENS);
		state.putBoolean("isScreenSwitching", m_isScreenSwitching);
		return state;
	}

	public void restoreFromBundle(Bundle state) {
		// m_fragments = ((SparseArray<ScreenSwitcherFragment>)
		// ((SparseArray<?>)state.getSparseParcelableArray("fragments")));
		m_currFragPos = state.getInt("m_currFragPos");
		m_prevFragPos = state.getInt("m_prevFragPos");
		NUMSCREENS = state.getInt("NUMSCREENS");
		m_isScreenSwitching = state.getBoolean("isScreenSwitching");
		m_fragments = new SparseArray<ScreenSwitcherFragment>();
		String[] frags = state.getStringArray("fragments");
		try {
			for (int i = 0; i < frags.length; i++) {
				Class<?> clazz = Class.forName(frags[i]);
				Constructor<?> ctor = clazz.getConstructor();
				Object object = ctor.newInstance();
				m_fragments.put(i, ((ScreenSwitcherFragment) object));
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		notifyDataSetChanged();
	}

	public ScreenSwitcherFragment nextFragment(int index, ScreenSwitcherFragment currFrag, Object... options) {
		ScreenSwitcherFragment fragment = null;
		if (options.length > 0 && options[0] != null) {
			int choice = 0;
			if (currFrag.getClass() == AuthenticationScreenFragment.class) {
				if (options.length > 1 && options[0].equals(LoginResult.SINGLEUSER) && ((String) options[2]).contains("SINGLEPROGRAM")) 
					choice = 0;
				else if (options.length > 1 && options[0].equals(LoginResult.SINGLEUSER))
						choice = 6;
				else if (options.length > 1 && options[0].equals(LoginResult.SINGLEUSER)
						&& options[1].equals(InOrOut.OUT))
					choice = 1;
				else if (options.length > 1 && options[0].equals(LoginResult.CHILDUSER)
						&& options[1].equals(InOrOut.IN))
					choice = 2;
				else if (options.length > 1 && options[0].equals(LoginResult.CHILDUSER)
						&& options[1].equals(InOrOut.OUT))
					choice = 2;
				else if (options.length > 1 && options[0].equals(LoginResult.STAFFUSER))
					choice = 5;
				else if (options[0].equals(LoginResult.MULTIUSER)
						|| options[0].equals(LoginResult.SINGLEUSER_STAFFFACING))
					choice = 2;
				else if (options[0].equals(LoginResult.NEWUSER))
					choice = 3;
				else if (((String) options[0]).contains("PROGRAMS"))
					choice = 2;
			} else if (currFrag.getClass() == RegisterScreenFragment.class) {
				if (((String) options[0]).contains("FAMILYUNIT"))
					choice = 1;
				else if (((String) options[0]).contains("FAMILYCONFIRMATION"))
					choice = 0;
				// else
				// choice = 0;
				// }
				// else if (currFrag.getClass() ==
				// SelectUserScreenFragment.class) {
				// if (options[0].equals(LoginResult.SUCCESS) &&
				// options[1].equals(InOrOut.IN))
				// choice = 1;
				// else if (options[0].equals(LoginResult.SUCCESS) &&
				// options[1].equals(InOrOut.OUT))
				// choice = 2;
				// else
				// choice = 0;
			} else if (currFrag.getClass() == CheckInOrOutScreenFragment.class) {
				if (options[0].equals("PAGERS"))
					choice = 1;
				else
					choice = 0;
			} else if (currFrag.getClass() == SelectUserScreenFragment.class) {
				if (options.length > 1 && options[1].equals(LoginResult.CHILDUSER) && options[0].equals(InOrOut.IN))
					choice = 3;
				else if (options.length > 1 && options[1].equals(LoginResult.CHILDUSER)
						&& options[0].equals(InOrOut.OUT))
					choice = 3;
				else if (options[0].equals(InOrOut.OUT))
					choice = 1;
				else if (options[0].equals(InOrOut.IN))
					choice = 0;
				else if (((String) options[0]).equals("ROOMS"))
					choice = 4;
				else if ((options[0]).equals("CUBBIES"))
					choice = 2;
				else if (((String) options[0]).equals("PROGRAMS"))
					choice = 5;
				else
					choice = 0;
			} else if (currFrag.getClass() == CheckInScreenFragment.class) {
				if (((String) options[0]).contains("CUBBIES"))
					choice = 0;
				else if (((String) options[0]).contains("NOCUBBY") || ((String) options[0]).contains("ROOMS"))
					choice = 1;
				else if (((String) options[0]).contains("WORKOUT"))
					choice = 2;
				else if (((String) options[0]).contains("UPDATE"))
					choice = 2;
				else if (((String) options[0]).contains("CONFIRM"))
					choice = 3;
				else if (((String) options[0]).contains("QUESTION"))
					choice = 4;
				else
					choice = 0;
			} else if (currFrag.getClass() == QuestionScreenFragment.class) {
				if (((String) options[0]).contains("CONFIRM"))
					choice = 2;
				else if (((String) options[0]).contains("WORKOUT"))
					choice = 3;
				else if (((String) options[0]).contains("ROOMS"))
					choice = 1;
				else if (((String) options[0]).contains("CUBBY"))
					choice = 0;
				else
					choice = 0;
			} else if (currFrag.getClass() == CubbyScreenFragment.class) {
				if (((String) options[0]).contains("CONFIRM"))
					choice = 1;
				else if (((String) options[0]).contains("WORKOUT"))
					choice = 2;
				else if (((String) options[0]).contains("ROOMS"))
					choice = 0;
				else
					choice = 0;

			} else if (currFrag.getClass() == CheckOutScreenFragment.class) {
				if (((String) options[0]).contains("CONFIRM"))
					choice = 0;
				else if (((String) options[0]).contains("REENTRY"))
					choice = 1;
				else
					choice = 0;
			} else if (currFrag.getClass() == SelectRoomScreenFragment.class) {
				if (((String) options[0]).contains("WORKOUT"))
					choice = 0;
				else if (((String) options[0]).contains("CONFIRM"))
					choice = 1;
				else
					choice = 0;
			} else if (currFrag.getClass() == SelectProgramScreenFragment.class) {
				if (((String) options[0]).contains("CONFIRM"))
					choice = 1;
				else if (((String) options[0]).contains("ROOMS"))
					choice = 2;
				else if (((String) options[0]).contains("CHECKIN"))
					choice = 0;
				else
					choice = 0;
			} else if (currFrag.getClass() == RegisterThankYouScreenFragment.class) {
				if (options[0].equals(LoginResult.MULTIUSER))
					choice = 0;
				else if (options[0].equals(LoginResult.SINGLEUSER))
					choice = 1;
				else
					choice = 0;
			} else if (currFrag.getClass() == WorkoutAreaScreenFragment.class) {
				if (((String) options[0]).contains("CONFIRM"))
					choice = 1;
				else
					choice = 0;
			} else if (currFrag.getClass() == ConfirmationScreenFragment.class) {
				if (((String) options[0]).contains("OFFER"))
					choice = 1;
				else
					choice = 0;
			}
			fragment = getNextFragment(index, currFrag, choice);
		} else {
			fragment = getNextFragment(index, currFrag, 0);
		}
		return fragment;
	}

	public int getCurrentIndex() {
		return m_currFragPos;
	}

	public ScreenSwitcherFragment getFragmentAtIndex(int index) {
		return m_fragments.get(index);
	}

	public void replaceNextPage(Object... options) {
		ScreenSwitcherFragment prevFrag = m_fragments.get(m_currFragPos);
		ScreenSwitcherFragment currFrag = m_fragments.get(m_currFragPos + 1);
		ScreenSwitcherFragment nextFrag = nextFragment(m_currFragPos + 1, prevFrag, options);
		if (nextFrag == null) {
			nextScreen();
			recalculateNumScreens(m_currFragPos + 1, nextFrag);
		} else if (nextFrag.getClass() != currFrag.getClass()) {
			nextFrag.addScreenManagerListener(m_activity);
			while (m_currFragPos + 1 < m_fragments.size())
				m_fragments.removeAt(m_currFragPos + 1);
			m_fragments.put(m_currFragPos + 1, nextFrag);
			recalculateNumScreens(m_currFragPos + 1, nextFrag);
		}
	}

	public void nextScreen(Object... options) {
		m_isScreenSwitching = true;
		if (options.length > 0 && options[0] != null)
			replaceNextPage(options);
		int item = m_screenSwitcher.getCurrentItem() + 1;
		if (item == NUMSCREENS)
			gotoFirstScreen();
		else
			m_screenSwitcher.setCurrentItem(item, true);
	}

	public void gotoFirstScreen() {
		m_isScreenSwitching = true;
		m_activity.onLoop();
		m_screenSwitcher.setCurrentItem(0, false);
		recalculateNumScreens(0, m_firstFragment);
	}

	public void gotoPinScreen() {
		m_isScreenSwitching = true;
		m_activity.onLoop();
		m_screenSwitcher.setCurrentItem(1, false);
		recalculateNumScreens(0, m_firstFragment);
	}

	public void previousScreen() {
		int item = m_screenSwitcher.getCurrentItem() - 1;
		if (item >= 0) {
			m_isScreenSwitching = true;
			m_screenSwitcher.setCurrentItem(item, true);
		}
	}

	public void refreshNextFragment() {
		m_isScreenSwitching = true;
		ScreenSwitcherFragment frag = getFragmentAtIndex(getCurrentIndex() + 1);
		FragmentTransaction transaction = m_activity.getFragmentManager().beginTransaction();
		if (Build.VERSION.SDK_INT >= 26) {
			transaction.setReorderingAllowed(false);
		}
		transaction.detach(frag).attach(frag).commit();
	}

	public void addScreenPath(Class<? extends ScreenSwitcherFragment> screen, List<ScreenSwitcherFragment> paths) {
		m_fragmentMap.put(screen, paths);
	}

	public boolean isScreenSwitching() {
		return m_isScreenSwitching;
	}

	public interface FragmentLifecycle {
		public void onPauseFragment();

		public void onResumeFragment();

		public void onFragmentVisible();
	}

	public interface ScreenManagerListener {
		public void onLoop();

		public void onFragmentCreated(ScreenSwitcherFragment fragment);

		public void onFragmentPaused(ScreenSwitcherFragment fragment);

		public void onFragmentResumed(ScreenSwitcherFragment fragment);

		public void onFragmentVisible(ScreenSwitcherFragment fragment);
	}

	private class FragmentVisibleListener implements OnPageChangeListener {
		@Override
		public void onPageScrollStateChanged(int arg0) {
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			//if (arg1 == 0 && arg2 == 0) {
				m_isScreenSwitching = false;
				if (m_fragments.get(arg0) != null)
					m_fragments.get(arg0).onFragmentVisible();
			//}
		}

		@Override
		public void onPageSelected(int arg0) {
		}
	}
}
