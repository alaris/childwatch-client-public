package com.alaris_us.daycareclient.widgets;

import android.graphics.Bitmap;
import android.view.View;

import com.alaris_us.daycareclient.utils.ImageFunctions;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

public class MemberCheckBoxModel {

	private TrackableField<Member>	m_member;
	private TrackableBoolean		m_isSelected;
	private TrackableInt			m_width;
	private Integer					m_color;
	private Bitmap					mColorizedImage;
	private TrackableBoolean    mIsNameVisibile;

	public MemberCheckBoxModel() {
		this(null, null, null);
	}

	public MemberCheckBoxModel(Member member, Integer width, Integer color) {
		m_member = new TrackableField<Member>(member);
		m_isSelected = new TrackableBoolean(false);
		m_width = new TrackableInt(width);
		m_color = color;
		mIsNameVisibile = new TrackableBoolean(true);

	}

	public Member getMember() {
		return m_member.get();
	}

	public void setMember(Member member) {
		m_member.set(member);
	}

	public String getMemberName() {
		return m_member.get().getFirstName() + " " + m_member.get().getLastName();
	}

	public Bitmap getMemberImage() {
		if (null == mColorizedImage) {
			mColorizedImage = ImageFunctions.createDirectCopy(m_member.get().getImage());
			ImageFunctions.colorizeBitmap(mColorizedImage, m_color, ImageFunctions.colorizeMix,
					ImageFunctions.colorizeSaturation);
		}
		return mColorizedImage;
	}

	public int getBackgroundColor() {
		return m_color;
	}

	public int getWidth() {
		return m_width.get();
	}

	public void setWidth(int width) {
		m_width.set(width);
	}

	public float getAlpha() {
		return ListViewFunctions.getTileAlpha(m_isSelected.get());
	}

	public boolean getIsSelected() {
		return m_isSelected.get();
	}

	public void setIsSelected(boolean isSelected) {
		m_isSelected.set(isSelected);
	}

	public void toggleIsSelected() {
		setIsSelected(!getIsSelected());
	}

	public int getCheckBoxRes() {
		return m_isSelected.get() ? View.VISIBLE : View.GONE;
	}
	
	public int getNameRes() {
		return mIsNameVisibile.get() ? View.VISIBLE : View.GONE;
	}

	public void setNameRes(boolean isNameVisible) {
		mIsNameVisibile.set(isNameVisible);
	}
	
	public int getValidRes() {
		return m_member.get().getIsValidMembership() ? View.GONE : View.GONE;
	}

}
