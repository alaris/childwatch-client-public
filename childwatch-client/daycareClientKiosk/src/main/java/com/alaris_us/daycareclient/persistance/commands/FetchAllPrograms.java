package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Program;

public class FetchAllPrograms extends Command<List<Program>> {

	Facility	m_facility;

	public FetchAllPrograms(Facility facility) {

		this(facility, null, null);
	}

	protected FetchAllPrograms(Facility facility, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_facility = facility;
	}

	@Override
	public void executeSource() {

		m_dataSource.getFacilitiesDAO().listAvailablePrograms(m_facility, this);

	}

	@Override
	public void handleResponse(List<Program> data) {
		m_model.setAllPrograms(data);
	}
}