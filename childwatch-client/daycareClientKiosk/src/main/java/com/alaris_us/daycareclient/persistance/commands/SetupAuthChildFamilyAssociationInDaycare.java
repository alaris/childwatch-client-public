package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.dao.FamilyAssociations.FamilyType;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.FamilyAssociation;
import com.alaris_us.daycaredata.to.Member;

public class SetupAuthChildFamilyAssociationInDaycare extends Command<FamilyAssociation> {
	private Family m_family;
	private Member m_child;

	public SetupAuthChildFamilyAssociationInDaycare(Family family, Member child) {
		this(family, child, null, null);
	};

	public SetupAuthChildFamilyAssociationInDaycare(Family family, Member child, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_family = family;
		m_child = child;
	}

	@Override
	public void executeSource() {
		FamilyAssociation fa = m_dataSource.getFamilyAssociationsDAO().getEmptyTO();
		fa.setPMember(m_child);
		fa.setPFamily(m_family);
		fa.setFamilyType(FamilyType.PRIMARY);
		
		m_dataSource.getFamilyAssociationsDAO().create(fa, this);
	}

	@Override
	public void handleResponse(FamilyAssociation familyAssociation) {

	}

}