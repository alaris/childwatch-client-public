package com.alaris_us.daycareclient.models;

import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.activities.MainActivityScreen;
import com.alaris_us.daycareclient.fragments.FamilyConfirmationScreenFragment;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.FamilyConfirmationItemModel;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class FamilyConfirmationScreenViewModel extends ViewModel {
	public FamilyConfirmationScreenViewModel(PersistenceLayer persistenceLayer,
			FamilyConfirmationScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private FamilyConfirmationScreenFragment fragment;
	private TrackableField<TrackableCollection<FamilyConfirmationItemModel>> m_allFamilyMembers;

	public TrackableCollection<FamilyConfirmationItemModel> getFamilyMembers() {
		return m_allFamilyMembers.get();
	}

	@Override
	protected void onPopulateModelData() {
		List<Member> members = getPersistenceLayer().getPersistenceData()
				.getAllYMCAMembers();
		m_allFamilyMembers.get().clear();
		int width = ListViewFunctions.calculateTileWidth(members.size());
		int pos = 0;
		for (Member member : members) {
			FamilyConfirmationItemModel model = new FamilyConfirmationItemModel(
					member, width, ListViewFunctions.getColorAt(pos++));
			m_allFamilyMembers.get().add(model);
		}
	}

	@Override
	public void onPersistModel() {
		// Don't need to persist any data here
	}

	public class YesClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			fragment.nextScreen();
		}
	}

	public class NoClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (getPersistenceLayer().getPersistenceData()
					.getRegistrationUnits() != null)
				fragment.prevScreen();
			else
				((MainActivityScreen) fragment.getActivity()).gotoFirstScreen();
		}
	}

	@Override
	public void onInitialize() {
		m_allFamilyMembers = new TrackableField<TrackableCollection<FamilyConfirmationItemModel>>(
				new TrackableCollection<FamilyConfirmationItemModel>());
		addWatch(CHANGE_ID.ALLYMCAMEMBERS);
	}
}
