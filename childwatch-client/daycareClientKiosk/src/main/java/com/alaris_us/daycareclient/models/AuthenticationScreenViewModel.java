package com.alaris_us.daycareclient.models;

import android.util.Log;

import com.alaris_us.daycareclient.data.DaycareCredential;
import com.alaris_us.daycareclient.data.DaycareCredential.FormOfCredential;
import com.alaris_us.daycareclient.fragments.AuthenticationScreenFragment;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceData.InOrOut;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycareclient.utils.State;
import com.alaris_us.daycareclient.utils.StateMachine;
import com.alaris_us.daycaredata.exceptions.DaycareDataExceptionNoObject;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.ValidMembership;
import com.alaris_us.externaldata.ExternalDataOperationComplete;
import com.alaris_us.externaldata.exception.ExternalDataException;
import com.alaris_us.externaldata.to.UnitInfo;
import com.bindroid.trackable.TrackableField;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class AuthenticationScreenViewModel extends ViewModel {
	public AuthenticationScreenViewModel(PersistenceLayer persistenceLayer, AuthenticationScreenFragment fragment,
			String enterPinText, String invalidPinText) {
		super(persistenceLayer);
		mInvalidPinText = invalidPinText;
		mEnterPinText = enterPinText;
		// TODO decouple
		mFragment = fragment;
	}

	@Override
	protected void onInitialize() {
		mInputText = new TrackableField<String>();
        mValidMembers = new ArrayList<Member>();
        allMembers = new ArrayList<Member>();
		ymcaParents = new ArrayList<Member>();
		ymcaChildren = new ArrayList<Member>();
		mAuthorizedChildren = new ArrayList<com.alaris_us.externaldata.to.Member>();
		mSelfCheckInMembers = new ArrayList<Member>();
		mDataSource = getPersistenceLayer().getPersistenceData().getDataSource();
	}

	protected void onReset() {
		// Watches
		addWatch(CHANGE_ID.FACILITY);
		addWatch(CHANGE_ID.INOROUT);
		addWatch(CHANGE_ID.ISOVERRIDE);
		addWatch(CHANGE_ID.ISREGOVERRIDE);
		// Writes
		mCredential = null;
		mValidDaycareMembersWithPin = null;
		mIsOverride = false;
		mIsRegOverride = false;
		mRegistrationUnits = null;
		// Fields
		mDialogMessageRes = 0;
		mDialogTitleRes = 0;
		mMembers = null;
	    mValidMembers = null;
        mNotUsingChildwatch = null;

		// System
		mInputMachine = new StateMachine(new AllInputState());
		mProcessingStateMachine = new StateMachine(new IdleState());
	}

	@Override
	protected void onPersistModel() {
		PersistenceData pd = getPersistenceLayer().getPersistenceData();
		pd.setCredential(mCredential);
		pd.setIsOverride(mIsOverride);
		pd.setIsRegOverride(mIsRegOverride);
		pd.setValidDaycareMembersWithPin(mValidDaycareMembersWithPin);
		pd.setRegistrationUnits(mRegistrationUnits);
	}

	@Override
	protected void onPopulateModelData() {
		PersistenceData pd = getPersistenceLayer().getPersistenceData();
		mInOrOut = pd.getInOrOut();
		mIsStaffFacing = pd.getIsStaffFacing();
		mOverridePIN = pd.getOverridePIN();
		mIsOverride = pd.getIsOverride();
		mFacility = pd.getFacility();
	}

	@Override
	protected void onUpdated() {
		// TODO Auto-generated method stub
		super.onUpdated();
	}

	private static int		PIN_MAX_LENGTH			= 10;
	private static String	STAFF_MEMBERSHIP_STRING	= "STAFF";

	public enum LoginResult {
		SINGLEUSER, MULTIUSER, NOTFOUND, NEWUSER, CHILDUSER, STAFFUSER, SINGLEUSER_STAFFFACING
	}

	// Fields used for UI
	private TrackableField<String>				mInputText;
	// Dependencies
	private final String						mInvalidPinText;
	private final String						mEnterPinText;
	private final AuthenticationScreenFragment	mFragment;
	// Persistent read
	private String								mOverridePIN;
	private InOrOut								mInOrOut;
	private boolean								mIsStaffFacing;
	// Persistent modify
	// Persistent write
	private boolean								mIsOverride;
	private boolean								mIsRegOverride;
	private DaycareCredential					mCredential;
	private List<Member>						mValidDaycareMembersWithPin;
	// System state
	private StateMachine						mInputMachine;
	private StateMachine						mProcessingStateMachine;
	// Fields
	private int									mDialogMessageRes;
	private int									mDialogTitleRes;
	private List<Member>						mMembers;
	private List<UnitInfo>						mRegistrationUnits;
    private List<Member> mValidMembers;
    private List<Member> mNotUsingChildwatch;
    private LoginResult mLoginResult;
    private List<Member> allMembers, ymcaParents, ymcaChildren;
    private List<com.alaris_us.externaldata.to.Member> mAuthorizedChildren;
	private String formattedDate;
	private Date defaultDate;
	private int lastIndex;
	private int loopIndex;
	private String mService;
	private String mChildcareNum;
	private Facility mFacility;
	private int authLoopIndex;
	private Family mExistingChildFamily;
	private HashMap<com.alaris_us.externaldata.to.Member, Family> mFamilyMap = new HashMap<>();
	private String mDataSource;
	private String mAddOn;
	private HashMap<String, List<String>> mPcHashMap = new HashMap<>();
	private int mAdultAddOnLoop;
	private int mAdultAddOnLoopsCompleted;
	private List<Member> mSelfCheckInMembers;

	public void setSelectedService(String service) {
		mService = service;
		getPersistenceLayer().getPersistenceData().setSelectedService(service + " - " + mChildcareNum);
	}

	public void setSelectedChildcareNum(String num) {
		mChildcareNum = num;
		getPersistenceLayer().getPersistenceData().setSelectedService(mService + " - " + num);
	}

	public String getEnterPinText() {
		return mInputText.get();
	}

	public int getDialogMessage() {
		return mDialogMessageRes;
	}

	public int getDialogTitle() {
		return mDialogTitleRes;
	}

	public void resolveAccountConflict() {
		mIsOverride = true;
		getPersistenceLayer().getPersistenceData().setIsOverride(true);
		if (mIsRegOverride)
			mProcessingStateMachine.changeState(new ExternalAuthenticationState());
		else
			mProcessingStateMachine.changeState(new InternalAuthenticationState());
	}

	public void overrideSuccess() {
		resolveAccountConflict();
	}

	public boolean isValidOverridePIN(String pin) {
		return pin.equals(mOverridePIN);
	}

	/**
	 * Fragment calls this method when the error animation starts Input should
	 * not be allowed
	 */
	public void onErrorAnimationStart() {
		mInputMachine.changeState(new BlockInputState());
	}

	/**
	 * Fragment calls this method when the error animation ends Since this is
	 * coming after an error, reset the input method
	 */
	public void onErrorAnimationEnd() {
		mInputMachine.changeState(new AllInputState());
	}

	/**
	 * Fragment calls this method when hardware key is pressed
	 * 
	 * @param digit
	 *            String representation of the digit
	 */
	public void pressKey(String digit) {
		((InputState) mInputMachine.getCurrentState()).pressKey(digit);
	}

	/**
	 * Fragment calls this method when hardware enter key is pressed
	 * 
	 */
	public void pressEnterKey() {
		((InputState) mInputMachine.getCurrentState()).pressEnterKey();
	}

	/**
	 * Fragment calls this method when an on-screen key is pressed
	 * 
	 * @param number
	 *            String representation of the key
	 */
	public void pressNumpadKey(String number) {
		((InputState) mInputMachine.getCurrentState()).pressNumpadKey(number);
	}

	/**
	 * Fragment calls this method when the on-screen enter button is pressed
	 */
	public void pressNumpadEnter() {
		((InputState) mInputMachine.getCurrentState()).pressNumpadEnter();
	}

	/**
	 * Fragment calls this method when the on-screen delete button is pressed
	 */
	public void pressNumpadDelete() {
		((InputState) mInputMachine.getCurrentState()).pressNumpadDelete();
	}

	private void setCredential(String text, FormOfCredential form) {
		mCredential = DaycareCredential.getNewInstance(text, form);
	}

	/*******************************************************************************************************************
	 * 
	 * Calls back to the View
	 *
	 ******************************************************************************************************************/
	private void onAccountConflict() {
		if (null != mFragment)
			mFragment.showResolveAccountConflictDialog();
	}

	private void onNoChildren() {
		mFragment.showNoChildrenDialog();
	}

	private void onInvalidMember() {
		mFragment.showInvalidMemberDialog();
	}

	private void onInvalidMemberSignUp() {
		mFragment.showInvalidMemberSignUpDialog();
	}

	private void onInvalidCheckDirection() {
		mFragment.showInvalidCheckDirection(mInOrOut.name());
	}

	private void onInvalidCredential() {
		mInputText.set(mInvalidPinText);
		mFragment.runErrorAnimation();
	}

	/**
	 * Call this upon successful authentication or required registration
	 */
	private void completeAuthentication(LoginResult result, List<Member> validMembers) {
		mValidDaycareMembersWithPin = validMembers;
		persistModel();
		getPersistenceLayer().getPersistenceData().setLoginResult(result);
		if (LoginResult.SINGLEUSER == result || LoginResult.CHILDUSER == result || LoginResult.STAFFUSER == result) {
			if (getPersistenceLayer().getPersistenceData().getAllPrograms().size() == 1) {
				getPersistenceLayer().getPersistenceData().setSelectedProgram(getPersistenceLayer().getPersistenceData().getAllPrograms().get(0));
				mFragment.nextScreen(result, mInOrOut, "SINGLEPROGRAM");
			} else {
				mFragment.nextScreen("PROGRAMS");
			}
		} else {
			mFragment.nextScreen(result);
		}
	}

	private class IdleState extends State {
	}

	/*******************************************************************************************************************
	 * 
	 * Methods below used for process model
	 *
	 ******************************************************************************************************************/
	/**
	 * Initial state called after any PIN or Barcode entered.
	 * 
	 * Possible future states: <li>ExternalAuthenticationState</li> <li>
	 * DaycareMemberFoundState</li> <li>MultipleDaycareMembersFoundState</li>
	 * <li>InvalidLoginState</li>
	 */
	private class InternalAuthenticationState extends State implements CommandStatusListener {
		@Override
		public void onEnter() {
			if (mCredential.getForm() == FormOfCredential.PHONE) {
				getPersistenceLayer().authenticateByPinInDaycareData(mCredential.getText(), this);
			} else if (mCredential.getForm() == FormOfCredential.BARCODE) {
				getPersistenceLayer().authenticateByBarcodeOrMagCardInDaycareData(mCredential.getText(), this);
			}
		}

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void valueChanged(Command<?> command) {
			Object result = command.getResult();
			if (result instanceof Member) {
				mMembers = new ArrayList<Member>();
				mMembers.add((Member) result);
			} else {
				mMembers = (List<Member>) (List<?>) command.getResult();
			}
			if (mFacility.getSelfCheckInOpts() != null) {
				getPersistenceLayer().fetchSelfCheckInMembers(mCredential, mFacility, new FetchSelfCheckInMembersCommandListener());
			} else {
				// Staff member
				boolean isAMemberOverridden = false;
				if (mMembers.size() == 1 && mMembers.get(0).getMembershipType() != null
						&& mMembers.get(0).getMembershipType().equals(STAFF_MEMBERSHIP_STRING)) {
					completeAuthentication(LoginResult.STAFFUSER, mMembers);
					return;
				}
				// Check for overrides and valid members
				mValidMembers = new ArrayList<Member>();
				mNotUsingChildwatch = new ArrayList<Member>();
				boolean authedChildNotAvail = false;
				for (Member member : mMembers) {
					isAMemberOverridden = isAMemberOverridden || member.getWasOverridden();
					if (DaycareHelpers.getAgeInYears(member.getBirthDate()) >= DaycareHelpers.LOWER_AGE_CUTTOFF) {
						if (DaycareHelpers.getAgeInYears(member.getBirthDate()) <= DaycareHelpers.UPPER_AGE_CUTTOFF)
							if ((member.getPFacility() == null && mInOrOut == InOrOut.OUT)
									|| (member.getPFacility() != null && mInOrOut == InOrOut.IN)) {
								authedChildNotAvail = true;
								continue;
							}
						if (member.getIsValidMembership() && member.getUsesChildWatch() && member.getPFacility() == null)
							// if showing invalids on select user screen
							mValidMembers.add(member);
						if (!member.getUsesChildWatch())
							mNotUsingChildwatch.add(member);
					}
				}
				// ===========================Business rules
				// In Override mode
				if (mIsOverride) {
					mProcessingStateMachine.changeState(new DaycareMembersFoundState());
					return;
				}
				// No Members
				else if (mMembers.isEmpty()) {
					mProcessingStateMachine.changeState(new ExternalAuthenticationState());
					//mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
					return;
				}
				// Some members overriden
				else if (mValidMembers.isEmpty() && isAMemberOverridden) {
					onAccountConflict();
					return;
				}
				// Single User with no valid members
				else if (mMembers.size() == 1 && mValidMembers.isEmpty()) {
					if (mDataSource.equals("ABC_MSF")) {
						mProcessingStateMachine.changeState(new InvalidLoginState(
								authedChildNotAvail ? InvalidLoginState.INVALIDCHECKDIRECTION : InvalidLoginState.INVALIDMEMBERSIGNUP));
						return;
					} else if (!mNotUsingChildwatch.isEmpty()) {
						mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
						return;
					} else {
						// If an adult and not a self check-in child, allow invalid member override
						if (DaycareHelpers.getAgeInYears(mMembers.get(0).getBirthDate()) > DaycareHelpers.UPPER_AGE_CUTTOFF)
							mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.INVALIDMEMBER));
						else
							mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
						return;
					}
				}
				// No Valid Members
				else if (mValidMembers.isEmpty()) {
					if (mDataSource.equals("ABC_MSF")) {
						mProcessingStateMachine.changeState(new InvalidLoginState(
								authedChildNotAvail ? InvalidLoginState.INVALIDCHECKDIRECTION : InvalidLoginState.INVALIDMEMBERSIGNUP));
						return;
					} else {
						if (!mNotUsingChildwatch.isEmpty()) {
							mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
							//return;
						} else {
							mProcessingStateMachine.changeState(new InvalidLoginState(
									authedChildNotAvail ? InvalidLoginState.INVALIDCHECKDIRECTION : InvalidLoginState.INVALIDMEMBER));
						}
						return;
					}
				}
				mProcessingStateMachine.changeState(new DaycareMembersFoundState());
			}
		}

		@Override
		public void onException(Exception e) {
			mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
		}

		@Override
		public void exception(Command<?> command, Exception e) {
			if (e instanceof DaycareDataExceptionNoObject) {
				mProcessingStateMachine.changeState(new ExternalAuthenticationState());
			} else {
				onException(e);
			}
		}
	}

	private class FetchSelfCheckInMembersCommandListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {

		}

		@Override
		public void finished(Command<?> command) {

		}

		@Override
		public void valueChanged(Command<?> command) {
			List<Member> allSelfCheckInMembers = (List<Member>) command.getResult();
			for (Member m : allSelfCheckInMembers) {
				if (m.getPFacility() == null)
					mSelfCheckInMembers.add(m);
			}
			getPersistenceLayer().getPersistenceData().setSelfCheckInChildren(mSelfCheckInMembers);
			boolean isAMemberOverridden = false;
			// Staff member
			if (mMembers.size() == 1 && mMembers.get(0).getMembershipType() != null
					&& mMembers.get(0).getMembershipType().equals(STAFF_MEMBERSHIP_STRING)) {
				completeAuthentication(LoginResult.STAFFUSER, mMembers);
				return;
			}
			// Check for overrides and valid members
			mValidMembers = new ArrayList<Member>();
			mNotUsingChildwatch = new ArrayList<Member>();
			boolean authedChildNotAvail = false;
			for (Member member : mMembers) {
				isAMemberOverridden = isAMemberOverridden || member.getWasOverridden();
				if (DaycareHelpers.getAgeInYears(member.getBirthDate()) >= DaycareHelpers.LOWER_AGE_CUTTOFF) {
					if (DaycareHelpers.getAgeInYears(member.getBirthDate()) <= DaycareHelpers.UPPER_AGE_CUTTOFF)
						if ((member.getPFacility() == null && mInOrOut == InOrOut.OUT)
								|| (member.getPFacility() != null && mInOrOut == InOrOut.IN)) {
							authedChildNotAvail = true;
							continue;
						}
					if (member.getIsValidMembership() && member.getUsesChildWatch() && member.getPFacility() == null)
						// if showing invalids on select user screen
						mValidMembers.add(member);
					if (!member.getUsesChildWatch())
						mNotUsingChildwatch.add(member);
				}
			}
			// Add Self Check In Members to Valid List
			for (Member selfCheckIn : mSelfCheckInMembers) {
				if (!mValidMembers.contains(selfCheckIn) && selfCheckIn.getIsValidMembership() == true)
					mValidMembers.add(selfCheckIn);
			}
			// ===========================Business rules
			// In Override mode
			if (mIsOverride) {
				mProcessingStateMachine.changeState(new DaycareMembersFoundState());
				return;
			}
			// No Members
			else if (mMembers.isEmpty()) {
				mProcessingStateMachine.changeState(new ExternalAuthenticationState());
				//mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
				return;
			}
			// Some members overriden
			else if (mValidMembers.isEmpty() && isAMemberOverridden) {
				onAccountConflict();
				return;
			}
			// Single User with no valid members
			else if (mMembers.size() == 1 && mValidMembers.isEmpty()) {
				if (mDataSource.equals("ABC_MSF")) {
					mProcessingStateMachine.changeState(new InvalidLoginState(
							authedChildNotAvail ? InvalidLoginState.INVALIDCHECKDIRECTION : InvalidLoginState.INVALIDMEMBERSIGNUP));
					return;
				}
				else if (!mNotUsingChildwatch.isEmpty()) {
					mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
					return;
				} else {
					// If an adult and not a self check-in child, allow invalid member override
					if (DaycareHelpers.getAgeInYears(mMembers.get(0).getBirthDate()) > DaycareHelpers.UPPER_AGE_CUTTOFF)
						mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.INVALIDMEMBER));
					else
						mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
					return;
				}
			}
			// No Valid Members
			else if (mValidMembers.isEmpty()) {
				if (mDataSource.equals("ABC_MSF")) {
					mProcessingStateMachine.changeState(new InvalidLoginState(
							authedChildNotAvail ? InvalidLoginState.INVALIDCHECKDIRECTION : InvalidLoginState.INVALIDMEMBERSIGNUP));
					return;
				} else {
					if (!mNotUsingChildwatch.isEmpty()) {
						mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
						return;
					} else {
						mProcessingStateMachine.changeState(new InvalidLoginState(
								authedChildNotAvail ? InvalidLoginState.INVALIDCHECKDIRECTION : InvalidLoginState.INVALIDMEMBER));
					}
					return;
				}
			}
			mProcessingStateMachine.changeState(new DaycareMembersFoundState());
		}

		@Override
		public void exception(Command<?> command, Exception e) {

		}
	}

	private class DaycareMembersFoundState extends State {
		@Override
		public void onEnter() {
			mProcessingStateMachine.changeState(new IdleState());
		}

		@Override
		public void onExit() {
			List<Member> membersToPass = new ArrayList<Member>();
			if (!mIsOverride)
				membersToPass = mValidMembers;
			else {
				//membersToPass = mMembers;
				for (Member member : mMembers)
					if (DaycareHelpers.getAgeInYears(member.getBirthDate()) > DaycareHelpers.UPPER_AGE_CUTTOFF)
						membersToPass.add(member);
			}

			if (membersToPass.size() > 1) {
				completeAuthentication(LoginResult.MULTIUSER, membersToPass);
			} else if (membersToPass.size() == 1 && mIsStaffFacing) {
				completeAuthentication(LoginResult.SINGLEUSER_STAFFFACING, membersToPass);
			} else if (membersToPass.size() == 1 && !mIsStaffFacing) {
				if (mSelfCheckInMembers.contains(membersToPass.get(0))) {
					getPersistenceLayer().getPersistenceData().setSelectedChildren(Arrays.asList(membersToPass.get(0)));
					mLoginResult = LoginResult.CHILDUSER;
					//getPersistenceLayer().getPersistenceData().setLoginResult(LoginResult.CHILDUSER);
					getPersistenceLayer()
							.selectUser(membersToPass.get(0), new SelectUserCommandListener(membersToPass));
				} else
					getPersistenceLayer()
							.selectUser(membersToPass.get(0), new SelectUserCommandListener(membersToPass));
			} else {
				onInvalidCredential();
			}
		}


		@Override
		public void onException(Exception e) {
			mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
		}
	}

	/**
	 * Called from InternalAuthenticationState when member is not found in the
	 * internal database.
	 * 
	 * Possible future states: <li>ExternalUnitInfoState</li> <li>
	 * InvalidLoginState</li>
	 */
	private class ExternalAuthenticationState extends State implements
			ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>> {
		@Override
		public void onEnter() {
			if (mCredential.getForm() == FormOfCredential.PHONE) {
				mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
			} else if (mCredential.getForm() == FormOfCredential.BARCODE) {
				try {
                    getPersistenceLayer().registerMember(mCredential.getText(), new RegisterMemberCommandListener());
				} catch (Exception e) {
					Log.e("Registration Error", "Error Registering Member");
					mFragment.showRegistrationErrorDialog();
				}
			}
		}

		@Override
		public void DataReceived(List<com.alaris_us.externaldata.to.Member> data, ExternalDataException e) {
			List<String> profitCenterAddOns = new ArrayList<String>(Arrays.asList("CCARE01", "CCARE02", "CHILDCARE", "CHILDCARE1", "CHILDCARE2"));
			mPcHashMap.clear();
			mAdultAddOnLoop = 0;
			mAdultAddOnLoopsCompleted = 0;
			boolean membersWithNoBirthDate = false;
			if (data == null || data.isEmpty()) {
				mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
			} else {
				// Check if any members have no birth date
				for (com.alaris_us.externaldata.to.Member member : data) {
					if (member.getBirthDate() == null)
						membersWithNoBirthDate = true;
				}
				if (membersWithNoBirthDate) {
					mFragment.showNoBirthDateDialog();
					return;
				}

				// Check if a Phone is unique to a child
				if (data.size() == 1)
					if (!DaycareHelpers.isAdult(data.get(0).getBirthDate(), 13)) {
						mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
						return;
					}

				if (mDataSource.equals("ABC_EOS")) {
					// Check if any adults have a profit center add on
					for (com.alaris_us.externaldata.to.Member member : data) {
						if (DaycareHelpers.isAdult(member.getBirthDate(), 18)) {
							mAdultAddOnLoop++;
							mPcHashMap.put(member.getMemberId(), profitCenterAddOns);
							for (String addOn : profitCenterAddOns) {
								getPersistenceLayer().fetchAddOns(member.getHomeFacility(), member.getMemberId(), addOn, new FetchAddOnCommandListener(member, addOn, data));
							}
						}
					}
				} else if (mDataSource.equals("Daxko") || mDataSource.equals("ActiveNet")) {
					// Filter available family units from list of returned
					// members
					List<String> familyUnits = new ArrayList<String>();
					for (com.alaris_us.externaldata.to.Member mem : data) {
						if (mIsRegOverride) {
							// Unique values only
							if (!familyUnits.contains(mem.getUnitID())) {
								familyUnits.add(mem.getUnitID());
							}
						} else {
							// Unique values only
							if (!familyUnits.contains(mem.getUnitID()) && mem.isActive()) {
								familyUnits.add(mem.getUnitID());
							}
						}
					}
					if (familyUnits.size() >= 1) {
						// Multiple family units. Fetch each family unit
						// individually.
						mProcessingStateMachine.changeState(new ExternalUnitInfoState(familyUnits));
					} else {
						// Oops. No family units found.
						mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.INACTIVEMEMBERONREG));
					}
				} else {
					// soft registration here
					createAndSeparateMembers(data);
				}
			}
		}

		@Override
		public void onException(Exception e) {
			mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
		}
	}

	/**
	 * Called from ExternalAuthenticationState when units are found.
	 * 
	 * Possible future states: <li>InvalidLoginState</li>
	 */
	private class ExternalUnitInfoState extends State implements ExternalDataOperationComplete<UnitInfo> {
		private final List<String>	familyUnits;
		private int					returnQueryCount	= 0;

		public ExternalUnitInfoState(List<String> familyUnits) {
			this.familyUnits = familyUnits;
		}

		@Override
		public void onEnter() {
			for (String unitId : familyUnits) {
				getPersistenceLayer().getExternalUnitInfo(unitId, this);
			}
		}

		@Override
		public void DataReceived(UnitInfo data, ExternalDataException arg1) {
			if (mRegistrationUnits == null)
				mRegistrationUnits = new ArrayList<UnitInfo>();
			mRegistrationUnits.add(data);
			returnQueryCount += 1;
			if (returnQueryCount == familyUnits.size()) {
				completeAuthentication(LoginResult.NEWUSER, null);
			}
		}

		@Override
		public void onException(Exception e) {
			mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
		}
	}

	private class FetchAddOnCommandListener implements ExternalDataOperationComplete<String> {

		private com.alaris_us.externaldata.to.Member adult;
		private String pcAddOn;
		private List<com.alaris_us.externaldata.to.Member> eosMembers;

		public FetchAddOnCommandListener(com.alaris_us.externaldata.to.Member member, String addOn, List<com.alaris_us.externaldata.to.Member> members) {
			adult = member;
			pcAddOn = addOn;
			eosMembers = members;
		}

		@Override
		public void DataReceived(String data, ExternalDataException arg1) {
			mPcHashMap.get(adult.getMemberId()).remove(pcAddOn);
			if (data != null)
				if (data.equals("true")) {
					mAddOn = pcAddOn;
				}
			if (mPcHashMap.get(adult.getMemberId()).isEmpty()) {
				mAdultAddOnLoopsCompleted++;
			}
			if (mAdultAddOnLoopsCompleted == mAdultAddOnLoop) {
				getPersistenceLayer().getPersistenceData().setProfitCenterAddOn(mAddOn);
				createAndSeparateMembers(eosMembers);
			}
		}
	}

	private class RegisterMemberCommandListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {

		}

		@Override
		public void valueChanged(Command<?> command) {
			String result = (String) command.getResult();

			if (result == null) {
				mFragment.showRegistrationErrorDialog();
			} else if (result.equals("Registration Successful!")) {
				try {
					if (mFacility.getExternalDataOpts().getBoolean("usesSoftRegistration"))
						mProcessingStateMachine.changeState(new InternalAuthenticationState());
					else
						getPersistenceLayer().authenticateByBarcodeOrMagCardInDaycareData(mCredential.getText(), new MemberLookupByBarcodeCommandListener());
				} catch (JSONException e) {
					e.printStackTrace();
					getPersistenceLayer().authenticateByBarcodeOrMagCardInDaycareData(mCredential.getText(), new MemberLookupByBarcodeCommandListener());
				}
			}
		}

		@Override
		public void exception(Command<?> command, Exception e) {
			if (e.getCause().getMessage().equals("Registration failed!")) {
				mFragment.showRegistrationErrorDialog();
			} else if (e.getCause().getMessage().equals("Member\'s birthdate is missing, please contact staff!")) {
				mFragment.showNoBirthDateDialog();
			} else {
				mFragment.showRegistrationErrorDialog();
			}
		}

	}


	private class MemberLookupByBarcodeCommandListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {

		}

		@Override
		public void finished(Command<?> command) {
			final Member member = (Member) command.getResult();
			getPersistenceLayer().selectUser(member, new CommandStatusListener() {
				@Override
				public void starting(Command<?> command) {

				}

				@Override
				public void finished(Command<?> command) {
					List<Member> familyMembers = new ArrayList<>();
					List<Member> parents = new ArrayList<>();
					parents.add(member);
					familyMembers.add(member);
					List<Member> children = getPersistenceLayer().getPersistenceData().getRelatedChildren();
					for (Member c : children) {
						familyMembers.add(c);
					}
					if (children.size() > 0) {
						getPersistenceLayer().getPersistenceData().setYMCAFamily(familyMembers, parents, children);
						completeAuthentication(LoginResult.NEWUSER, null);
					} else {
						mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.NOKIDS));
					}
				}

				@Override
				public void valueChanged(Command<?> command) {

				}

				@Override
				public void exception(Command<?> command, Exception e) {

				}
			});
		}

		@Override
		public void valueChanged(Command<?> command) {

		}

		@Override
		public void exception(Command<?> command, Exception e) {

		}
	}

	private class ExternalMemberCreationState extends State {

		public ExternalMemberCreationState() {
		}

		@Override
		public void onEnter() {
			mFragment.showRegistrationMemberDialog();
			if (mExistingChildFamily == null)
				getPersistenceLayer().registerFamilyInDaycare(ymcaParents, ymcaChildren, new RegisterFamilyStatusListener());
			else {
				getPersistenceLayer().registerFamilyInDaycare(ymcaParents, ymcaChildren, mExistingChildFamily, new RegisterFamilyStatusListener());
			}
		}
		
		@Override
		public void onExit() {
		}

		@Override
		public void onException(Exception e) {
			mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
		}
	}
	
	private class SelectUserCommandListener implements CommandStatusListener {

		private List<Member> m_membersToPass = new ArrayList<Member>();

		public SelectUserCommandListener(List<Member> membersToPass) {
			m_membersToPass = membersToPass;
		}

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			if (mLoginResult != null) {
				if (mLoginResult.equals(LoginResult.CHILDUSER))
					completeAuthentication(LoginResult.CHILDUSER, m_membersToPass);
				else
					completeAuthentication(LoginResult.SINGLEUSER, m_membersToPass);
			} else
				completeAuthentication(LoginResult.SINGLEUSER, m_membersToPass);
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}

	}

	/**
	 * Called when any state exceptions occour.
	 * 
	 * Possible future states: <li>IdleState</li>
	 */
	private class InvalidLoginState extends State {
		// public static final int NOCLEAR = 0;
		public static final int	CLEAR					= 1;
		public static final int	NOKIDS					= 2;
		public static final int	INVALIDMEMBER			= 3;
		public static final int	INACTIVEMEMBERONREG		= 4;
		public static final int	INVALIDCHECKDIRECTION	= 5;
		public static final int INVALIDMEMBERSIGNUP 	= 6;
		private int				m_type;

		public InvalidLoginState(int type) {
			m_type = type;
		}

		@Override
		public void onEnter() {
			if (m_type == NOKIDS) {
				onNoChildren();
				reset();
			} else if (m_type == INVALIDMEMBER) {
				onInvalidMember();
			} else if (m_type == INVALIDMEMBERSIGNUP) {
				onInvalidMemberSignUp();
			} else if (m_type == INACTIVEMEMBERONREG) {
				mIsRegOverride = true;
				onInvalidMember();
			} else if (m_type == INVALIDCHECKDIRECTION) {
				onInvalidCheckDirection();
				reset();
			} else {
				onInvalidCredential();
			}
		}

		@Override
		public void onExit() {
		}

		@Override
		public void onException(Exception e) {
			reset();
		}
	}

	/*******************************************************************************************************************
	 * 
	 * Methods below used for user input
	 *
	 ******************************************************************************************************************/
	public interface InputState {
		public abstract void pressKey(String digit);

		public abstract void pressEnterKey();

		public abstract void pressNumpadKey(String number);

		public abstract void pressNumpadEnter();

		public abstract void pressNumpadDelete();
	}

	private class AllInputState extends State implements InputState {
		private String input = "";

		@Override
		public void onEnter() {
			mInputText.set(mEnterPinText);
			setCredential("", null);
		}

		@Override
		public void onExit() {
			mInputText.set(input);
		}

		@Override
		public void pressKey(String digit) {
			input = digit;
			mInputMachine.changeState(new BarcodeInputState());
		}

		@Override
		public void pressNumpadKey(String number) {
			input = number;
			mInputMachine.changeState(new PinPadInputState());
		}

		@Override
		public void pressNumpadEnter() {
		}

		@Override
		public void pressNumpadDelete() {
		}

		@Override
		public void pressEnterKey() {
		}
	}

	private class PinPadInputState extends State implements InputState {
		private String	input;

		@Override
		public void onEnter() {
			input = mInputText.get();
		}

		@Override
		public void pressKey(String digit) {
		}

		@Override
		public void pressNumpadKey(String number) {
			input = input + number;
			mInputText.set(input);
			if (input.length() == PIN_MAX_LENGTH) {
				setCredential(input, FormOfCredential.PHONE);
				mInputMachine.changeState(new ValidateInputState());
			}
		}

		@Override
		public void pressNumpadEnter() {
			if (input.length() == PIN_MAX_LENGTH) {
				setCredential(input, FormOfCredential.PHONE);
				mInputMachine.changeState(new ValidateInputState());
			}
		}

		@Override
		public void pressNumpadDelete() {
			input = input.substring(0, input.length() - 1);
			mInputText.set(input);
			if (input.length() == 0)
				mInputMachine.changeState(new AllInputState());
		}

		@Override
		public void pressEnterKey() {
		}
	}

	private class BarcodeInputState extends State implements InputState {
		private String	input;

		@Override
		public void onEnter() {
			input = mInputText.get();
		}

		@Override
		public void pressKey(String digit) {
			input = input + digit;
			mInputText.set(input);
		}

		@Override
		public void pressNumpadKey(String number) {
		}

		@Override
		public void pressNumpadEnter() {
		}

		@Override
		public void pressNumpadDelete() {
		}

		@Override
		public void pressEnterKey() {
			setCredential(input, FormOfCredential.BARCODE);
			mInputMachine.changeState(new ValidateInputState());
		}
	}

    private class ManualBarcodeInputState extends State implements InputState {

        @Override
        public void onEnter() {
        }

        @Override
        public void pressKey(String digit) {
        }

        @Override
        public void pressNumpadKey(String number) {
        }

        @Override
        public void pressNumpadEnter() {
            String barcode = mFragment.getScanCardText();
            setCredential(barcode, FormOfCredential.BARCODE);
            mInputMachine.changeState(new ValidateInputState());
			mFragment.setScanCardText("");
		}

        @Override
        public void pressNumpadDelete() {
        }

        @Override
        public void pressEnterKey() {
            String barcode = mFragment.getScanCardText();
            setCredential(barcode, FormOfCredential.BARCODE);
            mInputMachine.changeState(new ValidateInputState());
        }
    }
    
    public void setInputStateToManualBarcode() {
        mInputMachine.changeState(new ManualBarcodeInputState());
    }
    
    public void setInputStateToPinPadInput() {
        mInputMachine.changeState(new PinPadInputState());
    }

	private class ValidateInputState extends State implements InputState {
		@Override
		public void onEnter() {
			persistModel();
			mProcessingStateMachine = new StateMachine(new InternalAuthenticationState());
		}

		@Override
		public void pressKey(String digit) {
		}

		@Override
		public void pressNumpadKey(String number) {
		}

		@Override
		public void pressNumpadEnter() {
		}

		@Override
		public void pressNumpadDelete() {
		}

		@Override
		public void pressEnterKey() {
		}
	}

	private class BlockInputState extends State implements InputState {
		@Override
		public void onEnter() {
		}

		@Override
		public void pressKey(String digit) {
		}

		@Override
		public void pressNumpadKey(String number) {
		}

		@Override
		public void pressNumpadEnter() {
		}

		@Override
		public void pressNumpadDelete() {
		}

		@Override
		public void pressEnterKey() {
		}
	}
	
	public void createAndSeparateMembers(List<com.alaris_us.externaldata.to.Member> data) {
		if (data == null) {
			reset();
		} else {
			defaultDate = DateGenerator.getDefaultDate();
			formattedDate = DateGenerator.getFormattedDateAtOffset(DateGenerator.CHILDSEPARATOR);
			allMembers.clear();
			ymcaParents.clear();
			ymcaChildren.clear();
			mAuthorizedChildren.clear();
			//lastIndex = data.size();
			loopIndex = 0;
			authLoopIndex = 0;
			Iterator<com.alaris_us.externaldata.to.Member> iterator = data.iterator();
			while (iterator.hasNext()) {
				com.alaris_us.externaldata.to.Member mem = iterator.next();
			    if (mem.getisAuthorizedPickup()) {
			    	mAuthorizedChildren.add(mem);
			        iterator.remove();
			    }
			}
			lastIndex = data.size();
			for (int i=0; i < data.size(); i++) {
				com.alaris_us.externaldata.to.Member mem = data.get(i);
				getPersistenceLayer().getMemberByMemberId(mem.getMemberId(), new GetMemberByMemberIdCommandListener(mem));
			}
		}
	}
	
	private class GetMemberByMemberIdCommandListener implements CommandStatusListener{

		private com.alaris_us.externaldata.to.Member member;
		
		public GetMemberByMemberIdCommandListener(com.alaris_us.externaldata.to.Member member){
			this.member = member;
		}
		
		@Override
		public void starting(Command<?> command) {
			
		}

		@Override
		public void finished(Command<?> command) {
			Object result = command.getResult();
			if (result == null) {
				if (member.getHomeFacility() != null) {
					if (member.getHomeFacility().equals(mFacility.getYmcaClientDataId())) {
						Member newMember = getPersistenceLayer().createMemberFromYMCAData(member, formattedDate, defaultDate, null, mFacility);
						String memberDate = DateGenerator.getFormattedDate(newMember.getBirthDate());
						if (memberDate.compareTo(formattedDate) < 0) {
							ymcaParents.add(newMember);
						} else {
							ymcaChildren.add(newMember);
						}
						allMembers.add(newMember);
						if (mFacility.getUsesValidMembership())
							getPersistenceLayer().isMembershipValidForFacility(mFacility, newMember.getMembershipType(), new IsMembershipValidForFacilityListener(newMember));
						else
							prepareForMemberRegistration();
					} else {
						getPersistenceLayer().getHomeFacilityByClubNumber(member.getHomeFacility(), new FetchHomeFacilityListener(member));
					}
				} else {
					Member newMember = getPersistenceLayer().createMemberFromYMCAData(member, formattedDate, defaultDate, null, mFacility);
					String memberDate = DateGenerator.getFormattedDate(newMember.getBirthDate());
					if (memberDate.compareTo(formattedDate) < 0) {
						ymcaParents.add(newMember);
					} else {
						ymcaChildren.add(newMember);
					}
					allMembers.add(newMember);
					if (mFacility.getUsesValidMembership())
						getPersistenceLayer().isMembershipValidForFacility(mFacility, newMember.getMembershipType(), new IsMembershipValidForFacilityListener(newMember));
					else
						prepareForMemberRegistration();
				}
			} else {
				//TODO use the child's family as the family for other members in the household
				if (mExistingChildFamily == null)
					if (!DaycareHelpers.isAdult(((Member) result).getBirthDate(), 18))
						mExistingChildFamily = ((Member) result).getPFamily();
				prepareForMemberRegistration();
				/*if (member.getHomeFacility().equals(mFacility.getYmcaClientDataId())) {
					Member newMember = getPersistenceLayer().createMemberFromYMCAData(member, formattedDate, defaultDate, mFacility);
					String memberDate = DateGenerator.getFormattedDate(newMember.getBirthDate());
					if (memberDate.compareTo(formattedDate) < 0) {
						ymcaParents.add(newMember);
					} else {
						ymcaChildren.add(newMember);
					}
					allMembers.add(newMember);
					prepareForMemberRegistration();
				} else {
					getPersistenceLayer().getHomeFacilityByClubNumber(member.getHomeFacility(), new FetchHomeFacilityListener(member));
				}*/
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
			
		}

		@Override
		public void exception(Command<?> command, Exception e) {
			
		}
	}

	private class IsMembershipValidForFacilityListener implements CommandStatusListener{

		private Member member;

		public IsMembershipValidForFacilityListener(Member member) {
			this.member = member;
		}

		@Override
		public void starting(Command<?> command) {

		}

		@Override
		public void finished(Command<?> command) {
			List<ValidMembership> result = (List<ValidMembership>) command.getResult();
			if (result.isEmpty())
				member.setIsValidMembership(false);
			else
				member.setIsValidMembership(true);
			prepareForMemberRegistration();
		}

		@Override
		public void valueChanged(Command<?> command) {

		}

		@Override
		public void exception(Command<?> command, Exception e) {

		}
	}

	private class FetchHomeFacilityListener implements CommandStatusListener {
		
		private com.alaris_us.externaldata.to.Member member;
		
		public FetchHomeFacilityListener(com.alaris_us.externaldata.to.Member member) {
			this.member = member;
		}
		
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			Facility homeFacility = (Facility) command.getResult();
			Member newMember = getPersistenceLayer().createMemberFromYMCAData(member, formattedDate, defaultDate, null, homeFacility);
			String memberDate = DateGenerator.getFormattedDate(newMember.getBirthDate());
			if (memberDate.compareTo(formattedDate) < 0) {
				ymcaParents.add(newMember);
			} else {
				ymcaChildren.add(newMember);
			}
			allMembers.add(newMember);
			if (mFacility.getUsesValidMembership())
				getPersistenceLayer().isMembershipValidForFacility(mFacility, newMember.getMembershipType(), new IsMembershipValidForFacilityListener(newMember));
			else
				prepareForMemberRegistration();		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
	
	private void prepareForMemberRegistration() {
		loopIndex++;
		if (lastIndex == loopIndex) {
			if (ymcaChildren.size() > 0) {
				getPersistenceLayer().getPersistenceData().setYMCAFamily(allMembers, ymcaParents, ymcaChildren);
				mProcessingStateMachine.changeState(new ExternalMemberCreationState());
			} else {
				new Exception("NO KIDS UNDER 12");
				mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.NOKIDS));
			}
		}
	}
	
	private class RegisterFamilyStatusListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			getPersistenceLayer().setupFamilyAssociationInDaycare(ymcaParents, ymcaChildren,
					new FamilyAssocFinishedListener());
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
	
	private class FamilyAssocFinishedListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			if (mAuthorizedChildren.isEmpty()) {
				mFragment.dismissRegistrationDialog();
				mProcessingStateMachine.changeState(new InternalAuthenticationState());
			} else {
				setupAuthorizedChildrenFamily();
				for (int i=0; i < mAuthorizedChildren.size(); i++) {
					com.alaris_us.externaldata.to.Member mem = mAuthorizedChildren.get(i);
					getPersistenceLayer().getMemberByMemberId(mem.getMemberId(), new GetAuthChildByMemberIdCommandListener(mem));
				}
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class GetAuthChildByMemberIdCommandListener implements CommandStatusListener{

		private com.alaris_us.externaldata.to.Member member;
		
		public GetAuthChildByMemberIdCommandListener(com.alaris_us.externaldata.to.Member member){
			this.member = member;
		}
		
		@Override
		public void starting(Command<?> command) {
			
		}

		@Override
		public void finished(Command<?> command) {
			Member result = (Member) command.getResult();
			Member authorizedBy = null;
			for (Member adult : ymcaParents) {
				if (adult.getMemberID().equals(member.getisAuthorizedBy()))
					authorizedBy = adult;
			}
			if (result == null) {
				if (member.getHomeFacility().equals(mFacility.getYmcaClientDataId())) {
					Family fam = mFamilyMap.get(member);
					Member newMember = getPersistenceLayer().createMemberFromYMCAData(member, formattedDate, defaultDate, null, mFacility);
					newMember.setPFamily(fam);
					String memberDate = DateGenerator.getFormattedDate(newMember.getBirthDate());
					allMembers.add(newMember);
					prepareForNewAuthChildRegistration(newMember, authorizedBy, false);
				} else {
					getPersistenceLayer().getHomeFacilityByClubNumber(member.getHomeFacility(), new FetchAuthChildHomeFacilityListener(member, authorizedBy, false));
				}
			} else {
				if (member.getHomeFacility().equals(mFacility.getYmcaClientDataId())) {
					prepareForNewAuthChildRegistration(result, authorizedBy, true);
				} else {
					getPersistenceLayer().getHomeFacilityByClubNumber(member.getHomeFacility(), new FetchAuthChildHomeFacilityListener(result, authorizedBy, true));
				}
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
			
		}

		@Override
		public void exception(Command<?> command, Exception e) {
			
		}
	}
	
	
private class FetchAuthChildHomeFacilityListener implements CommandStatusListener {
		
		private com.alaris_us.externaldata.to.Member externalMember;
		private Member member;
		private Member authorizedBy;
		private boolean childExists;
		
		public FetchAuthChildHomeFacilityListener(com.alaris_us.externaldata.to.Member externalMember, Member authorizedBy, boolean childExists) {
			this.externalMember = externalMember;
			this.authorizedBy = authorizedBy;
			this.childExists = childExists;
		}
		
		public FetchAuthChildHomeFacilityListener(Member member, Member authorizedBy, boolean childExists) {
			this.member = member;
			this.authorizedBy = authorizedBy;
			this.childExists = childExists;
		}
		
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			if (!childExists) {
				Facility homeFacility = (Facility) command.getResult();
				Family fam = mFamilyMap.get(externalMember);
				Member newMember = getPersistenceLayer().createMemberFromYMCAData(externalMember, formattedDate, defaultDate, null, homeFacility);
				newMember.setPFamily(fam);
				allMembers.add(newMember);
				prepareForNewAuthChildRegistration(newMember, authorizedBy, childExists);
			} else {
				prepareForNewAuthChildRegistration(member, authorizedBy, childExists);
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private void prepareForNewAuthChildRegistration(Member child, Member authorizedBy, boolean childExists) {
		if (!childExists)
			mProcessingStateMachine.changeState(new NewAuthMemberCreationState(child, authorizedBy));
		else
			mProcessingStateMachine.changeState(new ExistingAuthMemberCreationState(child, authorizedBy));
	}
	
	private class NewAuthMemberCreationState extends State {

		private Member m_child;
		private Member m_authorizedBy;
		
		public NewAuthMemberCreationState(Member child, Member authorizedBy) {
			m_child = child;
			m_authorizedBy = authorizedBy;
		}

		@Override
		public void onEnter() {
			getPersistenceLayer().registerAuthChildInDaycare(m_child, new RegisterAuthChildStatusListener(m_child, m_authorizedBy));
		}
		
		@Override
		public void onExit() {
		}

		@Override
		public void onException(Exception e) {
			mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
		}
	}
	
	private class RegisterAuthChildStatusListener implements CommandStatusListener {
		
		private Member m_child;
		private Member m_authorizedBy;
		
		public RegisterAuthChildStatusListener(Member child, Member authorizedBy) {
			m_child = child;
			m_authorizedBy = authorizedBy;
		}
		
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			getPersistenceLayer().setupAuthChildFamilyAssociationInDaycare(m_child, new AuthChildFamilyAssocFinishedListener(m_child, m_authorizedBy));
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
	
	private class AuthChildFamilyAssocFinishedListener implements CommandStatusListener {
		
		private Member m_child;
		private Member m_authorizedBy;
		
		public AuthChildFamilyAssocFinishedListener(Member child, Member authorizedBy) {
			m_child = child;
			m_authorizedBy = authorizedBy;
		}
		
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			getPersistenceLayer().setupAuthorization(m_child, m_authorizedBy, new SetupAuthorizationFinishedListener());
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
	
	private class SetupAuthorizationFinishedListener implements CommandStatusListener {
		
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
		authLoopIndex++;
		if (mAuthorizedChildren.size() == authLoopIndex) {
			mFragment.dismissRegistrationDialog();
			mProcessingStateMachine.changeState(new InternalAuthenticationState());
		}
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class ExistingAuthMemberCreationState extends State {

		private Member m_child;
		private Member m_authorizedBy;
		
		public ExistingAuthMemberCreationState(Member child, Member authorizedBy) {
			m_child = child;
			m_authorizedBy = authorizedBy;
		}

		@Override
		public void onEnter() {
			getPersistenceLayer().setupAuthorization(m_child, m_authorizedBy, new SetupAuthorizationFinishedListener());
		}
		
		@Override
		public void onExit() {
		}

		@Override
		public void onException(Exception e) {
			mProcessingStateMachine.changeState(new InvalidLoginState(InvalidLoginState.CLEAR));
		}
	}
	
	public void setupAuthorizedChildrenFamily() {
		List<String> uniqueFamilyIds = new ArrayList<>();
		for (com.alaris_us.externaldata.to.Member childToCompare : mAuthorizedChildren) {
			String tempFamilyId = childToCompare.getTempFamilyId();
			if (!uniqueFamilyIds.contains(tempFamilyId))
				uniqueFamilyIds.add(tempFamilyId);
		}
		for (String familyId : uniqueFamilyIds) {
			List<com.alaris_us.externaldata.to.Member> familyMembers = new ArrayList<>();
			Family family = getPersistenceLayer().getNewEmptyFamily();
			for (com.alaris_us.externaldata.to.Member authChild : mAuthorizedChildren) {
				if (authChild.getTempFamilyId().equals(familyId))
					familyMembers.add(authChild);
			}
			for (com.alaris_us.externaldata.to.Member mem : familyMembers) {
				mFamilyMap.put(mem, family);
			}
		}
	}

}
