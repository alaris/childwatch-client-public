package com.alaris_us.daycareclient.fragments;

import com.alaris_us.daycareclient.models.SelectProgramScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontEditText;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient.widgets.ProgramSelectionCheckBox;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

public class SelectProgramScreenFragment extends ScreenSwitcherFragment {
	SelectProgramScreenViewModel m_model;
	private View m_capacityExceededDialogView;
	private FontTextView capacityExceededBreakdownView;
	private View m_staffOverrideEntryDialogView;
	private View m_successfulOverrideDialogView;
	private FontEditText overridePIN;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.selectprogramscreen_layout,
				container, false);
		m_capacityExceededDialogView = inflater.inflate(R.layout.view_capacityexceededdialog_layout, container, false);
		m_staffOverrideEntryDialogView = inflater.inflate(R.layout.view_staffoverrideentrydialog_layout, container, false);
		m_successfulOverrideDialogView = inflater.inflate(R.layout.view_successfuloverridedialog_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		HorizontalListView timeList = (HorizontalListView) m_layout
				.findViewById(R.id.selectprogramscreen_tile_programlist);
		m_model = new SelectProgramScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		m_layout.findViewById(R.id.view_navigation_next_container).setId(
				R.id.selectprogramscreen_nav_next);
		m_layout.findViewById(R.id.view_navigation_back_container).setId(
				R.id.selectprogramscreen_nav_back);
		UiBinder.bind(this.m_layout, R.id.selectprogramscreen_tile_programlist,
				"Adapter", m_model, "Programs", BindingMode.TWO_WAY,
				new AdapterConverter(ProgramSelectionCheckBox.class, true, true));
		UiBinder.bind(m_layout, R.id.selectprogramscreen_nav_next,
				"OnClickListener", m_model, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.selectprogramscreen_nav_back,
				"OnClickListener", m_model, "BackClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.selectprogramscreen_nav_next, "Alpha",
				m_model, "NextAlpha", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout,
				R.id.view_navigation_withbuttons_selectall_textbox,
				"TextSizeRes", m_model, "SelectAllButtonTextSize",
				BindingMode.ONE_WAY);
		timeList.setOnItemClickListener(m_model.new ProgramClickListener());
		capacityExceededBreakdownView = (FontTextView) m_capacityExceededDialogView.findViewById(R.id.view_capacityexceededdialog_bottom_text);
		
		/* CapacityExceededDialog */
        UiBinder.bind(m_capacityExceededDialogView, R.id.view_capacityexceededdialog_reselect_button, "OnClickListener", m_model,
                "CapacityExceededDialogReselectClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(m_capacityExceededDialogView, R.id.view_capacityexceededdialog_home_button, "OnClickListener", m_model,
                "CapacityExceededDialogHomeClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(m_capacityExceededDialogView, R.id.view_capacityexceededdialog_override_button, "OnClickListener", m_model,
                "CapacityExceededDialogOverrideClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(new ReflectedProperty(this, "CapacityExceededDialog"), m_model, "CapacityExceededDialog",
                BindingMode.ONE_WAY_TO_SOURCE);
        UiBinder.bind(new ReflectedProperty(this, "CapacityExceededBreakdownText"), m_model, "CapacityExceededBreakdownText",
                BindingMode.ONE_WAY);
        
        /* StaffOverrideEntryDialog */
		UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_enter_button,
				"OnClickListener", m_model, "StaffOverrideEntryDialogEnterClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_cancel_button,
				"OnClickListener", m_model, "StaffOverrideEntryDialogCancelClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "StaffOverrideEntryDialog"), m_model, "StaffOverrideEntryDialog",
				BindingMode.ONE_WAY_TO_SOURCE);

		/* SuccessfulOverrideDialog */
		UiBinder.bind(m_successfulOverrideDialogView, R.id.view_successfuloverridedialog_ok_button, "OnClickListener",
				m_model, "SuccessfulOverrideDialogOkClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "SuccessfulOverrideDialog"), m_model, "SuccessfulOverrideDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		m_model.disconnect();
	}

	@Override
	public void onResumeFragment() {
		m_model = (null == m_model) ? new SelectProgramScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this) : m_model;
		m_model.reset();
		super.onResumeFragment();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		m_model.disconnect();
		m_model = null;
	}

	@Override
	public void initData() {
		m_model.reset();
	}
	
	public AlertDialog getCapacityExceededDialog() {
        FrameLayout capacityExceededDialogContainer = new FrameLayout(getActivity());
        capacityExceededDialogContainer.addView(m_capacityExceededDialogView);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(capacityExceededDialogContainer);
        AlertDialog capacityExceededDialog = builder.create();
        capacityExceededDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        capacityExceededDialog.setCancelable(false);
        capacityExceededDialog.show();
        capacityExceededDialog.dismiss();
        ViewGroup.LayoutParams lp = m_capacityExceededDialogView.getLayoutParams();
        lp.width = (int) getResources().getDimension(R.dimen.view_capacityexceededdialog_width);
        lp.height = (int) getResources().getDimension(R.dimen.view_capacityexceededdialog_height);
        m_capacityExceededDialogView.setLayoutParams(lp);
        ((View) m_capacityExceededDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
        return capacityExceededDialog;
    }
	
	public AlertDialog getStaffOverrideEntryDialog() {
		FrameLayout staffOverrideEntryDialogContainer = new FrameLayout(getActivity());
		staffOverrideEntryDialogContainer.addView(m_staffOverrideEntryDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(staffOverrideEntryDialogContainer);
		AlertDialog staffOverrideEntryDialog = builder.create();
		staffOverrideEntryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		staffOverrideEntryDialog.setCancelable(false);
		staffOverrideEntryDialog.show();
		staffOverrideEntryDialog.dismiss();
		ViewGroup.LayoutParams lp = m_staffOverrideEntryDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_height);
		m_staffOverrideEntryDialogView.setLayoutParams(lp);
		((View) m_staffOverrideEntryDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return staffOverrideEntryDialog;
	}
	
	public AlertDialog getSuccessfulOverrideDialog() {
		FrameLayout successfulOverrideDialogContainer = new FrameLayout(getActivity());
		successfulOverrideDialogContainer.addView(m_successfulOverrideDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(successfulOverrideDialogContainer);
		AlertDialog successfulOverrideDialog = builder.create();
		successfulOverrideDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		successfulOverrideDialog.setCancelable(false);
		successfulOverrideDialog.show();
		successfulOverrideDialog.dismiss();
		ViewGroup.LayoutParams lp = m_successfulOverrideDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_height);
		m_successfulOverrideDialogView.setLayoutParams(lp);
		((View) m_successfulOverrideDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return successfulOverrideDialog;
	}
    
	public String getDefaultCapacityExceededText() {
        return getResources().getString(R.string.view_capacityexceededdialog_bottom_text);
    }
    
    public String getDefaultRoomCapacityExceededText() {
		return getResources().getString(R.string.view_capacityexceededdialog_bottom_text_room);
	}
    
    public String getDefaultFacilityAndRoomCapacityExceededText() {
		return getResources().getString(R.string.view_capacityexceededdialog_bottom_text_facility_and_room);
	}
    
    public void setCapacityExceededBreakdownText(String breakdown) {
        capacityExceededBreakdownView.setText(breakdown);
    }
    
    public String getOverridePIN() {
		overridePIN = (FontEditText) m_staffOverrideEntryDialogView
				.findViewById(R.id.view_staffoverrideentrydialog_pin_edittext);
		return overridePIN.getText().toString();
	}

	public void setOverridePIN(String pin) {
		overridePIN.setText(pin);
		overridePIN.setHint("Invalid PIN, Please Retry");
	}

	public void setOverridePINHint(String hint) {
		overridePIN.setHint(hint);
	}
}
