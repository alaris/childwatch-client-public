package com.alaris_us.daycareclient.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.alaris_us.daycareclient.app.App;
import com.alaris_us.daycareclient.fragments.AuthenticationScreenFragment;
import com.alaris_us.daycareclient.fragments.CheckInScreenFragment;
import com.alaris_us.daycareclient.fragments.CheckOutScreenFragment;
import com.alaris_us.daycareclient.fragments.ConfirmationScreenFragment;
import com.alaris_us.daycareclient.fragments.CubbyScreenFragment;
import com.alaris_us.daycareclient.fragments.EmergencyReentryScreenFragment;
import com.alaris_us.daycareclient.fragments.EmergencyScreenFragment;
import com.alaris_us.daycareclient.fragments.EstimatedReturnScreenFragment;
import com.alaris_us.daycareclient.fragments.FamilyConfirmationScreenFragment;
import com.alaris_us.daycareclient.fragments.MainScreenFragment;
import com.alaris_us.daycareclient.fragments.MedicalScreenFragment;
import com.alaris_us.daycareclient.fragments.OfferScreenFragment;
import com.alaris_us.daycareclient.fragments.PopupFragment;
import com.alaris_us.daycareclient.fragments.QuestionScreenFragment;
import com.alaris_us.daycareclient.fragments.RegisterScreenFragment;
import com.alaris_us.daycareclient.fragments.RegisterThankYouScreenFragment;
import com.alaris_us.daycareclient.fragments.SelectFamilyUnitScreenFragment;
import com.alaris_us.daycareclient.fragments.SelectProgramScreenFragment;
import com.alaris_us.daycareclient.fragments.SelectRoomScreenFragment;
import com.alaris_us.daycareclient.fragments.SelectUserScreenFragment;
import com.alaris_us.daycareclient.fragments.SignUpSelectKidScreenFragment;
import com.alaris_us.daycareclient.fragments.SignUpSelectUserScreenFragment;
import com.alaris_us.daycareclient.fragments.ThankYouScreenFragment;
import com.alaris_us.daycareclient.fragments.WorkoutAreaScreenFragment;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.persistence.PersistenceLayer.PersistenceLayerListener;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherActivity;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherAdapter.ScreenManagerListener;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment.PersistanceLayerHost;
import com.alaris_us.daycareclient.services.Service_DisplayTimeoutDialog;
import com.alaris_us.daycareclient_dev.R;

public class MainActivityScreen extends ScreenSwitcherActivity implements PersistanceLayerHost, ScreenManagerListener,
		PersistenceLayerListener {
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		ScreenSwitcherFragment curFragment = getDisplayedFragment();
		/* Only accept alphanumeric keys */
		if ((keyCode >= KeyEvent.KEYCODE_0 && keyCode <= KeyEvent.KEYCODE_9)
				|| (keyCode >= KeyEvent.KEYCODE_A && keyCode <= KeyEvent.KEYCODE_Z)) {
			/* Decode keys to char. Our app only needs text */
			curFragment.onKeyPress((char) event.getUnicodeChar());
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_DEL) {
			curFragment.onDeleteKeyPress();
		}
		/* End of barcode indicated by enter key */
		else if (keyCode == KeyEvent.KEYCODE_ENTER) {
			curFragment.onEnterKeyPress();
			return true;
		}
		/* Pass out unused event */
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return true;
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		return true;
	}

	@Override
	public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
		return true;
	}

	@Override
	public boolean onKeyShortcut(int keyCode, KeyEvent event) {
		return true;
	}

	private AlertDialog mProgressDialog;
	private View progressDialogView;
	private App m_app;
	private PersistenceLayer m_persistanceLayer;
	// Authentication information
	private Service_DisplayTimeoutDialog service_displayTimeoutDialog;
	private final int RESETDELAYDEFAULT = 450; // TODO
												// change
												// back
												// to
												// 45secs
	private final int RESETDELAYLASTSCREEN = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Data configuration
		m_app = (App) getApplication();
		m_persistanceLayer = m_app.getPersistenceLayer();
		createProgressDialog();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
				requestPermissions(new String[] {Manifest.permission.CAMERA}, 1);
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		m_app.setCurrentActivity(this);
		m_persistanceLayer.startServices();
		m_persistanceLayer.addListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		m_persistanceLayer.stopServices();
		m_persistanceLayer.removeListener(this);
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		startTimeoutDialogService();
	}

    boolean mProgressDialogIsShowing = false;
	private void createProgressDialog() {
		FrameLayout progressdialogContainer = new FrameLayout(this);
		progressDialogView = getLayoutInflater().inflate(R.layout.view_progressdialog_layout, progressdialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setView(progressdialogContainer);
		mProgressDialog = builder.create();
        mProgressDialog.setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                mProgressDialogIsShowing = true;
            }
        });    
        mProgressDialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mProgressDialogIsShowing =false;
            }
        });

		mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mProgressDialog.show();
		mProgressDialog.dismiss();
		ViewGroup.LayoutParams lp = progressDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_progressdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_progressdialog_height);
		progressDialogView.setLayoutParams(lp);
		((View) progressDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
	}

	public void showProgressDialog() {
		if (mProgressDialog == null)
			createProgressDialog();
		if (!mProgressDialog.isShowing())
			mProgressDialog.show();
	}

	public void dismissProgressDialog() {
		if (mProgressDialog != null && mProgressDialog.isShowing())
			mProgressDialog.dismiss();
	}

	public void startTimeoutDialogService() {
		// Log.w("TIMEOUT", "START SERVICE");
		int delay = RESETDELAYDEFAULT;
		boolean showDialog = true;
		if (service_displayTimeoutDialog != null)
			service_displayTimeoutDialog.shutdown();
		ScreenSwitcherFragment currFrag = m_screenSwitcherAdapter.getFragmentAtIndex(m_screenSwitcherAdapter
				.getCurrentIndex());
		if (currFrag.getClass() == ThankYouScreenFragment.class) {
			delay = RESETDELAYLASTSCREEN;
			showDialog = false;
		} else
			delay = RESETDELAYDEFAULT;
		if (currFrag.getClass() != MainScreenFragment.class)
			service_displayTimeoutDialog = new Service_DisplayTimeoutDialog(this, delay, showDialog);
	}

	@Override
	public void onFragmentCreated(ScreenSwitcherFragment fragment) {
		m_persistanceLayer.updateCapacityInfo();
		super.onFragmentCreated(fragment);
	}

	@Override
	public void onFragmentPaused(ScreenSwitcherFragment fragment) {
		super.onFragmentPaused(fragment);
	}

	@Override
	public void onFragmentResumed(ScreenSwitcherFragment fragment) {
		super.onFragmentResumed(fragment);
	}

	@Override
	public void onFragmentVisible(ScreenSwitcherFragment fragment) {
		super.onFragmentVisible(fragment);
		m_persistanceLayer.updateCapacityInfo(); // Calls Service_Capacity to force update of member/staff data.
		startTimeoutDialogService();
	}

	@Override
	public void onLoop() {
		super.onLoop();
		m_persistanceLayer.getPersistenceData().clearDataOnLoop();
	}

	@Override
	public PersistenceLayer getPersistanceLayer() {
		return m_persistanceLayer;
	}

	@Override
	public ScreenSwitcher getScreenSwitcher() {
		return (ScreenSwitcher) findViewById(R.id.pager);
	}

	@Override
	public Map<Class<? extends ScreenSwitcherFragment>, List<ScreenSwitcherFragment>> getFragmentMap() {
		/* @formatter:off */
		Map<Class<? extends ScreenSwitcherFragment>, List<ScreenSwitcherFragment>> fragmentMap = new HashMap<Class<? extends ScreenSwitcherFragment>, List<ScreenSwitcherFragment>>();
		/*
		 * fragmentMap.put( MemberPhotoScreenFragment.class, new
		 * ArrayList<ScreenSwitcherFragment>(Arrays.asList(new
		 * CubbyScreenFragment(), new WorkoutAreaScreenFragment(), new
		 * PagerCheckOutScreenFragment())));
		 */
		fragmentMap.put(MainScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new AuthenticationScreenFragment())));
		/*
		 * fragmentMap.put( CheckInOrOutScreenFragment.class, new
		 * ArrayList<ScreenSwitcherFragment>(Arrays.asList(new
		 * AuthenticationScreenFragment(), new PagerCheckInScreenFragment())));
		 */
		fragmentMap.put(
				AuthenticationScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new CheckInScreenFragment(),
						new CheckOutScreenFragment(), new SelectUserScreenFragment(), new RegisterScreenFragment(),
						new CubbyScreenFragment(), new ConfirmationScreenFragment(), new SelectProgramScreenFragment())));
		fragmentMap.put(
				SelectUserScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new CheckInScreenFragment(),
						new CheckOutScreenFragment(), new CubbyScreenFragment(), new ConfirmationScreenFragment(),
						new SelectRoomScreenFragment(), new SelectProgramScreenFragment())));
		fragmentMap.put(
				SelectProgramScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new CheckInScreenFragment(), new ConfirmationScreenFragment(), new SelectRoomScreenFragment())));
		fragmentMap.put(
				CheckInScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new CubbyScreenFragment(),
						new SelectRoomScreenFragment(), new WorkoutAreaScreenFragment(), new ConfirmationScreenFragment(), new QuestionScreenFragment())));
		fragmentMap.put(QuestionScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new CubbyScreenFragment(), new SelectRoomScreenFragment(),
						new ConfirmationScreenFragment(), new WorkoutAreaScreenFragment())));
		fragmentMap.put(
				CubbyScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new SelectRoomScreenFragment(),
						new ConfirmationScreenFragment(), new WorkoutAreaScreenFragment())));
		fragmentMap.put(
				CheckOutScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new ConfirmationScreenFragment(),
						new EmergencyReentryScreenFragment())));
		fragmentMap.put(SelectRoomScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new WorkoutAreaScreenFragment(), new ConfirmationScreenFragment())));
		fragmentMap.put(WorkoutAreaScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new EstimatedReturnScreenFragment(), new ConfirmationScreenFragment())));
		fragmentMap.put(EstimatedReturnScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new ConfirmationScreenFragment())));
		fragmentMap.put(
				ConfirmationScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new ThankYouScreenFragment(),
						new OfferScreenFragment())));
		fragmentMap.put(OfferScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new ThankYouScreenFragment())));
		fragmentMap.put(
				RegisterScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new FamilyConfirmationScreenFragment(),
						new SelectFamilyUnitScreenFragment())));
		fragmentMap.put(SelectFamilyUnitScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new FamilyConfirmationScreenFragment())));
		fragmentMap.put(FamilyConfirmationScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new EmergencyScreenFragment())));
		fragmentMap.put(SignUpSelectUserScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new SignUpSelectKidScreenFragment())));
		fragmentMap.put(SignUpSelectKidScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new EmergencyScreenFragment())));
		fragmentMap.put(EmergencyScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new MedicalScreenFragment())));
		fragmentMap.put(MedicalScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new RegisterThankYouScreenFragment())));
		fragmentMap.put(
				RegisterThankYouScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new SelectUserScreenFragment(),
						new CheckInScreenFragment())));
		fragmentMap.put(EmergencyReentryScreenFragment.class,
				new ArrayList<ScreenSwitcherFragment>(Arrays.asList(new ConfirmationScreenFragment())));
		return fragmentMap;
	}

	@Override
	public ScreenSwitcherFragment getFirstScreen() {
		return new MainScreenFragment();
		// return new MemberPhotoScreenFragment();
	}

	@Override
	public void updating() {
		if (m_displayedFragment instanceof PopupFragment)
			showProgressDialog();
	}

	@Override
	public void updated() {
		dismissProgressDialog();
	}

	@Override
	public void changed(CHANGE_ID id, Object newValue) {
	}

	@Override
	public void exception(Exception e) {
	}
	
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!mProgressDialogIsShowing)
            return super.dispatchTouchEvent(ev);
        else
            return true;
    }

}
