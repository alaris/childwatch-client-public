package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;

public class CheckOutChild extends Command<Record> {

	private final Facility m_facility;
	private final Member m_child;
	private final Member m_parent;
	private final SignInType m_signInType;
	private String m_roomName;

	public CheckOutChild(Facility facility, Member child, Member parent, SignInType signInType) {
		this(facility, child, parent, signInType, null, null);
	};

	public CheckOutChild(Facility facility, Member child, Member parent, SignInType signInType, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_child=child;
		m_facility=facility;
		m_parent=parent;
		m_signInType=signInType;
		m_roomName = "";
	}

	@Override
	public void executeSource() {
		long diffMinutes = DateGenerator.getMinutesSince(m_child.getLastCheckIn()); 
		long minsLeft = m_child.getMinutesLeft().longValue();
		long updatedMinsLeft = minsLeft - diffMinutes;
		/*if (m_child.isGuest()){
			if (m_child.getGuestDaysLeft().intValue() <= 0 && !m_child.getWasOverridden())
				m_child.setIsValidMembership(false);
		}*/
		if (m_child.getPRoom() != null)
			m_roomName = m_child.getPRoom().getName();
		m_dataSource.getMembersDAO().checkOutMember(m_child, new CheckOutChildCommandListener(this));
	}

	@Override
	public void handleResponse(Record member) {
	}
	
	private class CheckOutChildCommandListener implements DaycareOperationComplete<Member> {
		private CheckOutChild m_listener;
		
		public CheckOutChildCommandListener(CheckOutChild listener){
			m_listener=listener;
		}

		@Override
		public void operationComplete(Member arg0, DaycareDataException arg1) {
			m_dataSource.getRecordsDAO().saveCheckOutRecord(m_child, m_facility, m_parent, m_signInType, m_roomName, m_listener);
		}
	}
	
}