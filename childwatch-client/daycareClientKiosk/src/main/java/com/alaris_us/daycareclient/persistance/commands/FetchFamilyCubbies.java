package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Family;

public class FetchFamilyCubbies extends Command<List<Cubby>> {
	Family m_family;

	public FetchFamilyCubbies(Family family) {

		this(family, null, null);
	}

	protected FetchFamilyCubbies(Family family, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_family=family;
	}

	@Override
	public void executeSource() {

		m_dataSource.getCubbiesDAO().listFamilyCubbies(m_family, this);

	}

	@Override
	public void handleResponse(List<Cubby> data) {
		m_model.setFamilyCubbies(data);
	}
}