package com.alaris_us.daycareclient.widgets;

import android.graphics.Bitmap;

import com.alaris_us.daycareclient.utils.ImageFunctions;
import com.alaris_us.daycaredata.to.DaycareDataTO;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.ParseDataMember;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.bindroid.trackable.TrackableField;

public class ConfirmationItemModel {

	private TrackableField<DaycareDataTO> m_item;
	private Integer m_width;
	private Integer m_color;
	private Integer m_height;
	private Bitmap mColorizedImage;

	public ConfirmationItemModel() {
		//this(null, null, null);
	}

	public ConfirmationItemModel(DaycareDataTO item, Integer width, Integer height, Integer color) {
		m_item = new TrackableField<DaycareDataTO>(item);
		m_width = width;
		m_height = height;
		m_color = color;
	}
	
	public ConfirmationItemModel(DaycareDataTO item, Integer width, Integer height) {
		m_item = new TrackableField<DaycareDataTO>(item);
		m_width = width;
		m_height = height;
	}
	
	public DaycareDataTO getItem() {
		return m_item.get();
	}

	public void setItem(DaycareDataTO item) {
		m_item.set(item);
	}

	public String getItemName() {
		if(m_item.get().getClass()==ParseDataMember.class)
			return ((Member)m_item.get()).getFirstName() + " " + ((Member)m_item.get()).getLastName();
		else
			return ((WorkoutArea)m_item.get()).getWorkoutName();
	}

	public Bitmap getItemImage() {
		if (null == mColorizedImage) {
			if(m_item.get().getClass()==ParseDataMember.class) {
				mColorizedImage = ImageFunctions.createDirectCopy(((Member)m_item.get()).getImage());
				ImageFunctions.colorizeBitmap(mColorizedImage, m_color, ImageFunctions.colorizeMix,
						ImageFunctions.colorizeSaturation);

				//return ImageFunctions.colorizeBitmap(((Member)m_item.get()).getImage(), m_color, ImageFunctions.colorizeMix, ImageFunctions.colorizeSaturation);
			}
			else {
				mColorizedImage = (Bitmap)((WorkoutArea) m_item.get()).getWorkoutImage();
			}
		}
		return mColorizedImage;
		
	}
	
	public String getItemRoom() {
		String roomText = "ROOM: ";
		if(m_item.get().getClass()==ParseDataMember.class)
			return roomText + ((Member)m_item.get()).getPRoom().getName();
		else
			return "";
	}
	
	public int getBackgroundColor(){
		return m_color;
	}

	public int getWidth() {
		return m_width;
	}
	
	public int getHeight(){
		return m_height;
	}
}
