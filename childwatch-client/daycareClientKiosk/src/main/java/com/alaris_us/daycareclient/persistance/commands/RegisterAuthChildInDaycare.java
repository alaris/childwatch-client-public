package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;

public class RegisterAuthChildInDaycare extends Command<Member> {
	private Family m_family;
	private Member m_child;
	private Facility m_facility;

	public RegisterAuthChildInDaycare(Member child, Facility facility) {
		this(child, facility, null, null);
	};

	public RegisterAuthChildInDaycare(Member children, Facility facility, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		//m_family = family;
		m_child = children;
		m_facility = facility;
	}

	@Override
	public void executeSource() {
		List<Member> members = new ArrayList<Member>();
		members.add(m_child);
		//m_model.setFamily(m_family);
		m_child.getPFamily().setName(findCommonLastName(members));
		//m_family.setName(findCommonLastName(members));
		//m_child.setPFamily(m_family);
		
		m_dataSource.getMembersDAO().create(m_child, this);
	}

	@Override
	public void handleResponse(Member members) {

	}

	private String findCommonLastName(List<Member> members) {
		String commonLastName = "";
		Map<String, Integer> map = new HashMap<String, Integer>();
		int count = 1;
		int max = 1;

		for (Member member : members) {
			String lastname = member.getLastName();
			if (map.containsKey(lastname)) {
				count = map.get(lastname) + 1;
				if (count > max)
					max = count;
				map.put(lastname, count);
			} else {
				map.put(lastname, 1);
			}
		}
		for (Entry<String, Integer> entry : map.entrySet()) {
			if (max == entry.getValue()) {
				commonLastName = entry.getKey();
				break;
			}
		}
		return commonLastName;
	}
	
	private class RegisterMemberCommandListener implements DaycareOperationComplete<List<Member>> {
		private DaycareOperationComplete<List<Member>> listener;

		public RegisterMemberCommandListener(DaycareOperationComplete<List<Member>> listener) {
			this.listener = listener;
		}

		@Override
		public void operationComplete(final List<Member> members, DaycareDataException arg1) {
			m_dataSource.getRecordsDAO().saveRegistrationEvents(members, m_facility,
					new DaycareOperationComplete<List<Record>>() {
						@Override
						public void operationComplete(List<Record> data, DaycareDataException e) {
							listener.operationComplete(members, e);
						}
					});
		}
	}

}