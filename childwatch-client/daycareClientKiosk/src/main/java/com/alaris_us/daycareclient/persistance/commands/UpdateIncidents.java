package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Incident;

public class UpdateIncidents extends Command<Incident> {
	Incident m_incident;
	boolean m_isAcknowledged;

	public UpdateIncidents(Incident incident, boolean isAcknowledged) {

		this(incident, isAcknowledged, null, null);
	}

	protected UpdateIncidents(Incident incident, boolean isAcknowledged, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_incident=incident;
		m_isAcknowledged = isAcknowledged;
	}

	@Override
	public void executeSource() {

		m_dataSource.getIncidentsDAO().updateIncidents(m_incident, m_isAcknowledged, this);

	}

	@Override
	public void handleResponse(Incident data) {
	}
}