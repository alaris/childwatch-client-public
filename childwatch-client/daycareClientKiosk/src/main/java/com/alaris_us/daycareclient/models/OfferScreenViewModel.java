package com.alaris_us.daycareclient.models;

import java.util.Random;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.alaris_us.daycareclient.fragments.OfferScreenFragment;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Offer;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class OfferScreenViewModel extends ViewModel {
	public OfferScreenViewModel(PersistenceLayer persistenceLayer, OfferScreenFragment fragment) {
		super(persistenceLayer);
		this.m_fragment = fragment;
		reset();
	}

	private TrackableField<Member>						m_currentMember;
	private TrackableField<AlertDialog>					m_emailDialog;
	private OfferScreenFragment							m_fragment;
	private TrackableField<AlertDialog>					m_verifyEmailDialog;
	private TrackableField<String>						m_emailAddress;
	private TrackableField<Boolean>						m_hasEmailOnFile;
	private TrackableField<TrackableCollection<Offer>>	m_offerList;
	private int											m_randOfferInt;

	@Override
	protected void onPopulateModelData() {
		m_offerList.set(new TrackableCollection<Offer>(getPersistenceLayer().getPersistenceData().getOffersList()));
		setRandOfferInt();
	}

	@Override
	public void onInitialize() {
		m_currentMember = new TrackableField<Member>(getPersistenceLayer().getPersistenceData().getCurrentMember());
		m_emailDialog = new TrackableField<AlertDialog>();
		m_verifyEmailDialog = new TrackableField<AlertDialog>();
		if (m_currentMember.get().getEmail() != null)
			m_emailAddress = new TrackableField<String>(m_currentMember.get().getEmail());
		m_hasEmailOnFile = new TrackableField<Boolean>();
		m_offerList = new TrackableField<TrackableCollection<Offer>>(new TrackableCollection<Offer>());
		addWatch(CHANGE_ID.CURRENTMEMBER);
	}

	public OnClickListener getEmailMeClickListener() {
		return new EmailMeClickListener();
	}

	public OnClickListener getNoThanksClickListener() {
		return new NoThanksClickListener();
	}

	public class EmailMeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_currentMember.set(getPersistenceLayer().getPersistenceData().getCurrentMember());
			m_hasEmailOnFile.set(m_currentMember.get().getEmail() != null
					&& !m_currentMember.get().getEmail().isEmpty());
			if (!m_hasEmailOnFile.get()) {
				showEmailDialog();
			} else
				showVerifyEmailDialog();
		}
	}

	public class NoThanksClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_fragment.nextScreen();
		}
	}

	public void setEmailDialog(AlertDialog alertDialog) {
		m_emailDialog.set(alertDialog);
	}

	private void showEmailDialog() {
		if (m_emailDialog.get() != null) {
			m_emailDialog.get().show();
		}
	}

	private void dismissEmailDialog() {
		if (m_emailDialog.get() != null && m_emailDialog.get().isShowing())
			m_emailDialog.get().dismiss();
	}

	public OnClickListener getEmailDialogOkClickListener() {
		return new EmailDialogOkClickListener();
	}

	public OnClickListener getEmailDialogCancelClickListener() {
		return new EmailDialogCancelClickListener();
	}

	public class EmailDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_emailAddress.set(m_fragment.getNewEmail());
			dismissEmailDialog();
			showVerifyEmailDialog();
		}
	}

	public class EmailDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_hasEmailOnFile.get()) {
				dismissEmailDialog();
				showVerifyEmailDialog();
			} else {
				dismissEmailDialog();
			}
		}
	}

	public void setVerifyEmailDialog(AlertDialog alertDialog) {
		m_verifyEmailDialog.set(alertDialog);
	}

	private void showVerifyEmailDialog() {
		if (m_verifyEmailDialog.get() != null) {
			m_verifyEmailDialog.get().show();
		}
	}

	private void dismissVerifyEmailDialog() {
		if (m_verifyEmailDialog.get() != null && m_verifyEmailDialog.get().isShowing())
			m_verifyEmailDialog.get().dismiss();
	}

	public OnClickListener getVerifyEmailDialogSendClickListener() {
		return new VerifyEmailDialogSendClickListener();
	}

	public OnClickListener getVerifyEmailDialogCancelClickListener() {
		return new VerifyEmailDialogCancelClickListener();
	}

	public OnClickListener getVerifyEmailDialogOtherClickListener() {
		return new VerifyEmailDialogOtherClickListener();
	}

	public class VerifyEmailDialogSendClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_fragment.getIsSetAsDefault()) {
				m_currentMember.get().setEmail(getEmailAddress());
				getPersistenceLayer().updateMember(m_currentMember.get(), new CommandListener());
			}
			dismissVerifyEmailDialog();
			// TODO send email to m_emailAddress
			getPersistenceLayer().addEmailToEmailQueue(m_currentMember.get(), m_emailAddress.get(),
					m_offerList.get().get(m_randOfferInt));
			m_fragment.nextScreen();
		}
	}

	public class VerifyEmailDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_hasEmailOnFile.get()) {
				dismissVerifyEmailDialog();
			} else {
				dismissVerifyEmailDialog();
				showEmailDialog();
			}
		}
	}

	public class VerifyEmailDialogOtherClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissVerifyEmailDialog();
			showEmailDialog();
		}
	}

	public String getEmailAddress() {
		return m_emailAddress.get();
	}

	public int getIsEmailOnFile() {
		if (m_hasEmailOnFile.get())
			return View.VISIBLE;
		else
			return View.GONE;
	}

	public LayoutParams getLayoutParams() {
		if (!m_hasEmailOnFile.get())
			return new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 5);
		else
			return new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 3.33f);
	}

	public String getHeaderText() {
		return m_offerList.get().get(m_randOfferInt).getDisplayHeader();
	}

	public String getDisplayText() {
		return m_offerList.get().get(m_randOfferInt).getDisplayText();
	}

	public Bitmap getDisplayImage() {
		return m_offerList.get().get(m_randOfferInt).getDisplayImage();
	}

	public static int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt(max);
		return randomNum;
	}

	public void setRandOfferInt() {
		m_randOfferInt = randInt(0, m_offerList.get().size());
	}

	private class CommandListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			dismissVerifyEmailDialog();
			m_fragment.nextScreen();
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
}
