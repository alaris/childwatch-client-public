/**
 * ImagePrint for printing
 * 
 * @author Brother Industries, Ltd.
 * @version 2.2
 */
package com.alaris_us.daycareclient.printing;

import android.content.Context;
import android.widget.Toast;

import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo.ErrorCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TemplatePrint extends BasePrint {

	private int CHILDTEMPLATE;
	private int SICKCHILDTEMPLATE;
	private int GUARDIANTEMPLATE;
	private JSONObject mLabelOpts;

	List<TemplateData> mPrintData = new ArrayList<TemplateData>();

    public TemplatePrint(Context context, MsgHandle mHandle, MsgDialog mDialog, String ip,  List<Integer> labelKeys, JSONObject labelOpts,
    		String model, String macAddress) {
        super(context, mHandle, mDialog, ip, model, macAddress);
        CHILDTEMPLATE = labelKeys.get(0);
		SICKCHILDTEMPLATE = labelKeys.get(1);
		GUARDIANTEMPLATE = labelKeys.get(2);
		mLabelOpts = labelOpts;
    }

	public void clearPrintData() {
		mPrintData.clear();
	}

	public void addChildPrint(String childName, Date birthDate, String barcodeData, String guardianName, List<String> guardianLocations, 
			String guardianPhone, String conditions, Date checkIn, String returnTime, String familyCubbiesString, String diaper, String feeding, 
			String potty, String program) {

		ChildTemplateData toPrint;

		if (null != conditions && conditions.length() > 0) {
			// Child has a condition
			toPrint = new SickChildTemplateData();
			toPrint.conditions = conditions;
		} else {
			// Child has no condition
			toPrint = new ChildTemplateData();
			toPrint.conditions = "";
		}

		// Perform required formatting calculation for age
		int age = DaycareHelpers.getAgeInYears(birthDate);
		if (age < 1) {
			age = DaycareHelpers.getAgeInMonths(birthDate);
			toPrint.age = age + " mo";
		} else {
			toPrint.age = age + " yr";
		}

		// Perform required formatting for birthdate
		toPrint.birthDate = DateFormat.getDateInstance().format(birthDate);

		// Perform required formatting for the locations
		if (null != guardianLocations && guardianLocations.size() > 0) {

			toPrint.guardianLocations = guardianLocations.get(0);

			for (int x = 1; x < guardianLocations.size(); x++) {
				toPrint.guardianLocations = toPrint.guardianLocations.concat(", ").concat(guardianLocations.get(x));
			}

		} else {
			toPrint.guardianLocations = "None";
		}

		// Perform required formatting for checkin date
		toPrint.checkInDate = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.getDefault()).format(checkIn);
				
		// Copy rest of fields
		if (null != barcodeData) {
			toPrint.barcodeData = barcodeData;
		}
		toPrint.guardianName = guardianName;
		if (null != guardianPhone) {
			toPrint.guardianPhone = guardianPhone;
		}
		toPrint.name = childName;
		toPrint.returnTime = returnTime;
		if (familyCubbiesString.isEmpty())
			familyCubbiesString = " ";
		toPrint.cubbies = familyCubbiesString;
        toPrint.diaper = diaper;
        toPrint.feeding = feeding;
        toPrint.potty = potty;
        //toPrint.program = program;

		// Add to list
		mPrintData.add(toPrint);

	}

	public void addGuardianPrint(List<String> childrenNames, List<String> childrenAges, String guardianName,
			Date checkIn, String barcodeData, String returnTime, String familyCubbiesString) {

		GuardianTemplateData toPrint = new GuardianTemplateData();

		// Perform required formatting for the child names
		if (null != childrenNames && childrenNames.size() > 0) {

			for (int x = 0; x < childrenNames.size() && x < 9; x++) {
				toPrint.children[x] = childrenNames.get(x);
			}

		} else {
			toPrint.children[0] = "None";
		}
		
		// Perform required formatting for the child ages
		if (null != childrenAges && childrenAges.size() > 0) {

			for (int x = 0; x < childrenAges.size() && x < 9; x++) {
				toPrint.ages[x] = childrenAges.get(x);
			}

		} else {
			toPrint.children[0] = " ";
		}

		// Perform required formatting for checkin date
		toPrint.checkInDate = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.getDefault()).format(checkIn);

		// Copy rest of fields
		if (null != barcodeData) {
			toPrint.barcodeData = barcodeData;
		}
		toPrint.name = guardianName;
		toPrint.returnTime = returnTime;
		if (familyCubbiesString.isEmpty())
			familyCubbiesString = " ";
		toPrint.cubbies = familyCubbiesString;

		// Add to list
		mPrintData.add(toPrint);

	}

	/** do the particular print */
	@Override
	protected void doPrint() {

		for (TemplateData curTemplate : mPrintData) {

			curTemplate.startPrint(mPrinter);

			curTemplate.doPrint(mPrinter);

			mPrintResult = mPrinter.flushPTTPrint();

			// if error, stop the next print
			if (mPrintResult.errorCode != ErrorCode.ERROR_NONE) {
				//Crashlytics.setString("Printer Error", mPrintResult.errorCode.toString());
				//Toast.makeText(mContext, mPrintResult.errorCode.toString(), Toast.LENGTH_LONG).show();
				break;
			}
		}
	}

	private abstract class TemplateData {
		String name = "";

		public abstract void startPrint(Printer prn);

		public void doPrint(Printer prn) {
			prn.replaceTextName(name, "name");
		}
	}

	private class ChildTemplateData extends TemplateData {
		String birthDate = "";
		String barcodeData = "";
		String guardianName = "";
		String guardianLocations = "";
		String guardianPhone = "";
		String age = "";
		String checkInDate = "";
		String returnTime = "";
		String cubbies = "";
		String diaper = "";
		String feeding = "";
        String potty = "";
        String conditions = "";
        String program = "";

		@Override
		public void startPrint(Printer prn) {
			prn.startPTTPrint(CHILDTEMPLATE, null);
		}

		@Override
		public void doPrint(Printer prn) {
			super.doPrint(prn);
			try {
				if (mLabelOpts.getBoolean("DOB"))
					prn.replaceTextName(birthDate, "birth");
			} catch (JSONException e) {
			}
			prn.replaceTextName(guardianName, "guardian");
			prn.replaceTextName(guardianLocations, "areas");
			try {
				if (mLabelOpts.getBoolean("guardianPhone"))
					prn.replaceTextName(guardianPhone, "phone");
			} catch (JSONException e) {
			}
			prn.replaceTextName(age, "age");
			prn.replaceTextName(checkInDate, "checkIn");
			prn.replaceTextName(returnTime, "returnBy");
			try {
				if (mLabelOpts.getBoolean("cubbies"))
					prn.replaceTextName(cubbies, "cubbies");
			} catch (JSONException e) {
			}
			try {
				if (mLabelOpts.getBoolean("diaper"))
					prn.replaceTextName(diaper, "diaper");
			} catch (JSONException e) {
			}
			try {
				if (mLabelOpts.getBoolean("feeding"))
					prn.replaceTextName(feeding, "feeding");
			} catch (JSONException e) {
			}
			try {
				if (mLabelOpts.getBoolean("potty"))
					prn.replaceTextName(potty, "potty");
			} catch (JSONException e) {
			}
			try {
				if (mLabelOpts.getBoolean("medical"))
					prn.replaceTextName(conditions, "conditions");
			} catch (JSONException e) {
			}
			try {
				if (mLabelOpts.getBoolean("program"))
					prn.replaceTextName(program, "program");
			} catch (JSONException e) {
			}
			prn.replaceTextName(barcodeData, "barcode");
		}
	}

	private class SickChildTemplateData extends ChildTemplateData {

		@Override
		public void startPrint(Printer prn) {
			prn.startPTTPrint(SICKCHILDTEMPLATE, null);
		}

		@Override
		public void doPrint(Printer prn) {
			super.doPrint(prn);
		}

	}

	private class GuardianTemplateData extends TemplateData {
		String children[] = { " ", " ", " ", " ", " ", " ", " " };
		String ages[] = { " ", " ", " ", " ", " ", " ", " " };
		String checkInDate = "";
		String barcodeData = "";
		String returnTime = "";
		String cubbies = "";

		@Override
		public void startPrint(Printer prn) {
			prn.startPTTPrint(GUARDIANTEMPLATE, null);
		}

		@Override
		public void doPrint(Printer prn) {
			super.doPrint(prn);
			prn.replaceTextName(children[0], "child1");
			prn.replaceTextName(children[1], "child2");
			prn.replaceTextName(children[2], "child3");
			prn.replaceTextName(children[3], "child4");
			prn.replaceTextName(children[4], "child5");
			prn.replaceTextName(children[5], "child6");
			prn.replaceTextName(children[6], "child7");
			
			prn.replaceTextName(ages[0], "age1");
			prn.replaceTextName(ages[1], "age2");
			prn.replaceTextName(ages[2], "age3");
			prn.replaceTextName(ages[3], "age4");
			prn.replaceTextName(ages[4], "age5");
			prn.replaceTextName(ages[5], "age6");
			prn.replaceTextName(ages[6], "age7");

			prn.replaceTextName(checkInDate, "checkIn");
			prn.replaceTextName(returnTime, "returnBy");
			try {
				if (mLabelOpts.getBoolean("cubbies"))
					prn.replaceTextName(cubbies, "cubbies");
			} catch (JSONException e) {
			}
			prn.replaceTextName(barcodeData, "barcode");
		}
	}
}