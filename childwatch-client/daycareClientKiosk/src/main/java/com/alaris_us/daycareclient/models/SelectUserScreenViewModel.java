package com.alaris_us.daycareclient.models;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.alaris_us.daycareclient.fragments.SelectUserScreenFragment;
import com.alaris_us.daycareclient.models.AuthenticationScreenViewModel.LoginResult;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceData.InOrOut;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.MemberCheckBox;
import com.alaris_us.daycareclient.widgets.MemberCheckBoxModel;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Recurrence;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Schedule;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

public class SelectUserScreenViewModel extends ViewModel {
	public SelectUserScreenViewModel(PersistenceLayer persistenceLayer, SelectUserScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private State m_currentState;
	private SelectUserScreenFragment fragment;
	private TrackableCollection<MemberCheckBoxModel> m_memberModels;
	private TrackableField<Member> m_selectedMember;
	private boolean m_isOverride;
	private TrackableField<TrackableCollection<Room>> m_assignedRooms;
	private List<Member>m_multiRoomChildren;
	private TrackableCollection<Member> m_allMembers;
	private final int m_singleUserTitleRes = R.string.selectuserscreen_tile_selectuser_text_singleuser;
	private final int m_multiUserTitleRes = R.string.selectuserscreen_tile_selectuser_text;
	private String mOverridePIN;
	private List<Program> m_allPrograms;
	private List<Program> m_availablePrograms;
	private int m_programSize;
	private int m_scheduleSize;
	private int m_programsIterated;
	private int m_schedulesIterated;
	private HashMap<Program, Schedule> m_availableProgramSchedules;
	private int m_programIndex;


	public TrackableCollection<MemberCheckBoxModel> getMembers() {
		return m_memberModels;
	}

	public void onReset() {
		m_isOverride = getPersistenceLayer().getPersistenceData().getIsOverride();
		m_currentState = new AwaitingSelectionState();
		m_currentState.enterState();
	}

	@Override
	protected void onPopulateModelData() {
		mOverridePIN = getPersistenceLayer().getPersistenceData().getOverridePIN();
		m_allMembers.clear();
		m_allMembers.addAll(getPersistenceLayer().getPersistenceData().getValidDaycareMembersWithPin());
		m_memberModels.clear();
		m_selectedMember.set(null);
		int pos = 0;
		int memNum = 0;
		for (Member member : m_allMembers) {
			if (!m_isOverride) {
				if (member.getUsesChildWatch())
					memNum++;
			} else {
				memNum++;
			}

		}
		int width = ListViewFunctions.calculateTileWidthForSelectMember(memNum);
		for (Member member : m_allMembers) {
			if (!m_isOverride) {
				if (member.getUsesChildWatch()) {
					MemberCheckBoxModel model = new MemberCheckBoxModel(member, width,
							ListViewFunctions.getColorAt(pos++));
					m_memberModels.add(model);
				}
			} else {
				MemberCheckBoxModel model = new MemberCheckBoxModel(member, width, ListViewFunctions.getColorAt(pos++));
				m_memberModels.add(model);
			}
		}
		if (m_allMembers.size() == 1) {
			fragment.getUserList().setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1));
			fragment.getSingleUserName().setText(m_allMembers.get(0).getFirstName() + " " + m_allMembers.get(0).getLastName());
			fragment.getSingleUserName().setVisibility(View.VISIBLE);
			m_memberModels.get(0).setIsSelected(true);
			m_memberModels.get(0).setNameRes(false);
			m_selectedMember.set(m_memberModels.get(0).getMember());
		} else {
			fragment.getSingleUserName().setVisibility(View.GONE);
			fragment.getUserList().setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 2));
		}
	}

	public OnClickListener getNextClickListener() {
		if (m_selectedMember.get() != null)
			return new NextClickListener();
		else
			return null;
	}

	public float getNextAlpha() {
		return ListViewFunctions.getTileAlpha(m_selectedMember.get() != null);
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof AwaitingSelectionState)
				m_currentState.exitState();
		}
	}

	public class MemberClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			if (m_currentState instanceof AwaitingSelectionState) {
				Member member = ((MemberCheckBox) arg1).getMember();
				MemberCheckBoxModel memberModel = ((MemberCheckBox) arg1).getModel();
				if (!memberModel.getIsSelected()) {
					for (MemberCheckBoxModel model : m_memberModels)
						model.setIsSelected(model.equals(memberModel));
					m_selectedMember.set(member);
				}
			}
		}
	}

	@Override
	public void onInitialize() {
		m_memberModels = new TrackableCollection<MemberCheckBoxModel>();
		m_selectedMember = new TrackableField<Member>();
		m_isOverride = getPersistenceLayer().getPersistenceData().getIsOverride();
		m_assignedRooms = new TrackableField<TrackableCollection<Room>>(new TrackableCollection<Room>());
		m_multiRoomChildren = new ArrayList<Member>();
		m_allMembers = new TrackableCollection<Member>();
		m_allPrograms = new ArrayList<Program>();
		m_availablePrograms = new ArrayList<>();
		m_availableProgramSchedules = new HashMap<>();
	}

	abstract class State {
		public abstract void enterState();

		public void exitState() {
			m_currentState = null;
		}

		public void exception(Exception e) {
			m_currentState = null;
		}
	}

	private class AwaitingSelectionState extends State {
		@Override
		public void enterState() {
			Log.w("AwaitingSelectionState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("AwaitingSelectionState", "ExitState");
			if (m_selectedMember.get() != null) {
				if (!m_isOverride && m_selectedMember.get().getIsValidMembership() == false)
					fragment.showInvalidMemberSelectedDialog();
				else {
					getPersistenceLayer().getAdmissionOverride(Arrays.asList(m_selectedMember.get()), getPersistenceLayer().getPersistenceData().getFacility(),
							new AdmissionOverrideCommandListener());
				}
			} else
				m_currentState.exception(new Exception("No Member Selected"));
		}

		@Override
		public void exception(Exception e) {
			Log.w("AwaitingSelectionState", "Exception: " + e.getMessage());
			reset();
		}
	}

	//TODO: Replace this with a kotlin class
	private class CalculateAvailableProgramsCommandState extends State {

		@Override
		public void enterState() {
			Log.w("CalculateAvailableProgramsCommandState", "EnterState");
			m_availablePrograms.clear();
			m_availableProgramSchedules.clear();
			m_allPrograms = getPersistenceLayer().getPersistenceData().getAllPrograms();
			m_programSize = m_allPrograms.size();
			m_programsIterated = 0;
			//for (Program p : m_allPrograms) {
				m_scheduleSize = 0;
				m_schedulesIterated = 0;
				getPersistenceLayer().fetchProgramSchedules(m_allPrograms.get(0), new FetchProgramSchedulesCommandListener(m_allPrograms.get(0)));
			//}
		}

		@Override
		public void exitState() {
			Log.w("CalculateAvailableProgramsCommandState", "ExitState");
			Member member = m_selectedMember.get();
			getPersistenceLayer().selectUser(member, new SelectUserCommandListener());
			m_currentState = new SelectUserCommandState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("CalculateAvailableProgramsCommandState", "Exception: " + e.getMessage());
		}
	}
	
	private class SelectUserCommandState extends State {

		@Override
		public void enterState() {
			Log.w("SelectUserCommandState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("SelectUserCommandState", "ExitState");
		}

		@Override
		public void exception(Exception e) {
			Log.w("SelectUserCommandState", "Exception: " + e.getMessage());
		}
	}

	private class FinishedState extends State {
		private Member m_member;

		public FinishedState(Member member) {
			m_member = member;
		}

		@Override
		public void enterState() {
			Log.w("FinishedState", "EnterState");
			getPersistenceLayer().getPersistenceData().setCurrentMember(m_member);

			if (getPersistenceLayer().getPersistenceData().getSelfCheckInChildren() != null) {
				if (getPersistenceLayer().getPersistenceData().getSelfCheckInChildren().contains(m_member)) {
					getPersistenceLayer().getPersistenceData().setLoginResult(LoginResult.CHILDUSER);
					getPersistenceLayer().getPersistenceData().setSelectedChildren(Arrays.asList(m_member));
				} else if (m_allMembers.size() == 1)
					getPersistenceLayer().getPersistenceData().setLoginResult(LoginResult.SINGLEUSER);
				else
					getPersistenceLayer().getPersistenceData().setLoginResult(LoginResult.MULTIUSER);
			} else if (m_allMembers.size() == 1)
				getPersistenceLayer().getPersistenceData().setLoginResult(LoginResult.SINGLEUSER);
			else
				getPersistenceLayer().getPersistenceData().setLoginResult(LoginResult.MULTIUSER);

			if (m_availablePrograms.size() == 1) {
				getPersistenceLayer().getPersistenceData().setSelectedProgram(m_availablePrograms.get(0));
				if (getPersistenceLayer().getPersistenceData().getLoginResult().equals(LoginResult.CHILDUSER)) {
					m_member.setInProgram(m_availablePrograms.get(0));
					getPersistenceLayer().fetchProgramRooms(m_availablePrograms.get(0),
							new FetchProgramRoomsCommandListener(m_member));
				} else
					fragment.nextScreen(getPersistenceLayer().getPersistenceData().getInOrOut());
			} else {
				if (getPersistenceLayer().getPersistenceData().getLoginResult().equals(LoginResult.CHILDUSER)) {
					for (Program p : m_availablePrograms) {
						if (DaycareHelpers.getIsAgeBetween(m_member.getBirthDate(), p.getMinAge().doubleValue(), p.getMaxAge().doubleValue())) {
							m_member.setInProgram(p);
							getPersistenceLayer().getPersistenceData().setSelectedProgram(p);
							break;
						}
					}
					getPersistenceLayer().fetchProgramRooms(m_member.getInProgram(),
							new FetchProgramRoomsCommandListener(m_member));
				} else
					fragment.nextScreen("PROGRAMS");
			}
		}

		@Override
		public void exitState() {
			Log.w("FinishedState", "ExitState");
		}

		@Override
		public void exception(Exception e) {
			Log.w("FinishedState", "Exception: " + e.getMessage());
		}
	}

	private class SelectUserCommandListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			List<Member> allChildren = new ArrayList<Member>();
			InOrOut inOrOut = getPersistenceLayer().getPersistenceData().getInOrOut();

			boolean hasRelated = false;

			if (inOrOut.equals(InOrOut.IN)) {
				List<Member> related = new ArrayList<Member>(getPersistenceLayer().getPersistenceData()
						.getRelatedInChildren());
				List<Member> proxy = new ArrayList<Member>(getPersistenceLayer().getPersistenceData()
						.getProxyInChildren());
				allChildren.addAll(related);
				allChildren.addAll(proxy);
				// Search family list to see if there are any children
				for (Member m : allChildren) {
					if (true == m.getUsesChildWatch()) {
						hasRelated = true;
					}
				}

				if (hasRelated) {
					m_currentState = new FinishedState(m_selectedMember.get());
					m_currentState.enterState();
					return;
				} else {
					fragment.showNoChildrenOfAgeDialog();
					return;
				}
			} else if (inOrOut.equals(InOrOut.NOKIDSOUT) || inOrOut.equals(InOrOut.OUT)) {
				fragment.showNoKidsForCheckDirectionDialog();
			} else if (inOrOut.equals(InOrOut.NOKIDSIN) || inOrOut.equals(InOrOut.NOKIDS)) {
				fragment.showNoChildrenOfAgeDialog();
			}

		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
	
	public int getTitleTextRes(){
		if (m_allMembers.size() == 1)
			return m_singleUserTitleRes;
		else 
			return m_multiUserTitleRes;
	}
	
	public void overrideSuccess() {
		getPersistenceLayer().getPersistenceData().setIsOverride(true);
		/*Member member = m_selectedMember.get();
		getPersistenceLayer().selectUser(member, new SelectUserCommandListener());
		m_currentState = new SelectUserCommandState();*/
		m_currentState = new CalculateAvailableProgramsCommandState();
		m_currentState.enterState();
	}
	
	public boolean isValidOverridePIN(String pin) {
		return pin.equals(mOverridePIN);
	}
	
private class FetchProgramSchedulesCommandListener implements CommandStatusListener {
		
		private Program m_program;
		
		public FetchProgramSchedulesCommandListener(Program program) {
			m_program = program;	
		}
		
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void valueChanged(Command<?> command) {
			List<Schedule> schedules = (List<Schedule>) command.getResult();
			m_scheduleSize = schedules.size();
			getPersistenceLayer().fetchScheduleRecurrences(schedules, new FetchScheduleRecurrencesCommandListener(m_program, schedules));
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
	
private class FetchScheduleRecurrencesCommandListener implements CommandStatusListener {
		
	private List<Schedule> m_schedule;
	private Program m_program;
	
	public FetchScheduleRecurrencesCommandListener(Program program, List<Schedule> schedule) {
		m_program = program;
		m_schedule = schedule;
	}
	
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			for (Schedule schedule : m_schedule) {
				Recurrence recurrence = schedule.getPRecurrence(); //TODO Add Logic for determining if program should be available
				if (DaycareHelpers.isProgramAvailable(schedule, recurrence))
					if (!m_availablePrograms.contains(m_program)) {
						m_availablePrograms.add(m_program);
						m_availableProgramSchedules.put(m_program, schedule);
						//m_schedulesIterated = m_schedule.size() - 1; // No need to go through schedules when 1 schedules matches today
					}
				m_schedulesIterated++;
				if (m_schedulesIterated == m_schedule.size()) {
					m_schedulesIterated = 0;
					m_programsIterated++;
					m_programIndex++;
					if (m_programsIterated == m_programSize) {
						if (m_availablePrograms.isEmpty()) {
							fragment.showNoProgramsAvailableDialog();
						} else {
							getPersistenceLayer().getPersistenceData().setAllAvailablePrograms(m_availablePrograms);
							getPersistenceLayer().getPersistenceData().setAvailableProgramSchedules(m_availableProgramSchedules);
							m_currentState.exitState();
						}
					} else {
						getPersistenceLayer().fetchProgramSchedules(m_allPrograms.get(m_programIndex), new FetchProgramSchedulesCommandListener(m_allPrograms.get(m_programIndex)));
					}
				}
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class FetchProgramRoomsCommandListener implements CommandStatusListener {
		private Member m_child;

		public FetchProgramRoomsCommandListener(Member child) {
			m_child = child;
		}

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void valueChanged(Command<?> command) {
			List<Room> rooms = (List<Room>) command.getResult();

			List<Room> qualifyingRooms = new ArrayList<Room>();
			if (!rooms.isEmpty()) {
				for (Room room : rooms) {
					if (DaycareHelpers.getIsAgeBetween(m_child.getBirthDate(), room.getMinAge().doubleValue(),
							room.getMaxAge().doubleValue())) {
						qualifyingRooms.add(room);
					}
				}
				if (qualifyingRooms.size() == 1) {
					m_child.setPRoom(qualifyingRooms.get(0));
				} else if (qualifyingRooms.size() > 1) {
					m_multiRoomChildren.add(m_child);
				}
				qualifyingRooms.clear();
				if (!m_multiRoomChildren.isEmpty()) {
					getPersistenceLayer().getPersistenceData().setMultiRoomChildren(m_multiRoomChildren);
					fragment.nextScreen("ROOMS");
				} else
					fragment.nextScreen(getPersistenceLayer().getPersistenceData().getInOrOut(), getPersistenceLayer().getPersistenceData().getLoginResult());
			}
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class AdmissionOverrideCommandListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			Boolean isOverride = (Boolean) command.getResult();
			if (isOverride) {
				fragment.showNoAdmissionsDialog();
			} else {
				m_currentState = new CalculateAvailableProgramsCommandState();
				m_currentState.enterState();
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
}
