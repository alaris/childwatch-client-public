package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;

public class ReservePagers extends Command<List<Pager>> {

	private final List<Member> m_children;
	private final List<Pager> m_pagers;
	private final Facility m_facility;
	private final Family m_family;
	private final Member m_proxy;

	public ReservePagers(List<Pager> pagers, List<Member> children, Family family, Facility facility, Member proxy) {
		this(pagers, children, family, facility, proxy, null, null);
	};

	public ReservePagers(List<Pager> pagers, List<Member> children, Family family, Facility facility, Member proxy, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_pagers=pagers;
		m_children=children;
		m_family=family;
		m_facility=facility;
		m_proxy=proxy;
	}

	@Override
	public void executeSource() {
		m_dataSource.getPagersDAO().reservePagers(m_pagers, m_children, m_facility, m_family, m_proxy, this);

	}

	@Override
	public void handleResponse(List<Pager> pagers) {
	}

}