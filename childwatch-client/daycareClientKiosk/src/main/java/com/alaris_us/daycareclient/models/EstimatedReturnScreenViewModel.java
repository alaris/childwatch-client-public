package com.alaris_us.daycareclient.models;

import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.EstimatedReturnCheckBox;
import com.alaris_us.daycareclient.widgets.EstimatedReturnCheckBoxModel;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Schedule;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

public class EstimatedReturnScreenViewModel extends ViewModel {
	public EstimatedReturnScreenViewModel(PersistenceLayer persistenceLayer, ScreenSwitcherFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
		reset();
	}

	private ScreenSwitcherFragment												fragment;
	private TrackableField<TrackableCollection<EstimatedReturnCheckBoxModel>>	m_timeModels;
	private TrackableField<EstimatedReturnCheckBoxModel>						m_selectedTime;
	private TrackableField<TrackableCollection<Member>>							m_selectedChildren;
	private Facility 																						m_facility;
	private boolean																					 m_isOverride;

	public TrackableCollection<EstimatedReturnCheckBoxModel> getTimes() {
		return m_timeModels.get();
	}

	@Override
	protected void onPopulateModelData() {
		m_facility = getPersistenceLayer().getPersistenceData().getFacility();
		m_isOverride = getPersistenceLayer().getPersistenceData().getIsOverride();
		m_selectedTime.set(null);
		m_timeModels.get().clear();
		m_selectedChildren.set(new TrackableCollection<Member>(getPersistenceLayer().getPersistenceData()
				.getSelectedDaycareChildren()));
		
		//Estimated Time Option Availability
		List<Integer> allTimes = Arrays.asList(30, 45, 60, 90, 120, 150, 180, 240, 300);
		List<Integer> filteredListOfTimes = new ArrayList<Integer>();
		filteredListOfTimes.addAll(allTimes);
		int pos = 0;
		DateTime returnTime = null;
		int minTimeLeft = Integer.MAX_VALUE;
		updateChildrenTimes();
		String currentDate = DateGenerator.getFormattedDate(DateGenerator.getCurrentDate());
		HashMap<Program, Schedule> ps = getPersistenceLayer().getPersistenceData().getAvailableProgramSchedules();
		Iterator it = ps.entrySet().iterator();
		Schedule schedule = null;
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        if (pair.getKey().equals(getPersistenceLayer().getPersistenceData().getSelectedProgram()))
	        	schedule = (Schedule) pair.getValue();
	    }
		for (Member child : m_selectedChildren.get()) {
			if (m_isOverride) {
				if (getPersistenceLayer().getPersistenceData().getSelectedProgram().getName().equals("STAFF"))
					minTimeLeft = m_facility.getWeeklyTimeLimit().intValue();
				else {
					if (getPersistenceLayer().getPersistenceData().getSelectedProgram().getTimeLimit().intValue() != 0)
						minTimeLeft = getPersistenceLayer().getPersistenceData().getSelectedProgram().getTimeLimit().intValue();
					else
						minTimeLeft = child.getPRoom().getTimeLimit().intValue();
				}
			} else if (DateGenerator.getFormattedDate(child.getLastCheckIn()).compareTo(currentDate) != 0) {
				if (getPersistenceLayer().getPersistenceData().getSelectedProgram().getName().equals("STAFF")) {
					DateTime lastCheckInDate = new DateTime(child.getLastCheckIn());
					DateTime lastSunday = DaycareHelpers.getLastSunday();
					if (null != child.getLastCheckIn() && lastCheckInDate.isAfter(lastSunday)) {
						minTimeLeft = child.getWeeklyMinutesLeft().intValue();
					}
				} else {
					DateTime lastCheckInDate = new DateTime(child.getLastCheckIn());
					DateTime lastSunday = DaycareHelpers.getLastSunday();
					if (m_facility.getWeeklyTimeLimit().intValue() != 0 && null != child.getLastCheckIn() && lastCheckInDate.isAfter(lastSunday)) {
						if (child.getMinutesLeft().intValue() < child.getWeeklyMinutesLeft().intValue())
							minTimeLeft = child.getMinutesLeft().intValue();
						else
							minTimeLeft = child.getWeeklyMinutesLeft().intValue();
					}
					else { /*if (m_facility.getWeeklyTimeLimit().intValue() != 0 && null != child.getLastCheckIn() && lastCheckInDate.isBefore(lastSunday))
						minTimeLeft = m_facility.getTimeLimit().intValue();
					else */
						if (getPersistenceLayer().getPersistenceData().getSelectedProgram().getTimeLimit().intValue() != 0)
							minTimeLeft = getPersistenceLayer().getPersistenceData().getSelectedProgram().getTimeLimit().intValue();
						else
							minTimeLeft = child.getPRoom().getTimeLimit().intValue();
					}
				}
			} else {
				if (getPersistenceLayer().getPersistenceData().getSelectedProgram().getName().equals("STAFF")) {
					minTimeLeft = child.getWeeklyMinutesLeft().intValue();
				} else {
					int timeLeft;
					if (getPersistenceLayer().getPersistenceData().getSelectedProgram().getUsesWeeklyMinutes()) {
						if (child.getMinutesLeft().intValue() < child.getWeeklyMinutesLeft().intValue())
							timeLeft = child.getMinutesLeft().intValue();
						else
							timeLeft = child.getWeeklyMinutesLeft().intValue();
					} else {
						if (m_facility.getUsesTimeTracking())
							timeLeft = child.getMinutesLeft().intValue();
						else {
							if (getPersistenceLayer().getPersistenceData().getSelectedProgram().getTimeLimit().intValue() != 0)
								timeLeft = getPersistenceLayer().getPersistenceData().getSelectedProgram().getTimeLimit().intValue();
							else
								timeLeft = child.getPRoom().getTimeLimit().intValue();
						}
					}
					if (timeLeft < minTimeLeft);
					minTimeLeft = timeLeft;
				}
			}

			DateTime closeTime = DaycareHelpers.getProgramScheduleClosingTime(schedule);
			if (returnTime == null || closeTime.isBefore(returnTime))
				returnTime = closeTime;
			
			DateTime time = (new DateTime()).plusMinutes(minTimeLeft);
			if (returnTime == null || time.isBefore(returnTime))
				returnTime = time;
		}
		//int returnTimeInMinutes = DaycareHelpers.getMinutesBetween(new DateTime(), returnTime);
		int minimumTime = DaycareHelpers.getMinutesBetween(new DateTime(), returnTime);
		
		for (Integer time : allTimes) {
			if (time > minimumTime)
				filteredListOfTimes.remove(time);
		}
		//int roundedDownValue = RoundToNearestFive(returnTimeInMinutes);
		if (!filteredListOfTimes.contains(minimumTime))
			filteredListOfTimes.add(minimumTime);
		Collections.sort(filteredListOfTimes);
		
		int height = ListViewFunctions.calculateTileWidth(filteredListOfTimes.size());
		for (Integer time : filteredListOfTimes) {
			EstimatedReturnCheckBoxModel model = new EstimatedReturnCheckBoxModel(time, height,
					ListViewFunctions.getColorAt(pos++));
			m_timeModels.get().add(model);
		}
		//getPersistenceLayer().getPersistenceData().setReturnTime(returnTime);
	}

	public void updateChildrenTimes() {
		String currentDate = DateGenerator.getFormattedDate(DateGenerator.getCurrentDate());
		Number facilityTimeLimit = m_facility.getTimeLimit();
		Number facilityWeeklyTimeLimit = m_facility.getWeeklyTimeLimit();
		for (Member child : m_selectedChildren.get()) {
			if (child.getInProgram() != null) {
				Number programTimeLimit = child.getInProgram().getTimeLimit().intValue();
				if (programTimeLimit.intValue() == 0) {
					Number roomTimeLimit = child.getPRoom().getTimeLimit();
					child = updateChildAtCheckIn(child, roomTimeLimit, facilityWeeklyTimeLimit, currentDate);
				} else
					child = updateChildAtCheckIn(child, programTimeLimit, facilityWeeklyTimeLimit, currentDate);
			} else {
				child = updateChildAtCheckIn(child, facilityTimeLimit, facilityWeeklyTimeLimit, currentDate);
			}
		}
	}
	
	public Member updateChildAtCheckIn(Member child, Number timeLimit, Number weeklyTimeLimit, String currentDate) {
		DateTime lastSunday = DaycareHelpers.getLastSunday();
		int minutesLeft = timeLimit.intValue();
		if (getPersistenceLayer().getPersistenceData().getIsOverride()) {
			child.setMinutesLeft(minutesLeft);
		} else if (null == child.getLastCheckIn()) {
			child.setMinutesLeft(minutesLeft);
		} else if (DateGenerator.getFormattedDate(child.getLastCheckIn()).compareTo(currentDate) != 0) {
			if (child.isGuest())
				if (child.getGuestDaysLeft().intValue() > 0)
					child.setGuestDaysLeft(child.getGuestDaysLeft().intValue() - 1);
				else
					minutesLeft = 0;
			DateTime lastCheckInDate = new DateTime(child.getLastCheckIn());
			if (lastCheckInDate.isBefore(lastSunday))
				child.setWeeklyMinutesLeft(weeklyTimeLimit);
			child.setMinutesLeft(minutesLeft);
		} else if (DateGenerator.getFormattedDate(child.getLastCheckIn()).compareTo(currentDate) == 0 && !m_facility.getUsesTimeTracking()) {
			child.setMinutesLeft(minutesLeft);
		}
		return child;
	}
	
	public OnClickListener getNextClickListener() {
		if (m_selectedTime.get() != null) {
			return new NextClickListener();
		} else
			return null;
	}

	public OnClickListener getBackClickListener() {
		return new BackClickListener();
	}

	public float getNextAlpha() {
		return ListViewFunctions.getTileAlpha(m_selectedTime.get() != null);
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			fragment.nextScreen();
		}
	}

	public class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			fragment.prevScreen();
		}
	}

	public class TimeClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> parent, View view, int position,
				long id) {
			EstimatedReturnCheckBoxModel timeModel = ((EstimatedReturnCheckBox) view).getModel();
			if (!timeModel.getIsSelected()) {
				for (EstimatedReturnCheckBoxModel model : m_timeModels.get())
					model.setIsSelected(model.equals(timeModel));
				m_selectedTime.set(timeModel);
			}
			getPersistenceLayer().getPersistenceData().setReturnTime(timeModel.getReturnTime());
			for (Member child : m_selectedChildren.get()) {
				child.setEstimatedPickupTime(timeModel.getReturnTimeAsString());
			}
		}
	}
	int RoundDown(int toRound)
	{
		return toRound - toRound % 15;
	}
	
	int RoundToNearestFive(int toRound)
	{
		if (toRound < 0)
			return 0;
		else if ((int) (5*(Math.floor(Math.abs(toRound/5)))) == 0)
			return 5;
		else
			return (int) (5*(Math.floor(Math.abs(toRound/5))));
	}
	
	@Override
	public void onInitialize() {
		m_timeModels = new TrackableField<TrackableCollection<EstimatedReturnCheckBoxModel>>(
				new TrackableCollection<EstimatedReturnCheckBoxModel>());
		m_selectedTime = new TrackableField<EstimatedReturnCheckBoxModel>();
		m_selectedChildren = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
	}
}