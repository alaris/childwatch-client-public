package com.alaris_us.daycareclient.widgets;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Question;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

public class SliderQuestionDialogItemModel {

	private TrackableField<Member> m_member;
	private TrackableBoolean m_sliderState;

	public SliderQuestionDialogItemModel() {
		this(null);
	}

	public SliderQuestionDialogItemModel(Member member) {
		m_member = new TrackableField<Member>(member);
		m_sliderState = new TrackableBoolean(false);
	}

	public Member getMember() {
		return m_member.get();
	}

	public void setMember(Member member) {
		m_member.set(member);
		setIsChecked(false);
	}

	public String getMemberName() {
		return m_member.get().getFirstName() + " " + m_member.get().getLastName();
	}

	public void setIsChecked(boolean isChecked) {
		m_sliderState.set(isChecked);
	}

	public boolean getSliderState() {
		return m_sliderState.get();
	}
}
