package com.alaris_us.daycareclient.fragments;

import com.alaris_us.daycareclient.models.RegisterThankYouScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.UiBinder;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

public class RegisterThankYouScreenFragment extends ScreenSwitcherFragment {
	private RegisterThankYouScreenViewModel	m_model;
	private AlertDialog m_noChildrenOfAgeDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.registerthankyouscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void initData() {
		m_model.reset();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (m_model == null)
			m_model = new RegisterThankYouScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
					this);
		UiBinder.bind(m_layout, R.id.registerthankyouscreen_tile_yourpin_text, "Text", m_model, "PinText",
				BindingMode.ONE_WAY);
		m_layout.findViewById(R.id.view_registerty_navigation_gohome_container).setOnClickListener(
				m_model.new HomeClickListener());
		m_layout.findViewById(R.id.view_registerty_navigation_checkin_container).setOnClickListener(m_model. new CheckInNowClickListener());
	}
	
	public void showNoChildrenOfAgeDialog() {
		FrameLayout noChildrendialogContainer = new FrameLayout(getActivity());
		View noChildrenDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_nochildrenofagedialog_layout,
				noChildrendialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noChildrendialogContainer);
		m_noChildrenOfAgeDialog = builder.create();
		m_noChildrenOfAgeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_noChildrenOfAgeDialog.setCancelable(false);
		m_noChildrenOfAgeDialog.show();
		ViewGroup.LayoutParams lp = noChildrenDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nochildrenofagedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nochildrenofagedialog_height);
		noChildrenDialogView.setLayoutParams(lp);
		((View) noChildrenDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		noChildrenDialogView.findViewById(R.id.view_nochildrenofagedialog_button).setOnClickListener(
				new NoChildrenOfAgeDialogClickListener());
	}

	public void dismissNoChildrenOfAgeDialog() {
		if (m_noChildrenOfAgeDialog != null && m_noChildrenOfAgeDialog.isShowing())
			m_noChildrenOfAgeDialog.dismiss();
	}
	
	public class NoChildrenOfAgeDialogClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoChildrenOfAgeDialog();
			getFragmentActivity().gotoFirstScreen();
		}
	}
}
