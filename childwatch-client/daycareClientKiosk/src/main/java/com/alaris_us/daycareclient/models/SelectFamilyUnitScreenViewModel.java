package com.alaris_us.daycareclient.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alaris_us.daycareclient.fragments.SelectFamilyUnitScreenFragment;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.FamilyUnitCheckBoxModel;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.externaldata.ExternalDataOperationComplete;
import com.alaris_us.externaldata.exception.ExternalDataException;
import com.alaris_us.externaldata.to.UnitInfo;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class SelectFamilyUnitScreenViewModel extends ViewModel {
	public SelectFamilyUnitScreenViewModel(PersistenceLayer persistenceLayer, SelectFamilyUnitScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private SelectFamilyUnitScreenFragment									fragment;
	private TrackableField<TrackableCollection<FamilyUnitCheckBoxModel>>	m_unitModels;
	private TrackableField<UnitInfo>														mSelectedUnit;

	public TrackableCollection<FamilyUnitCheckBoxModel> getMembers() {
		return m_unitModels.get();
	}

	@Override
	protected void onPopulateModelData() {
		List<UnitInfo> registrationUnits = getPersistenceLayer().getPersistenceData().getRegistrationUnits();
		m_unitModels.get().clear();
		mSelectedUnit.set(null);
		int height = ListViewFunctions.calculateTileWidth(registrationUnits.size());
		int pos = 0;
		for (UnitInfo unit : registrationUnits) {
			FamilyUnitCheckBoxModel model = new FamilyUnitCheckBoxModel(unit, height,
					ListViewFunctions.getColorAt(pos++));
			m_unitModels.get().add(model);
		}
	}

	public float getNextAlpha() {
		return ListViewFunctions.getTileAlpha(mSelectedUnit.get() != null);
	}

	public void next() {
		if (mSelectedUnit != null && mSelectedUnit.get() != null) {
			String unitId = mSelectedUnit.get().getUnitId();
			getPersistenceLayer().getExternalUnitMembers(unitId, new GetFamilyInYMCADataCommandListener());
		} else
			reset();
	}

	public void selectMember(UnitInfo unit, FamilyUnitCheckBoxModel memberModel) {
		if (!memberModel.getIsSelected()) {
			for (FamilyUnitCheckBoxModel model : m_unitModels.get())
				model.setIsSelected(model.equals(memberModel));
			mSelectedUnit.set(unit);
		}
	}

	@Override
	public void onInitialize() {
		m_unitModels = new TrackableField<TrackableCollection<FamilyUnitCheckBoxModel>>(
				new TrackableCollection<FamilyUnitCheckBoxModel>());
		mSelectedUnit = new TrackableField<UnitInfo>();
		// addWatch(CHANGE_ID.REGISTRATIONUNITS); //TODO
	}

	private class GetFamilyInYMCADataCommandListener implements
			ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>> {
		@Override
		public void DataReceived(List<com.alaris_us.externaldata.to.Member> data, ExternalDataException arg1) {
			if (data == null) {
				reset();
			} else {
				Date defaultDate = DateGenerator.getDefaultDate();
				String formattedDate = DateGenerator.getFormattedDateAtOffset(DateGenerator.CHILDSEPARATOR);
				List<Member> allMembers = new ArrayList<Member>();
				List<Member> ymcaParents = new ArrayList<Member>();
				List<Member> ymcaChildren = new ArrayList<Member>();
				for (com.alaris_us.externaldata.to.Member mem : data) {
					Member newMember = getPersistenceLayer().createMemberFromYMCAData(mem, formattedDate, defaultDate, null, null);
					String memberDate = DateGenerator.getFormattedDate(newMember.getBirthDate());
					if (memberDate.compareTo(formattedDate) < 0) {
						ymcaParents.add(newMember);
					} else {
						ymcaChildren.add(newMember);
					}
					allMembers.add(newMember);
				}
				if (ymcaChildren.size() > 0) {
					getPersistenceLayer().getPersistenceData().setYMCAFamily(allMembers, ymcaParents, ymcaChildren);
					fragment.nextScreen();
				} else {
					exception(new Exception("NO KIDS UNDER 12"));
				}
			}
		}
	}
}
