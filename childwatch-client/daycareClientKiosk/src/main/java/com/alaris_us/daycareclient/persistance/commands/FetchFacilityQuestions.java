package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Question;

public class FetchFacilityQuestions extends Command<List<Question>> {
    Facility m_facility;

    public FetchFacilityQuestions(Facility facility) {
        this(facility, null, null);
    }

    protected FetchFacilityQuestions(Facility facility, DaycareData dataSource, PersistenceData model) {
        super(dataSource, model);
        m_facility = facility;
    }

    @Override
    public void executeSource() {
        m_dataSource.getFacilitiesDAO().listAvailableQuestions(m_facility, this);
    }

    @Override
    public void handleResponse(List<Question> data) {
        List<Question> popupQuestions = new ArrayList<>();
        List<Question> fullScreenQuestions = new ArrayList<>();
        m_model.setAllFacilityQuestions(data);
        
        for (Question q : data) {
            if (q.getIsPopup())
                popupQuestions.add(q);
            else
                fullScreenQuestions.add(q);
        }
        m_model.setFacilityPopupQuestions(popupQuestions);
        m_model.setFacilityFullScreenQuestions(fullScreenQuestions);

    }
}
