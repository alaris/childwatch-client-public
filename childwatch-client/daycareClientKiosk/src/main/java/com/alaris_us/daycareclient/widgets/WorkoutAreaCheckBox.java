package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class WorkoutAreaCheckBox extends RelativeLayout implements BoundUi<WorkoutAreaCheckBoxModel>{

	private WorkoutAreaCheckBoxModel m_data;
	
	private ImageView m_checkBox;
	private ImageView m_areaImage;
	private TextView m_areaName;
	
	public WorkoutAreaCheckBox(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public WorkoutAreaCheckBox(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		View.inflate(getContext(), R.layout.view_listitem_workoutarealistadapter, this);
		
		m_areaImage = (ImageView) findViewById(R.id.view_listitem_workoutarealistadapter_image);
		m_areaName = (TextView) findViewById(R.id.view_listitem_workoutarealistadapter_name);
		m_checkBox = (ImageView) findViewById(R.id.view_listitem_workoutarealistadapter_checkmark);
		
	}
	
	public WorkoutArea getArea() {
		return m_data.getArea();
	}
	
	public WorkoutAreaCheckBoxModel getModel() {
		return m_data; 
	}

	public void setHeight(int height){
		ListViewFunctions.setTileHeight(this, height);
	}
	
	public void setWidth(int width){
		ListViewFunctions.setTileWidth(this, width);
	}
	
	@Override
	public void bind(WorkoutAreaCheckBoxModel dataSource) {
		m_data = dataSource;
		
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_areaName, "Text")), m_data, "AreaName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_areaImage, "ImageBitmap")), m_data, "AreaImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_areaImage, "BackgroundColor")), m_data, "BackgroundColor", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Width")), m_data, "Width", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_areaImage, "Alpha")), m_data, "Alpha", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_checkBox, "Visibility")), m_data, "CheckBoxRes", BindingMode.ONE_WAY);
	}
}
