package com.alaris_us.daycareclient.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.models.CheckInScreenViewModel;
import com.alaris_us.daycareclient.models.FamilyConfirmationScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher.ScrollDir;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment.PersistanceLayerHost;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FamilyConfirmationItem;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class FamilyConfirmationScreenFragment extends ScreenSwitcherFragment {
	FamilyConfirmationScreenViewModel	m_model;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.familyconfirmationscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		m_model = new FamilyConfirmationScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		UiBinder.bind(m_layout, R.id.familyconfirmationscreen_tile_userlist_container, "Adapter", m_model,
				"FamilyMembers", BindingMode.TWO_WAY, new AdapterConverter(FamilyConfirmationItem.class, true, true));
		UiBinder.bind(m_layout, R.id.confirmationscreen_tile_message_textbox, "TextRes", m_model, "MessageText",
				BindingMode.ONE_WAY);
		FontTextView yes = (FontTextView) m_layout.findViewById(R.id.view_navigation_next_textbox);
		yes.setTextRes(R.string.familyconfirmationscreen_tile_yes);
		FontTextView no = (FontTextView) m_layout.findViewById(R.id.view_navigation_back_textbox);
		no.setTextRes(R.string.familyconfirmationscreen_tile_no);
		m_layout.findViewById(R.id.view_navigation_next_container).setOnClickListener(m_model.new YesClickListener());
		m_layout.findViewById(R.id.view_navigation_back_container).setOnClickListener(m_model.new NoClickListener());
	}

	@Override
	public void onResumeFragment() {
		m_model = (null == m_model) ? new FamilyConfirmationScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this) : m_model;
		m_model.reset();
		super.onResumeFragment();
	}
	
	@Override
	public void initData() {
	}
}
