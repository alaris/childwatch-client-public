package com.alaris_us.daycareclient.models;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.fragments.SelectProgramScreenFragment;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.ProgramSelectionCheckBox;
import com.alaris_us.daycareclient.widgets.ProgramSelectionCheckBoxModel;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Room;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

import android.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;
import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

public class SelectProgramScreenViewModel extends ViewModel {
	public SelectProgramScreenViewModel(PersistenceLayer persistenceLayer, SelectProgramScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
		reset();
	}

	private SelectProgramScreenFragment fragment;
	private TrackableField<TrackableCollection<ProgramSelectionCheckBoxModel>> m_programModels;
	private TrackableField<ProgramSelectionCheckBoxModel> m_program;
	private List<Program> m_availablePrograms;
	private Facility m_facility;
	private TrackableField<AlertDialog> m_capacityExceededDialog;
	private int m_openSpots;
	private TrackableField<AlertDialog> m_staffOverrideEntryDialog;
	private TrackableField<AlertDialog> m_successfulOverrideDialog;
	private List<Member> m_multiRoomChildren;

	public TrackableCollection<ProgramSelectionCheckBoxModel> getPrograms() {
		return m_programModels.get();
	}

	@Override
	protected void onPopulateModelData() {
		m_programModels.get().clear();

		// TODO filter only available programs to show based on schedule
		m_availablePrograms = getPersistenceLayer().getPersistenceData().getAllAvailablePrograms();
		m_facility = getPersistenceLayer().getPersistenceData().getFacility();
		int pos = 0;
		int height = ListViewFunctions.calculateTileWidth(m_availablePrograms.size());
		for (Program p : m_availablePrograms) {
			ProgramSelectionCheckBoxModel model = new ProgramSelectionCheckBoxModel(p, height,
					ListViewFunctions.getColorAt(pos++));
			m_programModels.get().add(model);
		}

	}

	public OnClickListener getNextClickListener() {
		if (m_program.get() != null) {
			return new NextClickListener();
		} else
			return null;
	}

	public OnClickListener getBackClickListener() {
		return new BackClickListener();
	}

	public float getNextAlpha() {
		return ListViewFunctions.getTileAlpha(m_program.get() != null);
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			List<Member> selfCheckIns = getPersistenceLayer().getPersistenceData().getSelfCheckInChildren();
			int checkedIntoProgramCount = 0;
			if (m_facility.getMaxCapacityType() != null) {
                if (m_facility.getMaxCapacityType().equals("PROGRAM")) {
                    if (getPersistenceLayer().getPersistenceData().getCheckedInMembers() != null) {
                        List<Member> checkedInMembers = getPersistenceLayer().getPersistenceData().getCheckedInMembers();
                        for (Member m : checkedInMembers) {
                            if (m.getInProgram().equals(m_program.get().getProgram()))
                                checkedIntoProgramCount++;
                        }
                        if (checkedIntoProgramCount >= (m_program.get().getProgram()).getMaxCapacity().intValue()) {
                            showCapacityExceededDialog();
                        } else {
                            if (selfCheckIns.contains(getPersistenceLayer().getPersistenceData().getCurrentMember())) {
								getPersistenceLayer().getPersistenceData().getCurrentMember().setInProgram(m_program.get().getProgram());
								getPersistenceLayer().fetchProgramRooms(m_program.get().getProgram(),
										new FetchProgramRoomsCommandListener(getPersistenceLayer().getPersistenceData().getCurrentMember()));
							} else
                                fragment.nextScreen("CHECKIN");
                        }
                    } else {
                        getPersistenceLayer().fetchCheckedInMembers(new FetchCheckedInMembersListener());
                    }
                } else {
                    if (selfCheckIns.contains(getPersistenceLayer().getPersistenceData().getCurrentMember())) {
						getPersistenceLayer().getPersistenceData().getCurrentMember().setInProgram(m_program.get().getProgram());
						getPersistenceLayer().fetchProgramRooms(m_program.get().getProgram(),
								new FetchProgramRoomsCommandListener(getPersistenceLayer().getPersistenceData().getCurrentMember()));
					} else
                        fragment.nextScreen("CHECKIN");
                }
            } else {
                if (selfCheckIns.contains(getPersistenceLayer().getPersistenceData().getCurrentMember())) {
					getPersistenceLayer().getPersistenceData().getCurrentMember().setInProgram(m_program.get().getProgram());
					getPersistenceLayer().fetchProgramRooms(m_program.get().getProgram(),
							new FetchProgramRoomsCommandListener(getPersistenceLayer().getPersistenceData().getCurrentMember()));
				} else
                    fragment.nextScreen("CHECKIN");
            }
		}
	}

	public class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			fragment.prevScreen();
		}
	}

	public class ProgramClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> parent, View view, int position,
				long id) {
			ProgramSelectionCheckBoxModel programModel = ((ProgramSelectionCheckBox) view).getModel();
			if (!programModel.getIsSelected()) {
				for (ProgramSelectionCheckBoxModel model : m_programModels.get())
					model.setIsSelected(model.equals(programModel));
				m_program.set(programModel);
			}
			getPersistenceLayer().getPersistenceData().setSelectedProgram(programModel.getProgram());
		}
	}

	public void setCapacityExceededDialog(AlertDialog alertDialog) {
		m_capacityExceededDialog.set(alertDialog);
	}
	
	private void showCapacityExceededDialog() {
		if (m_capacityExceededDialog.get() != null) {
			String breakdownText = "'";
			if (m_facility.getRatioType().equals("FACILITYANDROOM")) {
				breakdownText = fragment.getDefaultFacilityAndRoomCapacityExceededText();
				fragment.setCapacityExceededBreakdownText(breakdownText);
			} else {
				breakdownText = fragment.getDefaultCapacityExceededText();
				if (m_openSpots < 0)
					m_openSpots = 0;
				if (m_openSpots == 1) {
					breakdownText += "s";
					breakdownText = breakdownText.replace("spots", "spot");
				}
				String composedBreakdownText = breakdownText.replace("[SPOTS]", Integer.toString(m_openSpots));
				fragment.setCapacityExceededBreakdownText(composedBreakdownText);
			}
			m_capacityExceededDialog.get().show();
		}
	}
	
	private void dismissCapacityExceededDialog() {
		if (m_capacityExceededDialog.get() != null && m_capacityExceededDialog.get().isShowing())
			m_capacityExceededDialog.get().dismiss();
	}

	public OnClickListener getCapacityExceededDialogReselectClickListener() {
		return new CapacityExceededDialogReselectClickListener();
	}

	public OnClickListener getCapacityExceededDialogHomeClickListener() {
		return new CapacityExceededDialogHomeClickListener();
	}

	public OnClickListener getCapacityExceededDialogOverrideClickListener() {
		return new CapacityExceededDialogOverrideClickListener();
	}

	public class CapacityExceededDialogReselectClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissCapacityExceededDialog();
		}
	}

	public class CapacityExceededDialogHomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissCapacityExceededDialog();
			fragment.firstScreen();
		}
	}

	public class CapacityExceededDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissCapacityExceededDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public String getCapacityExceededBreakdownText() {
		String breakdownText = fragment.getDefaultCapacityExceededText();
		return breakdownText.replace("[SPOTS]", Integer.toString(m_openSpots));
	}

	private class FetchCheckedInMembersListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			List<Member> checkedInMembers = (List<Member>) command.getResult();
			int checkedIntoProgramCount = 0;
			for (Member m : checkedInMembers) {
				if (m.getInProgram().equals(m_program.get().getProgram()))
					checkedIntoProgramCount++;
			}
			if (checkedIntoProgramCount >= m_program.get().getProgram().getMaxCapacity().intValue()) {
				showCapacityExceededDialog();
			} else
				fragment.nextScreen();
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}
	
	public void setStaffOverrideEntryDialog(AlertDialog alertDialog) {
		m_staffOverrideEntryDialog.set(alertDialog);
	}

	private void showStaffOverrideEntryDialog() {
		if (m_staffOverrideEntryDialog.get() != null) {
			m_staffOverrideEntryDialog.get().show();
		}
	}

	public void dismissStaffOverrideEntryDialog() {
		if (m_staffOverrideEntryDialog != null && m_staffOverrideEntryDialog.get().isShowing())
			m_staffOverrideEntryDialog.get().dismiss();
	}

	public void setSuccessfulOverrideDialog(AlertDialog alertDialog) {
		m_successfulOverrideDialog.set(alertDialog);
	}

	private void showSuccessfulOverrideDialog() {
		if (m_successfulOverrideDialog.get() != null) {
			m_successfulOverrideDialog.get().show();
		}
	}

	public void dismissSuccessfulOverrideDialog() {
		if (m_successfulOverrideDialog != null && m_successfulOverrideDialog.get().isShowing())
			m_successfulOverrideDialog.get().dismiss();
	}
	
	public OnClickListener getStaffOverrideEntryDialogEnterClickListener() {
		return new StaffOverrideEntryDialogEnterClickListener();
	}

	public OnClickListener getStaffOverrideEntryDialogCancelClickListener() {
		return new StaffOverrideEntryDialogCancelClickListener();
	}

	public class StaffOverrideEntryDialogEnterClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_facility.getOverridePIN().equals(fragment.getOverridePIN())) {
				dismissStaffOverrideEntryDialog();
				showSuccessfulOverrideDialog();
			} else {
				fragment.setOverridePIN("");
				fragment.setOverridePINHint("Invalid PIN, Please Retry");
			}
		}
	}

	public class StaffOverrideEntryDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissStaffOverrideEntryDialog();
		}
	}

	public OnClickListener getSuccessfulOverrideDialogOkClickListener() {
		return new SuccessfulOverrideDialogOkClickListener();
	}

	public class SuccessfulOverrideDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getPersistenceLayer().getPersistenceData().setIsOverride(true);
			dismissSuccessfulOverrideDialog();
			fragment.nextScreen();
		}
	}

	private class FetchProgramRoomsCommandListener implements CommandStatusListener {
		private Member m_child;

		public FetchProgramRoomsCommandListener(Member child) {
			m_child = child;
		}

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void valueChanged(Command<?> command) {
			List<Room> rooms = (List<Room>) command.getResult();

			List<Room> qualifyingRooms = new ArrayList<Room>();
			if (!rooms.isEmpty()) {
				for (Room room : rooms) {
					if (DaycareHelpers.getIsAgeBetween(m_child.getBirthDate(), room.getMinAge().doubleValue(),
							room.getMaxAge().doubleValue())) {
						qualifyingRooms.add(room);
					}
				}
				if (qualifyingRooms.size() == 1) {
					m_child.setPRoom(qualifyingRooms.get(0));
				} else if (qualifyingRooms.size() > 1) {
					m_multiRoomChildren.add(m_child);
				}
				qualifyingRooms.clear();
				if (!m_multiRoomChildren.isEmpty()) {
					getPersistenceLayer().getPersistenceData().setMultiRoomChildren(m_multiRoomChildren);
					fragment.nextScreen("ROOMS");
				} else
					fragment.nextScreen("CONFIRM");
			}
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	@Override
	public void onInitialize() {
		m_programModels = new TrackableField<TrackableCollection<ProgramSelectionCheckBoxModel>>(
				new TrackableCollection<ProgramSelectionCheckBoxModel>());
		m_program = new TrackableField<ProgramSelectionCheckBoxModel>();
		m_availablePrograms = new ArrayList<Program>();
		m_capacityExceededDialog = new TrackableField<AlertDialog>();
		m_staffOverrideEntryDialog = new TrackableField<AlertDialog>();
		m_successfulOverrideDialog = new TrackableField<AlertDialog>();
		m_multiRoomChildren = new ArrayList<Member>();
	}
}
