package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Member;

public class FetchMemberByMemberId extends Command<Member> {

	private String id;

	public FetchMemberByMemberId(String memberId) {

		this(memberId, null, null);
	}

	protected FetchMemberByMemberId(String memberId, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		id = memberId;
	}

	@Override
	public void executeSource() {
		m_dataSource.getMembersDAO().getMemberByMemberIdInclude(id, this);
	}

	@Override
	public void handleResponse(Member data) {

	}

}