package com.alaris_us.daycareclient.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.json.JSONObject;

import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient_dev.R;

import android.app.Activity;
import android.content.res.Resources;

public class Service_Temperature implements Runnable {
	ScheduledThreadPoolExecutor	scheduledTemperatureService;
	PersistenceLayer			persistenceLayer;
	Activity					activity;
	String						temp	= "";
	String						defaultTemp;
	String						unit;

	public Service_Temperature(PersistenceLayer persistenceLayer,
			Resources resources) {
		this.persistenceLayer = persistenceLayer;
		this.unit = resources.getString(R.string.mainscreen_tile_temperature_unit);
		this.defaultTemp = resources.getString(R.string.fragment_dummy_text);
		startService();
	}

	private void startService() {
		scheduledTemperatureService = new ScheduledThreadPoolExecutor(1);
		scheduledTemperatureService.scheduleWithFixedDelay(
				Service_Temperature.this, 0, 300, TimeUnit.SECONDS);
	}

	@Override
	public void run() {
		do {
			temp = getCurrentTemp();
			if (temp == null || temp.equals("")) {
				persistenceLayer.getPersistenceData().setCurrentTemperature(
						defaultTemp, unit);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else
				persistenceLayer.getPersistenceData().setCurrentTemperature(
						temp, unit);
		} while (temp == null || temp == "");
	}

	public String getCurrentTemp() {
		String temp = "";
		String zipcode = persistenceLayer.getPersistenceData()
				.getFacilityZipCode();
		if (zipcode == null || zipcode.equals(""))
			return null;
		String queryString = "https://api.openweathermap.org/data/2.5/weather?zip=" + zipcode + "&units=imperial"
				+ "&appId=36769e47b96eae3eabb5371524df7082";
		URL url;
		try {
			url = new URL(queryString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			connection.setUseCaches(false);
			connection.setDoOutput(true);
			
			BufferedReader reader = new BufferedReader(
		             new InputStreamReader(connection.getInputStream()));
		     String inputLine;
		     StringBuffer response = new StringBuffer();
		     while ((inputLine = reader.readLine()) != null) {
		     	response.append(inputLine);
		     }
		     reader.close();
		     JSONObject myResponse;
			try {
				myResponse = new JSONObject(response.toString());
				Double tempDouble = myResponse.getJSONObject("main").getDouble("temp");
				temp = String.valueOf(tempDouble.intValue());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public void shutdown() {
		scheduledTemperatureService.shutdown();
	}
}
