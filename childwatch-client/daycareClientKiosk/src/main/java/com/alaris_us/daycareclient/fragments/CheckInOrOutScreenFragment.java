package com.alaris_us.daycareclient.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.models.CheckInOrOutScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient_dev.R;

public class CheckInOrOutScreenFragment extends ScreenSwitcherFragment {
	CheckInOrOutScreenViewModel	mModel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		m_layout = inflater.inflate(R.layout.checkinoroutscreen_layout, container, false);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mModel = new CheckInOrOutScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer());
		m_layout.findViewById(R.id.checkinoroutscreen_tile_in_container).setOnClickListener(new CheckInClickListener());
		m_layout.findViewById(R.id.checkinoroutscreen_tile_out_container).setOnClickListener(
				new CheckOutClickListener());
		// Navigation Button(s)
		m_layout.findViewById(R.id.checkinoroutscreen_tile_back_halfarrow).setOnClickListener(new BackClickListener());
	}

	@Override
	public void onPauseFragment() {
		//super.onPauseFragment();
		//mModel.disconnect();
	}

	@Override
	public void onResumeFragment() {
		super.onResumeFragment();
		mModel.reset();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mModel.disconnect();
		mModel = null;
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}

	public class CheckInClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			nextScreen(mModel.checkIn());
		}
	}

	public class CheckOutClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			nextScreen(mModel.checkOut());
		}
	}
	
	protected void onReset(){
	}
	
	protected void onPopulateModelData() {
	}
}
