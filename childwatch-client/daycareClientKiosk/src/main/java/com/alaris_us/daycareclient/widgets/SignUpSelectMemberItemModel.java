package com.alaris_us.daycareclient.widgets;

import android.view.View;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;

public class SignUpSelectMemberItemModel {

	private TrackableField<Member>	m_member;
	private TrackableBoolean		m_isSelected;
	private Integer					m_width;
	private Integer					m_color;

	public SignUpSelectMemberItemModel() {
		this(null, null, null);
	}

	public SignUpSelectMemberItemModel(Member member, Integer width, Integer color) {
		m_member = new TrackableField<Member>(member);
		m_isSelected = new TrackableBoolean(false);
		m_width = width;
		m_color = color;

	}

	public Member getMember() {
		return m_member.get();
	}

	public void setMember(Member member) {
		m_member.set(member);
	}

	public String getMemberName() {
		return m_member.get().getFirstName() + " " + m_member.get().getLastName();
	}

	public int getMemberBG() {
		return m_color;
	}

	public int getWidth() {
		return m_width;
	}

	public float getAlpha() {
		return ListViewFunctions.getTileAlpha(m_isSelected.get());
	}

	public boolean getIsSelected() {
		return m_isSelected.get();
	}

	public void setIsSelected(boolean isSelected) {
		m_isSelected.set(isSelected);
	}

	public void toggleIsSelected() {
		setIsSelected(!getIsSelected());
	}

	public int getCheckBoxRes() {
		return m_isSelected.get() ? View.VISIBLE : View.GONE;
	}

	public boolean getIsMemberActive() { // TODO use this for inactive check
		return m_member.get().isActive();
	}

	public int getInactiveMarkerVisibility() { // TODO use this to determine
												// Inactive Image Visibility
		return getIsMemberActive() ? View.INVISIBLE : View.VISIBLE;
	}
}
