package com.alaris_us.daycareclient.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.alaris_us.daycareclient.models.RegisterScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient_dev.R;

public class RegisterScreenFragment extends ScreenSwitcherFragment implements PopupFragment {
	protected RegisterScreenViewModel m_model;
	private AlertDialog m_noChildrenDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.registerscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void initData() {
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		m_model = new RegisterScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this);
		m_layout.findViewById(R.id.registerscreen_tile_register_container).setOnClickListener(
				m_model.new RegisterClickListener());
		m_layout.findViewById(R.id.registerscreen_tile_notnow_container).setOnClickListener(
				m_model.new NotNowClickListener());
	}

	public void showNoChildrenDialog() {
		FrameLayout noChildrendialogContainer = new FrameLayout(getActivity());
		View noChildrenDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_nochildrendialog_layout,
				noChildrendialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noChildrendialogContainer);
		m_noChildrenDialog = builder.create();
		m_noChildrenDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_noChildrenDialog.setCancelable(false);
		m_noChildrenDialog.show();
		ViewGroup.LayoutParams lp = noChildrenDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nochildrendialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nochildrendialog_height);
		noChildrenDialogView.setLayoutParams(lp);
		((View) noChildrenDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		noChildrenDialogView.findViewById(R.id.view_nochildrendialog_button).setOnClickListener(
				m_model.new NoChildrenDialogClickListener());
	}

	public void dismissNoChildrenDialog() {
		if (m_noChildrenDialog != null && m_noChildrenDialog.isShowing())
			m_noChildrenDialog.dismiss();
	}
}
