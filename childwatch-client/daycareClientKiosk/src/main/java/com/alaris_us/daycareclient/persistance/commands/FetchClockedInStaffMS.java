package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Staff;

/**
 * This fetch command was made to be moved to microservice achitecture.
 */
public class FetchClockedInStaffMS extends Command<List<Staff>> {

    private Facility m_facility;

    public FetchClockedInStaffMS(Facility facility) {

        this(facility, null, null);
    }

    protected FetchClockedInStaffMS(Facility facility, DaycareData dataSource, PersistenceData model) {
        super(dataSource, model);
        m_facility = facility;
    }

    @Override
    public void executeSource() {
        m_dataSource.getStaffDAO().listClockedInFacilityStaff(m_facility, this);
    }

    @Override
    public void handleResponse(List<Staff> data) {
        m_model.setClockedInStaff(data);
    }
}
