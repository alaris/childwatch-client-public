package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;

public class ReserveCubbies extends Command<List<Cubby>> {

	private final List<Member> m_children;
	private final List<Cubby> m_cubbies;
	private final Facility m_facility;
	private final Family m_family;

	public ReserveCubbies(List<Cubby> cubbies, List<Member> children, Family family, Facility facility) {
		this(cubbies, children, family, facility, null, null);
	};

	public ReserveCubbies(List<Cubby> cubbies, List<Member> children, Family family, Facility facility, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_cubbies=cubbies;
		m_children=children;
		m_family=family;
		m_facility=facility;
	}

	@Override
	public void executeSource() {
		m_dataSource.getCubbiesDAO().reserveCubbies(m_cubbies, m_children, m_facility, m_family, this);
		
	}

	@Override
	public void handleResponse(List<Cubby> cubbies) {
		// TODO dont need to do anything?
	}

}