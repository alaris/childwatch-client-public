package com.alaris_us.daycareclient.utils;

public abstract class State {
	public void onEnter() {
	}

	public void onExit() {
	}

	public void onException(Exception e) {
	}
}
