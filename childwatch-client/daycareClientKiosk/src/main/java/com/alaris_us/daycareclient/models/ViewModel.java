package com.alaris_us.daycareclient.models;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.persistence.PersistenceLayer.PersistenceLayerListener;
import com.alaris_us.daycareclient.utils.LogUtils;

public abstract class ViewModel implements PersistenceLayerListener {
	private final PersistenceLayer	mPersistenceLayer;
	private final String			mLogTag;
	private final List<CHANGE_ID>	mWatches;

	protected ViewModel(PersistenceLayer persistenceLayer) {
		mWatches = new ArrayList<CHANGE_ID>();
		mLogTag = LogUtils.makeLogTag(this.getClass());
		mPersistenceLayer = persistenceLayer;
		persistenceLayer.addListener(this);
		initialize();
		//reset();
	}

	public PersistenceLayer getPersistenceLayer() {
		LogUtils.LOGD(mLogTag, "getPersistenceLayer()");
		return mPersistenceLayer;
	}

	// ======================================================================================================
	public final void fetchModel() {
		LogUtils.LOGD(mLogTag, "fetchModel()");
		onFetchModel();
	}

	public final void initialize() {
		LogUtils.LOGD(mLogTag, "initialize()");
		onInitialize();
	}

	public final void persistModel() {
		LogUtils.LOGV(mLogTag, "persistModel()");
		onPersistModel();
	}

	public final void populateModelData() {
		LogUtils.LOGD(mLogTag, "populateModelData()");
		onPopulateModelData();
	}

	public final void reset() {
		LogUtils.LOGD(mLogTag, "reset()");
		onReset();
		populateModelData();
	}

	public final void disconnect() {
		LogUtils.LOGD(mLogTag, "disconnect()");
		getPersistenceLayer().removeListener(this);
	}

	// ======================================================================================================
	protected void onFetchModel() {
		// Life-cycle methods
	}

	protected void onPersistModel() {
		// Life-cycle methods
	}

	protected void onPopulateModelData() {
		// Life-cycle methods
	}

	protected void onInitialize() {
		// Life-cycle methods
	}

	protected void onException(Exception e) {
		// Life-cycle methods
	}

	protected void onUpdating() {
		// Life-cycle methods
	}

	protected void onUpdated() {
		// Life-cycle methods
	}

	protected void onReset() {
		// Life-cycle methods
	}

	// ======================================================================================================
	@Override
	public final void updating() {
		LogUtils.LOGD(mLogTag, "updating()");
		onUpdating();
	}

	@Override
	public final void updated() {
		LogUtils.LOGD(mLogTag, "updated()");
		onUpdated();
	}

	@Override
	public final void changed(CHANGE_ID id, Object newValue) {
		if (mWatches.contains(id)) {
			LogUtils.LOGD(mLogTag, "changed(" + id.name() + ")");
			populateModelData();
		}
	}

	@Override
	public final void exception(Exception e) {
		LogUtils.LOGD(mLogTag, "exception(" + e.getMessage() + ")");
		onException(e);
	}

	// ======================================================================================================
	public void addWatch(CHANGE_ID toAdd) {
		mWatches.add(toAdd);
	}

	public void removeWatch(CHANGE_ID toRemove) {
		mWatches.remove(toRemove);
	}

	public void clearWatches() {
		mWatches.clear();
	}
}
