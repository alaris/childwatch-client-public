package com.alaris_us.daycareclient.widgets;

import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

public class SelectSelfCheckInDialogItemModel {

	private TrackableField<Member> m_member;
	private TrackableInt m_height;
	private TrackableBoolean m_canSelfCheckIn;

	public SelectSelfCheckInDialogItemModel() {
		this(null, null);
	}

	public SelectSelfCheckInDialogItemModel(Member member, Integer height) {
		m_member = new TrackableField<Member>(member);
		m_height = new TrackableInt(height);
		m_canSelfCheckIn = new TrackableBoolean(false);
	}

	public Member getMember() {
		return m_member.get();
	}

	public void setMember(Member member) {
		m_member.set(member);
		Boolean canSelfCheckIn = member.getCanCheckSelfIn();
		setCanSelfCheckIn(canSelfCheckIn == null ? false : canSelfCheckIn.booleanValue());
	}

	public String getMemberName() {
		return m_member.get().getFirstName() + " " + m_member.get().getLastName();
	}

	public void setCanSelfCheckIn(boolean canSelfCheckIn) {
		m_canSelfCheckIn.set(canSelfCheckIn);
	}

	public boolean getCanSelfCheckIn() {
		return m_canSelfCheckIn.get();
	}

	public int getHeight() {
		return m_height.get();
	}

	public void setHeight(int heigth) {
		m_height.set(heigth);
	}
}
