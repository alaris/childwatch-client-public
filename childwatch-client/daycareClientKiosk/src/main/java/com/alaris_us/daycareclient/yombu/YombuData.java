package com.alaris_us.daycareclient.yombu;

import com.alaris_us.daycaredata.util.ConnectivityUtility;

public class YombuData {
    private final String mYombuBaseUrl;
    private ConnectivityUtility mConnectivityUtility;

    public YombuData(String yombuBaseUrl, ConnectivityUtility connectivityUtility) {
        mYombuBaseUrl = yombuBaseUrl;
        mConnectivityUtility = connectivityUtility;
    }

    public YombuData(String yombuBaseUrl) {
        mYombuBaseUrl = yombuBaseUrl;
        mConnectivityUtility = null;
    }

    public YombuCommand getYombuCommands(){
        return new YombuCommand(mYombuBaseUrl, mConnectivityUtility);
    }
}