package com.alaris_us.daycareclient.app;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender.Method;
import org.acra.sender.HttpSender.Type;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient_dev.R;

@ReportsCrashes(formKey = "", httpMethod = Method.PUT, reportType = Type.JSON, formUri = "http://alaris.iriscouch.com/acra-myapp/_design/acra-storage/_update/report", formUriBasicAuthLogin = "ymcaKiosk", formUriBasicAuthPassword = "joystick")
public class App extends Application {
	private PersistenceLayer	m_persistanceLayer;
	private static Context		m_context;
	private static Activity		m_currentActivity;

	@Override
	public void onCreate() {
		super.onCreate();
		if (isErrorReportEnabled())
			ACRA.init(this);
		m_context = this;
		m_persistanceLayer = new PersistenceLayer(this);
	}

	public boolean isErrorReportEnabled() {
		return getResources().getBoolean(R.bool.IS_ERROR_REPORT_ENABLED);
	}

	public PersistenceLayer getPersistenceLayer() {
		return m_persistanceLayer;
	}

	public static Context getContext() {
		return m_context;
	}

	public static Activity getCurrentActivity() {
		return m_currentActivity;
	}

	public void setCurrentActivity(Activity activity) {
		m_currentActivity = activity;
	}
}