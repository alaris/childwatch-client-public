package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Room;

public class FetchProgramRooms extends Command<List<Room>> {

	Program	m_program;

	public FetchProgramRooms(Program program) {

		this(program, null, null);
	}

	protected FetchProgramRooms(Program program, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_program = program;
	}

	@Override
	public void executeSource() {

		m_dataSource.getProgramsDAO().listProgramRooms(m_program, this);

	}

	@Override
	public void handleResponse(List<Room> data) {
		m_model.setSelectedProgramRooms(data);
	}
}