package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.alaris_us.daycareclient_dev.R;

public class SquareImageButton extends ImageButton {
	
	private final int WIDTH =0;
	private final int HEIGHT =1;
	private int squareBy = 0;

	public SquareImageButton(Context context) {
		super(context);
	}

	public SquareImageButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context,attrs);
	}

	public SquareImageButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs,R.styleable.SquareImageButton);
		try{
			squareBy = a.getInteger(R.styleable.SquareImageButton_squareBy, HEIGHT);
		}finally{
			a.recycle();
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int measureSpec = squareBy==WIDTH? widthMeasureSpec: heightMeasureSpec;
		//measureSpec = MeasureSpec.makeMeasureSpec(measureSpec, MeasureSpec.EXACTLY);
		super.onMeasure(measureSpec, measureSpec);
		//int size = squareBy==WIDTH? getMeasuredWidth(): getMeasuredHeight();
		//setMeasuredDimension(measureSpec, measureSpec);
	}

	/*@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		int size=w<h?w:h;
		setMeasuredDimension(size, size);
	}*/

/*	public void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		int w=r-l;
		int h=b-t;
		int size=w<h?w:h;
		LayoutParams lp= getLayoutParams();
		if(lp!=null){
			lp.height=size;
			lp.width=size;
			setLayoutParams(lp);
		}
		//setMeasuredDimension(size, size);
	}*/
	

}
