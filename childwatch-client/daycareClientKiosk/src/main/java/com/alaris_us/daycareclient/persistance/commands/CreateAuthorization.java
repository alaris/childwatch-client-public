package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Authorization;
import com.alaris_us.daycaredata.to.Member;

public class CreateAuthorization extends Command<Authorization> {
	private Member m_child;
	private Member m_authorizedBy;
	
	public CreateAuthorization(Member child, Member authorizedBy) {
		this(child, authorizedBy, null, null);
	};

	public CreateAuthorization(Member child, Member authorizedBy, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_child = child;
		m_authorizedBy = authorizedBy;
	}

	@Override
	public void executeSource() {
		Authorization auth = m_dataSource.getAuthorizationsDAO().getEmptyTO();
		auth.setAuthorizedFor(m_child);
		auth.setAuthorizedMember(m_authorizedBy);
		
		m_dataSource.getAuthorizationsDAO().create(auth, this);
	}

	@Override
	public void handleResponse(Authorization authorization) {

	}

}