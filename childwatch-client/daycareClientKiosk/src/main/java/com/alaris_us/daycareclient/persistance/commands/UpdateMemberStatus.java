package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;

public class UpdateMemberStatus
	extends Command<List<Member>> {

	private List<Member> m_members;
	private Facility m_facility;

	public UpdateMemberStatus(List<Member> members, Facility facility) {
		this(members, facility, null, null);
	};
	UpdateMemberStatus(List<Member> members, Facility facility, DaycareData dataSource,
			PersistenceData model) {

		super(dataSource, model);
		m_members = members;
		m_facility = facility;
	}

	@Override
	public void executeSource() {

		m_dataSource.getMembersDAO().updateMemberStatus(m_members, m_facility, this);

	}

	@Override
	public void handleResponse(
			List<Member> data) {


	}

}