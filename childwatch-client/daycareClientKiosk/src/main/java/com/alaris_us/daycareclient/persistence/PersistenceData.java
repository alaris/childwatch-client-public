package com.alaris_us.daycareclient.persistence;

import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Pair;

import com.alaris_us.daycareclient.data.DaycareCredential;
import com.alaris_us.daycareclient.models.AuthenticationScreenViewModel.LoginResult;
import com.alaris_us.daycareclient.utils.GeneralFunctions;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Offer;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Question;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Schedule;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycaredata.to.User;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.externaldata.to.UnitInfo;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PersistenceData {
	public interface PersistenceDataListener {
		public void valueChanged(CHANGE_ID id, Object newValue);
	}

	// Alert Updater
	private static final int ALERT_CALCULATIONS_INTERVAL = 1000;
	private Handler m_alertHandler;
	private Runnable m_alertCalulator;
	// Local persistent transfer objects
	private DateTime m_returnTime;

	// Change Fields
	public static enum CHANGE_ID {
		USER, FACILITY, TEMPERATURE, DATETIME, CAPACITY, ALLWORKOUTAREAS, SELECTEDWORKOUTAREAS, CURRENTMEMBER, ALLDAYCAREMEMBERS, ALLVALIDDAYCAREMEMBERS, ALLVALIDRELATEDCHILDREN, ALLPROXYCHILDREN, SELECTEDDAYCARECHILDREN, RELATEDINOUTCHILDREN, VALIDPROXYINOUTCHILDREN, SIGNINTYPE, ALLYMCAMEMBERS, ALLYMCAPARENTSANDCHILDREN, SELECTEDYMCAPARENTS, SELECTEDYMCACHILDREN, CREDENTIAL, INOROUT, ALLCUBBIES, AVAILABLECUBBIES, FAMILYCUBBIES, FAMILY, MEMBERSTOUPDATE, INCIDENTSLIST, OFFERSLIST, RELATEDCHILDREN, PROXYCHILDREN, VALIDRELATEDCHILDREN, VALIDPROXYCHILDREN, PRIMARYFAMILY, SECONDARYFAMILY, ISOVERRIDE, REGISTRATIONUNITS, ISREGOVERRIDE, ISINVALIDMEMBERSELECTEDOVERRIDE, ALLROOMS, RETURNTIME, ALLPAGERS, AVAILABLEPAGERS, FAMILYPAGERS, ASSIGNEDROOMS, MINROOMAGE, MAXROOMAGE, MULTIROOMCHILDREN, LOGINRESULT, ALLPROGRAMS, SELECTEDPROGRAM, ALLAVAILABLEPROGRAMS, AVAILABLEPROGRAMSCHEDULES, CLOCKEDINSTAFF, CHECKEDINMEMBERS, CHECKEDINMEMBERSCOUNT, SELECTEDPROGRAMROOMS, ALLFACILITYQUESTIONS, FACILITYPOPUPQUESTIONS, FACILITYFULLSCREENQUESTIONS, ISREGISTRATION, ALLOWEDSELFCHECKINMEMBERS, SELFCHECKINCHILDREN
	}

	private String mPrinterIP;
	private String mPrinterModel;
	private String mPrinterMacAddress;
	private User m_user;
	private List<Member> m_daycareMembersWithPin;
	private List<Member> m_validDaycareMembersWithPin;
	private List<Member> m_allYMCAMembers;
	private List<Member> m_allYMCAParents;
	private List<Member> m_allYMCAChildren;
	private List<Member> m_membersToUpdate;
	private List<Member> m_selectedYMCAParents;
	private List<Member> m_selectedYMCAChildren;
	private Member m_currentMember;
	private Facility m_facility;
	private List<Member> m_validRelatedInChildren;
	private List<Member> m_validProxyInChildren;
	private List<Member> m_validRelatedOutChildren;
	private List<Member> m_validProxyOutChildren;
	private List<Member> m_selectedDaycareChildren;
	private List<WorkoutArea> m_allWorkoutAreas;
	private List<WorkoutArea> m_selectedWorkoutAreas;
	private String m_currentTemperature;
	private String m_currentTemperatureUnit;
	private CharSequence m_currentDateTime;
	private SignInType m_signInType;
	private DaycareCredential mCredential;
	private InOrOut m_inOrOut;
	private List<Cubby> m_allCubbies;
	private List<Cubby> m_availableCubbies;
	private List<Cubby> m_familyCubbies;
	private Family m_family;
	private List<Incident> m_incidentsList;
	private List<Offer> m_offersList;
	private List<Member> m_relatedChildren;
	private List<Member> m_proxyChildren;
	private List<Member> m_validRelatedChildren;
	private List<Member> m_validProxyChildren;
	private Family m_primaryFamily;
	private Family m_secondaryFamily;
	private boolean m_isOverride = false;
	private boolean m_isRegOverride = false;
	private boolean m_isInvalidMemberSelectedOverride = false;
	private List<UnitInfo> m_registrationUnits;
	private List<Room> m_allRooms;
	private List<Room> m_assignedRooms;
	private Double m_minRoomAge;
	private Double m_maxRoomAge;
	private List<Member> m_multiRoomChildren;
	private LoginResult m_loginResult;
	private List<Program> m_allPrograms;
	private Program m_selectedProgram;
	private List<Program> m_allAvailablePrograms;
	private HashMap<Program, Schedule> m_availableProgramSchedules;
	private List<Staff> m_clockedInStaff;
	private Integer m_checkedInMembersCount;
	private List<Member> m_checkedInMembers;
	private List<Room> m_selectedProgramRooms;
	private List<Question> m_allFacilityQuestions;
	private List<Question> m_facilityPopupQuestions;
	private List<Question> m_facilityFullScreenQuestions;
	private String mDataSource;
	private String m_selectedService;
	private String m_capacityInfo;
	private String m_profitCenterAddOn;
	private String m_selectedLanguage;
	private boolean m_isRegistration;
	private List<Member> m_allowedSelfCheckInMembers;
	private List<Member> m_selfCheckInChildren;
	private List<Pager> m_availablePagers;
	private List<Pager> m_familyPagers;

	public enum InOrOut {
		NOKIDS, IN, OUT, INANDOUT, NOKIDSIN, NOKIDSOUT
	}

	// Listener
	PersistenceDataListener m_listener;

	public PersistenceData(PersistenceDataListener listener) {
		m_listener = listener;
		// Setup alert handler
		m_alertHandler = new Handler();
		m_alertCalulator = new Runnable() {
			@Override
			public void run() {
				updateAlerts();
				m_alertHandler.postDelayed(m_alertCalulator, ALERT_CALCULATIONS_INTERVAL);
			}
		};
		m_alertHandler.postDelayed(m_alertCalulator, ALERT_CALCULATIONS_INTERVAL);
		this.m_selectedYMCAChildren = new ArrayList<Member>();
	}

	public void setAllWorkoutAreas(List<WorkoutArea> allWorkoutAreas) {
		GeneralFunctions.sortTOList(allWorkoutAreas);
		m_allWorkoutAreas = allWorkoutAreas;
		notifyValueChanged(CHANGE_ID.ALLWORKOUTAREAS, allWorkoutAreas);
	}

    public void setAllFacilityQuestions(List<Question> allFacilityQuestions) {
        m_allFacilityQuestions = allFacilityQuestions;
        notifyValueChanged(CHANGE_ID.ALLFACILITYQUESTIONS, allFacilityQuestions);
    }

    public void setFacilityPopupQuestions(List<Question> facilityPopupQuestions) {
        m_facilityPopupQuestions = facilityPopupQuestions;
        notifyValueChanged(CHANGE_ID.FACILITYPOPUPQUESTIONS, facilityPopupQuestions);
    }

    public void setFacilityFullScreenQuestions(List<Question> facilityFullScreenQuestions) {
        m_facilityFullScreenQuestions = facilityFullScreenQuestions;
        notifyValueChanged(CHANGE_ID.FACILITYFULLSCREENQUESTIONS, facilityFullScreenQuestions);
    }

	public void setFacility(Facility facility) {
		m_facility = facility;
		notifyValueChanged(CHANGE_ID.FACILITY, facility);
	}

    public void setClockedInStaff(List<Staff> staff) {
        m_clockedInStaff = staff;
        notifyValueChanged(CHANGE_ID.CLOCKEDINSTAFF, staff);
    }

    public void setCheckedInMembersCount(Integer checkedInMembers) {
		m_checkedInMembersCount = checkedInMembers;
		notifyValueChanged(CHANGE_ID.CHECKEDINMEMBERSCOUNT, checkedInMembers);
	}

	public void setCheckedInMembers(List<Member> checkedInMembers) {
		m_checkedInMembers = checkedInMembers;
		notifyValueChanged(CHANGE_ID.CHECKEDINMEMBERS, checkedInMembers);
	}

	public List<Member> getCheckedInMembers() {
		return m_checkedInMembers;
	}

	public void setFamily(Family family) {
		m_family = family;
		notifyValueChanged(CHANGE_ID.FAMILY, family);
	}

	public void setCredential(DaycareCredential toSet) {
		mCredential = toSet;
		notifyValueChanged(CHANGE_ID.CREDENTIAL, toSet);
	}

	public void setCurrentMember(Member currentMember) {
		this.m_currentMember = currentMember;
		notifyValueChanged(CHANGE_ID.CURRENTMEMBER, currentMember);
	}

	public void setMembersToUpdate(List<Member> members) {
		this.m_membersToUpdate = members;
		notifyValueChanged(CHANGE_ID.MEMBERSTOUPDATE, members);
	}

	public void setDaycareMembersWithPin(List<Member> members) {
		GeneralFunctions.sortTOList(members);
		this.m_daycareMembersWithPin = members;
		notifyValueChanged(CHANGE_ID.ALLDAYCAREMEMBERS, members);
	}

	public void setValidDaycareMembersWithPin(List<Member> members) {
		GeneralFunctions.sortTOList(members);
		this.m_validDaycareMembersWithPin = members;
		notifyValueChanged(CHANGE_ID.ALLVALIDDAYCAREMEMBERS, members);
	}

	public void setAllYMCAMembers(List<Member> members) {
		GeneralFunctions.sortTOList(members);
		this.m_allYMCAMembers = members;
		notifyValueChanged(CHANGE_ID.ALLYMCAMEMBERS, members);
	}

	public void setYMCAFamily(List<Member> family, List<Member> parents, List<Member> children) {
		setAllYMCAMembers(family);
		setYMCAParentsAndChildren(parents, children);
	}

	public void setAllRelatedChildren(List<Member> relatedChildren) {
		this.m_relatedChildren = relatedChildren;
		notifyValueChanged(CHANGE_ID.RELATEDCHILDREN, m_relatedChildren);
	}

	public void setAllProxyChildren(List<Member> proxyChildren) {
		this.m_proxyChildren = proxyChildren;
		notifyValueChanged(CHANGE_ID.PROXYCHILDREN, m_proxyChildren);
	}

	/**
	 * CapacityInfo is generated based on the current state of other properties from persistence data.
	 */
	public void setCapacityInfo(String capacityInfo) {
		this.m_capacityInfo = capacityInfo;
		notifyValueChanged(CHANGE_ID.CAPACITY, m_capacityInfo);
	}

	public void setAllValidRelatedChildren(List<Member> in, List<Member> out) {
		List<Member> all = new ArrayList<Member>();
		if (in != null)
			all.addAll(in);
		if (out != null)
			all.addAll(out);
		GeneralFunctions.sortTOList(in);
		GeneralFunctions.sortTOList(out);
		this.m_validRelatedInChildren = in;
		this.m_validRelatedOutChildren = out;
		notifyValueChanged(CHANGE_ID.ALLVALIDRELATEDCHILDREN, all);
	}

	public void setAllValidProxyChildren(List<Member> in, List<Member> out) {
		List<Member> all = new ArrayList<Member>();
		if (in != null)
			all.addAll(in);
		if (out != null)
			all.addAll(out);
		GeneralFunctions.sortTOList(in);
		GeneralFunctions.sortTOList(out);
		this.m_validProxyInChildren = in;
		this.m_validProxyOutChildren = out;
		notifyValueChanged(CHANGE_ID.VALIDPROXYINOUTCHILDREN, all);
	}

	public void setAllDaycareChildren(List<Member> related, List<Member> proxies) {
		setAllRelatedChildren(related);
		setAllProxyChildren(proxies);
	}

	public void setAllValidDaycareChildren(List<Member> related, List<Member> proxies) {
		Pair<List<Member>, List<Member>> relatedInOut = sortAndUpdateChildren(related);
		Pair<List<Member>, List<Member>> proxyInOut = sortAndUpdateChildren(proxies);
		List<Member> relatedIn = relatedInOut.first;
		List<Member> relatedOut = relatedInOut.second;
		List<Member> proxyIn = proxyInOut.first;
		List<Member> proxyOut = proxyInOut.second;
		setAllValidRelatedChildren(relatedIn, relatedOut);
		setAllValidProxyChildren(proxyIn, proxyOut);
		calculateInOrOut(relatedIn, relatedOut, proxyIn, proxyOut); // TODO
																	// comment
																	// in/out
																	// depending
																	// on if
																	// using
																	// in/out
																	// screen
		checkIfNoKids(relatedIn, relatedOut, proxyIn, proxyOut);
		notifyValueChanged(CHANGE_ID.INOROUT, InOrOut.NOKIDS); // TODO comment
																// in/out
																// depending on
																// if using
																// in/out screen
	}

	public void checkIfNoKids(List<Member> relatedIn, List<Member> relatedOut, List<Member> proxyIn,
			List<Member> proxyOut) {
		InOrOut inOrOut = getInOrOut();
		if (((relatedIn == null || relatedIn.isEmpty()) && (proxyIn == null || proxyIn.isEmpty()) && inOrOut == InOrOut.IN))
			inOrOut = InOrOut.NOKIDSIN;
		else if (((relatedOut == null || relatedOut.isEmpty()) && (proxyOut == null || proxyOut.isEmpty()) && inOrOut == InOrOut.OUT))
			inOrOut = InOrOut.NOKIDSOUT;
		setInOrOut(inOrOut);
	}

	public void setValidRelatedChildren(List<Member> validRelatedChildren) {
		this.m_validRelatedChildren = validRelatedChildren;
		notifyValueChanged(CHANGE_ID.VALIDRELATEDCHILDREN, validRelatedChildren);
	}

	public void setValidProxyChildren(List<Member> validProxyChildren) {
		this.m_validProxyChildren = validProxyChildren;
		notifyValueChanged(CHANGE_ID.VALIDPROXYCHILDREN, validProxyChildren);
	}

	private Pair<List<Member>, List<Member>> sortAndUpdateChildren(List<Member> children) {
		List<Member> in = new ArrayList<Member>();
		List<Member> out = new ArrayList<Member>();
		for (Member child : children) {
			if (child.getPFacility() == null) {
				in.add(child);
			} else
				out.add(child);
		}
		return new Pair<List<Member>, List<Member>>(in, out);
	}

	public void calculateInOrOut(List<Member> relatedIn, List<Member> relatedOut, List<Member> proxyIn,
			List<Member> proxyOut) {
		InOrOut inOrOut;
		if ((relatedIn == null || relatedIn.isEmpty()) && (relatedOut == null || relatedOut.isEmpty())
				&& (proxyIn == null || proxyIn.isEmpty()) && (proxyOut == null || proxyOut.isEmpty())) {
			inOrOut = InOrOut.NOKIDS;
		} else if ((relatedIn == null || relatedIn.isEmpty()) && (proxyIn == null || proxyIn.isEmpty())) {
			inOrOut = InOrOut.OUT;
		} else if ((relatedOut == null || relatedOut.isEmpty()) && (proxyOut == null || proxyOut.isEmpty())) {
			inOrOut = InOrOut.IN;
		} else {
			// TODO South hampton only does provider checkouts. This is a hack.
			inOrOut = InOrOut.IN;
		}
		setInOrOut(inOrOut);
	}

	public void setInOrOut(InOrOut inOrOut) {
		m_inOrOut = inOrOut;
		notifyValueChanged(CHANGE_ID.INOROUT, inOrOut);
	}

	public void setYMCAParentsAndChildren(List<Member> ymcaParents, List<Member> ymcaChildren) {
		GeneralFunctions.sortTOList(ymcaParents);
		GeneralFunctions.sortTOList(ymcaChildren);
		this.m_allYMCAParents = ymcaParents;
		this.m_allYMCAChildren = ymcaChildren;
		notifyValueChanged(CHANGE_ID.ALLYMCAPARENTSANDCHILDREN, ymcaParents);
	}

	public void setSelectedChildren(List<Member> selectedChildren) {
		GeneralFunctions.sortTOList(selectedChildren);
		this.m_selectedDaycareChildren = selectedChildren;
		notifyValueChanged(CHANGE_ID.SELECTEDDAYCARECHILDREN, selectedChildren);
	}

	public void setSelectedYMCAParents(List<Member> members) {
		GeneralFunctions.sortTOList(members);
		this.m_selectedYMCAParents = members;
		notifyValueChanged(CHANGE_ID.SELECTEDYMCAPARENTS, members);
	}

	public void setSelectedYMCAChildren(List<Member> members) {
		GeneralFunctions.sortTOList(members);
		this.m_selectedYMCAChildren.clear();
		if (members != null)
			this.m_selectedYMCAChildren.addAll(members);
		notifyValueChanged(CHANGE_ID.SELECTEDYMCACHILDREN, members);
	}

	public void setSelectedProgram(Program selectedProgram) {
		this.m_selectedProgram = selectedProgram;
		notifyValueChanged(CHANGE_ID.SELECTEDPROGRAM, selectedProgram);
	}

	public void setSelectedWorkoutAreas(List<WorkoutArea> selectedWorkoutAreas) {
		GeneralFunctions.sortTOList(selectedWorkoutAreas);
		this.m_selectedWorkoutAreas = selectedWorkoutAreas;
		notifyValueChanged(CHANGE_ID.SELECTEDWORKOUTAREAS, selectedWorkoutAreas);
	}

	public void setCurrentTemperature(String currentTemperature, String currentTemperatureUnit) {
		this.m_currentTemperature = currentTemperature;
		this.m_currentTemperatureUnit = currentTemperatureUnit;
		notifyValueChanged(CHANGE_ID.TEMPERATURE, currentTemperature);
	}

	public void setCurrentDateTime(CharSequence charSequence) {
		this.m_currentDateTime = charSequence;
		notifyValueChanged(CHANGE_ID.DATETIME, charSequence);
	}

	public void setAllCubbies(List<Cubby> cubbies) {
		GeneralFunctions.sortTOList(cubbies);
		this.m_allCubbies = cubbies;
		notifyValueChanged(CHANGE_ID.ALLCUBBIES, cubbies);
	}

	public void setAvailableCubbies(List<Cubby> cubbies) {
		GeneralFunctions.sortTOList(cubbies);
		this.m_availableCubbies = cubbies;
		notifyValueChanged(CHANGE_ID.AVAILABLECUBBIES, cubbies);
	}

	public void setFamilyCubbies(List<Cubby> cubbies) {
		GeneralFunctions.sortTOList(cubbies);
		this.m_familyCubbies = cubbies;
		notifyValueChanged(CHANGE_ID.FAMILYCUBBIES, cubbies);
	}

	public List<Cubby> getAllCubbies() {
		return (m_allCubbies == null) ? new ArrayList<Cubby>() : m_allCubbies;
	}

	public List<Cubby> getAvailableCubbies() {
		return (m_availableCubbies == null) ? new ArrayList<Cubby>() : m_availableCubbies;
	}

	public List<Cubby> getFamilyCubbies() {
		return (m_familyCubbies == null) ? new ArrayList<Cubby>() : m_familyCubbies;
	}

	public void setAvailablePagers(List<Pager> pagers) {
		GeneralFunctions.sortTOList(pagers);
		this.m_availablePagers = pagers;
		notifyValueChanged(CHANGE_ID.AVAILABLEPAGERS, pagers);
	}

	public List<Pager> getAvailablePagers() {
		return (m_availablePagers == null) ? new ArrayList<Pager>() : m_availablePagers;
	}

	public void setFamilyPagers(List<Pager> pagers) {
		GeneralFunctions.sortTOList(pagers);
		this.m_familyPagers = pagers;
		notifyValueChanged(CHANGE_ID.FAMILYPAGERS, pagers);
	}

	public List<Pager> getFamilyPagers() {
		return (m_familyPagers == null) ? new ArrayList<Pager>() : m_familyPagers;
	}

	public void setAllRooms(List<Room> rooms) {
		GeneralFunctions.sortTOList(rooms);
		this.m_allRooms = rooms;
		notifyValueChanged(CHANGE_ID.ALLROOMS, rooms);
	}

	public List<Room> getAllRooms() {
		return (m_allRooms == null) ? new ArrayList<Room>() : m_allRooms;
	}

	public void setAllPrograms(List<Program> programs) {
		GeneralFunctions.sortTOList(programs);
		this.m_allPrograms = programs;
		notifyValueChanged(CHANGE_ID.ALLPROGRAMS, programs);
	}

	public List<Program> getAllPrograms() {
		return (m_allPrograms == null) ? new ArrayList<Program>() : m_allPrograms;
	}

	public void setSelectedProgramRooms(List<Room> rooms) {
		GeneralFunctions.sortTOList(rooms);
		this.m_selectedProgramRooms = rooms;
		notifyValueChanged(CHANGE_ID.SELECTEDPROGRAMROOMS, rooms);
	}

	public List<Room> getSelectedProgramRooms() {
		return (m_selectedProgramRooms == null) ? new ArrayList<Room>() : m_selectedProgramRooms;
	}

	public List<Program> getAllAvailablePrograms() {
		return (m_allAvailablePrograms == null) ? new ArrayList<Program>() : m_allAvailablePrograms;
	}

	public void setAllAvailablePrograms(List<Program> availablePrograms) {
		GeneralFunctions.sortTOList(availablePrograms);
		this.m_allAvailablePrograms = availablePrograms;
		notifyValueChanged(CHANGE_ID.ALLAVAILABLEPROGRAMS, availablePrograms);
	}

	public HashMap<Program, Schedule> getAvailableProgramSchedules() {
		return m_availableProgramSchedules;
	}

	public void setAvailableProgramSchedules(HashMap<Program, Schedule> availableProgramSchedules) {
		this.m_availableProgramSchedules = availableProgramSchedules;
		notifyValueChanged(CHANGE_ID.AVAILABLEPROGRAMSCHEDULES, availableProgramSchedules);
	}

	public void setAssingedRooms(List<Room> rooms) {
		GeneralFunctions.sortTOList(rooms);
		this.m_assignedRooms = rooms;
		notifyValueChanged(CHANGE_ID.ASSIGNEDROOMS, rooms);
	}

	public List<Room> getAssignedRooms() {
		return (m_assignedRooms == null) ? new ArrayList<Room>() : m_assignedRooms;
	}

	public void setIncidentsList(List<Incident> incidents) {
		this.m_incidentsList = incidents;
		notifyValueChanged(CHANGE_ID.INCIDENTSLIST, incidents);
	}

	public InOrOut getInOrOut() {
		return m_inOrOut;
	}

	public Family getFamily() {
		return m_family;
	}

	public DaycareCredential getCredential() {
		return mCredential;
	}

	public SignInType getSignInType() {
		return (m_signInType == null) ? SignInType.OTHER : m_signInType;
	}

	public void setSignInType(SignInType signInType) {
		m_signInType = signInType;
		notifyValueChanged(CHANGE_ID.SIGNINTYPE, signInType);
	}

	public boolean getIsStaffFacing() {
		return (m_user == null) ? false : m_user.isStaffFacing();
	}

	public String getFacilityCity() {
		return (m_facility == null) ? null : m_facility.getCity();
	}

	public String getFacilityZipCode() {
		return (m_facility == null) ? null : m_facility.getZipCode();
	}

	public String getFacilityState() {
		return (m_facility == null) ? null : m_facility.getUSState();
	}

	public Bitmap getFacilityImage() {
		return (m_facility == null) ? null : m_facility.getImageFile();
	}

	public String getOverridePIN() {
		return (m_facility == null) ? null : m_facility.getOverridePIN();
	}

	public String getFacilityClientId() {
		return (m_facility == null) ? "" : m_facility.getYmcaClientDataId();
	}

	public String getYmcaName() {
		return (m_user == null) ? null : m_facility.getYMCAName();
	}

	public List<Member> getMembersToUpdate() {
		return (m_membersToUpdate == null) ? new ArrayList<Member>() : m_membersToUpdate;
	}

	public List<Member> getDaycareMembersWithPin() {
		return (m_daycareMembersWithPin == null) ? new ArrayList<Member>() : m_daycareMembersWithPin;
	}

	public List<Member> getValidDaycareMembersWithPin() {
		return (m_validDaycareMembersWithPin == null) ? new ArrayList<Member>() : m_validDaycareMembersWithPin;
	}

	public List<Member> getAllYMCAMembers() {
		return (m_allYMCAMembers == null) ? new ArrayList<Member>() : m_allYMCAMembers;
	}

	public List<Member> getAllYMCAParents() {
		return (m_allYMCAParents == null) ? new ArrayList<Member>() : m_allYMCAParents;
	}

	public List<Member> getAllYMCAChildren() {
		return (m_allYMCAChildren == null) ? new ArrayList<Member>() : m_allYMCAChildren;
	}

	public Member getCurrentMember() {
		return m_currentMember;
	}

	public List<Member> getRelatedInChildren() {
		return (m_validRelatedInChildren == null) ? new ArrayList<Member>() : m_validRelatedInChildren;
	}

	public List<Member> getProxyInChildren() {
		return (m_validProxyInChildren == null) ? new ArrayList<Member>() : m_validProxyInChildren;
	}

	public List<Member> getRelatedOutChildren() {
		return (m_validRelatedOutChildren == null) ? new ArrayList<Member>() : m_validRelatedOutChildren;
	}

	public List<Member> getProxyOutChildren() {
		return (m_validProxyOutChildren == null) ? new ArrayList<Member>() : m_validProxyOutChildren;
	}

	public List<Member> getSelectedDaycareChildren() {
		return (m_selectedDaycareChildren == null) ? new ArrayList<Member>() : m_selectedDaycareChildren;
	}

	public List<Member> getSelectedYMCAChildren() {
		return (m_selectedYMCAChildren == null) ? new ArrayList<Member>() : m_selectedYMCAChildren;
	}

	public List<Member> getSelectedYMCAParents() {
		return (m_selectedYMCAParents == null) ? new ArrayList<Member>() : m_selectedYMCAParents;
	}

	public String getCurrentTemperature() {
		return m_currentTemperature;
	}

	public String getCurrentTemperatureUnit() {
		return m_currentTemperatureUnit;
	}

	public CharSequence getCurrentDateTime() {
		return m_currentDateTime;
	}

	public String getCapacityInfo() { return m_capacityInfo; }

	public List<WorkoutArea> getAllWorkoutAreas() {
		return (m_allWorkoutAreas == null) ? new ArrayList<WorkoutArea>() : m_allWorkoutAreas;
	}

    public List<Question> getAllFacilityQuestions() {
        return (m_allFacilityQuestions == null) ? new ArrayList<Question>() : m_allFacilityQuestions;
    }

    public List<Question> getFacilityPopupQuestions() {
        return (m_facilityPopupQuestions == null) ? new ArrayList<Question>() : m_facilityPopupQuestions;
    }

    public List<Question> getFacilityFullScreenQuestions() {
        return (m_facilityFullScreenQuestions == null) ? new ArrayList<Question>() : m_facilityFullScreenQuestions;
    }

	public List<WorkoutArea> getSelectedWorkoutAreas() {
		return (m_selectedWorkoutAreas == null) ? new ArrayList<WorkoutArea>() : m_selectedWorkoutAreas;
	}

	public Program getSelectedProgram() {
		return m_selectedProgram;
	}

	public Facility getFacility() {
		return m_facility;
	}

    public List<Staff> getClockedInStaff() {
        return m_clockedInStaff;
    }

    public Integer getCheckedInMembersCount() {
		return m_checkedInMembersCount;
	}

	public List<Incident> getIncidentsList() {
		return (m_incidentsList == null) ? new ArrayList<Incident>() : m_incidentsList;
	}

	public void setUser(User user) {
		m_user = user;
		mPrinterIP = user.getPrinterIP();
        mPrinterModel = user.getPrinterModel();
        mPrinterMacAddress = user.getPrinterMacAddress();
		if (m_listener != null)
			m_listener.valueChanged(CHANGE_ID.USER, m_user);
	}

	public User getUser() {
		return m_user;
	}

	public void setAllOffers(List<Offer> offers) {
		this.m_offersList = offers;
		notifyValueChanged(CHANGE_ID.OFFERSLIST, offers);
	}

	public List<Offer> getOffersList() {
		return (m_offersList == null) ? new ArrayList<Offer>() : m_offersList;
	}

	public List<Member> getValidRelatedChildren() {
		return (m_validRelatedChildren == null) ? new ArrayList<Member>() : m_validRelatedChildren;
	}

	public List<Member> getValidProxyChildren() {
		return (m_validProxyChildren == null) ? new ArrayList<Member>() : m_validProxyChildren;
	}

	public List<Member> getRelatedChildren() {
		return (m_relatedChildren == null) ? new ArrayList<Member>() : m_relatedChildren;
	}

	public List<Member> getProxyChildren() {
		return (m_proxyChildren == null) ? new ArrayList<Member>() : m_proxyChildren;
	}

	public Family getPrimaryFamily() {
		return m_primaryFamily;
	}

	public void setPrimaryFamily(Family primary) {
		m_primaryFamily = primary;
	}

	public Family getSecondaryFamily() {
		return m_secondaryFamily;
	}

	public void setSecondaryFamily(Family secondary) {
		m_secondaryFamily = secondary;
	}

	public boolean getIsOverride() {
		return m_isOverride;
	}

	public void setIsOverride(boolean override) {
		m_isOverride = override;
		notifyValueChanged(CHANGE_ID.ISOVERRIDE, override);
	}

	public boolean getIsRegOverride() {
		return m_isRegOverride;
	}

	public void setIsRegOverride(boolean regOverride) {
		m_isRegOverride = regOverride;
		notifyValueChanged(CHANGE_ID.ISREGOVERRIDE, regOverride);
	}

	public List<UnitInfo> getRegistrationUnits() {
		return (m_registrationUnits == null) ? new ArrayList<UnitInfo>() : m_registrationUnits;
	}

	public void setRegistrationUnits(List<UnitInfo> registrationUnits) {
		m_registrationUnits = registrationUnits;
		notifyValueChanged(CHANGE_ID.REGISTRATIONUNITS, registrationUnits);
	}

	public boolean getIsInvalidMemberSelectedOverride() {
		return m_isInvalidMemberSelectedOverride;
	}

	public void setIsInvalidMemberSelectedOverride(boolean invalidMemberSelectedOverride) {
		m_isInvalidMemberSelectedOverride = invalidMemberSelectedOverride;
		notifyValueChanged(CHANGE_ID.ISINVALIDMEMBERSELECTEDOVERRIDE, invalidMemberSelectedOverride);
	}

	public void setReturnTime(DateTime returnTime) {
		m_returnTime = returnTime;
		notifyValueChanged(CHANGE_ID.RETURNTIME, returnTime);
	}

	public DateTime getReturnTime() {
		return m_returnTime;
	}

	public Double getMinRoomAge() {
		return m_minRoomAge;
	}

	public void setMinRoomAge(double minRoomAge) {
		m_minRoomAge = minRoomAge;
	}

	public Double getMaxRoomAge() {
		return m_maxRoomAge;
	}

	public void setMaxRoomAge(double maxRoomAge) {
		m_maxRoomAge = maxRoomAge;
	}

	public void setMultiRoomChildren(List<Member> multiRoomChildren) {
		this.m_multiRoomChildren = multiRoomChildren;
		notifyValueChanged(CHANGE_ID.MULTIROOMCHILDREN, multiRoomChildren);
	}

	public List<Member> getMultiRoomChildren() {
		return (m_multiRoomChildren == null) ? new ArrayList<Member>() : m_multiRoomChildren;
	}

	public LoginResult getLoginResult(){
		return m_loginResult;
	}

	public void setLoginResult(LoginResult loginResult){
		this.m_loginResult = loginResult;
		notifyValueChanged(CHANGE_ID.LOGINRESULT, loginResult);
	}

	public boolean getIsRegistration(){
		return m_isRegistration;
	}

	public void setIsRegistration(boolean isRegistration){
		this.m_isRegistration = isRegistration;
		notifyValueChanged(CHANGE_ID.ISREGISTRATION, isRegistration);
	}

	// TODO use this when logging in
	private void notifyValueChanged(CHANGE_ID id, Object value) {
		if (m_listener == null)
			return;
		m_listener.valueChanged(id, value);
	}

	public List<Member> getSelfCheckInChildren() {
		return m_selfCheckInChildren == null ? new ArrayList<Member>() : m_selfCheckInChildren;
	}

	public void setSelfCheckInChildren(List<Member> selfCheckins) {
		this.m_selfCheckInChildren = selfCheckins;
		notifyValueChanged(CHANGE_ID.SELFCHECKINCHILDREN, selfCheckins);

	}
	public String getPrinterIP() {
		return mPrinterIP;
	}

    public String getPrinterModel() {
        return mPrinterModel;
    }

    public String getPrinterMacAddress() {
        return mPrinterMacAddress;
    }

    public void setDataSource(String dataSource) {
    	mDataSource = dataSource;
    }

    public String getDataSource() {
		return (mDataSource == null) ? "" : mDataSource;
    }

	public String getSelectedService() {
		return m_selectedService;
	}

	public void setSelectedService(String service) {
		m_selectedService = service;
	}

	public String getProfitCenterAddOn() {
		return m_profitCenterAddOn;
	}

	public void setProfitCenterAddOn(String addOn) {
		m_profitCenterAddOn = addOn;
	}

	public String getSelectedLanguage() {
		return m_selectedLanguage;
	}

	public void setSelectedLanguage(String language) {
		m_selectedLanguage = language;
	}

	public List<Member> getAllowedSelfCheckInMembers() {
		return m_allowedSelfCheckInMembers;
	}

	public void setAllowedSelfCheckInMembers(List<Member> members) {
		m_allowedSelfCheckInMembers = members;
		notifyValueChanged(CHANGE_ID.ALLOWEDSELFCHECKINMEMBERS, members);
	}

	public void clearDataOnLoop() {
		setDaycareMembersWithPin(null);
		setYMCAFamily(null, null, null);
		setSelectedYMCAParents(null);
		setSelectedYMCAChildren(null);
		setCurrentMember(null);
		setAllRelatedChildren(null);
		setAllProxyChildren(null);
		setAllValidRelatedChildren(null, null);
		setAllValidProxyChildren(null, null);
		setSelectedChildren(null);
		setSelectedWorkoutAreas(null);
		setCredential(null);
		setInOrOut(null);
		setFamilyCubbies(null);
		setFamily(null);
		setAvailableCubbies(null);
		setMembersToUpdate(null);
		setPrimaryFamily(null);
		setSecondaryFamily(null);
		setIsOverride(false);
		setIsRegOverride(false);
		setRegistrationUnits(null);
		setIsInvalidMemberSelectedOverride(false);
		setReturnTime(null);
		setMultiRoomChildren(null);
		setLoginResult(null);
		setSelectedProgram(null);
		setSelectedService(null);
		setProfitCenterAddOn(null);
		setSelectedLanguage(null);
		setIsRegistration(false);
		setAllowedSelfCheckInMembers(null);
		setValidDaycareMembersWithPin(null);
		setSelfCheckInChildren(null);
		System.gc();
	}

	private void updateAlerts() {
	}

	public void persistFacility() {
	}
}
