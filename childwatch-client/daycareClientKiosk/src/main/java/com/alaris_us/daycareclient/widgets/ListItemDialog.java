package com.alaris_us.daycareclient.widgets;

import com.alaris_us.daycareclient.app.App;

import android.app.Dialog;
import android.content.Context;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class ListItemDialog extends Dialog {
	private WindowManager.LayoutParams m_dialogAttrs;
	private View m_container;
	private int m_layoutRes;
	
	public ListItemDialog(Context context, int layoutRes) {
		super(context);
		m_layoutRes=layoutRes;
		init(context); 
	}
	
	private void init(Context context){
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setCanceledOnTouchOutside(false);
		makeDialogAttrs();
		getWindow().setBackgroundDrawable(null);
		setOwnerActivity(App.getCurrentActivity());
		setAllTextWatchers(getBaseView(), new KeyboardTextChangedListener());
	}
	
	public void setBounds(int x, int y, int width, int height){
		if(m_dialogAttrs==null)
			makeDialogAttrs();
		m_dialogAttrs.width=width;
		m_dialogAttrs.height=height;
		m_dialogAttrs.x=x;
		//m_dialogAttrs.y=y;
		getWindow().setAttributes(m_dialogAttrs);
	}
	private void makeDialogAttrs(){
		m_dialogAttrs=new WindowManager.LayoutParams();
		m_dialogAttrs.horizontalMargin=0f;
		m_dialogAttrs.verticalMargin=0f;
		m_dialogAttrs.gravity=Gravity.START|Gravity.CENTER;
		m_dialogAttrs.flags=WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL|WindowManager.LayoutParams.FLAG_FULLSCREEN|WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
		m_dialogAttrs.softInputMode=WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN|WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN;
		getWindow().setAttributes(m_dialogAttrs);
		setContentView(m_layoutRes);
		m_container=findViewById(android.R.id.content);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		boolean superResult=super.dispatchTouchEvent(ev);
		if(ev.getAction()==MotionEvent.ACTION_DOWN){
			if(!superResult)
				hideKeyboard();
			getOwnerActivity().onUserInteraction();
		}
		return superResult;
	}

	private void hideKeyboard(){
		InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		IBinder token = m_container.getWindowToken();
		if (token != null)
			imm.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public View getBaseView(){
		return m_container;
	}
	
	private void setAllTextWatchers(View view, TextWatcher textWatcher){
		if(view instanceof EditText)
			((EditText) view).addTextChangedListener(textWatcher);
		else if(view instanceof ViewGroup){
			for(int i=0; i<((ViewGroup)view).getChildCount(); i++){
				setAllTextWatchers(((ViewGroup)view).getChildAt(i), textWatcher);
			}
		}
		
	}
	
	private class KeyboardTextChangedListener implements TextWatcher{

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			getOwnerActivity().onUserInteraction();
		}

		@Override
		public void afterTextChanged(Editable s) {
			
		}
	}
}
