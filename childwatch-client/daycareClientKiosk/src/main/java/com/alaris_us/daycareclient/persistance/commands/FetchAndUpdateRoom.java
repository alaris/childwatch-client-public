package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Room;

public class FetchAndUpdateRoom extends Command<Room> {

	Room	m_room;

	public FetchAndUpdateRoom(Room room) {

		this(room, null, null);
	}

	protected FetchAndUpdateRoom(Room room, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_room = room;
	}

	@Override
	public void executeSource() {

		m_dataSource.getRoomsDAO().read(m_room.getObjectId(), this);

	}

	@Override
	public void handleResponse(Room data) {
	}
}