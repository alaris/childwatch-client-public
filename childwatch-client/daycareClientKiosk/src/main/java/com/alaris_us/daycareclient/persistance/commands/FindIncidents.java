package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;

public class FindIncidents extends Command<List<Incident>> {
	List<Member> m_members;

	public FindIncidents(List<Member> members) {

		this(members, null, null);
	}

	protected FindIncidents(List<Member> members, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_members=members;
	}

	@Override
	public void executeSource() {

		m_dataSource.getIncidentsDAO().findIncidents(m_members, this);

	}

	@Override
	public void handleResponse(List<Incident> data) {
		if (!data.isEmpty())
			m_model.setIncidentsList(data);		
	}
}