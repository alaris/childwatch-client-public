package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Recurrence;
import com.alaris_us.daycaredata.to.Schedule;

public class FetchScheduleRecurrences extends Command<List<Recurrence>> {

	List<Schedule>	m_schedules;

	public FetchScheduleRecurrences(List<Schedule> schedule) {

		this(schedule, null, null);
	}

	protected FetchScheduleRecurrences(List<Schedule> schedule, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_schedules = schedule;
	}

	@Override
	public void executeSource() {

		List<Recurrence> m_recurrences = new ArrayList<Recurrence>();
		for (Schedule s : m_schedules) {
			m_recurrences.add(s.getPRecurrence());
		}
		m_dataSource.getRecurrencesDAO().fetchAllObjectsIfNeeded(m_recurrences, this);

	}

	@Override
	public void handleResponse(List<Recurrence> data) {
	}
}