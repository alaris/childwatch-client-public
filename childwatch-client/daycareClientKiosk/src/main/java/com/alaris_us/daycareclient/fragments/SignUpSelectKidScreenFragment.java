package com.alaris_us.daycareclient.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.alaris_us.daycareclient.models.SignUpSelectMemberScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher.ScrollDir;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient.widgets.SignUpSelectMemberItem;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class SignUpSelectKidScreenFragment extends ScreenSwitcherFragment {
	SignUpSelectMemberScreenViewModel	m_model;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.signupselectkidscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		HorizontalListView parentList = (HorizontalListView) m_layout.findViewById(R.id.signupselectkidscreen_tile_kidlist_container);
		m_model = new SignUpSelectMemberScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		UiBinder.bind(m_layout, R.id.signupselectkidscreen_tile_kidlist_container, "Adapter", m_model, "Members",
				BindingMode.TWO_WAY, new AdapterConverter(SignUpSelectMemberItem.class, true, true));
		UiBinder.bind(m_layout, R.id.view_navigation_next_container, "OnClickListener", m_model, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.view_navigation_next_container, "Alpha", m_model, "NextAlpha", BindingMode.ONE_WAY);
		parentList.setOnItemClickListener(m_model.new MemberClickListener());
		/*
		 * parentList.setChoiceMode(ListView.CHOICE_MODE_NONE);
		 * parentList.setDrawingCacheEnabled(true);
		 * parentList.setAlwaysDrawnWithCacheEnabled(true);
		 * parentList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
		 * parentList.setScrollingCacheEnabled(false);
		 * parentList.setAnimationCacheEnabled(false);
		 */
		// Navigation Button(s)
		m_layout.findViewById(R.id.view_navigation_back_container).setOnClickListener(new BackClickListener());
	}

	@Override
	public void initData() {
		setCanScroll(ScrollDir.BACKWARD, false);
		m_model.reset();
	}

	public class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}
}
