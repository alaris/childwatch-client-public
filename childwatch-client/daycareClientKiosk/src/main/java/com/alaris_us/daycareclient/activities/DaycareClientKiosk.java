package com.alaris_us.daycareclient.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alaris_us.daycareclient.app.App;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.exceptions.DaycareDataExceptionNoObject;
import com.alaris_us.daycaredata.to.Room;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import com.alaris_us.daycareclient.utils.AutoUpdateApk;

public class DaycareClientKiosk extends Activity {
	abstract class State {
		public abstract void enterState();

		public void exitState() {
			m_currentState = null;
		}

		public void exception(Exception e) {
			m_currentState = null;
		}
	}

	private State				m_currentState;
	private PersistenceLayer	m_persistenceLayer;
	// Authentication information
	private String				m_androidID;
	// Controls
	private TextView			m_textStatus, m_textDeviceID, m_textErrorDetail, m_versionText;
	private Button				m_buttonRegister;
	private ProgressBar			m_progressBar;
	private String				versionName;
	private int					versionCode;
	private App					m_app;
	//private AutoUpdateApk aua;															// Auto
																								// Update
																								// APK

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_initialization);
		// Get control references
		m_buttonRegister = (Button) findViewById(R.id.initialize_buttonRegister);
		m_textDeviceID = (TextView) findViewById(R.id.initialize_textDeviceID);
		m_textStatus = (TextView) findViewById(R.id.initialize_textStatus);
		m_textErrorDetail = (TextView) findViewById(R.id.initialize_textErrorDetail);
		m_progressBar = (ProgressBar) findViewById(R.id.initializeProgressBar);
		m_versionText = (TextView) findViewById(R.id.initialize_versionText);
		// Data configuration
		m_app = (App) getApplication();
		m_persistenceLayer = m_app.getPersistenceLayer();
		// Register button
		m_buttonRegister.setOnClickListener(new RegistrationOnClickListener());
		// Version Text
		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionName = pInfo.versionName;
			versionCode = pInfo.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		m_versionText.setText("Version: " + versionName + " Rev " + versionCode);
		boolean updatesEnabled = getResources().getBoolean(R.bool.APP_UPDATES_ENABLED);
		/*if (updatesEnabled) {
			aua = new AutoUpdateApk(getApplicationContext());
			aua.checkUpdatesManually();
		}*/

	}

	@Override
	protected void onResume() {
		super.onResume();
		initialize();
	}

	private void displayStatus(String status) {
		m_textStatus.setText(status);
		m_textErrorDetail.setText("");
	}

	private void displayStatus(String status, Exception e) {
		m_textStatus.setText(status);
		if (e != null)
			m_textErrorDetail.setText(e.getMessage());
	}

	private void initialize() {
		m_androidID = m_persistenceLayer.getAndroidId();
		m_textDeviceID.setText("Device ID: " + m_androidID);
		m_currentState = new SigninState();
		m_currentState.enterState();
	}

	private class SigninState extends State {
		@Override
		public void enterState() {
			displayStatus("Signing In");
			m_persistenceLayer.login(new InitilizationCommandListener());
		}

		@Override
		public void exitState() {
			m_currentState = new FetchFacilityState();
		}

		@Override
		public void exception(Exception e) {
			displayStatus("Could not login", e);
			// Allow registration if user not found
			if (e instanceof DaycareDataExceptionNoObject) {
				m_buttonRegister.setVisibility(View.VISIBLE);
			}
			m_currentState = null;
		}
	}

	private class RegistrationState extends State {
		@Override
		public void enterState() {
			displayStatus("Registering User");
			m_persistenceLayer.registerUser(new InitilizationCommandListener());
		}

		@Override
		public void exitState() {
			m_buttonRegister.setVisibility(View.GONE);
			m_currentState = new FetchFacilityState();
		}

		@Override
		public void exception(Exception e) {
			displayStatus("Could not register device", e);
			// Don't allow user to re-register
			m_buttonRegister.setVisibility(View.GONE);
			m_currentState = null;
		}
	}

	private class FetchFacilityState extends State {
		@Override
		public void enterState() {
			displayStatus("Fetching Facility Information");
			m_persistenceLayer.fetchFacility(new InitilizationCommandListener());
		}

		@Override
		public void exitState() {
			m_currentState = new FetchWorkoutAreasState();
		}

		@Override
		public void exception(Exception e) {
			// m_persistenceLayer.initializeYMCAData();
			displayStatus("Could not fetch facility information");
			m_currentState = null;
		}
	}

	private class FetchWorkoutAreasState extends State {
		@Override
		public void enterState() {
			displayStatus("Fetching Workout Areas Information");
			m_persistenceLayer.fetchAllWorkoutLocations(new InitilizationCommandListener());
		}

		@Override
		public void exitState() {
			m_currentState = new FetchCubbiesState();
		}

		@Override
		public void exception(Exception e) {
			displayStatus("Could not fetch workout areas information");
			m_currentState = null;
		}
	}

	private class FetchCubbiesState extends State {
		@Override
		public void enterState() {
			displayStatus("Fetching Cubby Information");
			m_persistenceLayer.fetchAllCubbies(new InitilizationCommandListener());
		}

		@Override
		public void exitState() {
			m_currentState = new FetchRoomsState();
		}

		@Override
		public void exception(Exception e) {
			displayStatus("Could not fetch cubby information");
			m_currentState = null;
		}
	}

	private class FetchRoomsState extends State {
		@Override
		public void enterState() {
			displayStatus("Fetching Rooms Information");
			m_persistenceLayer.fetchAllRooms(new InitilizationCommandListener());
		}

		@Override
		public void exitState() {
			List<Room> assignedRooms = new ArrayList<Room>();
			if (m_persistenceLayer.getPersistenceData().getAllRooms() != null){
				for (Room room : m_persistenceLayer.getPersistenceData().getAllRooms()) {
					assignedRooms.add(room);
					/*if (room.getPAssignedTablet() != null)
						if (room.getPAssignedTablet().getUserName().equals(m_persistenceLayer.getAndroidId())){
							assignedRooms.add(room);
						}*/
				}
				m_persistenceLayer.getPersistenceData().setAssingedRooms(assignedRooms);
				List<Double> roomAges = new ArrayList<Double>();
				for (Room room : assignedRooms){
					roomAges.add(room.getMinAge().doubleValue());
					roomAges.add(room.getMaxAge().doubleValue());
				}
				if (!roomAges.isEmpty()){
					Collections.sort(roomAges);
					m_persistenceLayer.getPersistenceData().setMinRoomAge(roomAges.get(0));
					m_persistenceLayer.getPersistenceData().setMaxRoomAge(roomAges.get(roomAges.size()-1));
				}
			}
			m_currentState = new FetchProgramsState();
		}

		@Override
		public void exception(Exception e) {
			displayStatus("Could not fetch room information");
			m_currentState = null;
		}
	}

	private class FetchProgramsState extends State {
		@Override
		public void enterState() {
			displayStatus("Fetching Programs Information");
			m_persistenceLayer.fetchAllPrograms(new InitilizationCommandListener());
		}

		@Override
		public void exitState() {
			m_currentState = new FetchCheckedInMembersState();
		}

		@Override
		public void exception(Exception e) {
			displayStatus("Could not fetch program information");
			m_currentState = null;
		}
	}

	private class FetchCheckedInMembersState extends State {
		@Override
		public void enterState() {
			displayStatus("Fetching Checked In Members");
			m_persistenceLayer.fetchCheckedInMembers(new InitilizationCommandListener());
		}

		@Override
		public void exitState() { m_currentState = new FetchClockedInStaffState(); }

		@Override
		public void exception(Exception e) {
			displayStatus("Could not fetch checked in member data.");
		}
	}

	private class FetchClockedInStaffState extends State {
		@Override
		public void enterState() {
			displayStatus("Fetching Clocked In Staff");
			m_persistenceLayer.fetchClockedInStaff(new InitilizationCommandListener());
		}

		@Override
		public void exitState() { m_currentState = new InitializeExternalDataSourceState(); }

		@Override
		public void exception(Exception e) {
			displayStatus("Could not fetch clocked in staff.");
			m_currentState = null;
		}
	}
	
	private class InitializeExternalDataSourceState extends State {
		@Override
		public void enterState() {
			displayStatus("Fetching External Data Information");
			m_persistenceLayer.buildExternalDataSource(m_persistenceLayer.getPersistenceData().getFacility());
			m_currentState = new InitializationCompleteState();
			m_currentState.enterState();
		}
	}

	
	private class InitializationCompleteState extends State {
		@Override
		public void enterState() {
			m_persistenceLayer.startServices();
			Intent intent = new Intent(DaycareClientKiosk.this, MainActivityScreen.class);
			startActivity(intent);
		}
	}

	private class InitilizationCommandListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
			m_progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		public void finished(Command<?> command) {
			m_progressBar.setVisibility(View.GONE);
			if (m_currentState != null)
				m_currentState.enterState();
		}

		@Override
		public void valueChanged(Command<?> command) {
			m_currentState.exitState();
		}

		@Override
		public void exception(Command<?> command, Exception e) {
			m_currentState.exception(e);
		}
	}

	private class RegistrationOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_currentState = new RegistrationState();
			m_currentState.enterState();
		}
	}
}
