package com.alaris_us.daycareclient.yombu;


public interface YombuRESTOperationComplete<T> {

    public void operationComplete(T data, YombuRESTException e);

}
