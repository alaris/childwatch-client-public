package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;

public class SaveConsentRecord extends Command<Record> {

	private final Facility m_facility;
	private final Member m_parent;

	public SaveConsentRecord(Member parent, Facility facility) {
		this(parent, facility, null, null);
	};

	public SaveConsentRecord(Member parent, Facility facility, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_facility = facility;
		m_parent = parent;
	}

	@Override
	public void executeSource() {
		m_dataSource.getRecordsDAO().saveConsentRecord(m_parent, m_facility, this);
	}

	@Override
	public void handleResponse(Record record) {
		
	}
}