package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Member;

import java.util.List;

public class UpdateAllMembers extends Command<List<Member>> {

	private final List<Member> m_members;

	public UpdateAllMembers(List<Member> members) {
		this(members, null, null);
	};

	public UpdateAllMembers(List<Member> members, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_members = members;
	}

	@Override
	public void executeSource() {
		m_dataSource.getMembersDAO().updateAll(m_members, this);
	}

	@Override
	public void handleResponse(List<Member> members) {
	}

}