package com.alaris_us.daycareclient.yombu;

import org.json.JSONObject;

public class YombuDataObject{
    private JSONObject mJSONObject;

    public YombuDataObject(JSONObject json){
        mJSONObject = json;
    }

    public YombuDataObject(YombuDataObject obj){
        this(obj.getData());
    }

    protected JSONObject getData(){
        return mJSONObject;
    }
}
