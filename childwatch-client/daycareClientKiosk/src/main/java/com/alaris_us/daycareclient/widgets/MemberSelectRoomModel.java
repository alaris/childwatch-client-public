package com.alaris_us.daycareclient.widgets;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycareclient.utils.ImageFunctions;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Room;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

public class MemberSelectRoomModel {

	private TrackableField<Member> m_member;
	private TrackableInt m_width;
	private Integer m_color;
	private Bitmap mColorizedImage;
	private List<Room> mRooms;
	private List<Room> mQualifyingRooms;

	public MemberSelectRoomModel() {
		this(null, null, null, null);
	}

	public MemberSelectRoomModel(Member member, Integer width, Integer color, List<Room> rooms) {
		m_member = new TrackableField<Member>(member);
		m_width = new TrackableInt(width);
		m_color = color;
		mRooms = rooms;
		mQualifyingRooms = new ArrayList<Room>();
		setInitialRoom();
	}

	public Member getMember() {
		return m_member.get();
	}

	public void setMember(Member member) {
		m_member.set(member);
	}

	public String getMemberName() {
		return m_member.get().getFirstName() + " " + m_member.get().getLastName();
	}

	public Bitmap getMemberImage() {
		if (null == mColorizedImage) {
			mColorizedImage = ImageFunctions.createDirectCopy(m_member.get().getImage());
			ImageFunctions.colorizeBitmap(mColorizedImage, m_color, ImageFunctions.colorizeMix,
					ImageFunctions.colorizeSaturation);
		}
		return mColorizedImage;
	}

	public int getBackgroundColor() {
		return m_color;
	}

	public int getWidth() {
		return m_width.get();
	}

	public void setWidth(int width) {
		m_width.set(width);
	}

	public List<String> getSelectedRoomNameList() {
		List<String> roomNameList = new ArrayList<String>();
		mQualifyingRooms.clear();
		for (Room r : mRooms)
			if (DaycareHelpers.getIsAgeBetween(getMember().getBirthDate(), r.getMinAge().doubleValue(), r.getMaxAge().doubleValue())){
				roomNameList.add(r.getName());
				mQualifyingRooms.add(r);
			}
		
		return roomNameList;
	}

	public OnItemSelectedListener getSelectRoomNameListOnItemSelectedListener() {
		return new SelectRoomNameListOnItemSelectedListener();
	}

	public class SelectRoomNameListOnItemSelectedListener implements OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			getMember().setPRoom(mQualifyingRooms.get(position));
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		}
	}

	public int getDefaultSelection() {
		Member child = getMember();
		Room roomForChild = null;
		int position = 0;

		if (child.getPRoom() != null) {
			for (Room room : mQualifyingRooms)
				if (room.equals(child.getPRoom())) {
					position = mQualifyingRooms.indexOf(room);
					break;
				}
			return position;
		} else if (mQualifyingRooms.size() == 1) {
			roomForChild = mQualifyingRooms.get(0);
			child.setPRoom(mQualifyingRooms.get(0));
		} else {
			for (Room r : mQualifyingRooms)
				if (r.getName().contains("STAY")) {
					roomForChild = r;
					child.setPRoom(roomForChild);
				}
		}

		for (Room room : mQualifyingRooms)
			if (room.equals(roomForChild)) {
				position = mQualifyingRooms.indexOf(room);
				break;
			}

		return position;
	}

	public boolean getEnabled() {
		if (mQualifyingRooms.size() == 1)
			return false;
		else
			return true;
	}
	
	public int getVisibility() {
		if (mQualifyingRooms.size() == 1)
			return View.GONE;
		else
			return View.VISIBLE;
	}
	
	public void setInitialRoom() {
		mQualifyingRooms.clear();
		if (m_member.get().getPRoom() == null) {
			for (Room r : mRooms)
				if (DaycareHelpers.getIsAgeBetween(getMember().getBirthDate(), r.getMinAge().doubleValue(),
						r.getMaxAge().doubleValue())) {
					mQualifyingRooms.add(r);
					m_member.get().setPRoom(mQualifyingRooms.get(0));
				}
		}
	}

}
