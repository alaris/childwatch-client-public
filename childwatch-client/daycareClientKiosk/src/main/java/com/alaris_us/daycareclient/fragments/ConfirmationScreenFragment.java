package com.alaris_us.daycareclient.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.alaris_us.daycareclient.models.ConfirmationScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.ConfirmationItem;
import com.alaris_us.daycareclient.widgets.ConfirmationItemVert;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

public class ConfirmationScreenFragment extends ScreenSwitcherFragment {
	private ConfirmationScreenViewModel m_model;
	private View summaryIn, summaryOut;
	private int incidentDialogWidth;
	private int incidentDialogHeight;
	private final int incidentDialogWidthRes = R.dimen.view_incidentdialog_width;
	private final int incidentDialogHeightRes = R.dimen.view_incidentdialog_height;
	private View m_incidentDialogView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		m_layout = inflater.inflate(R.layout.confirmationscreen_layout,
				container, false);
		summaryIn = inflater.inflate(
				R.layout.confirmationscreen_tile_summary_in, container, false);
		summaryOut = inflater.inflate(
				R.layout.confirmationscreen_tile_summary_out, container, false);
		m_incidentDialogView = inflater.inflate(
				R.layout.view_incidentdialog_layout, container, false);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		m_model = new ConfirmationScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this, summaryIn, summaryOut);
		m_layout.findViewById(R.id.view_navigation_next_container).setId(
				R.id.confirmationscreen_nav_finish);
		m_layout.findViewById(R.id.view_navigation_back_container).setId(
				R.id.confirmationscreen_nav_back);
		incidentDialogWidth = (int) getResources().getDimension(
				incidentDialogWidthRes);
		incidentDialogHeight = (int) getResources().getDimension(
				incidentDialogHeightRes);
		UiBinder.bind(summaryIn,
				R.id.confirmationscreen_tile_summary_children_in, "Adapter",
				m_model, "Children", BindingMode.TWO_WAY, new AdapterConverter(
						ConfirmationItemVert.class, true, true));
		UiBinder.bind(summaryOut,
				R.id.confirmationscreen_tile_summary_children_out, "Adapter",
				m_model, "Children", BindingMode.TWO_WAY, new AdapterConverter(
						ConfirmationItem.class, true, true));
		UiBinder.bind(summaryIn, R.id.confirmationscreen_tile_summary_areas,
				"Adapter", m_model, "Areas", BindingMode.TWO_WAY,
				new AdapterConverter(ConfirmationItemVert.class, true, true));
		UiBinder.bind(m_layout, R.id.confirmationscreen_layout_tileholder,
				"View", m_model, "Tile", BindingMode.ONE_WAY);
		UiBinder.bind(summaryIn,
				R.id.confirmationscreen_tile_message_children_textbox,
				"TextRes", m_model, "InOrOutMessage", BindingMode.ONE_WAY);
		UiBinder.bind(summaryOut,
				R.id.confirmationscreen_tile_message_children_textbox,
				"TextRes", m_model, "InOrOutMessage", BindingMode.ONE_WAY);
		UiBinder.bind(summaryIn,
				R.id.confirmationscreen_tile_message_children_textbox,
				"TextSizeRes", m_model, "InOrOutMessageTextSize",
				BindingMode.ONE_WAY);
		UiBinder.bind(summaryOut,
				R.id.confirmationscreen_tile_message_children_textbox,
				"TextSizeRes", m_model, "InOrOutMessageTextSize",
				BindingMode.ONE_WAY);
		FontTextView confirm = (FontTextView) m_layout
				.findViewById(R.id.view_navigation_next_textbox);
		confirm.setTextRes(R.string.confirmationscreen_tile_confirm_text);
		m_layout.findViewById(R.id.confirmationscreen_nav_finish)
				.setOnClickListener(m_model.new ConfirmClickListener());
		UiBinder.bind(m_incidentDialogView, R.id.view_incidentdialog_ok_button,
				"OnClickListener", m_model, "IncidentDialogOkClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "IncidentDialog"), m_model,
				"IncidentDialog", BindingMode.ONE_WAY_TO_SOURCE);
		// Navigation Button(s)
		m_layout.findViewById(R.id.confirmationscreen_nav_back)
				.setOnClickListener(new BackClickListener());
	}

	@Override
	public void onPauseFragment() {
		super.onPauseFragment();
		if (m_model != null)
			m_model.disconnect();
	}

	@Override
	public void onResumeFragment() {
		m_model = (null == m_model) ? new ConfirmationScreenViewModel(
				((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this, summaryIn, summaryOut) : m_model;
		m_model.reset();
		super.onResumeFragment();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		m_model.disconnect();
		m_model = null;
	}

	public View getSummaryIn() {
		return summaryIn;
	}

	public View getSummaryOut() {
		return summaryOut;
	}

	public AlertDialog getIncidentDialog() {
		FrameLayout incidentDialogContainer = new FrameLayout(getActivity());
		incidentDialogContainer.addView(m_incidentDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(incidentDialogContainer);
		AlertDialog incidentDialog = builder.create();
		incidentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		incidentDialog.setCancelable(false);
		incidentDialog.show();
		incidentDialog.dismiss();
		ViewGroup.LayoutParams lp = m_incidentDialogView.getLayoutParams();
		lp.width = incidentDialogWidth;
		lp.height = incidentDialogHeight;
		m_incidentDialogView.setLayoutParams(lp);
		((View) m_incidentDialogView.getParent().getParent().getParent())
				.setBackgroundResource(R.color.invis);
		return incidentDialog;
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}

}
