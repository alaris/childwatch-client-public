package com.alaris_us.daycareclient.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.alaris_us.daycareclient.models.MedicalScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient.widgets.MedicalScreenItem;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class MedicalScreenFragment extends ScreenSwitcherFragment {
	MedicalScreenViewModel	m_model;
	private AlertDialog		m_registrationDialog, m_memberAlreadyExistsDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.medicalscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		HorizontalListView medicalList = (HorizontalListView) m_layout.findViewById(R.id.medicalscreen_tile_medicallist_container);
		m_model = new MedicalScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this);
		UiBinder.bind(m_layout, R.id.medicalscreen_tile_medicallist_container, "Adapter", m_model, "Members",
				BindingMode.TWO_WAY, new AdapterConverter(MedicalScreenItem.class, true, true));
		medicalList.setOnItemClickListener(m_model.new MemberClickListener());
		m_layout.findViewById(R.id.view_navigation_next_container).setOnClickListener(m_model.new NextClickListener());
		FontTextView finish = (FontTextView) m_layout.findViewById(R.id.view_navigation_next_textbox);
		finish.setTextRes(R.string.medicalscreen_tile_finish);
		medicalList.setOnTouchListener(new ItemTouchListener());
		// Navigation Button(s)
		m_layout.findViewById(R.id.view_navigation_back_container).setOnClickListener(new BackClickListener());
	}

	@Override
	public void initData() {
	}

	@Override
	public void onPauseFragment() {
		m_model.dismissAllDialogs();
		super.onPauseFragment();
	}

	private class ItemTouchListener implements OnTouchListener {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN)
				m_model.dismissAllDialogs();
			else if (MotionEvent.ACTION_UP == event.getAction())
				return v.performClick();
			return false;
		}
	}

	public void showRegistrationMemberDialog() {
		FrameLayout registrationDialogContainer = new FrameLayout(getActivity());
		View invalidMemberDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_registrationdialog_layout,
				registrationDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(registrationDialogContainer);
		m_registrationDialog = builder.create();
		m_registrationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_registrationDialog.setCancelable(false);
		m_registrationDialog.show();
		ViewGroup.LayoutParams lp = invalidMemberDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_registrationdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_registrationdialog_height);
		invalidMemberDialogView.setLayoutParams(lp);
		((View) invalidMemberDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
	}

	public void dismissRegistrationDialog() {
		if (m_registrationDialog != null && m_registrationDialog.isShowing())
			m_registrationDialog.dismiss();
	}
	
	public void showMemberAlreadyExistsDialog() {
		FrameLayout memberAlreadyExistsDialogContainer = new FrameLayout(getActivity());
		View memberAlreadyExistsDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_memberalreadyexistsdialog_layout,
				memberAlreadyExistsDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(memberAlreadyExistsDialogContainer);
		m_memberAlreadyExistsDialog = builder.create();
		m_memberAlreadyExistsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_memberAlreadyExistsDialog.setCancelable(false);
		m_memberAlreadyExistsDialog.show();
		ViewGroup.LayoutParams lp = memberAlreadyExistsDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_memberalreadyexistsdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_memberalreadyexistsdialog_height);
		memberAlreadyExistsDialogView.setLayoutParams(lp);
		((View) memberAlreadyExistsDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		m_memberAlreadyExistsDialog.findViewById(R.id.view_memberalreadyexistsdialog_button)
				.setOnClickListener(new MemberAlreadyExistsOkClickListener());
	}

	public void dismissMemberAlreadyExistsDialog() {
		if (m_memberAlreadyExistsDialog != null && m_memberAlreadyExistsDialog.isShowing())
			m_memberAlreadyExistsDialog.dismiss();
	}

	public class MemberAlreadyExistsOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissRegistrationDialog();
			dismissMemberAlreadyExistsDialog();
			getFragmentActivity().gotoFirstScreen();
		}
	}

	public class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_model.createChildrenToSave();
			getFragmentActivity().prevScreen();
		}
	}
}
