package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;

public class CheckInChildLog extends Command<Record> {

	private Facility m_facility;
	private Member m_child, m_parent;
	private SignInType m_signInType;

	public CheckInChildLog(Facility facility, Member child, Member parent, SignInType signInType) {
		this(facility, child, parent, signInType, null, null);
	};

	public CheckInChildLog(Facility facility, Member child, Member parent, SignInType signInType, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_facility = facility;
		m_child=child;
		m_parent=parent;
		m_signInType=signInType;
	}

	@Override
	public void executeSource() {
		m_dataSource.getRecordsDAO().saveCheckInEvent(m_child, m_facility, m_parent, m_signInType, null, null, this);
	}

	@Override
	public void handleResponse(Record record) {
		//TODO dont need to do anything?
	}
	
}