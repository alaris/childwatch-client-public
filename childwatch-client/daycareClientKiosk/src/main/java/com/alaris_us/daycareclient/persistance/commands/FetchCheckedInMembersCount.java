package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;

public class FetchCheckedInMembersCount extends Command<Integer> {

	private Facility m_facility;

	public FetchCheckedInMembersCount(Facility facility) {

		this(facility, null, null);
	}

	protected FetchCheckedInMembersCount(Facility facility, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_facility = facility;
	}

	@Override
	public void executeSource() {
		m_dataSource.getFacilitiesDAO().countCheckedInMembers(m_facility, this);
	}

	@Override
	public void handleResponse(Integer data) {
		m_model.setCheckedInMembersCount(data);
	}
}