package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.WorkoutArea;

public class FetchAllWorkoutAreas extends Command<List<WorkoutArea>> {

	Facility m_facility;

	public FetchAllWorkoutAreas(Facility facility) {

		this(facility, null, null);
	}

	protected FetchAllWorkoutAreas(Facility facility, DaycareData dataSource, PersistenceData model) {

		super(dataSource, model);
		m_facility = facility;
	}

	@Override
	public void executeSource() {

		m_dataSource.getFacilitiesDAO().listSelectedWorkoutAreas(m_facility, this);

	}

	@Override
	public void handleResponse(List<WorkoutArea> data) {
		m_model.setAllWorkoutAreas(data);

	}
}