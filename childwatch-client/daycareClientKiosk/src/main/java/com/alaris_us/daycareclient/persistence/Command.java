package com.alaris_us.daycareclient.persistence;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;

public abstract class Command<T>
	implements DaycareOperationComplete<T> {

	public interface CommandStatusListener {

		public void starting(
				Command<?> command);

		public void finished(
				Command<?> command);

		public void valueChanged(
				Command<?> command);

		public void exception(
				Command<?> command,
				Exception e);

	}

	protected DaycareData					m_dataSource;

	protected PersistenceData				m_model;

	protected List<CommandStatusListener>	m_listeners	= new ArrayList<CommandStatusListener>();

	protected T								m_data;

	protected Command() {

		this(null, null);
	}

	protected Command(DaycareData dataSource, PersistenceData model) {

		m_model = model;
		m_dataSource = dataSource;
	}

	public void setDataSource(
			DaycareData dataSource) {

		m_dataSource = dataSource;

	}

	public void setPersistentModel(
			PersistenceData model) {

		m_model = model;

	}

	public T getResult() {

		return m_data;
	}

	public void addListener(
			CommandStatusListener lis) {

		m_listeners.add(lis);

	}

	public void removeListener(
			CommandStatusListener lis) {

		m_listeners.remove(lis);

	}

	public final void execute() {

		statusUpdateStart();
		executeSource();
	}

	public abstract void executeSource();

	public abstract void handleResponse(
			T data);

	@Override
	public void operationComplete(
			T data,
			DaycareDataException e) {

		reportException(e);

		if (e == null) {
			m_data = data;
			handleResponse(data);
			statusChanged();

		}
		statusUpdateComplete();
	}

	public void reportException(
			DaycareDataException e) {

		if (e == null) return;
		
		for (CommandStatusListener cl : m_listeners) {
			cl.exception(this, e);
		}
	}

	protected void statusChanged() {

		for (CommandStatusListener cl : m_listeners) {
			if (cl != null) {
			cl.valueChanged(this);
			}
		}

	}

	protected void statusUpdateStart() {

		for (CommandStatusListener cl : m_listeners) {
			if (cl != null) {
			cl.starting(this);
			}
		}

	}

	protected void statusUpdateComplete() {

		for (CommandStatusListener cl : m_listeners) {
			if (cl != null) {
			cl.finished(this);
			}
		}

	}

}
