package com.alaris_us.daycareclient.fragments;

import com.alaris_us.daycareclient.models.SelectUserScreenViewModel;
import com.alaris_us.daycareclient.persistence.PersistenceData.InOrOut;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher.ScrollDir;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontEditText;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient.widgets.MemberCheckBox;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

public class SelectUserScreenFragment extends ScreenSwitcherFragment {
	SelectUserScreenViewModel	mModel;
	private AlertDialog			m_syncingChildrenDialog, m_noKidsForCheckDirectionDialog, m_invalidMemberSelectedDialog, 
		m_staffOverrideEntryDialog, m_successfulOverrideDialog, m_noChildrenOfAgeDialog, m_noProgramsAvailableDialog, mNoAdmissionsDialog;
	private String dir, otherDir;
	private HorizontalListView mUserList;
	private FontTextView mSingleUserName;
	private FontTextView mSelectUserTitle;
	private View m_staffOverrideEntryDialogView;
	private FontEditText overridePIN;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.selectuserscreen_layout, container, false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mModel = new SelectUserScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this);
		mUserList = (HorizontalListView) m_layout.findViewById(R.id.selectuserscreen_tile_userlist);
		mSingleUserName = (FontTextView) m_layout.findViewById(R.id.selectuserscreen_single_user_name);
		mSelectUserTitle = (FontTextView) m_layout.findViewById(R.id.selectuserscreen_tile_selectuser_textbox_text);
		m_layout.findViewById(R.id.view_navigation_next_container).setId(R.id.selectuserscreen_nav_next);
		m_layout.findViewById(R.id.view_navigation_back_container).setId(R.id.selectuserscreen_nav_back);
		UiBinder.bind(m_layout, R.id.selectuserscreen_tile_userlist, "Adapter", mModel, "Members",
				BindingMode.TWO_WAY, new AdapterConverter(MemberCheckBox.class, true, true));
		UiBinder.bind(m_layout, R.id.selectuserscreen_nav_next, "OnClickListener", mModel, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.selectuserscreen_nav_next, "Alpha", mModel, "NextAlpha", BindingMode.ONE_WAY);
		mUserList.setOnItemClickListener(mModel.new MemberClickListener());
		UiBinder.bind(m_layout, R.id.selectuserscreen_tile_selectuser_textbox_text, "TextRes", mModel, "TitleTextRes", BindingMode.ONE_WAY);

		// Navigation Button(s)
		m_layout.findViewById(R.id.selectuserscreen_nav_back).setOnClickListener(new BackClickListener());
	}

	@Override
	public void initData() {
		setCanScroll(ScrollDir.BACKWARD, false);
		mModel.reset();
	}

	// public void showNoChildrenDialog() {
	// FrameLayout noChildrendialogContainer = new FrameLayout(getActivity());
	// View noChildrenDialogView =
	// m_activity.getLayoutInflater().inflate(R.layout.view_nochildrendialog_layout,
	// noChildrendialogContainer);
	// AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	// builder.setView(noChildrendialogContainer);
	// m_noChildrenDialog = builder.create();
	// m_noChildrenDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	// m_noChildrenDialog.setCancelable(false);
	// m_noChildrenDialog.show();
	// ViewGroup.LayoutParams lp = noChildrenDialogView.getLayoutParams();
	// lp.width = (int)
	// getResources().getDimension(R.dimen.view_nochildrendialog_width);
	// lp.height = (int)
	// getResources().getDimension(R.dimen.view_nochildrendialog_height);
	// noChildrenDialogView.setLayoutParams(lp);
	// ((View)
	// noChildrenDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
	// noChildrenDialogView.findViewById(R.id.view_nochildrendialog_button).setOnClickListener(
	// m_model.new NoChildrenDialogClickListener());
	// }
	//
	// public void dismissNoChildrenDialog() {
	// if (m_noChildrenDialog != null && m_noChildrenDialog.isShowing())
	// m_noChildrenDialog.dismiss();
	// }
	public void showSyncingChildrenDialog() {
		FrameLayout syncingChidrenDialogContainer = new FrameLayout(getActivity());
		View invalidMemberDialogView = m_activity.getLayoutInflater().inflate(
				R.layout.view_syncingchildrendialog_layout, syncingChidrenDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(syncingChidrenDialogContainer);
		m_syncingChildrenDialog = builder.create();
		m_syncingChildrenDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_syncingChildrenDialog.setCancelable(false);
		m_syncingChildrenDialog.show();
		ViewGroup.LayoutParams lp = invalidMemberDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_syncingchildrendialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_syncingchildrendialog_height);
		invalidMemberDialogView.setLayoutParams(lp);
		((View) invalidMemberDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
	}

	public void dismissSyncingChildrenDialog() {
		if (m_syncingChildrenDialog != null && m_syncingChildrenDialog.isShowing())
			m_syncingChildrenDialog.dismiss();
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}

	@Override
	public void onDestroyView() {
		mModel.disconnect();
		mModel = null;
		super.onDestroyView();
	}
	
	public void showNoKidsForCheckDirectionDialog() { //TODO
		FrameLayout noKidsForCheckDirectionDialogContainer = new FrameLayout(getActivity());
		View noKidsForCheckDirectionDialogView = m_activity.getLayoutInflater().inflate(
				R.layout.view_nokidsforcheckdirectiondialog_layout,
				noKidsForCheckDirectionDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noKidsForCheckDirectionDialogContainer);
		m_noKidsForCheckDirectionDialog = builder.create();
		m_noKidsForCheckDirectionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_noKidsForCheckDirectionDialog.setCancelable(false);
		m_noKidsForCheckDirectionDialog.show();
		ViewGroup.LayoutParams lp = noKidsForCheckDirectionDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nokidsforcheckdirectiondialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nokidsforcheckdirectiondialog_height);
		noKidsForCheckDirectionDialogView.setLayoutParams(lp);
		((View) noKidsForCheckDirectionDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		dir = "In"; //((PersistanceLayerHost) getActivity()).getPersistanceLayer().getPersistenceData().getInOrOut().toString();
		otherDir = "Out";
		/*if (dir.equals("NOKIDSIN")){
			dir = "In";
			otherDir = "Out";
		}
		else {
			dir = "Out";
			otherDir = "In";
		}*/
		String title = getResources().getString(R.string.view_nokidsforcheckdirectiondialog_title_text).replace("[DIR]", dir);
		String msg = getResources().getString(R.string.view_nokidsforcheckdirectiondialog_message_text).replace("[DIR]", otherDir);
		((TextView) noKidsForCheckDirectionDialogView.findViewById(R.id.view_nokidsforcheckdirectiondialog_title)).setText(title);
		((TextView) noKidsForCheckDirectionDialogView.findViewById(R.id.view_nokidsforcheckdirectiondialog_message)).setText(msg);
		//noKidsForCheckDirectionDialogView.findViewById(R.id.view_nokidsforcheckdirectiondialog_yes_button).setOnClickListener(
				//new NoKidsForCheckDirectionYesClickListener());
		noKidsForCheckDirectionDialogView.findViewById(R.id.view_nokidsforcheckdirectiondialog_home_button).setOnClickListener(
				new NoKidsForCheckDirectionHomeClickListener());
	}
	
	public void dismissNoKidsForCheckDirectionDialog() {
		if (m_noKidsForCheckDirectionDialog != null && m_noKidsForCheckDirectionDialog.isShowing())
			m_noKidsForCheckDirectionDialog.dismiss();
	}
	
	public class NoKidsForCheckDirectionYesClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_noKidsForCheckDirectionDialog.dismiss();
			if(otherDir.equals("In")){
				((PersistanceLayerHost) getActivity()).getPersistanceLayer().getPersistenceData().setInOrOut(InOrOut.IN);
				getFragmentActivity().nextScreen(InOrOut.IN);
			}
			else{
				((PersistanceLayerHost) getActivity()).getPersistanceLayer().getPersistenceData().setInOrOut(InOrOut.OUT);
				getFragmentActivity().nextScreen(InOrOut.OUT);
			}
		}
	}
	
	public class NoKidsForCheckDirectionHomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_noKidsForCheckDirectionDialog.dismiss();
			getFragmentActivity().gotoFirstScreen();
		}
	}
	
	public void showInvalidMemberSelectedDialog() {
		FrameLayout invalidMemberSelectedDialogContainer = new FrameLayout(getActivity());
		View invalidMemberDialogView = m_activity.getLayoutInflater().inflate(
				R.layout.view_invalidmemberselecteddialog_layout, invalidMemberSelectedDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(invalidMemberSelectedDialogContainer);
		m_invalidMemberSelectedDialog = builder.create();
		m_invalidMemberSelectedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_invalidMemberSelectedDialog.setCancelable(false);
		m_invalidMemberSelectedDialog.show();
		ViewGroup.LayoutParams lp = invalidMemberDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_invalidmemberselecteddialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_invalidmemberselecteddialog_height);
		invalidMemberDialogView.setLayoutParams(lp);
		((View) invalidMemberDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		
		invalidMemberDialogView.findViewById(R.id.view_invalidmemberselecteddialog_override_button).setOnClickListener(
				new InvalidMemberSelectedOverrideClickListener());
		invalidMemberDialogView.findViewById(R.id.view_invalidmemberselecteddialog_home_button).setOnClickListener(
				new InvalidMemberSelectedHomeClickListener());
		invalidMemberDialogView.findViewById(R.id.view_invalidmemberselecteddialog_reselect_button).setOnClickListener(
				new InvalidMemberSelectedReselectClickListener());
	}

	public void dismissInvalidMemberSelectedDialog() {
		if (m_invalidMemberSelectedDialog != null && m_invalidMemberSelectedDialog.isShowing())
			m_invalidMemberSelectedDialog.dismiss();
	}
	
	public class InvalidMemberSelectedOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissInvalidMemberSelectedDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public class InvalidMemberSelectedHomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_invalidMemberSelectedDialog.dismiss();
			getFragmentActivity().gotoFirstScreen();
		}
	}
	
	public class InvalidMemberSelectedReselectClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_invalidMemberSelectedDialog.dismiss();
		}
	}
	
	private void showStaffOverrideEntryDialog() {
		FrameLayout staffOverrideEntryDialogContainer = new FrameLayout(getActivity());
		m_staffOverrideEntryDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_staffoverrideentrydialog_layout, staffOverrideEntryDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(staffOverrideEntryDialogContainer);
		m_staffOverrideEntryDialog = builder.create();
		m_staffOverrideEntryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_staffOverrideEntryDialog.setCancelable(false);
		m_staffOverrideEntryDialog.show();
		ViewGroup.LayoutParams lp = m_staffOverrideEntryDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_height);
		m_staffOverrideEntryDialogView.setLayoutParams(lp);
		((View) m_staffOverrideEntryDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		m_staffOverrideEntryDialogView.findViewById(R.id.view_staffoverrideentrydialog_enter_button).setOnClickListener(
				new StaffOverrideEntryDialogEnterClickListener());
		m_staffOverrideEntryDialogView.findViewById(R.id.view_staffoverrideentrydialog_cancel_button).setOnClickListener(
				new StaffOverrideEntryDialogCancelClickListener());
	}

	private void dismissStaffOverrideEntryDialog() {
		if (m_staffOverrideEntryDialog != null && m_staffOverrideEntryDialog.isShowing())
			m_staffOverrideEntryDialog.dismiss();
	}
	
	private String getOverridePIN() {
		overridePIN = (FontEditText) m_staffOverrideEntryDialogView.findViewById(R.id.view_staffoverrideentrydialog_pin_edittext);
		return overridePIN.getText().toString();
	}
	
	private class StaffOverrideEntryDialogEnterClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (mModel.isValidOverridePIN(getOverridePIN())) {
				dismissStaffOverrideEntryDialog();
				showSuccessfulOverrideDialog();
			} else {
				overridePIN.setText("");
				overridePIN.setHint("Invalid PIN, Please Retry");
			}
		}
	}
	
	private class StaffOverrideEntryDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissStaffOverrideEntryDialog();
			showInvalidMemberSelectedDialog();
		}
	}

	private void showSuccessfulOverrideDialog() {
		FrameLayout successfulOverrideDialogContainer = new FrameLayout(getActivity());
		View successfulOverrideDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_successfuloverridedialog_layout,
				successfulOverrideDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(successfulOverrideDialogContainer);
		m_successfulOverrideDialog = builder.create();
		m_successfulOverrideDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_successfulOverrideDialog.setCancelable(false);
		m_successfulOverrideDialog.show();
		ViewGroup.LayoutParams lp = successfulOverrideDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_height);
		successfulOverrideDialogView.setLayoutParams(lp);
		((View) successfulOverrideDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		successfulOverrideDialogView.findViewById(R.id.view_successfuloverridedialog_ok_button).setOnClickListener(
				new SuccessfulOverrideDialogOkClickListener());
	}
	
	private void dismissSuccessfulOverrideDialog() {
		if (m_successfulOverrideDialog != null && m_successfulOverrideDialog.isShowing())
			m_successfulOverrideDialog.dismiss();
	}
	
	private class SuccessfulOverrideDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_successfulOverrideDialog != null && m_successfulOverrideDialog.isShowing())
				dismissSuccessfulOverrideDialog();
			mModel.overrideSuccess();
		}
	}
	
	public HorizontalListView getUserList() {
		return mUserList;
	}

	public FontTextView getSingleUserName() {
		return mSingleUserName;
	}

	public FontTextView getSelectUserTitle() {
		return mSelectUserTitle;
	}
	
	public void showNoChildrenOfAgeDialog() {
		FrameLayout noChildrendialogContainer = new FrameLayout(getActivity());
		View noChildrenDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_nochildrenofagedialog_layout,
				noChildrendialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noChildrendialogContainer);
		m_noChildrenOfAgeDialog = builder.create();
		m_noChildrenOfAgeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_noChildrenOfAgeDialog.setCancelable(false);
		m_noChildrenOfAgeDialog.show();
		ViewGroup.LayoutParams lp = noChildrenDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_nochildrenofagedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_nochildrenofagedialog_height);
		noChildrenDialogView.setLayoutParams(lp);
		((View) noChildrenDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		noChildrenDialogView.findViewById(R.id.view_nochildrenofagedialog_button).setOnClickListener(
				new NoChildrenOfAgeDialogClickListener());
	}

	public void dismissNoChildrenOfAgeDialog() {
		if (m_noChildrenOfAgeDialog != null && m_noChildrenOfAgeDialog.isShowing())
			m_noChildrenOfAgeDialog.dismiss();
	}
	
	public class NoChildrenOfAgeDialogClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoChildrenOfAgeDialog();
			getFragmentActivity().gotoFirstScreen();
		}
	}
	
	public void showNoProgramsAvailableDialog() {
		FrameLayout noProgramsAvailableDialogContainer = new FrameLayout(getActivity());
		View noProgramsAvailableDialogView = m_activity.getLayoutInflater().inflate(R.layout.view_noprogramsavailabledialog_layout,
				noProgramsAvailableDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noProgramsAvailableDialogContainer);
		m_noProgramsAvailableDialog = builder.create();
		m_noProgramsAvailableDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		m_noProgramsAvailableDialog.setCancelable(false);
		m_noProgramsAvailableDialog.show();
		ViewGroup.LayoutParams lp = noProgramsAvailableDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_noprogramsavailabledialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_noprogramsavailabledialog_height);
		noProgramsAvailableDialogView.setLayoutParams(lp);
		((View) noProgramsAvailableDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);
		noProgramsAvailableDialogView.findViewById(R.id.view_noprogramsavailabledialog_button).setOnClickListener(
				new NoProgramsAvailableDialogClickListener());
	}

	public void dismissNoProgramsAvailableDialog() {
		if (m_noProgramsAvailableDialog != null && m_noProgramsAvailableDialog.isShowing())
			m_noProgramsAvailableDialog.dismiss();
	}
	
	public class NoProgramsAvailableDialogClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoProgramsAvailableDialog();
			getFragmentActivity().gotoFirstScreen();
		}
	}

	public void showNoAdmissionsDialog() {
		FrameLayout noAdmissionsDialogContainer = new FrameLayout(getActivity());
		View noAdmissionsDialogView = m_activity.getLayoutInflater().inflate(
				R.layout.view_noadmissions_layout, noAdmissionsDialogContainer);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(noAdmissionsDialogContainer);
		mNoAdmissionsDialog = builder.create();
		mNoAdmissionsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mNoAdmissionsDialog.setCancelable(false);
		mNoAdmissionsDialog.show();
		ViewGroup.LayoutParams lp = noAdmissionsDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_noadmissionsdialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_noadmissionsdialog_height);
		noAdmissionsDialogView.setLayoutParams(lp);
		((View) noAdmissionsDialogView.getParent().getParent()).setBackgroundResource(R.color.invis);

		noAdmissionsDialogView.findViewById(R.id.view_noadmissionsdialog_override_button).setOnClickListener(
				new NoAdmissionsOverrideClickListener());
		noAdmissionsDialogView.findViewById(R.id.view_noadmissionsdialog_ok_button).setOnClickListener(
				new NoAdmissionsOkClickListener());
	}

	public void dismissNoAdmissionsDialog() {
		if (mNoAdmissionsDialog != null && mNoAdmissionsDialog.isShowing())
			mNoAdmissionsDialog.dismiss();
	}

	public class NoAdmissionsOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoAdmissionsDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public class NoAdmissionsOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoAdmissionsDialog();
			getFragmentActivity().gotoFirstScreen();
		}
	}
}
