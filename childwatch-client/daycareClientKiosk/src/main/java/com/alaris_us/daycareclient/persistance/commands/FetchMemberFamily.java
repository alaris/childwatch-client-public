package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;

public class FetchMemberFamily extends Command<Family> {

	private Member m_member;

	public FetchMemberFamily(Member member) {

		this(member, null, null);
	}

	protected FetchMemberFamily(Member member, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_member = member;
	}

	@Override
	public void executeSource() {
		m_dataSource.getFamiliesDAO().getFamily(m_member, this);
	}

	@Override
	public void handleResponse(Family data) {

		m_model.setFamily(data);

	}

}