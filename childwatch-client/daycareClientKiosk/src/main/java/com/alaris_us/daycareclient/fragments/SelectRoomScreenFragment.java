package com.alaris_us.daycareclient.fragments;

import com.alaris_us.daycareclient.models.SelectRoomScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher.ScrollDir;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontEditText;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient.widgets.MemberSelectRoom;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

public class SelectRoomScreenFragment extends ScreenSwitcherFragment {
	SelectRoomScreenViewModel	mModel;

    private View m_capacityExceededDialogView;
    private FontTextView capacityExceededBreakdownView;
    private View m_staffOverrideEntryDialogView;
    private View m_successfulOverrideDialogView;
    private FontEditText overridePIN;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.selectroomscreen_layout, container, false);
        m_capacityExceededDialogView = inflater.inflate(R.layout.view_capacityexceededdialog_layout, container, false);
        m_staffOverrideEntryDialogView = inflater.inflate(R.layout.view_staffoverrideentrydialog_layout, container,
                false);
        m_successfulOverrideDialogView = inflater.inflate(R.layout.view_successfuloverridedialog_layout, container,
                false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mModel = new SelectRoomScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(), this);
		//HorizontalListView userList = (HorizontalListView) m_layout.findViewById(R.id.selectroomscreen_tile_roomlist_container);
		m_layout.findViewById(R.id.view_navigation_next_container).setId(R.id.selectroomscreen_nav_next);
		m_layout.findViewById(R.id.view_navigation_back_container).setId(R.id.selectroomscreen_nav_back);
        capacityExceededBreakdownView = (FontTextView) m_capacityExceededDialogView.findViewById(R.id.view_capacityexceededdialog_bottom_text);
		UiBinder.bind(m_layout, R.id.selectroomscreen_tile_roomlist_container, "Adapter", mModel, "Members",
				BindingMode.TWO_WAY, new AdapterConverter(MemberSelectRoom.class, true, true));
		
        /* CapacityExceededDialog */
        UiBinder.bind(m_capacityExceededDialogView, R.id.view_capacityexceededdialog_reselect_button, "OnClickListener", mModel,
                "CapacityExceededDialogReselectClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(m_capacityExceededDialogView, R.id.view_capacityexceededdialog_home_button, "OnClickListener", mModel,
                "CapacityExceededDialogHomeClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(m_capacityExceededDialogView, R.id.view_capacityexceededdialog_override_button, "OnClickListener", mModel,
                "CapacityExceededDialogOverrideClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(new ReflectedProperty(this, "CapacityExceededDialog"), mModel, "CapacityExceededDialog",
                BindingMode.ONE_WAY_TO_SOURCE);
        UiBinder.bind(new ReflectedProperty(this, "CapacityExceededBreakdownText"), mModel, "CapacityExceededBreakdownText",
                BindingMode.ONE_WAY);

        /* StaffOverrideEntryDialog */
        UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_enter_button,
                "OnClickListener", mModel, "StaffOverrideEntryDialogEnterClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_cancel_button,
                "OnClickListener", mModel, "StaffOverrideEntryDialogCancelClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(new ReflectedProperty(this, "StaffOverrideEntryDialog"), mModel, "StaffOverrideEntryDialog",
                BindingMode.ONE_WAY_TO_SOURCE);

        /* SuccessfulOverrideDialog */
        UiBinder.bind(m_successfulOverrideDialogView, R.id.view_successfuloverridedialog_ok_button, "OnClickListener",
                mModel, "SuccessfulOverrideDialogOkClickListener", BindingMode.ONE_WAY);
        UiBinder.bind(new ReflectedProperty(this, "SuccessfulOverrideDialog"), mModel, "SuccessfulOverrideDialog",
                BindingMode.ONE_WAY_TO_SOURCE);

		// Navigation Button(s)
		UiBinder.bind(m_layout, R.id.selectroomscreen_nav_next, "OnClickListener", mModel, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.selectroomscreen_nav_back, "OnClickListener", mModel, "BackClickListener",
				BindingMode.ONE_WAY);
	}

	@Override
	public void initData() {
		setCanScroll(ScrollDir.BACKWARD, false);
		mModel.reset();
	}

	@Override
	public void onDestroyView() {
		mModel.disconnect();
		mModel = null;
		super.onDestroyView();
	}

    public AlertDialog getCapacityExceededDialog() {
        FrameLayout capacityExceededDialogContainer = new FrameLayout(getActivity());
        capacityExceededDialogContainer.addView(m_capacityExceededDialogView);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(capacityExceededDialogContainer);
        AlertDialog capacityExceededDialog = builder.create();
        capacityExceededDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        capacityExceededDialog.setCancelable(false);
        capacityExceededDialog.show();
        capacityExceededDialog.dismiss();
        ViewGroup.LayoutParams lp = m_capacityExceededDialogView.getLayoutParams();
        lp.width = (int) getResources().getDimension(R.dimen.view_capacityexceededdialog_width);
        lp.height = (int) getResources().getDimension(R.dimen.view_capacityexceededdialog_height);
        m_capacityExceededDialogView.setLayoutParams(lp);
        ((View) m_capacityExceededDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
        return capacityExceededDialog;
    }
    
    public String getDefaultRoomCapacityExceededText() {
        return getResources().getString(R.string.view_capacityexceededdialog_bottom_text_room);
    }
    
    public void setCapacityExceededBreakdownText(String breakdown) {
        capacityExceededBreakdownView.setText(breakdown);
    }
    
    public AlertDialog getStaffOverrideEntryDialog() {
        FrameLayout staffOverrideEntryDialogContainer = new FrameLayout(getActivity());
        staffOverrideEntryDialogContainer.addView(m_staffOverrideEntryDialogView);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(staffOverrideEntryDialogContainer);
        AlertDialog staffOverrideEntryDialog = builder.create();
        staffOverrideEntryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        staffOverrideEntryDialog.setCancelable(false);
        staffOverrideEntryDialog.show();
        staffOverrideEntryDialog.dismiss();
        ViewGroup.LayoutParams lp = m_staffOverrideEntryDialogView.getLayoutParams();
        lp.width = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_width);
        lp.height = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_height);
        m_staffOverrideEntryDialogView.setLayoutParams(lp);
        ((View) m_staffOverrideEntryDialogView.getParent().getParent().getParent())
                .setBackgroundResource(R.color.invis);
        return staffOverrideEntryDialog;
    }

    public String getOverridePIN() {
        overridePIN = (FontEditText) m_staffOverrideEntryDialogView
                .findViewById(R.id.view_staffoverrideentrydialog_pin_edittext);
        return overridePIN.getText().toString();
    }

    public void setOverridePIN(String pin) {
        overridePIN.setText(pin);
        overridePIN.setHint("Invalid PIN, Please Retry");
    }

    public void setOverridePINHint(String hint) {
        overridePIN.setHint(hint);
    }

    public AlertDialog getSuccessfulOverrideDialog() {
        FrameLayout successfulOverrideDialogContainer = new FrameLayout(getActivity());
        successfulOverrideDialogContainer.addView(m_successfulOverrideDialogView);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(successfulOverrideDialogContainer);
        AlertDialog successfulOverrideDialog = builder.create();
        successfulOverrideDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        successfulOverrideDialog.setCancelable(false);
        successfulOverrideDialog.show();
        successfulOverrideDialog.dismiss();
        ViewGroup.LayoutParams lp = m_successfulOverrideDialogView.getLayoutParams();
        lp.width = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_width);
        lp.height = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_height);
        m_successfulOverrideDialogView.setLayoutParams(lp);
        ((View) m_successfulOverrideDialogView.getParent().getParent().getParent())
                .setBackgroundResource(R.color.invis);
        return successfulOverrideDialog;
    }

}
