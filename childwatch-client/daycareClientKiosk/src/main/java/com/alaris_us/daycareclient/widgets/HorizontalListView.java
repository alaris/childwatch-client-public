package com.alaris_us.daycareclient.widgets;

import it.sephiroth.android.library.widget.HListView;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

import com.alaris_us.daycareclient_dev.R;

public class HorizontalListView extends HListView {
	public HorizontalListView(Context context) {
		super(context);
		initialize();
	}

	public HorizontalListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	public HorizontalListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}

	public void initialize() {
		setChoiceMode(ListView.CHOICE_MODE_NONE);
		setDrawingCacheEnabled(true);
		setAlwaysDrawnWithCacheEnabled(true);
		setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
		setScrollingCacheEnabled(true);
		setAnimationCacheEnabled(false);
		setDivider(new ColorDrawable(getResources().getColor(R.color.dividerLine)));
		setDividerWidth((int) getResources().getDimension(R.dimen.default_lineheight));
		setSelector(new ColorDrawable(getResources().getColor(R.color.invis)));
		//setFilterTouchesWhenObscured(false);
		//setScrollContainer(true);
	}
	
	public void setPosition(int pos){
		smoothScrollToPosition(pos);
	}
	
	/*public void showKeyboard(){
		App.getCurrentActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(this, InputMethodManager.SHOW_FORCED);
	}

	@Override
	public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
		BaseInputConnection fic = new BaseInputConnection(this, false);
		outAttrs.actionLabel = null;
		outAttrs.inputType = InputType.TYPE_NULL;
		outAttrs.imeOptions = EditorInfo.IME_ACTION_NEXT;
		return fic;
	}

	@Override
	public boolean onCheckIsTextEditor() {
		return true;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.e("adfadsfadsfdsafadsfadsfasd",""+event);
		switch(event.getAction()){ 
		case MotionEvent.ACTION_DOWN:
			showKeyboard();
			break;
		}
		return super.onTouchEvent(event);
	}*/
	
}
