package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;

/**
 * This fetch command is made to be moved to microservice architecture.
 */
public class FetchCheckedInMembersMS extends Command<List<Member>> {

    private Facility m_facility;

    public FetchCheckedInMembersMS(Facility facility) {

        this(facility, null, null);
    }

    protected FetchCheckedInMembersMS(Facility facility, DaycareData dataSource, PersistenceData model) {
        super(dataSource, model);
        m_facility = facility;
    }

    @Override
    public void executeSource() {
        m_dataSource.getFacilitiesDAO().listCheckedInMembers(m_facility, this);
    }

    @Override
    public void handleResponse(List<Member> data) {
        m_model.setCheckedInMembers(data);
    }
}