package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class MemberCheckBox extends RelativeLayout implements BoundUi<MemberCheckBoxModel>{

	private MemberCheckBoxModel m_data;
	
	private ImageView m_checkBox;
	private ImageView m_memberImage;
	private TextView m_memberName;
	private FontTextView m_invalidMarker;
	
	public MemberCheckBox(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public MemberCheckBox(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		View.inflate(getContext(), R.layout.view_listitem_layout, this);
		
		m_memberImage = (ImageView) findViewById(R.id.view_listitem_image);
		m_memberName = (TextView) findViewById(R.id.view_listitem_name);
		m_checkBox = (ImageView) findViewById(R.id.view_listitem_checkmark);
		m_invalidMarker = (FontTextView) findViewById(R.id.view_listitem_invalidmarker);
	}
	
	public Member getMember() {
		return m_data.getMember();
	}
	
	public MemberCheckBoxModel getModel() {
		return m_data; 
	}

	public void setHeight(int height){
		ListViewFunctions.setTileHeight(this, height);
	}
	
	public void setWidth(int width){
		ListViewFunctions.setTileWidth(this, width);
	}
	
	@Override
	public void bind(MemberCheckBoxModel dataSource) {
		m_data = dataSource;
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberName, "Text")), m_data, "MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberImage, "ImageBitmap")), m_data, "MemberImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberImage, "BackgroundColor")), m_data, "BackgroundColor", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Width")), m_data, "Width", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberImage, "Alpha")), m_data, "Alpha", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_checkBox, "Visibility")), m_data, "CheckBoxRes", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberName, "Visibility")), m_data, "NameRes", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_invalidMarker, "Visibility")), m_data, "ValidRes", BindingMode.ONE_WAY);

	}
}
