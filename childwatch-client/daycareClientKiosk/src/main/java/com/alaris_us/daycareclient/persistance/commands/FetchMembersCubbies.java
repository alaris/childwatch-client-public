package com.alaris_us.daycareclient.persistance.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;

public class FetchMembersCubbies extends Command<List<List<Cubby>>> {
	Facility m_facility;
	List<Member> m_members;

	public FetchMembersCubbies(Facility facility, List<Member> members) {
		this(facility, members, null, null);
	}

	protected FetchMembersCubbies(Facility facility, List<Member> members, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_facility = facility;
		m_members = members;
	}

	@Override
	public void executeSource() {
		m_dataSource.getCubbiesDAO().listReservedCubbies(m_facility, m_members, this);
	}

	@Override
	public void handleResponse(List<List<Cubby>> data) {
		List<Cubby> uniqueCubbies = new ArrayList<Cubby>();
		List<Cubby> cubbiesToCompare = new ArrayList<Cubby>();
		List<String> cubbyIds = new ArrayList<String>();
		for (List<Cubby> cubbyList : data)
			for (Cubby cubby : cubbyList)
				cubbiesToCompare.add(cubby);

		for (Cubby c : cubbiesToCompare)
			if (!cubbyIds.contains(c.getObjectId()))
				cubbyIds.add(c.getObjectId());

		for (String id : cubbyIds)
			for (Cubby c : cubbiesToCompare)
				if (c.getObjectId().equals(id)) {
					uniqueCubbies.add(c);
					break;
				}

		m_model.setFamilyCubbies(uniqueCubbies);
	}
}