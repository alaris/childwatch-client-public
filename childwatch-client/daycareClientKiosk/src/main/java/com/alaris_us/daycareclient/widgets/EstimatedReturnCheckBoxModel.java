package com.alaris_us.daycareclient.widgets;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import android.view.View;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableInt;

public class EstimatedReturnCheckBoxModel {

	private Integer	m_timeEstimate;
	private TrackableBoolean		m_isSelected;
	private TrackableInt			m_width;
	private Integer					m_color;

	public EstimatedReturnCheckBoxModel() {
		this(null, null, null);
	}

	public EstimatedReturnCheckBoxModel(Integer timeEstimate, Integer width, Integer color) {
		m_timeEstimate = timeEstimate;
		m_isSelected = new TrackableBoolean(false);
		m_width = new TrackableInt(width);
		m_color = color;
	}

	public String getTimeEstimate() { //TODO remove decimal formatter
		//DecimalFormat df = new DecimalFormat("#.#");
		DateTime dateTime = getReturnTime();
		DateTimeFormatter dtf = new DateTimeFormatterBuilder().appendPattern("h:mm a").toFormatter();
		String formattedTime = dateTime.toString(dtf);
		//return df.format(m_timeEstimate) + " Minutes\n\n" + "(" + formattedTime + ")";
		return m_timeEstimate + " Minutes\n\n" + "(" + formattedTime + ")";
	}

	public void setTimeEstimate(Integer timeEstimate) {
		m_timeEstimate = timeEstimate;
	}

	public DateTime getReturnTime() {
		Integer minutes = m_timeEstimate;
		DateTime dateTime = new DateTime();
		dateTime = dateTime.plusMinutes(minutes.intValue());
		return dateTime;
	}
	
	public String getReturnTimeAsString() {
		Integer minutes = m_timeEstimate;
		LocalTime localTime = new LocalTime();
		localTime = localTime.plusMinutes(minutes.intValue());
		DateTimeFormatter dtf = new DateTimeFormatterBuilder().appendPattern("hh:mm a").toFormatter();
		String formattedTime = localTime.toString(dtf);
		return formattedTime;
	}
	
	public int getBackgroundColor() {
		return m_color;
	}

	public int getWidth() {
		return m_width.get();
	}

	public void setWidth(int width) {
		m_width.set(width);
	}

	public float getAlpha() {
		return ListViewFunctions.getTileAlpha(m_isSelected.get());
	}

	public boolean getIsSelected() {
		return m_isSelected.get();
	}

	public void setIsSelected(boolean isSelected) {
		m_isSelected.set(isSelected);
	}

	public void toggleIsSelected() {
		setIsSelected(!getIsSelected());
	}

	public int getCheckBoxRes() {
		return m_isSelected.get() ? View.VISIBLE : View.GONE;
	}

}
