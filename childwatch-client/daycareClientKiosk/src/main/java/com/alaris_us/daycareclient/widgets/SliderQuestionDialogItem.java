package com.alaris_us.daycareclient.widgets;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class SliderQuestionDialogItem extends LinearLayout implements BoundUi<SliderQuestionDialogItemModel>{

	private SliderQuestionDialogItemModel m_data;

	private View m_layout;

	public SliderQuestionDialogItem(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public SliderQuestionDialogItem(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		m_layout = View.inflate(getContext(), R.layout.view_sliderquestiondialog_item, this);
	}

	public Member getMember() {
		return m_data.getMember();
	}

	public SliderQuestionDialogItemModel getModel() {
		return m_data;
	}

	public boolean getIsChecked() {
		Slider slider = (Slider) m_layout.findViewById(R.id.view_sliderquestiondialog_item_switch);
		return slider.isChecked();
	}

	@Override
	public void bind(SliderQuestionDialogItemModel dataSource) {
		m_data = dataSource;
		UiBinder.bind(m_layout, R.id.view_sliderquestiondialog_item_name, "Text", m_data, "MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.view_sliderquestiondialog_item_switch, "IsChecked", m_data, "IsChecked", BindingMode.TWO_WAY);
	}
}
