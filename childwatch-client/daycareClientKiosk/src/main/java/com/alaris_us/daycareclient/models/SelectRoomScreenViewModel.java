package com.alaris_us.daycareclient.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.alaris_us.daycareclient.fragments.SelectRoomScreenFragment;
import com.alaris_us.daycareclient.models.AuthenticationScreenViewModel.LoginResult;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.MemberSelectRoomModel;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

import android.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class SelectRoomScreenViewModel extends ViewModel {
	public SelectRoomScreenViewModel(PersistenceLayer persistenceLayer, SelectRoomScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private State m_currentState;
	private SelectRoomScreenFragment fragment;
	private TrackableCollection<MemberSelectRoomModel> m_memberModels;
    private TrackableField<AlertDialog> m_capacityExceededDialog;
    private TrackableField<AlertDialog> m_staffOverrideEntryDialog;
    private TrackableField<AlertDialog> m_successfulOverrideDialog;

	public TrackableCollection<MemberSelectRoomModel> getMembers() {
		return m_memberModels;
	}

	public void onReset() {
		m_currentState = new AwaitingSelectionState();
		m_currentState.enterState();
	}

	@Override
	protected void onPopulateModelData() {
		List<Member> selectedChildren = getPersistenceLayer().getPersistenceData().getSelectedDaycareChildren();
		m_memberModels.clear();
		int pos = 0;
		int memNum = 0;
		memNum = selectedChildren.size();
		int height = ListViewFunctions.calculateTileWidth(memNum);
		for (Member member : selectedChildren) {
			MemberSelectRoomModel model = new MemberSelectRoomModel(member, height,
					ListViewFunctions.getColorAt(pos++), getPersistenceLayer().getPersistenceData().getSelectedProgramRooms());
			m_memberModels.add(model);
		}
	}

	public OnClickListener getNextClickListener() {
		return new NextClickListener();
	}

	public OnClickListener getBackClickListener() {
		return new BackClickListener();
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof AwaitingSelectionState)
				m_currentState.exitState();
		}
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			fragment.prevScreen();
		}
	}

	@Override
	public void onInitialize() {
		m_memberModels = new TrackableCollection<MemberSelectRoomModel>();
        m_capacityExceededDialog = new TrackableField<AlertDialog>();
        m_staffOverrideEntryDialog = new TrackableField<AlertDialog>();
        m_successfulOverrideDialog = new TrackableField<AlertDialog>();
	}

	abstract class State {
		public abstract void enterState();

		public void exitState() {
			m_currentState = null;
		}

		public void exception(Exception e) {
			m_currentState = null;
		}
	}

	private class AwaitingSelectionState extends State {
		@Override
		public void enterState() {
			Log.w("AwaitingSelectionState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("AwaitingSelectionState", "ExitState");
            if (!getPersistenceLayer().getPersistenceData().getIsOverride()) {
            	if (getPersistenceLayer().getPersistenceData().getFacility().getRatioType() != null) {
            		if (getPersistenceLayer().getPersistenceData().getFacility().getRatioType().equals("ROOM")) {
		                if (getPersistenceLayer().getPersistenceData().getFacility().getRatio().intValue() == 0) {
		                    Map<String, Integer> roomAvailability = new HashMap<>();
		                    List<Room> allRooms = getPersistenceLayer().getPersistenceData().getAllRooms();
		                    List<Staff> clockedInStaff = getPersistenceLayer().getPersistenceData().getClockedInStaff();
		                    List<Member> checkedInMembers = getPersistenceLayer().getPersistenceData().getCheckedInMembers();
		                    for (Room r : allRooms) {
		                        int checkedIntoRoomCount = 0;
		                        int clockedInStaffToRoom = 0;
		                        for (Member m : checkedInMembers) {
		                            if (m.getPRoom().equals(r))
		                                checkedIntoRoomCount++;
		                        }
		                        for (Staff s : clockedInStaff) {
		                            if (s.getPRoom().equals(r))
		                                clockedInStaffToRoom++;
		                        }
		                        List<Member> memberSelectedForRoom = new ArrayList<>();
		                        for (MemberSelectRoomModel selectRoomModel : m_memberModels) {
		                            if (selectRoomModel.getMember().getPRoom() != null)
		                                if (selectRoomModel.getMember().getPRoom().equals(r))
		                                    memberSelectedForRoom.add(selectRoomModel.getMember());
		                        }
		                        int openRoomSpots = 0;
		                        openRoomSpots = (r.getRatio().intValue() * clockedInStaffToRoom) - checkedIntoRoomCount;
		                        if (memberSelectedForRoom.size() > openRoomSpots)
		                            roomAvailability.put(r.getName(), openRoomSpots);
		                    }
		                    if (!roomAvailability.isEmpty())
		                        showCapacityExceededDialog(roomAvailability);
		                    else {
		                        m_currentState = new FinishSelectionState();
		                        m_currentState.enterState();
		                    }
		                }
            		} else {
            			m_currentState = new FinishSelectionState();
                        m_currentState.enterState();
            		}
                } else {
                    m_currentState = new FinishSelectionState();
                    m_currentState.enterState();
                }
            } else {
                m_currentState = new FinishSelectionState();
                m_currentState.enterState();
            }

		}

		@Override
		public void exception(Exception e) {
			Log.w("AwaitingSelectionState", "Exception: " + e.getMessage());
			reset();
		}
	}
	
    private class FinishSelectionState extends State {
        @Override
        public void enterState() {
            Log.w("FinishSelectionState", "EnterState");
            if (getPersistenceLayer().getPersistenceData().getLoginResult() == null)
                fragment.nextScreen("WORKOUT");
            else if (getPersistenceLayer().getPersistenceData().getLoginResult().equals(LoginResult.CHILDUSER))
                fragment.nextScreen("CONFIRM");
            else
                fragment.nextScreen("WORKOUT");
        }

        @Override
        public void exitState() {
        }
        
        @Override
        public void exception(Exception e) {
            Log.w("FinishSelectionState", "Exception: " + e.getMessage());
            reset();
        }
    }
    private void showCapacityExceededDialog(Map<String, Integer> availableRooms) {
        if (m_capacityExceededDialog.get() != null) {
            String toReturn = "";
            Iterator it =availableRooms.entrySet().iterator();
            while (it.hasNext()) {
            	String breakdownText = fragment.getDefaultRoomCapacityExceededText();
                Map.Entry pair = (Map.Entry) it.next();
                if (pair.getValue().toString().equals("1")) {
                    breakdownText = breakdownText.replace("remain", "remains");
                    breakdownText = breakdownText.replace("spots", "spot");
                }
                String composedBreakdownText = breakdownText.replace("[SPOTS]", pair.getValue().toString());
                composedBreakdownText = composedBreakdownText.replace("[ROOM]", pair.getKey().toString());
                toReturn += composedBreakdownText + "\n";
                it.remove();
            }
            fragment.setCapacityExceededBreakdownText(toReturn);
            m_capacityExceededDialog.get().show();
        }
    }
    
    public void setCapacityExceededDialog(AlertDialog alertDialog) {
        m_capacityExceededDialog.set(alertDialog);
    }
    
    private void dismissCapacityExceededDialog() {
        if (m_capacityExceededDialog.get() != null && m_capacityExceededDialog.get().isShowing())
            m_capacityExceededDialog.get().dismiss();
    }
    
    public OnClickListener getCapacityExceededDialogReselectClickListener() {
        return new CapacityExceededDialogReselectClickListener();
    }

    public OnClickListener getCapacityExceededDialogHomeClickListener() {
        return new CapacityExceededDialogHomeClickListener();
    }

    public OnClickListener getCapacityExceededDialogOverrideClickListener() {
        return new CapacityExceededDialogOverrideClickListener();
    }

    public class CapacityExceededDialogReselectClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            dismissCapacityExceededDialog();
            m_currentState = new AwaitingSelectionState();
            m_currentState.enterState();
        }
    }

    public class CapacityExceededDialogHomeClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            dismissCapacityExceededDialog();
            fragment.firstScreen();
        }
    }

    public class CapacityExceededDialogOverrideClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            dismissCapacityExceededDialog();
            showStaffOverrideEntryDialog();
        }
    }
    
    public void setStaffOverrideEntryDialog(AlertDialog alertDialog) {
        m_staffOverrideEntryDialog.set(alertDialog);
    }

    private void showStaffOverrideEntryDialog() {
        if (m_staffOverrideEntryDialog.get() != null) {
            m_staffOverrideEntryDialog.get().show();
        }
    }

    public void dismissStaffOverrideEntryDialog() {
        if (m_staffOverrideEntryDialog != null && m_staffOverrideEntryDialog.get().isShowing())
            m_staffOverrideEntryDialog.get().dismiss();
    }

    public void setSuccessfulOverrideDialog(AlertDialog alertDialog) {
        m_successfulOverrideDialog.set(alertDialog);
    }

    private void showSuccessfulOverrideDialog() {
        if (m_successfulOverrideDialog.get() != null) {
            m_successfulOverrideDialog.get().show();
        }
    }

    public void dismissSuccessfulOverrideDialog() {
        if (m_successfulOverrideDialog != null && m_successfulOverrideDialog.get().isShowing())
            m_successfulOverrideDialog.get().dismiss();
    }
    
    public OnClickListener getStaffOverrideEntryDialogEnterClickListener() {
        return new StaffOverrideEntryDialogEnterClickListener();
    }

    public OnClickListener getStaffOverrideEntryDialogCancelClickListener() {
        return new StaffOverrideEntryDialogCancelClickListener();
    }

    public class StaffOverrideEntryDialogEnterClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            if (getPersistenceLayer().getPersistenceData().getFacility().getOverridePIN().equals(fragment.getOverridePIN())) {
                dismissStaffOverrideEntryDialog();
                showSuccessfulOverrideDialog();
            } else {
                fragment.setOverridePIN("");
                fragment.setOverridePINHint("Invalid PIN, Please Retry");
            }
        }
    }

    public class StaffOverrideEntryDialogCancelClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            dismissStaffOverrideEntryDialog();
        }
    }

    public OnClickListener getSuccessfulOverrideDialogOkClickListener() {
        return new SuccessfulOverrideDialogOkClickListener();
    }

    public class SuccessfulOverrideDialogOkClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            dismissSuccessfulOverrideDialog();
            getPersistenceLayer().getPersistenceData().setIsOverride(true);
            m_currentState = new FinishSelectionState();
            m_currentState.enterState();
            
        }
    }

}
