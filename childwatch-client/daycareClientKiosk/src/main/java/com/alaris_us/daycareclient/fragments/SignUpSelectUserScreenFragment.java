package com.alaris_us.daycareclient.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.alaris_us.daycareclient.models.SignUpSelectMemberScreenViewModel;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcher.ScrollDir;
import com.alaris_us.daycareclient.screenswitcher.ScreenSwitcherFragment;
import com.alaris_us.daycareclient.widgets.FontEditText;
import com.alaris_us.daycareclient.widgets.FontTextView;
import com.alaris_us.daycareclient.widgets.HorizontalListView;
import com.alaris_us.daycareclient.widgets.SignUpSelectMemberItem;
import com.alaris_us.daycareclient_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

public class SignUpSelectUserScreenFragment extends ScreenSwitcherFragment {
	SignUpSelectMemberScreenViewModel	m_model;
	private View						m_invalidMemberSelectedDialogView;
	private FontTextView				invalidBreakdownView;
	private View						m_staffOverrideEntryDialogView;
	private View						m_successfulOverrideDialogView;
	private FontEditText				overridePIN;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		m_layout = inflater.inflate(R.layout.signupselectuserscreen_layout, container, false);
		m_invalidMemberSelectedDialogView = inflater.inflate(R.layout.view_invalidmemberselecteddialog_layout,
				container, false);
		m_staffOverrideEntryDialogView = inflater.inflate(R.layout.view_staffoverrideentrydialog_layout, container,
				false);
		m_successfulOverrideDialogView = inflater.inflate(R.layout.view_successfuloverridedialog_layout, container,
				false);
		super.onCreateView(inflater, container, savedInstanceState);
		return m_layout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		HorizontalListView parentList = (HorizontalListView) m_layout.findViewById(R.id.signupselectuserscreen_tile_userlist_container);
		m_model = new SignUpSelectMemberScreenViewModel(((PersistanceLayerHost) getActivity()).getPersistanceLayer(),
				this);
		invalidBreakdownView = (FontTextView) m_invalidMemberSelectedDialogView.findViewById(R.id.view_invalidmemberselecteddialog_bottom_text);
		UiBinder.bind(m_layout, R.id.signupselectuserscreen_tile_userlist_container, "Adapter", m_model, "Members",
				BindingMode.TWO_WAY, new AdapterConverter(SignUpSelectMemberItem.class, true, true));
		UiBinder.bind(m_layout, R.id.view_navigation_next_container, "OnClickListener", m_model, "NextClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.view_navigation_next_container, "Alpha", m_model, "NextAlpha", BindingMode.ONE_WAY);
		parentList.setOnItemClickListener(m_model.new MemberClickListener());
		UiBinder.bind(m_invalidMemberSelectedDialogView, R.id.view_invalidmemberselecteddialog_reselect_button,
				"OnClickListener", m_model, "InvalidMemberSelectedDialogReselectClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_invalidMemberSelectedDialogView, R.id.view_invalidmemberselecteddialog_home_button,
				"OnClickListener", m_model, "InvalidMemberSelectedDialogHomeClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_invalidMemberSelectedDialogView, R.id.view_invalidmemberselecteddialog_override_button,
				"OnClickListener", m_model, "InvalidMemberSelectedDialogOverrideClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "InvalidMemberSelectedDialog"), m_model,
				"InvalidMemberSelectedDialog", BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "InvalidBreakdownText"), m_model, "InvalidBreakdownText",
				BindingMode.ONE_WAY);
		UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_enter_button,
				"OnClickListener", m_model, "StaffOverrideEntryDialogEnterClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(m_staffOverrideEntryDialogView, R.id.view_staffoverrideentrydialog_cancel_button,
				"OnClickListener", m_model, "StaffOverrideEntryDialogCancelClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "StaffOverrideEntryDialog"), m_model, "StaffOverrideEntryDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(m_successfulOverrideDialogView, R.id.view_successfuloverridedialog_ok_button, "OnClickListener",
				m_model, "SuccessfulOverrideDialogOkClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "SuccessfulOverrideDialog"), m_model, "SuccessfulOverrideDialog",
				BindingMode.ONE_WAY_TO_SOURCE);
		// Navigation Button(s)
		m_layout.findViewById(R.id.view_navigation_back_container).setOnClickListener(new BackClickListener());
	}

	public AlertDialog getInvalidMemberSelectedDialog() {
		FrameLayout invalidMemberSelectedDialogContainer = new FrameLayout(getActivity());
		invalidMemberSelectedDialogContainer.addView(m_invalidMemberSelectedDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(invalidMemberSelectedDialogContainer);
		AlertDialog invalidMemberSelectedDialog = builder.create();
		invalidMemberSelectedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		invalidMemberSelectedDialog.setCancelable(false);
		invalidMemberSelectedDialog.show();
		invalidMemberSelectedDialog.dismiss();
		ViewGroup.LayoutParams lp = m_invalidMemberSelectedDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_invalidmemberselecteddialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_invalidmemberselecteddialog_height);
		m_invalidMemberSelectedDialogView.setLayoutParams(lp);
		((View) m_invalidMemberSelectedDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return invalidMemberSelectedDialog;
	}

	public void setInvalidBreakdownText(String breakdown) {
		invalidBreakdownView.setText(breakdown);
	}

	public AlertDialog getStaffOverrideEntryDialog() {
		FrameLayout staffOverrideEntryDialogContainer = new FrameLayout(getActivity());
		staffOverrideEntryDialogContainer.addView(m_staffOverrideEntryDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(staffOverrideEntryDialogContainer);
		AlertDialog staffOverrideEntryDialog = builder.create();
		staffOverrideEntryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		staffOverrideEntryDialog.setCancelable(false);
		staffOverrideEntryDialog.show();
		staffOverrideEntryDialog.dismiss();
		ViewGroup.LayoutParams lp = m_staffOverrideEntryDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_staffoverrideentrydialog_height);
		m_staffOverrideEntryDialogView.setLayoutParams(lp);
		((View) m_staffOverrideEntryDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return staffOverrideEntryDialog;
	}

	public String getOverridePIN() {
		overridePIN = (FontEditText) m_staffOverrideEntryDialogView.findViewById(R.id.view_staffoverrideentrydialog_pin_edittext);
		return overridePIN.getText().toString();
	}

	public void setOverridePIN(String pin) {
		overridePIN.setText(pin);
		overridePIN.setHint("Invalid PIN, Please Retry");
	}

	public void setOverridePINHint(String hint) {
		overridePIN.setHint(hint);
	}

	public AlertDialog getSuccessfulOverrideDialog() {
		FrameLayout successfulOverrideDialogContainer = new FrameLayout(getActivity());
		successfulOverrideDialogContainer.addView(m_successfulOverrideDialogView);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(successfulOverrideDialogContainer);
		AlertDialog successfulOverrideDialog = builder.create();
		successfulOverrideDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		successfulOverrideDialog.setCancelable(false);
		successfulOverrideDialog.show();
		successfulOverrideDialog.dismiss();
		ViewGroup.LayoutParams lp = m_successfulOverrideDialogView.getLayoutParams();
		lp.width = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_width);
		lp.height = (int) getResources().getDimension(R.dimen.view_successfuloverridedialog_height);
		m_successfulOverrideDialogView.setLayoutParams(lp);
		((View) m_successfulOverrideDialogView.getParent().getParent().getParent()).setBackgroundResource(R.color.invis);
		return successfulOverrideDialog;
	}

	@Override
	public void initData() {
		setCanScroll(ScrollDir.BACKWARD, false);
		m_model.reset();
	}

	private class BackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getFragmentActivity().prevScreen();
		}
	}
}
