package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.User;

public class FetchFacility
	extends Command<Facility> {

	private User	m_user;

	public FetchFacility(User user) {

		this(user, null, null);
	}

	protected FetchFacility(User user, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_user = user;
	}

	@Override
	public void executeSource() {

		m_dataSource.getUsersDAO().getFacility(m_user, this);

	}

	@Override
	public void handleResponse(
			Facility data) {

		m_model.setFacility(data);

	}

}