package com.alaris_us.daycareclient.models;

import android.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.alaris_us.daycareclient.fragments.CheckInScreenFragment;
import com.alaris_us.daycareclient.models.AuthenticationScreenViewModel.LoginResult;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycareclient.utils.DaycareHelpers;
import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient.widgets.MemberCheckBox;
import com.alaris_us.daycareclient.widgets.MemberCheckBoxModel;
import com.alaris_us.daycareclient.widgets.SelectSelfCheckInDialogItemModel;
import com.alaris_us.daycareclient.widgets.SliderQuestionDialogItem;
import com.alaris_us.daycareclient.widgets.SliderQuestionDialogItemModel;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycareclient.yombu.YombuRESTException;
import com.alaris_us.daycareclient.yombu.YombuRESTOperationComplete;
import com.alaris_us.daycareclient.yombu.YombuWaiverResponse;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Document;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Question;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.externaldata.ExternalDataOperationComplete;
import com.alaris_us.externaldata.exception.ExternalDataException;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;
import com.yombu.waiverlibrary.YombuWaiver;
import com.yombu.waiverlibrary.callbacks.YombuInitializationCallback;
import com.yombu.waiverlibrary.callbacks.YombuWaiverProcessingCallback;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;

public class CheckInScreenViewModel extends ViewModel {
	private State m_currentState;

	public CheckInScreenViewModel(PersistenceLayer persistenceLayer, CheckInScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
		reset();
	}

	private final int showProxyRes = R.string.view_navigation_withbuttons_showproxy,
			hideProxyRes = R.string.view_navigation_withbuttons_hideproxy,
			selectAllRes = R.string.view_navigation_withbuttons_selectall,
			selectNoneRes = R.string.view_navigation_withbuttons_selectnone;
	private final int selectAllWithProxyTextSize = R.dimen.view_navigation_withbuttons_selectall_textsize,
			selectAllWithoutProxyTextSize = R.dimen.view_navigation_withbuttons_selectall_withoutproxy_textsize;
	private LayoutParams layoutParams = new LayoutParams(0, LayoutParams.MATCH_PARENT);
	private CheckInScreenFragment fragment;
	private TrackableBoolean isShowingProxyChildren;
	private TrackableBoolean isProxyButtonVisible;
	private TrackableBoolean isProxyButtonInlayout;
	private TrackableBoolean isAllSelected;
	private TrackableField<Facility> m_facility;
	private TrackableField<TrackableCollection<MemberCheckBoxModel>> m_visibleChildrenModels;
	private TrackableField<TrackableCollection<MemberCheckBoxModel>> m_relatedChildrenModels;
	private TrackableField<TrackableCollection<MemberCheckBoxModel>> m_proxyChildrenModels;
	private TrackableField<TrackableCollection<Member>> m_selectedChildren;
	private TrackableField<TrackableCollection<Cubby>> m_familyCubbies;
	private TrackableField<Member> m_currentMember;
	private TrackableField<AlertDialog> m_pictureDialog;
	private TrackableField<AlertDialog> m_noTimeLeftDialog;
	private TrackableField<AlertDialog> m_staffOverrideEntryDialog;
	private TrackableField<AlertDialog> m_successfulOverrideDialog;
	private TrackableField<AlertDialog> m_noGuestDaysLeftDialog;
	private TrackableCollection<Member> m_childrenWithNoDailyTime;
	private TrackableCollection<Member> m_childrenWithNoWeeklyTime;
	private TrackableCollection<Member> m_childrenWithNoGuestDays;
	private TrackableField<TrackableCollection<Room>> m_facilityRooms;
	private boolean roomsAreFull;
	private TrackableField<AlertDialog> m_roomsAreFullDialog;
	private TrackableField<TrackableCollection<Member>> m_noRoomForChildren;
	private TrackableField<TrackableCollection<Room>> m_assignedRooms;
	private TrackableField<AlertDialog> m_noKidsForCheckDirectionDialog;
    private TrackableField<AlertDialog> m_questionDialog;
	private List<Member> m_multiRoomChildren;
	private TrackableField<AlertDialog> m_timesAlmostUpDialog;
	private TrackableField<AlertDialog> m_freeAndPaidTimeDialog;
	private TrackableCollection<Member> m_childrenWithLittleDailyTime;
	private TrackableCollection<Member> m_childrenWithLittleWeeklyTime;
	private final int m_singleChildTitleRes = R.string.checkinscreen_tile_selectchild_text_singlechild;
	private final int m_multiChildTitleRes = R.string.checkinscreen_tile_selectchild_text;
	private TrackableField<Program> m_selectedProgram;
	private Integer m_checkedInMembersCount;
	private TrackableField<AlertDialog> m_capacityExceededDialog;
	private int m_openSpots;
	private List<Member> m_checkedInMembers;
    private String m_questionTitleText = "";
    private String m_questionBodyText = "";
	private List<Question> m_popupQuestionsToDisplay;
	private Question m_currentQuestion;
	private TrackableField<AlertDialog> m_selectableChildrenLimitDialog;
    private List<Cubby> m_availableCubbies;
    private String m_waiverContents;
    private TrackableCollection<SelectSelfCheckInDialogItemModel> m_selectCanSelfCheckInChildren;
	private TrackableField<AlertDialog> m_selectSelfCheckInDialog;
	private TrackableField<AlertDialog> m_sliderQuestionDialog;
	private TrackableCollection<SliderQuestionDialogItemModel> m_sliderQuestionChildren;
	private String m_sliderQuestionTitleText = "";

	public TrackableCollection<MemberCheckBoxModel> getVisibleChildren() {
		return m_visibleChildrenModels.get();
	}

	public void onReset() {
		m_currentState = new AwaitingSelectionState();
		m_currentState.enterState();
	}

	@Override
	protected void onPopulateModelData() {
		m_currentMember.set(getPersistenceLayer().getPersistenceData().getCurrentMember());
		if (m_currentState instanceof AwaitingSelectionState) {
			int visibleChildSize = 0, visibleProxySize = 0;
			List<Member> relatedChildren, proxyChildren;
			isShowingProxyChildren.set(false);
			isAllSelected.set(false);
			m_facility.set(getPersistenceLayer().getPersistenceData().getFacility());
			m_familyCubbies
					.set(new TrackableCollection<Cubby>(getPersistenceLayer().getPersistenceData().getFamilyCubbies()));
			m_facilityRooms
					.set(new TrackableCollection<Room>(getPersistenceLayer().getPersistenceData().getAllRooms()));
			relatedChildren = getPersistenceLayer().getPersistenceData().getRelatedInChildren();
			proxyChildren = getPersistenceLayer().getPersistenceData().getProxyInChildren();
			m_visibleChildrenModels.get().clear();
			m_relatedChildrenModels.get().clear();
			m_proxyChildrenModels.get().clear();
			m_selectedChildren.get().clear();
			int width = 0;
			int pos = 0;
			// TODO: View should not be responsible for width. Move to list.

			m_selectedProgram.set(getPersistenceLayer().getPersistenceData().getSelectedProgram());
			double minAge = 0;
			double maxAge = 0;
			if (m_selectedProgram.get() != null) {
				minAge = m_selectedProgram.get().getMinAge().doubleValue();
				maxAge = m_selectedProgram.get().getMaxAge().doubleValue();
			}
			for (Member child : relatedChildren) {
				if (child.getUsesChildWatch() && DaycareHelpers.getIsAgeBetween(child.getBirthDate(), minAge, maxAge)) {
					visibleChildSize++;
				}
			}
			for (Member child : proxyChildren) {
				if (child.getUsesChildWatch() && DaycareHelpers.getIsAgeBetween(child.getBirthDate(), minAge, maxAge)) {
					visibleProxySize++;
				}
			}

			if (relatedChildren == null || relatedChildren.isEmpty() || visibleChildSize == 0)
				width = ListViewFunctions.calculateTileWidthForSelectMember(visibleProxySize);
			else
				width = ListViewFunctions.calculateTileWidthForSelectMember(visibleChildSize + visibleProxySize);
			
			if (getPersistenceLayer().getPersistenceData().getLoginResult() != null) {
				if (getPersistenceLayer().getPersistenceData().getLoginResult().equals(LoginResult.CHILDUSER)) {
					for (Member child : relatedChildren) {
						if (getPersistenceLayer().getPersistenceData().getCurrentMember().equals(child)) {
							MemberCheckBoxModel model = new MemberCheckBoxModel(child, width,
									ListViewFunctions.getColorAt(pos++));
							m_relatedChildrenModels.get().add(model);
						}
					}
				} else {
					for (Member child : relatedChildren) {
						if (child.getUsesChildWatch() && DaycareHelpers.getIsAgeBetween(child.getBirthDate(), minAge, maxAge)) {
							MemberCheckBoxModel model = new MemberCheckBoxModel(child, width,
									ListViewFunctions.getColorAt(pos++));
							m_relatedChildrenModels.get().add(model);
						}
					}
					for (Member child : proxyChildren) {
						if (child.getUsesChildWatch() && DaycareHelpers.getIsAgeBetween(child.getBirthDate(), minAge, maxAge)) {
							MemberCheckBoxModel model = new MemberCheckBoxModel(child, width,
									ListViewFunctions.getColorAt(pos++));
							m_relatedChildrenModels.get().add(model);
						}
					}
				}
			} else {
				for (Member child : relatedChildren) {
					if (child.getUsesChildWatch() && DaycareHelpers.getIsAgeBetween(child.getBirthDate(), minAge, maxAge)) {
						MemberCheckBoxModel model = new MemberCheckBoxModel(child, width,
								ListViewFunctions.getColorAt(pos++));
						m_relatedChildrenModels.get().add(model);
					}
				}
				for (Member child : proxyChildren) {
					if (child.getUsesChildWatch() && DaycareHelpers.getIsAgeBetween(child.getBirthDate(), minAge, maxAge)) {
						MemberCheckBoxModel model = new MemberCheckBoxModel(child, width,
								ListViewFunctions.getColorAt(pos++));
						m_relatedChildrenModels.get().add(model);
					}
				}
			}

			if (m_relatedChildrenModels.get().isEmpty()) {
				m_visibleChildrenModels.get().addAll(m_proxyChildrenModels.get());
				isProxyButtonVisible.set(false);
			} else {
				m_visibleChildrenModels.get().addAll(m_relatedChildrenModels.get());
				isProxyButtonVisible.set(!m_proxyChildrenModels.get().isEmpty());
			}

			if (m_visibleChildrenModels.get().size() == 1) {
				fragment.getUserList()
						.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1));
				fragment.getSingleUserName().setText(m_visibleChildrenModels.get().get(0).getMemberName());
				fragment.getSingleUserName().setVisibility(View.VISIBLE);
				m_visibleChildrenModels.get().get(0).setIsSelected(true);
				m_visibleChildrenModels.get().get(0).setNameRes(false);
				m_selectedChildren.get().add(m_visibleChildrenModels.get().get(0).getMember());
			} else {
				fragment.getSingleUserName().setVisibility(View.GONE);
				fragment.getUserList()
						.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 2));
			}
		}
	}

	@Override
	public void onPersistModel() {
	}

	public OnClickListener getNextClickListener() {
		if (!m_selectedChildren.get().isEmpty())
			return new NextClickListener();
		else
			return null;
	}

	public float getNextAlpha() {
		return ListViewFunctions.getTileAlpha(!m_selectedChildren.get().isEmpty());
	}

	public int getProxyButtonText() {
		return isShowingProxyChildren.get() ? hideProxyRes : showProxyRes;
	}

	public int getSelectAllButtonText() {
		return isAllSelected.get() ? selectNoneRes : selectAllRes;
	}

	public int getSelectAllButtonTextSize() {
		return isProxyButtonVisible.get() ? selectAllWithProxyTextSize : selectAllWithoutProxyTextSize;
	}

	public int getProxyButtonVisibility() {
		isProxyButtonInlayout.set(isProxyButtonVisible.get());
		if (isProxyButtonVisible.get())
			return View.VISIBLE;
		else
			return View.GONE;
	}

	public int getScrollPosition() {
		return isShowingProxyChildren.get() ? m_visibleChildrenModels.get().size() - 1 : 0;
	}

	public LayoutParams getSelectAllLayoutParams() {
		layoutParams.weight = isProxyButtonInlayout.get() ? 1.5f : 3;
		return layoutParams;
	}

	public class NextClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof AwaitingSelectionState)
				m_currentState.exitState();
		}
	}

	public class SelectAllClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof AwaitingSelectionState) {
				isAllSelected.set(!isAllSelected.get());
				m_selectedChildren.get().clear();
				for (MemberCheckBoxModel memberModel : m_visibleChildrenModels.get()) {
					if (isAllSelected.get())
						m_selectedChildren.get().add(memberModel.getMember());
					memberModel.setIsSelected(isAllSelected.get());
				}
			}
		}
	}

	public class ShowProxyClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof AwaitingSelectionState) {
				boolean isShowingProxies = !isShowingProxyChildren.get();
				isShowingProxyChildren.set(isShowingProxies);
				if (isShowingProxies) {
					m_visibleChildrenModels.get().addAll(m_proxyChildrenModels.get());
				} else {
					m_visibleChildrenModels.get().removeAll(m_proxyChildrenModels.get());
					for (MemberCheckBoxModel memberModel : m_proxyChildrenModels.get()) {
						memberModel.setIsSelected(false);
						m_selectedChildren.get().remove(memberModel.getMember());
					}
				}
				for (MemberCheckBoxModel memberModel : m_visibleChildrenModels.get())
					memberModel.setWidth(ListViewFunctions.calculateTileWidth(m_visibleChildrenModels.get().size()));
			}
		}
	}

	public class ChildClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> parent, View view, int position,
				long id) {
			if (m_currentState instanceof AwaitingSelectionState) {
				Member member = ((MemberCheckBox) view).getMember();
				MemberCheckBoxModel model = ((MemberCheckBox) view).getModel();
				model.toggleIsSelected();
				if (model.getIsSelected())
					m_selectedChildren.get().add(member);
				else
					m_selectedChildren.get().remove(member);
			}
		}
	}

	public void setPictureDialog(AlertDialog alertDialog) {
		m_pictureDialog.set(alertDialog);
	}

	private void showPictureDialog() {
		if (m_pictureDialog.get() != null) {
			m_pictureDialog.get().show();
		}
	}

	private void dismissPictureDialog() {
		if (m_pictureDialog.get() != null && m_pictureDialog.get().isShowing())
			m_pictureDialog.get().dismiss();
	}

	public OnClickListener getPictureDialogYesClickListener() {
		return new PictureDialogYesClickListener();
	}

	public OnClickListener getPictureDialogSkipClickListener() {
		return new PictureDialogSkipClickListener();
	}

	public class PictureDialogYesClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof ShowingPictureDialogState) {
				((ShowingPictureDialogState) m_currentState).exitState(true);
			}
		}
	}

	public class PictureDialogSkipClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentState instanceof ShowingPictureDialogState) {
				((ShowingPictureDialogState) m_currentState).exitState(false);
			}
		}
	}

	public void setNoTimeLeftDialog(AlertDialog alertDialog) {
		m_noTimeLeftDialog.set(alertDialog);
	}

	private void showNoTimeLeftDialog() {
		if (m_noTimeLeftDialog.get() != null) {
			fragment.setNoTimeTitleText(getNoTimeTitleText());
			m_noTimeLeftDialog.get().show();
		}
	}

	private void dismissNoTimeLeftDialog() {
		if (m_noTimeLeftDialog.get() != null && m_noTimeLeftDialog.get().isShowing())
			m_noTimeLeftDialog.get().dismiss();
	}

	public void setStaffOverrideEntryDialog(AlertDialog alertDialog) {
		m_staffOverrideEntryDialog.set(alertDialog);
	}

	private void showStaffOverrideEntryDialog() {
		if (m_staffOverrideEntryDialog.get() != null) {
			m_staffOverrideEntryDialog.get().show();
		}
	}

	public void dismissStaffOverrideEntryDialog() {
		if (m_staffOverrideEntryDialog != null && m_staffOverrideEntryDialog.get().isShowing())
			m_staffOverrideEntryDialog.get().dismiss();
	}

	public void setSuccessfulOverrideDialog(AlertDialog alertDialog) {
		m_successfulOverrideDialog.set(alertDialog);
	}

	private void showSuccessfulOverrideDialog() {
		if (m_successfulOverrideDialog.get() != null) {
			m_successfulOverrideDialog.get().show();
		}
	}

	public void dismissSuccessfulOverrideDialog() {
		if (m_successfulOverrideDialog != null && m_successfulOverrideDialog.get().isShowing())
			m_successfulOverrideDialog.get().dismiss();
	}

	public void setNoGuestDaysLeftDialog(AlertDialog alertDialog) {
		m_noGuestDaysLeftDialog.set(alertDialog);
	}

	private void showNoGuestDaysLeftDialog() {
		if (m_noGuestDaysLeftDialog.get() != null) {
			m_noGuestDaysLeftDialog.get().show();
		}
	}

	private void dismissNoGuestDaysLeftDialog() {
		if (m_noGuestDaysLeftDialog.get() != null && m_noGuestDaysLeftDialog.get().isShowing())
			m_noGuestDaysLeftDialog.get().dismiss();
	}

	public OnClickListener getNoTimeLeftDialogReselectClickListener() {
		return new NoTimeLeftDialogReselectClickListener();
	}

	public OnClickListener getNoTimeLeftDialogHomeClickListener() {
		return new NoTimeLeftDialogHomeClickListener();
	}

	public OnClickListener getNoTimeLeftDialogOverrideClickListener() {
		return new NoTimeLeftDialogOverrideClickListener();
	}

	public class NoTimeLeftDialogReselectClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoTimeLeftDialog();
		}
	}

	public class NoTimeLeftDialogHomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoTimeLeftDialog();
			fragment.firstScreen();
		}
	}

	public class NoTimeLeftDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoTimeLeftDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public String getTimeBreakdownText() {
		String breakdownText = "";
		List<Member> childrenWIthNoTime = new ArrayList<>();
		childrenWIthNoTime.addAll(m_childrenWithNoWeeklyTime);
		childrenWIthNoTime.addAll(m_childrenWithNoDailyTime);
		for (Member child : childrenWIthNoTime)
			breakdownText = breakdownText + child.getFirstName() + " " + child.getLastName() + "\n";
		return breakdownText;
	}
	
	public String getNoTimeTitleText() {
		String titleType = "";
		if (!m_childrenWithNoWeeklyTime.isEmpty())
			titleType = "Weekly";
		else
			titleType = "Daily";
		return titleType;
	}

	public OnClickListener getNoGuestDaysLeftDialogReselectClickListener() {
		return new NoGuestDaysLeftDialogReselectClickListener();
	}

	public OnClickListener getNoGuestDaysLeftDialogHomeClickListener() {
		return new NoGuestDaysLeftDialogHomeClickListener();
	}

	public OnClickListener getNoGuestDaysLeftDialogOverrideClickListener() {
		return new NoGuestDaysLeftDialogOverrideClickListener();
	}

	public class NoGuestDaysLeftDialogReselectClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoGuestDaysLeftDialog();
		}
	}

	public class NoGuestDaysLeftDialogHomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoGuestDaysLeftDialog();
			fragment.firstScreen();
		}
	}

	public class NoGuestDaysLeftDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoGuestDaysLeftDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public String getGuestBreakdownText() {
		String breakdownText = "";
		for (Member child : m_childrenWithNoGuestDays)
			breakdownText = breakdownText + child.getFirstName() + " " + child.getLastName() + "\n";
		return breakdownText;
	}

	public OnClickListener getStaffOverrideEntryDialogEnterClickListener() {
		return new StaffOverrideEntryDialogEnterClickListener();
	}

	public OnClickListener getStaffOverrideEntryDialogCancelClickListener() {
		return new StaffOverrideEntryDialogCancelClickListener();
	}

	public class StaffOverrideEntryDialogEnterClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_facility.get().getOverridePIN().equals(fragment.getOverridePIN())) {
				dismissStaffOverrideEntryDialog();
				showSuccessfulOverrideDialog();
			} else {
				fragment.setOverridePIN("");
				fragment.setOverridePINHint("Invalid PIN, Please Retry");
			}
		}
	}

	public class StaffOverrideEntryDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissStaffOverrideEntryDialog();
		}
	}

	public OnClickListener getSuccessfulOverrideDialogOkClickListener() {
		return new SuccessfulOverrideDialogOkClickListener();
	}

	public class SuccessfulOverrideDialogOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getPersistenceLayer().getPersistenceData().setIsOverride(true);
			dismissSuccessfulOverrideDialog();
			if (!getPersistenceLayer().getPersistenceData().getAssignedRooms().isEmpty()) {
				m_assignedRooms.get().addAll(getPersistenceLayer().getPersistenceData().getAssignedRooms());
				for (Room room : m_assignedRooms.get()) {
					for (Member child : m_selectedChildren.get()) {
						if (DaycareHelpers.getIsAgeBetween(child.getBirthDate(), room.getMinAge().doubleValue(),
								room.getMaxAge().doubleValue())) {
							child.setPRoom(room);
						}
					}
				}
			}
			m_currentState = new FetchQuestionsState(m_facility.get(), m_familyCubbies.get(), m_selectedChildren.get());
			m_currentState.enterState();
		}
	}

	public void setRoomsAreFullDialog(AlertDialog alertDialog) {
		m_roomsAreFullDialog.set(alertDialog);
	}

	private void showRoomsAreFullDialog() {
		if (m_roomsAreFullDialog.get() != null) {
			m_roomsAreFullDialog.get().show();
		}
	}

	private void dismissRoomsAreFullDialog() {
		if (m_roomsAreFullDialog.get() != null && m_roomsAreFullDialog.get().isShowing())
			m_roomsAreFullDialog.get().dismiss();
	}

	public OnClickListener getRoomsAreFullDialogReselectClickListener() {
		return new RoomsAreFullDialogReselectClickListener();
	}

	public OnClickListener getRoomsAreFullDialogHomeClickListener() {
		return new RoomsAreFullDialogHomeClickListener();
	}

	public OnClickListener getRoomsAreFullDialogOverrideClickListener() {
		return new RoomsAreFullDialogOverrideClickListener();
	}

	public class RoomsAreFullDialogReselectClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			roomsAreFull = false;
			dismissRoomsAreFullDialog();
			m_currentState = new AwaitingSelectionState();
			m_currentState.enterState();
		}
	}

	public class RoomsAreFullDialogHomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			roomsAreFull = false;
			dismissRoomsAreFullDialog();
			fragment.firstScreen();
		}
	}

	public class RoomsAreFullDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissRoomsAreFullDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public String getRoomsFullBreakdownText() {
		String breakdownText = "";
		for (Member child : m_noRoomForChildren.get())
			breakdownText = breakdownText + child.getFirstName() + " " + child.getLastName() + "\n";
		return breakdownText;
	}

	public void setNoKidsForCheckDirectionDialog(AlertDialog alertDialog) {
		m_noKidsForCheckDirectionDialog.set(alertDialog);
	}

	private void dismissNoKidsForCheckDirectionDialog() {
		if (m_noKidsForCheckDirectionDialog.get() != null && m_noKidsForCheckDirectionDialog.get().isShowing())
			m_noKidsForCheckDirectionDialog.get().dismiss();
	}

	public OnClickListener getNoKidsForCheckDirectionClickListener() {
		return new NoKidsForCheckDirectionClickListener();
	}

	public class NoKidsForCheckDirectionClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissNoKidsForCheckDirectionDialog();
			// fragment.getFragmentActivity().gotoPinScreen();
		}
	}

    public void setQuestionDialog(AlertDialog alertDialog) {
        m_questionDialog.set(alertDialog);
    }

    private void showQuestionDialog() {
        if (m_questionDialog.get() != null) {
            m_questionDialog.get().show();
        }
    }

    private void dismissQuestionDialog() {
        if (m_questionDialog.get() != null && m_questionDialog.get().isShowing())
            m_questionDialog.get().dismiss();
    }

    public OnClickListener getQuestionDialogAcceptClickListener() {
        return new QuestionDialogAcceptClickListener();
    }

    public class QuestionDialogAcceptClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
        	if (m_currentQuestion.getType().equals("Consent")) {
				m_currentMember.get().setAcknowledgedConsent(true);
				getPersistenceLayer().saveConsentRecord(m_currentMember.get(), m_facility.get());
			} else if (m_currentQuestion.getType().equals("Notice")) {
				m_currentMember.get().setAcknowledgedNotice(true);
			}
			dismissQuestionDialog();
			m_currentState.exitState();
        }
    }

    public OnClickListener getQuestionDialogCancelClickListener() {
        return new QuestionDialogCancelClickListener();
    }

    public class QuestionDialogCancelClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            dismissQuestionDialog();
            m_currentState = new AwaitingSelectionState();
            m_currentState.enterState();
        }
    }

	public void setTimesAlmostUpDialog(AlertDialog alertDialog) {
		m_timesAlmostUpDialog.set(alertDialog);
	}

	private void showTimesAlmostUpDialog() {
		if (m_timesAlmostUpDialog.get() != null) {
			fragment.setTimesAlmostUpTitleText(getTimesAlmostUpTitleText());
			m_timesAlmostUpDialog.get().show();
		}
	}

	private void dismissTimesAlmostUpDialog() {
		if (m_timesAlmostUpDialog.get() != null && m_timesAlmostUpDialog.get().isShowing())
			m_timesAlmostUpDialog.get().dismiss();
	}

	public OnClickListener getTimesAlmostUpDialogOkClickListener() {
		return new TimesAlmostUpOkClickListener();
	}

	public class TimesAlmostUpOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissTimesAlmostUpDialog();
			m_currentState = new FetchQuestionsState(m_facility.get(), m_familyCubbies.get(), m_selectedChildren.get());
			m_currentState.enterState();
		}
	}

	public String getMinutesLeftBreakdownText() {
		String breakdownText = fragment.getDefaultMinutesLeftText();
		List<Integer> timesLeft = new ArrayList<Integer>();
		for (Member child : m_childrenWithLittleWeeklyTime) {
			timesLeft.add(child.getWeeklyMinutesLeft().intValue());
		}
		for (Member child : m_childrenWithLittleDailyTime) {
			timesLeft.add(child.getMinutesLeft().intValue());
		}
		int min = Collections.min(timesLeft);
		return breakdownText.replace("[MIN]", Integer.toString(min));
	}
	
	public String getTimesAlmostUpTitleText() {
		String titleType = "";
		if (!m_childrenWithLittleWeeklyTime.isEmpty() && !m_childrenWithLittleDailyTime.isEmpty()) {
			List<Integer> dailyTimesLeft = new ArrayList<Integer>();
			List<Integer> weeklyTimesLeft = new ArrayList<Integer>();
			for (Member dailyChild : m_childrenWithLittleDailyTime)
				dailyTimesLeft.add(dailyChild.getMinutesLeft().intValue());
			for (Member weeklyChild : m_childrenWithLittleWeeklyTime)
				weeklyTimesLeft.add(weeklyChild.getWeeklyMinutesLeft().intValue());
			int dailyMin = Collections.min(dailyTimesLeft);
			int weeklyMin = Collections.min(weeklyTimesLeft);
			if (dailyMin <= weeklyMin)
				titleType = "Day";
			else
				titleType = "Week";
		} else if (!m_childrenWithLittleWeeklyTime.isEmpty())
			titleType = "Week";
		else
			titleType = "Day";
		return titleType;
	}

	public void setCapacityExceededDialog(AlertDialog alertDialog) {
		m_capacityExceededDialog.set(alertDialog);
	}

	private void showCapacityExceededDialog() {
		if (m_capacityExceededDialog.get() != null) {
			String breakdownText = "'";
			if (m_facility.get().getRatioType().equals("FACILITYANDROOM")) {
				breakdownText = fragment.getDefaultFacilityAndRoomCapacityExceededText();
				fragment.setCapacityExceededBreakdownText(breakdownText);
			} else {
				breakdownText = fragment.getDefaultCapacityExceededText();
				if (m_openSpots < 0)
					m_openSpots = 0;
				if (m_openSpots == 1) {
					breakdownText += "s";
					breakdownText = breakdownText.replace("spots", "spot");
				}
				String composedBreakdownText = breakdownText.replace("[SPOTS]", Integer.toString(m_openSpots));
				fragment.setCapacityExceededBreakdownText(composedBreakdownText);
			}
			m_capacityExceededDialog.get().show();
		}
	}

	private void showCapacityExceededDialog(Map<String, Integer> availableRooms) {
		if (m_capacityExceededDialog.get() != null) {
			String toReturn = "";
			Iterator it = availableRooms.entrySet().iterator();
			while (it.hasNext()) {
				String breakdownText = fragment.getDefaultRoomCapacityExceededText();
				Map.Entry pair = (Map.Entry) it.next();
				if (pair.getValue().toString().equals("1")) {
					breakdownText = breakdownText.replace("remain", "remains");
					breakdownText = breakdownText.replace("spots", "spot");
				}
				String composedBreakdownText = breakdownText.replace("[SPOTS]", pair.getValue().toString());
				composedBreakdownText = composedBreakdownText.replace("[ROOM]", pair.getKey().toString());
				toReturn += composedBreakdownText + "\n";
				it.remove();
			}
			fragment.setCapacityExceededBreakdownText(toReturn);
			m_capacityExceededDialog.get().show();
		}
	}

	private void dismissCapacityExceededDialog() {
		if (m_capacityExceededDialog.get() != null && m_capacityExceededDialog.get().isShowing())
			m_capacityExceededDialog.get().dismiss();
	}

	public OnClickListener getCapacityExceededDialogReselectClickListener() {
		return new CapacityExceededDialogReselectClickListener();
	}

	public OnClickListener getCapacityExceededDialogHomeClickListener() {
		return new CapacityExceededDialogHomeClickListener();
	}

	public OnClickListener getCapacityExceededDialogOverrideClickListener() {
		return new CapacityExceededDialogOverrideClickListener();
	}

	public class CapacityExceededDialogReselectClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissCapacityExceededDialog();
			m_currentState = new AwaitingSelectionState();
			m_currentState.enterState();
		}
	}

	public class CapacityExceededDialogHomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissCapacityExceededDialog();
			fragment.firstScreen();
		}
	}

	public class CapacityExceededDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissCapacityExceededDialog();
			showStaffOverrideEntryDialog();
		}
	}

	public String getCapacityExceededBreakdownText() {
		String breakdownText = fragment.getDefaultCapacityExceededText();
		return breakdownText.replace("[SPOTS]", Integer.toString(m_openSpots));
	}

	public void setFreeAndPaidTImeDialog(AlertDialog alertDialog) {
		m_freeAndPaidTimeDialog.set(alertDialog);
	}

	private void showFreeAndPaidTImeDialog() {
		if (m_freeAndPaidTimeDialog.get() != null) {
			m_freeAndPaidTimeDialog.get().show();
		}
	}

	private void dismissFreeAndPaidTImeDialog() {
		if (m_freeAndPaidTimeDialog.get() != null && m_freeAndPaidTimeDialog.get().isShowing())
			m_freeAndPaidTimeDialog.get().dismiss();
	}
	
	public OnClickListener getFreeAndPaidTImeDialogOkClickListener() {
		return new FreeAndPaidTImeOkClickListener();
	}

	public class FreeAndPaidTImeOkClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissFreeAndPaidTImeDialog();
			m_currentState = new FetchQuestionsState(m_facility.get(), m_familyCubbies.get(), m_selectedChildren.get());
			m_currentState.enterState();
		}
	}
	
	public String getFreeAndPaidTimeLeftBreakdownText() {
		String toReturn = "";
		List<Member> m_childrenWithLittleTime = new ArrayList<>();
		for (Member dailyChild : m_childrenWithLittleDailyTime) {
			if (!m_childrenWithLittleTime.contains(dailyChild))
				m_childrenWithLittleTime.add(dailyChild);
		}
		for (Member weeklyChild : m_childrenWithLittleWeeklyTime) {
			if (!m_childrenWithLittleTime.contains(weeklyChild))
				m_childrenWithLittleTime.add(weeklyChild);
		}
		for (Member child : m_childrenWithLittleTime) {
			String breakdownText = fragment.getDefaultFreeAndPaidTimeLeftText();
			String name = DaycareHelpers.formatName(child.getFirstName() + " " + child.getLastName()) + ": ";
			if (child.getMinutesLeft().intValue() > 0) {
				Number paidMinsLeft = child.getWeeklyMinutesLeft().intValue() - child.getMinutesLeft().intValue();
				breakdownText = breakdownText.replace("[FREEMIN]", child.getMinutesLeft().toString());
				breakdownText = breakdownText.replace("[PAIDMIN]", paidMinsLeft.toString());
			} else {
				breakdownText = breakdownText.replace("[FREEMIN]", (child.getMinutesLeft().intValue() < 0) ? "0" : child.getMinutesLeft().toString());
				breakdownText = breakdownText.replace("[PAIDMIN]", child.getWeeklyMinutesLeft().toString());
			}
			toReturn = toReturn + name + "\n" + breakdownText + "\n\n";
		}
		return toReturn;
	}
	
	@Override
	public void onInitialize() {
		isShowingProxyChildren = new TrackableBoolean();
		isProxyButtonVisible = new TrackableBoolean();
		isProxyButtonInlayout = new TrackableBoolean();
		isAllSelected = new TrackableBoolean();
		m_visibleChildrenModels = new TrackableField<TrackableCollection<MemberCheckBoxModel>>(
				new TrackableCollection<MemberCheckBoxModel>());
		m_relatedChildrenModels = new TrackableField<TrackableCollection<MemberCheckBoxModel>>(
				new TrackableCollection<MemberCheckBoxModel>());
		m_proxyChildrenModels = new TrackableField<TrackableCollection<MemberCheckBoxModel>>(
				new TrackableCollection<MemberCheckBoxModel>());
		m_selectedChildren = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		m_facility = new TrackableField<Facility>();
		m_familyCubbies = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		m_currentMember = new TrackableField<Member>();
		m_pictureDialog = new TrackableField<AlertDialog>();
		m_noTimeLeftDialog = new TrackableField<AlertDialog>();
		m_staffOverrideEntryDialog = new TrackableField<AlertDialog>();
		m_successfulOverrideDialog = new TrackableField<AlertDialog>();
		m_noGuestDaysLeftDialog = new TrackableField<AlertDialog>();
		m_childrenWithNoDailyTime = new TrackableCollection<Member>();
		m_childrenWithNoWeeklyTime = new TrackableCollection<Member>();
		m_childrenWithNoGuestDays = new TrackableCollection<Member>();
		m_facilityRooms = new TrackableField<TrackableCollection<Room>>(new TrackableCollection<Room>());
		roomsAreFull = false;
		m_roomsAreFullDialog = new TrackableField<AlertDialog>();
		m_noRoomForChildren = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		m_assignedRooms = new TrackableField<TrackableCollection<Room>>(new TrackableCollection<Room>());
		m_noKidsForCheckDirectionDialog = new TrackableField<AlertDialog>();
        m_questionDialog = new TrackableField<AlertDialog>();
		m_multiRoomChildren = new ArrayList<Member>();
		m_timesAlmostUpDialog = new TrackableField<AlertDialog>();
		m_childrenWithLittleDailyTime = new TrackableCollection<Member>();
		m_childrenWithLittleWeeklyTime = new TrackableCollection<Member>();
		m_selectedProgram = new TrackableField<Program>();
		m_capacityExceededDialog = new TrackableField<AlertDialog>();
		m_freeAndPaidTimeDialog = new TrackableField<AlertDialog>();
		m_selectableChildrenLimitDialog = new TrackableField<AlertDialog>();
		m_selectCanSelfCheckInChildren = new TrackableCollection<SelectSelfCheckInDialogItemModel>();
		m_selectSelfCheckInDialog = new TrackableField<AlertDialog>();
		m_sliderQuestionChildren = new TrackableCollection<SliderQuestionDialogItemModel>();
		m_sliderQuestionDialog = new TrackableField<AlertDialog>();
		addWatch(CHANGE_ID.RELATEDINOUTCHILDREN);
		addWatch(CHANGE_ID.VALIDPROXYINOUTCHILDREN);
	}

	abstract class State {
		public abstract void enterState();

		public void exitState() {
			m_currentState = null;
		}

		public void exception(Exception e) {
			m_currentState = null;
		}
	}

	private class AwaitingSelectionState extends State {
		@Override
		public void enterState() {
			Log.w("AwaitingSelectionState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("AwaitingSelectionState", "ExitState");
			List<Member> children = m_selectedChildren.get();
			getPersistenceLayer().getPersistenceData().setSelectedChildren(children);
			if (!m_selectedChildren.get().isEmpty()) {
				if (getPersistenceLayer().getPersistenceData().getIsRegistration()) {
					if (m_facility.get().getSelfCheckInOpts() != null) {
						try {
							if (m_facility.get().getSelfCheckInOpts().getString("type").equals("prompted")) {
								m_currentState = new CheckCanCheckSelfInState();
								m_currentState.enterState();
							} else {
								m_currentState = new AssignRoomsState(m_facility.get(), children, m_familyCubbies.get());
								m_currentState.enterState();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					} else {
						m_currentState = new AssignRoomsState(m_facility.get(), children, m_familyCubbies.get());
						m_currentState.enterState();
					}
				} else {
					m_currentState = new AssignRoomsState(m_facility.get(), children, m_familyCubbies.get());
					m_currentState.enterState();
				}
			} else {
				m_currentState.exception(new Exception("No Children Selected"));
			}
		}

		@Override
		public void exception(Exception e) {
			Log.w("AwaitingSelectionState", "Exception: " + e.getMessage());
			reset();
		}
	}

	private String checkTimeRemaining(List<Member> children) {
		m_childrenWithNoDailyTime.clear();
		m_childrenWithNoWeeklyTime.clear();
		m_childrenWithNoGuestDays.clear();
		m_childrenWithLittleDailyTime.clear();
		m_childrenWithLittleWeeklyTime.clear();
		String currentDate = DateGenerator.getFormattedDate(DateGenerator.getCurrentDate());
		DateTime lastSunday = DaycareHelpers.getLastSunday();
		for (Member child : children) {
			int minutesLeft = child.getMinutesLeft().intValue();
			DateTime lastCheckInDate = new DateTime(child.getLastCheckIn());
			if (child.isGuest() && child.getGuestDaysLeft().intValue() <= 0)
				m_childrenWithNoGuestDays.add(child);
			else if (null != child.getLastCheckIn() && lastCheckInDate.isAfter(lastSunday)) {
				if (m_selectedProgram.get().getUsesWeeklyMinutes()) {
					int weeklyMinutesLeft = child.getWeeklyMinutesLeft().intValue();
					if (weeklyMinutesLeft <= 0) {
						m_childrenWithNoWeeklyTime.add(child);
					} else if (weeklyMinutesLeft <= 120 && weeklyMinutesLeft > 0)
						m_childrenWithLittleWeeklyTime.add(child);
				}
				if (null != child.getLastCheckIn()
						&& DateGenerator.getFormattedDate(child.getLastCheckIn()).compareTo(currentDate) == 0) {
					if (minutesLeft <= 0) {
						m_childrenWithNoDailyTime.add(child);
					} else if (m_selectedProgram.get().getTimeLimit().intValue() != 0) {
						if (minutesLeft < m_selectedProgram.get().getTimeLimit().intValue() && minutesLeft > 0)
							m_childrenWithLittleDailyTime.add(child);
					} else if (child.getPRoom().getTimeLimit().intValue() != 0) {
						if (minutesLeft < child.getPRoom().getTimeLimit().intValue() && minutesLeft > 0)
							m_childrenWithLittleDailyTime.add(child);
					}
				}

			} else if (null != child.getLastCheckIn()
					&& DateGenerator.getFormattedDate(child.getLastCheckIn()).compareTo(currentDate) == 0) {
				if (minutesLeft <= 0) {
					m_childrenWithNoDailyTime.add(child);
				} else if (m_selectedProgram.get().getTimeLimit().intValue() != 0) {
					if (minutesLeft < m_selectedProgram.get().getTimeLimit().intValue() && minutesLeft > 0)
						m_childrenWithLittleDailyTime.add(child);
				} else if (child.getPRoom().getTimeLimit().intValue() != 0) {
					if (minutesLeft < child.getPRoom().getTimeLimit().intValue() && minutesLeft > 0)
						m_childrenWithLittleDailyTime.add(child);
				}
			}
		}
		if (m_facility.get().getUsesTimeTracking()) {
			if (!m_childrenWithNoDailyTime.isEmpty() || !m_childrenWithNoWeeklyTime.isEmpty())
				return "NO_TIME";
			else if (!m_childrenWithNoGuestDays.isEmpty())
				return "NO_GUEST_DAYS";
			else if (!m_childrenWithLittleDailyTime.isEmpty() || !m_childrenWithLittleWeeklyTime.isEmpty())
				return "LITTLE_TIME";
			else
				return "AVAILABLE";
		} else {
			if (!m_childrenWithNoGuestDays.isEmpty())
				return "NO_GUEST_DAYS";
			else
				return "AVAILABLE";
		}
	}

	private class DisplayQuestionState extends State {

		private Question m_question;
		JSONObject m_choice = new JSONObject();

		public DisplayQuestionState(Question q) {
			m_question = q;
			m_currentQuestion = q;
			try {
				m_choice = (JSONObject) q.getChoices().get(0);
				m_questionTitleText = m_question.getTitle();
				if (m_question.getType().equals("Consent")) {
					m_questionBodyText = m_choice.getString("body");
					fragment.setQuestionTitleText(m_questionTitleText);
					fragment.setQuestionBodyText(m_questionBodyText);
				} else if (m_question.getType().equals("Diaper")) {
					fragment.setSliderQuestionTitleText(m_questionTitleText);
				} else if (m_question.getType().equals("Feeding")) {
					fragment.setSliderQuestionTitleText(m_questionTitleText);
				} else if (m_question.getType().equals("Enrichment")) {
					fragment.setSliderQuestionTitleText(m_questionTitleText);
				} else {
					m_questionBodyText = m_choice.getString("body");
					fragment.setQuestionTitleText(m_questionTitleText);
					fragment.setQuestionBodyText(m_questionBodyText);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void enterState() {
			Log.w("QuestionState", "EnterState");
			if (m_question.getType().equals("Consent")) {
				if (!m_currentMember.get().getAcknowledgedConsent())
					showQuestionDialog();
				else
					exitState();
			} else if (m_question.getType().equals("Notice")) {
				if (!m_currentMember.get().getAcknowledgedNotice())
					showQuestionDialog();
				else
					exitState();
			} else if (m_question.getType().equals("Diaper")) {
				m_sliderQuestionChildren.clear();
				for (Member mem : m_selectedChildren.get()) {
					try {
						if (DaycareHelpers.getAgeInYears(mem.getBirthDate()) >= m_choice.getInt("minAge") &&
								DaycareHelpers.getAgeInYears(mem.getBirthDate()) <= m_choice.getInt("maxAge") && mem.getAllowsDiaperChange() == null) {
							m_sliderQuestionChildren.add(new SliderQuestionDialogItemModel(mem));
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				if (!m_sliderQuestionChildren.isEmpty())
					showSliderQuestionDialog();
				else
					exitState();
			} else if (m_question.getType().equals("Feeding")) {
				m_sliderQuestionChildren.clear();
				for (Member mem : m_selectedChildren.get()) {
					try {
						if (DaycareHelpers.getAgeInYears(mem.getBirthDate()) >= m_choice.getInt("minAge") &&
								DaycareHelpers.getAgeInYears(mem.getBirthDate()) <= m_choice.getInt("maxAge") && mem.getAllowsFeeding() == null) {
							m_sliderQuestionChildren.add(new SliderQuestionDialogItemModel(mem));
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				if (!m_sliderQuestionChildren.isEmpty())
					showSliderQuestionDialog();
				else
					exitState();
			} else if (m_question.getType().equals("Enrichment")) {
				m_sliderQuestionChildren.clear();
				for (Member mem : m_selectedChildren.get()) {
					try {
						if (DaycareHelpers.getAgeInYears(mem.getBirthDate()) >= m_choice.getInt("minAge") &&
								DaycareHelpers.getAgeInYears(mem.getBirthDate()) <= m_choice.getInt("maxAge") && mem.getAllowsEnrichment() == null) {
							m_sliderQuestionChildren.add(new SliderQuestionDialogItemModel(mem));
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				if (!m_sliderQuestionChildren.isEmpty())
					showSliderQuestionDialog();
				else
					exitState();
			} else {
				showQuestionDialog();
			}
		}

		@Override
		public void exitState() {
			Log.w("QuestionState", "ExitState");

			if (!m_popupQuestionsToDisplay.isEmpty()) {
				m_currentState = new DisplayQuestionState(m_popupQuestionsToDisplay.get(0));
				m_popupQuestionsToDisplay.remove(0);
				m_currentState.enterState();
			} else {
				m_currentState = new FetchCubbiesState(m_facility.get(), m_familyCubbies.get());
				m_currentState.enterState();
			}
		}

		@Override
		public void exception(Exception e) {
			Log.w("QuestionState", "Exception: " + e.getMessage());
		}
	}
	
	private class AssignRoomsState extends State {
		private List<Member> m_children;
		private Facility m_facility;
		private List<Cubby> m_familyCubbies;
		private boolean m_isUpdate;

		public AssignRoomsState(Facility facility, List<Member> children, List<Cubby> familyCubbies) {
			m_facility = facility;
			m_familyCubbies = familyCubbies;
			m_children = children;
		}

		public AssignRoomsState(Facility facility, List<Member> children, List<Cubby> familyCubbies, boolean isUpdate) {
			m_facility = facility;
			m_familyCubbies = familyCubbies;
			m_children = children;
			m_isUpdate = isUpdate;
		}

		@Override
		public void enterState() {
			Log.w("AssignRoomsState", "EnterState");
			m_noRoomForChildren.get().clear();
			m_multiRoomChildren.clear();
			m_assignedRooms.get().clear();
			m_assignedRooms.get().addAll(getPersistenceLayer().getPersistenceData().getAssignedRooms());
																											
			for (Member child : m_selectedChildren.get()) {
				child.setInProgram(m_selectedProgram.get());
			}
			getPersistenceLayer().fetchProgramRooms(m_selectedProgram.get(),
					new FetchProgramRoomsCommandListener(m_children, m_isUpdate));
		}

		@Override
		public void exitState() {
			Log.w("AssignRoomsState", "ExitState");
            m_currentState = new FetchQuestionsState(m_facility, m_familyCubbies, m_children);
			m_currentState.enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("AssignRoomsState", "Exception: " + e.getMessage());
		}
	}

    private class FetchQuestionsState extends State {
        private Facility m_facility;
        private List<Cubby> m_familyCubbies;
        private List<Member> m_children;

        public FetchQuestionsState(Facility facility, List<Cubby> familyCubbies, List<Member> children) {
            m_facility = facility;
            m_familyCubbies = familyCubbies;
            m_children = children;
        }

        @Override
        public void enterState() {
            Log.w("FetchingQuestionsState", "EnterState");
            getPersistenceLayer().fetchFacilityQuestions(m_facility, new FetchFacilityQuestionsCommandListener());
        }

        public void exitState(List<Question> availableQuestions) {
			Log.w("FetchingQuestionsState", "ExitState");
			m_popupQuestionsToDisplay = new ArrayList<Question>(getPersistenceLayer().getPersistenceData().getFacilityPopupQuestions());
			if (availableQuestions.size() == 1) {
				if (!m_popupQuestionsToDisplay.isEmpty()) {
					m_currentState = new DisplayQuestionState(m_popupQuestionsToDisplay.get(0));
					m_popupQuestionsToDisplay.remove(0);
					m_currentState.enterState();
				} else {
					// fetch capacity, then cubbies then go to full screen question
					m_currentState = new CheckCapacityState(m_facility, m_children, m_familyCubbies);
		            m_currentState.enterState();
				}
			} else if (availableQuestions.size() == 0) {
				m_currentState = new CheckCapacityState(m_facility, m_children, m_familyCubbies);
                m_currentState.enterState();
			} else if (availableQuestions.size() > 1) {
				if (!m_popupQuestionsToDisplay.isEmpty()) {
					m_currentState = new DisplayQuestionState(m_popupQuestionsToDisplay.get(0));
					m_popupQuestionsToDisplay.remove(0);
					m_currentState.enterState();
				} else {
					// fetch capacity, then cubbies then go to full screen question
					m_currentState = new CheckCapacityState(m_facility, m_children, m_familyCubbies);
					m_currentState.enterState();
				}
			}
		}

        @Override
        public void exception(Exception e) {
            Log.w("FetchingQuestionsState", "Exception: " + e.getMessage());
            reset();
        }
    }

	private class FetchCubbiesState extends State {
		private Facility m_facility;
		private List<Cubby> m_familyCubbies;

		public FetchCubbiesState(Facility facility, List<Cubby> familyCubbies) {
			m_facility = facility;
			m_familyCubbies = familyCubbies;
		}

		@Override
		public void enterState() {
			Log.w("FetchingCubbiesState", "EnterState");
			getPersistenceLayer().fetchAvailableCubbies(m_facility, new FetchAvailableCubbiesCommandListener());
		}

		public void exitState(final List<Cubby> availableCubbies) {
            Log.w("FetchingCubbiesState", "ExitState");
			m_availableCubbies = availableCubbies;
			if (m_facility.getLocationId() != null) {
                boolean hasUnsignedWaiver = false;
                for (Member child : getPersistenceLayer().getPersistenceData().getRelatedInChildren()) {
                    if (child.getDocuments() == null)
                        hasUnsignedWaiver = true;
                }

                String resellerId = fragment.getResources().getString(R.string.YOMBU_RESELLER_ID);
                String apiKey = fragment.getResources().getString(R.string.YOMBU_API_KEY);
                String merchantId = fragment.getResources().getString(R.string.YOMBU_MERCHANT_ID);
                String locationId = m_facility.getLocationId();

                if (hasUnsignedWaiver) {
                    YombuWaiver.getInstance().initialize(resellerId, apiKey, merchantId, locationId,
                            new YombuInitializationCallback() {
                                @Override
                                public void onInitializationSuccess() {
                                    try {
                                        YombuWaiver.getInstance()
                                                .setName(m_currentMember.get().getFirstName(), m_currentMember.get().getLastName())
                                                .setPhone(m_currentMember.get().getUserPIN().get(0).toString())
                                                .setEmail(m_currentMember.get().getEmail());
                                        for (Member child : getPersistenceLayer().getPersistenceData().getRelatedInChildren()) {
                                            YombuWaiver.getInstance().addMinor(child.getFirstName(), child.getLastName(), child.getBirthDate());
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    YombuWaiver.getInstance().displayWaiver(new YombuWaiverProcessingCallback() {
                                        @Override
                                        public void onWaiverSuccess(final String documentId) {
                                            getPersistenceLayer().fetchWaiver(documentId, fragment.getResources().getString(R.string.YOMBU_ACCESS_TOKEN), new YombuRESTOperationComplete() {
                                                @Override
                                                public void operationComplete(Object data, YombuRESTException e) {
                                                    m_waiverContents = ((YombuWaiverResponse) data).getContents();
                                                    byte[] encodedByte = Base64.decode(m_waiverContents, Base64.DEFAULT);
                                                    final ParseFile pdf = new ParseFile("waiver.pdf", encodedByte, "pdf");
                                                    pdf.saveInBackground(new SaveCallback() {
                                                        @Override
                                                        public void done(ParseException e) {
                                                            Document doc = getPersistenceLayer().getNewEmptyDocument();
                                                            doc.setDocumentId(documentId);
                                                            doc.setFile(pdf);
                                                            getPersistenceLayer().saveDocument(doc, new SaveDocumentListener());
                                                        }
                                                    });
                                                }
                                            });
                                        }

                                        @Override
                                        public void onWaiverFailure(String message) {
                                            Log.i("Yombu", "FAILED");
                                        }
                                    });
                                }

                                @Override
                                public void onInitializationFailure(String s) {
                                    Log.i("Yombu", "INIT FAILED");
                                }
                            }
                    );
                } else {
                    boolean hasFullScreenQuestion = false;
                    if (!getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().isEmpty()) {
                        if (getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().get(0).getType().equals("Bag")) {
                            for (Member child : m_selectedChildren.get()) {
                                if (DaycareHelpers.getAgeInYears(child.getBirthDate()) < 4) {
                                    hasFullScreenQuestion = true;
                                }
                            }
                        } else if (getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().get(0).getType().equals("Pager")) {
                            hasFullScreenQuestion = true;
                        }
                    }
                    if (hasFullScreenQuestion)
                        fragment.nextScreen("QUESTION");
                    else if (!availableCubbies.isEmpty() || !m_familyCubbies.isEmpty()) {
                        fragment.nextScreen("CUBBIES");
                    } else if (!m_multiRoomChildren.isEmpty())
                        fragment.nextScreen("ROOMS");
                    else if (m_currentMember.get().getObjectId().equals(m_selectedChildren.get().get(0).getObjectId()))
                        fragment.nextScreen("CONFIRM");
                    else
                        fragment.nextScreen("WORKOUT");
                    m_currentState = new FinishedState();
                    m_currentState.exitState();
                }
            } else {
                boolean hasFullScreenQuestion = false;
                if (!getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().isEmpty()) {
                    if (getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().get(0).getType().equals("Bag")) {
                        for (Member child : m_selectedChildren.get()) {
                            if (DaycareHelpers.getAgeInYears(child.getBirthDate()) < 4) {
                                hasFullScreenQuestion = true;
                            }
                        }
                    } else if (getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().get(0).getType().equals("Pager")) {
                        hasFullScreenQuestion = true;
                    }
                }
                if (hasFullScreenQuestion)
                    fragment.nextScreen("QUESTION");
                else if (!availableCubbies.isEmpty() || !m_familyCubbies.isEmpty()) {
                    fragment.nextScreen("CUBBIES");
                } else if (!m_multiRoomChildren.isEmpty())
                    fragment.nextScreen("ROOMS");
                else if (m_currentMember.get().getObjectId().equals(m_selectedChildren.get().get(0).getObjectId()))
                    fragment.nextScreen("CONFIRM");
                else
                    fragment.nextScreen("WORKOUT");
                m_currentState = new FinishedState();
                m_currentState.exitState();
            }

			/*getPersistenceLayer().fetchWaiver("", "", new YombuRESTOperationComplete() {
				@Override
				public void operationComplete(Object data, YombuRESTException e) {
					String contents = ((YombuWaiverResponse) data).getContents();
				}
			});*/

			/*JSONObject a = new JSONObject();
			getPersistenceLayer().pushExternalDataDocument("", a, new ExternalDataOperationComplete<String>() {
				@Override
				public void DataReceived(String s, ExternalDataException e) {
					String result = s;
				}
			});*/
		}

		@Override
		public void exception(Exception e) {
			Log.w("FetchingCubbiesState", "Exception: " + e.getMessage());
			reset();
		}
	}

	private class ShowingPictureDialogState extends State {
		private List<Cubby> m_familyCubbies;
		private Facility m_facility;
		private List<Member> m_children;

		public ShowingPictureDialogState(Facility facilty, List<Member> children, List<Cubby> cubbies) {
			m_facility = facilty;
			m_familyCubbies = cubbies;
			m_children = children;
		}

		@Override
		public void enterState() {
			Log.w("ShowingDialogState", "EnterState");
			showPictureDialog();
		}

		public void exitState(boolean isUpdate) {
			Log.w("ShowingDialogState", "ExitState");
			dismissPictureDialog();
			if (isUpdate) {
				m_currentState = new AssignRoomsState(m_facility, m_children, m_familyCubbies, true);
			} else {
				m_currentState = new AssignRoomsState(m_facility, m_children, m_familyCubbies);
			}
			m_currentState.enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("ShowingDialogState", "Exception: " + e.getMessage());
		}
	}

	private class CheckCapacityState extends State {
		private List<Member> m_children;
		private Facility m_facility;
		private List<Cubby> m_familyCubbies;

		public CheckCapacityState(Facility facility, List<Member> children, List<Cubby> familyCubbies) {
			m_facility = facility;
			m_familyCubbies = familyCubbies;
			m_children = children;
		}

		@Override
		public void enterState() {
			Log.w("CheckCapacityState", "EnterState");
			if (m_facility.getRatioType() != null) {
				if (m_facility.getRatioType().equals("FACILITYANDROOM"))
					getPersistenceLayer().fetchCheckedInMembers(new FetchCheckedInMembersListener());
				else if (m_facility.getRatioType().equals("ROOM"))
					getPersistenceLayer().fetchCheckedInMembers(new FetchCheckedInMembersListener());
				else
					getPersistenceLayer().fetchCheckedInMembersCount(new FetchCheckedInMembersCountListener());
			} else if (m_facility.getRatio().intValue() != 0)
				getPersistenceLayer().fetchCheckedInMembersCount(new FetchCheckedInMembersCountListener());
			else
				getPersistenceLayer().fetchCheckedInMembers(new FetchCheckedInMembersListener());
		}

		@Override
		public void exitState() {
			Log.w("CheckCapacityState", "ExitState");
			m_currentState = new FetchCubbiesState(m_facility, m_familyCubbies);
			m_currentState.enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("CheckCapacityState", "Exception: " + e.getMessage());
		}
	}

	private class CheckCanCheckSelfInState extends State {

		@Override
		public void enterState() {
			Log.w("CheckCanCheckSelfInState", "EnterState");
			m_selectCanSelfCheckInChildren.clear();
			for (Member mem : getPersistenceLayer().getPersistenceData().getRelatedInChildren())
				m_selectCanSelfCheckInChildren.add(new SelectSelfCheckInDialogItemModel(mem, 100));
			showSelectSelfCheckInDialog();
		}

		@Override
		public void exitState() {
			Log.w("CheckCanCheckSelfInState", "ExitState");
			List<Member> allowedToSelfCheckIn = new ArrayList<Member>();
			for (SelectSelfCheckInDialogItemModel model : m_selectCanSelfCheckInChildren) {
				if (model.getCanSelfCheckIn())
					allowedToSelfCheckIn.add(model.getMember());
			}
			getPersistenceLayer().getPersistenceData().setAllowedSelfCheckInMembers(allowedToSelfCheckIn);
			m_currentState = new AssignRoomsState(m_facility.get(), m_selectedChildren.get(), m_familyCubbies.get());
			m_currentState.enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("CheckCanCheckSelfInState", "Exception: " + e.getMessage());
		}
	}
		private class FinishedState extends State {
		@Override
		public void enterState() {
			Log.w("FinishedState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("FinishedState", "ExitState");
		}

		@Override
		public void exception(Exception e) {
			Log.w("FinishedState", "Exception: " + e.getMessage());
		}
	}

	private class FetchAvailableCubbiesCommandListener implements CommandStatusListener {
		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void valueChanged(Command<?> command) {
			if (m_currentState instanceof FetchCubbiesState)
				((FetchCubbiesState) m_currentState).exitState((List<Cubby>) command.getResult());
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class FetchProgramRoomsCommandListener implements CommandStatusListener {
		private List<Member> m_children;
		private boolean m_isUpdate;

		public FetchProgramRoomsCommandListener(List<Member> children, boolean isUpdate) {
			m_children = children;
			m_isUpdate = isUpdate;
		}

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void valueChanged(Command<?> command) {
			List<Room> rooms = (List<Room>) command.getResult();

			List<Room> qualifyingRooms = new ArrayList<Room>();
			if (!rooms.isEmpty()) {
				for (Member child : m_children) {
					for (Room room : rooms) {
						if (DaycareHelpers.getIsAgeBetween(child.getBirthDate(), room.getMinAge().doubleValue(),
								room.getMaxAge().doubleValue())) {
							qualifyingRooms.add(room);
						}
					}
					if (qualifyingRooms.size() == 1) {
						child.setPRoom(qualifyingRooms.get(0));
					} else if (qualifyingRooms.size() > 1) {
						m_multiRoomChildren.add(child);
					}
					qualifyingRooms.clear();
				}
				getPersistenceLayer().getPersistenceData().setMultiRoomChildren(m_multiRoomChildren);
			}

			if (getPersistenceLayer().getPersistenceData().getIsOverride()) {
				m_currentState.exitState();
			} else {
				String options = checkTimeRemaining(m_children);
				if (options.equals("NO_TIME")) {
					showNoTimeLeftDialog();
				} else if (options.equals("LITTLE_TIME") && m_facility.get().getUsesTimeTracking()) {
					showTimesAlmostUpDialog();
				} else if (options.equals("NO_GUEST_DAYS")) {
					showNoGuestDaysLeftDialog();
				} else {
					String dataSource = getPersistenceLayer().getPersistenceData().getDataSource();
					if (dataSource.equals("ABC_EOS")) {
						int selectionMax = 10;
						String service = "";
						String addOn = "";
						if (m_currentMember.get().getService() != null)
							service = m_currentMember.get().getService();
						if (m_currentMember.get().getAddOn() != null)
							addOn = m_currentMember.get().getAddOn();

						if (service.equals("-Childcare UpTo1") || addOn.equals("CCARE01")) {
							selectionMax = 1;
						} else if (service.equals("-Childcare UpTo2") || addOn.equals("CCARE02")) {
							selectionMax = 2;
						} else if (service.equals("-Childcare") || addOn.equals("CHILDCARE") ||
								service.equals("-Childcare UpTo3")) {
							selectionMax = 3;
						} else if (service.equals("-Childcare UpTo4") || addOn.equals("CHILDCARE1")) {
							selectionMax = 4;
						} else if (addOn.equals("CHILDCARE2")) {
							selectionMax = 5;
						}

						if (m_selectedChildren.get().size() > selectionMax) {
							showSelectableChildrenLimitDialog();
						} else {
							if (m_isUpdate && !roomsAreFull) {
								fragment.nextScreen("UPDATE");
								m_currentState = new FinishedState();
							} else if (roomsAreFull) {
								showRoomsAreFullDialog();
							} else
								m_currentState.exitState();
						}
					} else {
						if (m_isUpdate && !roomsAreFull) {
							fragment.nextScreen("UPDATE");
							m_currentState = new FinishedState();
						} else if (roomsAreFull) {
							showRoomsAreFullDialog();
						} else
							m_currentState.exitState();
					}
				}
			}
			/*if (m_isUpdate && !roomsAreFull) {
				fragment.nextScreen("UPDATE");
				m_currentState = new FinishedState();
			} else if (roomsAreFull) {
				showRoomsAreFullDialog();
			} else
				m_currentState.exitState();*/
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class FetchCheckedInMembersCountListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			m_checkedInMembersCount = (Integer) command.getResult();
			getPersistenceLayer().fetchClockedInStaff(new FetchClockedInStaffListener());
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class FetchClockedInStaffListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {
		}

		@SuppressWarnings("unchecked")
		@Override
		public void finished(Command<?> command) {
			List<Staff> clockedInStaff = (List<Staff>) command.getResult();
			Integer clockedInStaffNum = clockedInStaff.size();
			Facility facility = getPersistenceLayer().getPersistenceData().getFacility();
			Integer facilityRatio = facility.getRatio().intValue();
			List<Room> allRooms = getPersistenceLayer().getPersistenceData().getAllRooms();
			
			if (getPersistenceLayer().getPersistenceData().getIsOverride())
				m_currentState.exitState();
			else if (m_facility.get().getRatioType() != null) {
				if (m_facility.get().getRatioType().equals("FACILITYANDROOM")) {
					int maxCapacity = getPersistenceLayer().getPersistenceData().getFacility().getMaxCapacity().intValue();
					int currentMaxCapacity = m_facility.get().getRatio().intValue() * clockedInStaffNum;
					double availableCapacity = 0;
					boolean capExceeded = false;
					if (getPersistenceLayer().getPersistenceData().getFacility().getMaxCapacityType() != null)
						if (getPersistenceLayer().getPersistenceData().getFacility().getMaxCapacityType().equals("FACILITY") && currentMaxCapacity > maxCapacity) {
							availableCapacity = maxCapacity;
						} else {
							availableCapacity = currentMaxCapacity;
						}
					else
						availableCapacity = currentMaxCapacity;
					for (Room r : allRooms) {
						int checkedIntoRoomCount = 0;
						for (Member m : m_checkedInMembers) {
							if (m.getPRoom().equals(r)) {
								checkedIntoRoomCount++;
							}
						}
						availableCapacity = availableCapacity - (checkedIntoRoomCount * r.getCapacityMultiplier().doubleValue());
					}
					for (Member m : m_selectedChildren.get()) {
						if (m.getPRoom() != null) {
							availableCapacity= availableCapacity - m.getPRoom().getCapacityMultiplier().doubleValue();
							if (availableCapacity < 0)
								capExceeded = true;
						}
					}
					if (capExceeded)
						showCapacityExceededDialog();
					else
						m_currentState.exitState();
				} else if (m_facility.get().getRatioType().equals("ROOM")) {
					Map<String, Integer> roomAvailability = new HashMap<>();
					for (Room r : allRooms) {
						int checkedIntoRoomCount = 0;
						int clockedInStaffToRoom = 0;
						for (Member m : m_checkedInMembers) {
							if (m.getPRoom().equals(r))
								checkedIntoRoomCount++;
						}
						for (Staff s : clockedInStaff) {
							if (s.getPRoom().equals(r))
								clockedInStaffToRoom++;
						}
						List<Member> memberSelectedForRoom = new ArrayList<>();
						for (Member m : m_selectedChildren.get()) {
							if (m.getPRoom() != null)
								if (m.getPRoom().equals(r))
									memberSelectedForRoom.add(m);
						}
						int openRoomSpots = 0;
						openRoomSpots = (r.getRatio().intValue() * clockedInStaffToRoom) - checkedIntoRoomCount;
						if (memberSelectedForRoom.size() > openRoomSpots)
							roomAvailability.put(r.getName(), openRoomSpots);
					}
					if (!roomAvailability.isEmpty())
						showCapacityExceededDialog(roomAvailability);
					else
						m_currentState.exitState();
				} else if (m_facility.get().getRatioType().equals("FACILITY")) {
					m_openSpots = (clockedInStaffNum * facilityRatio) - m_checkedInMembersCount;
					if (m_checkedInMembersCount < clockedInStaffNum * facilityRatio) {
						if (m_selectedChildren.get().size() > m_openSpots) {
							showCapacityExceededDialog();
						} else {
							m_currentState.exitState();
						}
					} else {
						m_openSpots = 0;
						showCapacityExceededDialog();
					}
				}
			} else if (facilityRatio != 0) {
				m_openSpots = (clockedInStaffNum * facilityRatio) - m_checkedInMembersCount;
				if (m_checkedInMembersCount < clockedInStaffNum * facilityRatio) {
					if (m_selectedChildren.get().size() > m_openSpots) {
						showCapacityExceededDialog();
					} else {
						m_currentState.exitState();
					}
				} else {
					m_openSpots = 0;
					showCapacityExceededDialog();
				}
			} else if (facility.getMaxCapacityType() != null) {
				if (facility.getMaxCapacityType().equals("FACILITY")) {
					if (m_checkedInMembers.size() + m_selectedChildren.get().size() > facility.getMaxCapacity()
							.intValue()) {
						m_openSpots = facility.getMaxCapacity().intValue() - m_checkedInMembers.size();
						showCapacityExceededDialog();
					} else
						m_currentState.exitState();
				} else if (facility.getMaxCapacityType().equals("PROGRAM") || facility.getMaxCapacityType().equals("ROOM")) {
					int checkedIntoProgramCount = 0;
					Program selectedProgram = getPersistenceLayer().getPersistenceData().getSelectedProgram();
					for (Member m : m_checkedInMembers) {
						if (m.getInProgram().equals(selectedProgram))
							checkedIntoProgramCount++;
					}
					if (checkedIntoProgramCount + m_selectedChildren.get().size() > selectedProgram.getMaxCapacity()
							.intValue()) {
						m_openSpots = selectedProgram.getMaxCapacity().intValue() - checkedIntoProgramCount;
						showCapacityExceededDialog();
					} else
						m_currentState.exitState();
				}
			} else if (m_facility.get().getRatioType() == null) {
				m_currentState.exitState();
			} else {
				m_currentState.exitState();
			}
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class FetchCheckedInMembersListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			m_checkedInMembers = (List<Member>) command.getResult();
			getPersistenceLayer().fetchClockedInStaff(new FetchClockedInStaffListener());
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

    private class FetchFacilityQuestionsCommandListener implements CommandStatusListener {
        @Override
        public void starting(Command<?> command) {
        }

        @Override
        public void finished(Command<?> command) {
        }

        @SuppressWarnings("unchecked")
        @Override
        public void valueChanged(Command<?> command) {
            if (m_currentState instanceof FetchQuestionsState)
                ((FetchQuestionsState) m_currentState).exitState((List<Question>) command.getResult());
        }

        @Override
        public void exception(Command<?> command, Exception e) {
        }
    }

	public int getTitleTextRes() {
		if (m_visibleChildrenModels.get().size() == 1)
			return m_singleChildTitleRes;
		else
			return m_multiChildTitleRes;
	}

	public void setSelectableChildrenLimitDialog(AlertDialog alertDialog) {
		m_selectableChildrenLimitDialog.set(alertDialog);
	}

	private void showSelectableChildrenLimitDialog() {
		if (m_selectableChildrenLimitDialog.get() != null) {
			m_selectableChildrenLimitDialog.get().show();
		}
	}

	private void dismissSelectableChildrenLimitDialog() {
		if (m_selectableChildrenLimitDialog.get() != null && m_selectableChildrenLimitDialog.get().isShowing())
			m_selectableChildrenLimitDialog.get().dismiss();
	}

	public String getSelectableChildrenLimitText() {
		String breakdownText = fragment.getDefaultSelectableChildrenLimitText();
		int selectionMax = 0;
		String service = "";
		String addOn = "";
		if (m_currentMember.get().getService() != null)
			service = m_currentMember.get().getService();
		if (m_currentMember.get().getAddOn() != null)
			addOn = m_currentMember.get().getAddOn();

		if (service.equals("-Childcare") || addOn.equals("CHILDCARE") ||
				service.equals("-Childcare UpTo3")) {
			selectionMax = 3;
		} else if (service.equals("-Childcare UpTo1") || addOn.equals("CCARE01")) {
			selectionMax = 1;
		} else if (service.equals("-Childcare UpTo2") || addOn.equals("CCARE02")) {
			selectionMax = 2;
		} else if (service.equals("-Childcare UpTo4") || addOn.equals("CHILDCARE1")) {
			selectionMax = 4;
		} else if (addOn.equals("CHILDCARE2")) {
			selectionMax = 5;
		}
		return breakdownText.replace("[NUM]", Integer.toString(selectionMax));
	}

	public OnClickListener getSelectableChildrenLimitDialogReselectClickListener() {
		return new SelectableChildrenLimitDialogReselectClickListener();
	}

	public OnClickListener getSelectableChildrenLimitDialogHomeClickListener() {
		return new SelectableChildrenLimitDialogHomeClickListener();
	}

	public OnClickListener getSelectableChildrenLimitDialogOverrideClickListener() {
		return new SelectableChildrenLimitDialogOverrideClickListener();
	}

	public class SelectableChildrenLimitDialogReselectClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissSelectableChildrenLimitDialog();
		}
	}

	public class SelectableChildrenLimitDialogHomeClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissSelectableChildrenLimitDialog();
			fragment.firstScreen();
		}
	}

	public class SelectableChildrenLimitDialogOverrideClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissSelectableChildrenLimitDialog();
			showStaffOverrideEntryDialog();
		}
	}

	private class SaveDocumentListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
			Document d = (Document) command.getResult();
			List<Document> documents = new ArrayList<>();
			documents.add(d);
			if (d != null) {
				for (Member child : getPersistenceLayer().getPersistenceData().getRelatedInChildren()) {
					child.setDocuments(documents);
				}
				getPersistenceLayer().updateAllMembers(getPersistenceLayer().getPersistenceData().getRelatedInChildren(), new SaveAllMembersListener());
			}

		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	private class SaveAllMembersListener implements CommandStatusListener {

		@Override
		public void starting(Command<?> command) {
		}

		@Override
		public void finished(Command<?> command) {
            getPersistenceLayer().pushExternalDataDocument(m_currentMember.get().getMemberID(), m_waiverContents, new ExternalDataOperationComplete<String>() {
                @Override
                public void DataReceived(String s, ExternalDataException e) {
                    boolean diaperBagChild = false;
                    if (!getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().isEmpty()) {
                        if (getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().get(0).getType().equals("Bag")) {
                            for (Member child : m_selectedChildren.get()) {
                                if (DaycareHelpers.getAgeInYears(child.getBirthDate()) < 4) {
                                    diaperBagChild = true;
                                }
                            }
                        }
                    }
                    if (diaperBagChild)
                        fragment.nextScreen("QUESTION");
                    else if (m_availableCubbies.isEmpty() || !m_familyCubbies.get().isEmpty()) {
                        fragment.nextScreen("CUBBIES");
                    } else if (!m_multiRoomChildren.isEmpty())
                        fragment.nextScreen("ROOMS");
                    else if (m_currentMember.get().getObjectId().equals(m_selectedChildren.get().get(0).getObjectId()))
                        fragment.nextScreen("CONFIRM");
                    else
                        fragment.nextScreen("WORKOUT");
                    m_currentState = new FinishedState();
                    m_currentState.exitState();
                }
            });
		}

		@Override
		public void valueChanged(Command<?> command) {
		}

		@Override
		public void exception(Command<?> command, Exception e) {
		}
	}

	public TrackableCollection<SelectSelfCheckInDialogItemModel> getSelectCanSelfCheckInChildren() {
		return m_selectCanSelfCheckInChildren;
	}

	public void setSelectSelfCheckInDialog(AlertDialog alertDialog) {
		m_selectSelfCheckInDialog.set(alertDialog);
	}

	private void showSelectSelfCheckInDialog() {
		if (m_selectSelfCheckInDialog.get() != null) {
			m_selectSelfCheckInDialog.get().show();
		}
	}

	private void dismissSelectSelfCheckInDialog() {
		if (m_selectSelfCheckInDialog.get() != null && m_selectSelfCheckInDialog.get().isShowing())
			m_selectSelfCheckInDialog.get().dismiss();
	}

	public OnClickListener getSelectSelfCheckInDialogConfirmClickListener() {
		return new SelectSelfCheckInDialogConfirmClickListener();
	}

	public class SelectSelfCheckInDialogConfirmClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissSelectSelfCheckInDialog();
			m_currentState.exitState();
		}
	}

	public OnClickListener getSelectSelfCheckInDialogCancelClickListener() {
		return new SelectSelfCheckInDialogCancelClickListener();
	}

	public class SelectSelfCheckInDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissSelectSelfCheckInDialog();
			m_currentState = new AwaitingSelectionState();
			m_currentState.enterState();
		}
	}

	public TrackableCollection<SliderQuestionDialogItemModel> getSliderQuestionChildren() {
		return m_sliderQuestionChildren;
	}

	public void setSliderQuestionDialog(AlertDialog alertDialog) {
		m_sliderQuestionDialog.set(alertDialog);
	}

	private void showSliderQuestionDialog() {
		if (m_sliderQuestionDialog.get() != null) {
			m_sliderQuestionDialog.get().show();
		}
	}

	private void dismissSliderQuestionDialog() {
		if (m_sliderQuestionDialog.get() != null && m_sliderQuestionDialog.get().isShowing())
			m_sliderQuestionDialog.get().dismiss();
	}

	public OnClickListener getSliderQuestionDialogConfirmClickListener() {
		return new SliderQuestionDialogConfirmClickListener();
	}

	public class SliderQuestionDialogConfirmClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (m_currentQuestion.getType().equals("Diaper")) {
				for (SliderQuestionDialogItemModel model : m_sliderQuestionChildren) {
					model.getMember().setAllowsDiaperChange(model.getSliderState());
				}
			} else if (m_currentQuestion.getType().equals("Feeding")) {
				for (SliderQuestionDialogItemModel model : m_sliderQuestionChildren) {
					model.getMember().setAllowsFeeding(model.getSliderState());
				}
			} else if (m_currentQuestion.getType().equals("Enrichment")) {
				for (SliderQuestionDialogItemModel model : m_sliderQuestionChildren) {
					model.getMember().setAllowsEnrichment(model.getSliderState());
				}
			}
			dismissSliderQuestionDialog();
			m_currentState.exitState();
		}
	}

	public OnClickListener getSliderQuestionDialogCancelClickListener() {
		return new SliderQuestionDialogCancelClickListener();
	}

	public class SliderQuestionDialogCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			dismissSliderQuestionDialog();
			m_currentState = new AwaitingSelectionState();
			m_currentState.enterState();
		}
	}

}
