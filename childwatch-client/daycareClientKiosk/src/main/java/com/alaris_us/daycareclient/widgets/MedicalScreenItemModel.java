package com.alaris_us.daycareclient.widgets;

import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareclient.models.MedicalScreenViewModel.SetForAllClickListener;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;


public class MedicalScreenItemModel {

	private TrackableField<Member> m_member;
	private TrackableField<String> m_itemMedicalMessage;

	private Integer m_height;
	private Integer m_width;
	private Integer m_color;
	
	private TrackableBoolean m_isSelected;
	private ClearClickListener m_clearListener;
	private SetForAllClickListener m_setForAllListener;


	public MedicalScreenItemModel() {
		this(null, null, null, null);
	}

	public MedicalScreenItemModel(Member member, Integer width, Integer color, SetForAllClickListener setForAllListener) {
		m_member = new TrackableField<Member>(member);
		m_itemMedicalMessage = new TrackableField<String>("");
		m_width = width;
		m_color = color;
		m_isSelected = new TrackableBoolean(false);
		m_clearListener = new ClearClickListener();
		m_setForAllListener = setForAllListener;
		extractDataFromMember(member);
	}
	
	private void extractDataFromMember(Member member) {
		String medConcerns = member.getMedicalConcerns();
		if (medConcerns != null && !medConcerns.isEmpty()) {
			m_itemMedicalMessage.set(medConcerns);
		}
	}

	public Member getMember() {
		return m_member.get();
	}

	public void setMember(Member member) {
		m_member.set(member);
	}

	public String getMemberName() {
		return m_member.get().getFirstName() + " " + m_member.get().getLastName();
	}

	public void setItemMedicalMessage(String msg){
		m_itemMedicalMessage.set(msg);
	}
	
	public String getItemMedicalMessage() {
		String msg=m_itemMedicalMessage.get();
		return msg.isEmpty()? null : msg;
	}
	
	public int getMemberBG() {
		return m_color;
	}

	public int getHeight() {
		return m_height;
	}
	
	public int getWidth() {
		return m_width;
	}
	
	public ClearClickListener getClearClickListener() {
		return m_clearListener;
	}
	public SetForAllClickListener getSetForAllClickListener() {
		return m_setForAllListener;
	}

	public void setSelected(boolean isSelected) {
		m_isSelected.set(isSelected);
	}
	
	public boolean getIsSelected() {
		return m_isSelected.get();
	}
	
	public class ClearClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			m_itemMedicalMessage.set("");
		}
	}
}
