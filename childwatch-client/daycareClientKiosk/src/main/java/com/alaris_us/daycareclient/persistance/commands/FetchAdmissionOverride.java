package com.alaris_us.daycareclient.persistance.commands;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FetchAdmissionOverride extends Command<Boolean> {

	private List<Member> m_members;
	private Facility m_facility;

	public FetchAdmissionOverride(List<Member> members, Facility facility) {
		this(members, facility, null, null);
	}

	public FetchAdmissionOverride(List<Member> members, Facility facility, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_members = members;
		m_facility = facility;
	}

	@Override
	public void executeSource() {
		List<String> memberObjIds = new ArrayList<>();
		for (Member m : m_members) {
			memberObjIds.add(m.getObjectId());
		}
		m_dataSource.getAdmissionsDAO().calculateAdmissionOverride(memberObjIds, m_facility.getObjectId(), this);
	}

	public void handleResponse(Boolean result) {

	}
}
