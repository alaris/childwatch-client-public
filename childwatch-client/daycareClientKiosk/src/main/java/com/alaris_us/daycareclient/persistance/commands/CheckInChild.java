package com.alaris_us.daycareclient.persistance.commands;

import java.util.List;

import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData;
import com.alaris_us.daycareclient.utils.DateGenerator;
import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycaredata.to.WorkoutArea;

public class CheckInChild extends Command<Record> {

	private final Facility m_facility;
	private final Member m_child;
	private final Member m_parent;
	private final SignInType m_signInType;
	private final List<WorkoutArea> m_workoutAreas;
	private final String m_service;
	private final String m_language;

	public CheckInChild(Facility facility, Member child, Member parent, SignInType signInType, List<WorkoutArea> workoutAreas, String service, String language) {
		this(facility, child, parent, signInType, workoutAreas, service, language, null, null);
	};

	public CheckInChild(Facility facility, Member child, Member parent, SignInType signInType, List<WorkoutArea> workoutAreas, String service, String language, DaycareData dataSource, PersistenceData model) {
		super(dataSource, model);
		m_facility = facility;
		m_child = child;
		m_parent = parent;
		m_signInType=signInType;
		m_workoutAreas = workoutAreas;
		m_service = service;
		m_language = language;
	}

	@Override
	public void executeSource() {
		m_dataSource.getMembersDAO().checkInMember(m_child, m_parent, m_facility, DateGenerator.getCurrentDate(), m_signInType, new CheckInChildCommandListener(this));
	}

	@Override
	public void handleResponse(Record record) {
		
	}
	
	private class CheckInChildCommandListener implements DaycareOperationComplete<Member> {
		private CheckInChild m_listener;
		
		public CheckInChildCommandListener(CheckInChild listener){
			m_listener=listener;
		}

		@Override
		public void operationComplete(Member arg0, DaycareDataException arg1) {
			m_dataSource.getRecordsDAO().saveCheckInEvent(m_child, m_facility, m_parent, m_signInType, m_workoutAreas, null, m_service, m_language, m_listener);
			
		}
	}

}