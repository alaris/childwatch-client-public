package com.alaris_us.daycareclient.yombu;

import android.os.Handler;

import com.alaris_us.daycaredata.util.ConnectivityUtility;

import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class YombuDAO {
    protected final String mYombuBaseUrl;
    protected ConnectivityUtility mConnectivityUtility;

    public YombuDAO(String yombuuBaseUrl, ConnectivityUtility connectivityUtility) {
        mYombuBaseUrl = yombuuBaseUrl;
        mConnectivityUtility = connectivityUtility;
    }

    protected void runYombuRESTTask(YombuREST.RequestType reqType, String urlPath,
                                     YombuRESTOperationComplete<YombuDataObject> callback) {
        runYombuRESTTask(reqType, urlPath, null, null, callback);
    }

    protected void runYombuRESTTask(YombuREST.RequestType reqType, String urlPath, Map<String, Object> body, JSONObject headers,
                                     YombuRESTOperationComplete<YombuDataObject> callback) {
        //if (mConnectivityUtility.isInternetConnected()) {
            String url = mYombuBaseUrl + "/" + urlPath;
            YombuREST YombuREST = new YombuREST(reqType, url, body, headers);
            YombuRESTTaskRunner runner = new YombuRESTTaskRunner(YombuREST, callback);
            runner.start();
        /*} else {
            callback.operationComplete(null, new YombuRESTException(599, "No Internet Connected"));
        }*/
    }

    private class YombuRESTTaskRunner implements Runnable {
        private final static long DEFAULT_DELAY = 1000;
        private final static int MAX_RETRY = 15;
        private long mDelay;
        private Handler mHandler;
        private AtomicInteger mCount;
        private YombuREST mTask;
        private YombuRESTOperationComplete<YombuDataObject> mCallback;

        public YombuRESTTaskRunner(YombuREST YombuREST, YombuRESTOperationComplete<YombuDataObject> callback) {
            this(YombuREST, DEFAULT_DELAY, callback);
        }

        public YombuRESTTaskRunner(YombuREST YombuREST, long delay,
                                    YombuRESTOperationComplete<YombuDataObject> callback) {
            mHandler = new Handler();
            mCount = new AtomicInteger(0);
            mTask = YombuREST;
            mDelay = delay;
            mCallback = callback;
        }

        public void start() {
            long delay = mCount.get() == 0 ? 0 : mDelay;
            mHandler = new Handler(mHandler.getLooper());
            mHandler.postDelayed(this, delay);
        }

        @Override
        public void run() {
            mTask = mCount.get() == 0 ? mTask : mTask.clone();
            mTask.execute(new RetryCallback());
        }

        private class RetryCallback implements YombuRESTOperationComplete<YombuDataObject> {
            @Override
            public void operationComplete(YombuDataObject data, YombuRESTException e) {
                if (e != null) {
                    if (e.getCode() == 404 && mCount.incrementAndGet() <= MAX_RETRY)
                        start();
                    else
                        mCallback.operationComplete(null, e);
                } else {
                    mCallback.operationComplete(data, null);
                }
            }
        }
    }
}
