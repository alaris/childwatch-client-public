package com.alaris_us.daycareclient.persistence;

import android.util.Log;

import com.alaris_us.daycareclient.persistence.Command.CommandStatusListener;
import com.alaris_us.daycaredata.DaycareData;

public class PersistenceCommandLoggingInvoker
	extends Invoker {

	protected PersistenceCommandLoggingInvoker(DaycareData dataSource, PersistenceData model,
			InvokerStatusListener listener) {

		super(dataSource, model, listener);
	}

	private final String	TAG	= this.getClass().getSimpleName();

	@Override
	public void invoke(
			Command<?> command,
			CommandStatusListener listener) {

		Log.d(TAG, "Command Invoked: " + String.valueOf(command));
		super.invoke(command, listener);
	}

	@Override
	public void starting(
			Command<?> command) {

		Log.d(TAG, "Starting: " + String.valueOf(command));
		Log.d(TAG, "Has Result: " + String.valueOf(command.getResult()));
		super.starting(command);
	}

	@Override
	public void finished(
			Command<?> command) {

		Log.d(TAG, "Completed: " + String.valueOf(command));
		Log.d(TAG, "Has Result: " + String.valueOf(command.getResult()));
		super.finished(command);
	}

	@Override
	public void valueChanged(
			Command<?> command) {

		Log.d(TAG, "Value Changed: " + String.valueOf(command));
		Log.d(TAG, "Has Result: " + String.valueOf(command.getResult()));
		super.valueChanged(command);
	}

	@Override
	public void exception(
			Command<?> command,
			Exception e) {

		Log.d(TAG, "Error: " + String.valueOf(command));
		Log.d(TAG, "Has Result: " + String.valueOf(command.getResult()));
		Log.d(TAG, "With Exception: " + String.valueOf(e));
		super.exception(command, e);
	}

}
