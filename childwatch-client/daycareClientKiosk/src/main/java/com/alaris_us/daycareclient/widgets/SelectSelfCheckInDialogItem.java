package com.alaris_us.daycareclient.widgets;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class SelectSelfCheckInDialogItem extends LinearLayout implements BoundUi<SelectSelfCheckInDialogItemModel>{

	private SelectSelfCheckInDialogItemModel m_data;

	private View m_layout;

	public SelectSelfCheckInDialogItem(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public SelectSelfCheckInDialogItem(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		m_layout = View.inflate(getContext(), R.layout.view_selectselfcheckindialog_item, this);
	}

	public Member getMember() {
		return m_data.getMember();
	}

	public SelectSelfCheckInDialogItemModel getModel() {
		return m_data;
	}

	public void setHeight(int height){
		ListViewFunctions.setTileHeight(this, height);
	}

	@Override
	public void bind(SelectSelfCheckInDialogItemModel dataSource) {
		m_data = dataSource;
		UiBinder.bind(m_layout, R.id.view_selectselfcheckindialog_item_name, "Text", m_data, "MemberName", BindingMode.ONE_WAY);
		//UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Height")), m_data, "Height", BindingMode.ONE_WAY);
		UiBinder.bind(m_layout, R.id.view_selectselfcheckindialog_item_switch, "IsChecked", m_data, "CanSelfCheckIn", BindingMode.TWO_WAY);
	}
}
