package com.alaris_us.daycareclient.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alaris_us.daycareclient.utils.ListViewFunctions;
import com.alaris_us.daycareclient_dev.R;
import com.alaris_us.daycaredata.to.Member;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class MemberSelectRoom extends RelativeLayout implements BoundUi<MemberSelectRoomModel>{

	private MemberSelectRoomModel m_data;
	
	private ImageView m_memberImage;
	private TextView m_memberName;
	private View m_layout;
	
	public MemberSelectRoom(Context context, AttributeSet attr) {
		super(context, attr);
		initLayout(context);
	}

	public MemberSelectRoom(Context context) {
		super(context);
		initLayout(context);
	}

	private void initLayout(Context context) {
		m_layout = View.inflate(getContext(), R.layout.view_listitem_selectroom, this);
		
		m_memberImage = (ImageView) findViewById(R.id.view_listitem_image);
		m_memberName = (TextView) findViewById(R.id.view_listitem_name);
	}
	
	public Member getMember() {
		return m_data.getMember();
	}
	
	public MemberSelectRoomModel getModel() {
		return m_data; 
	}

	public void setHeight(int height){
		ListViewFunctions.setTileHeight(this, height);
	}
	
	public void setWidth(int width){
		ListViewFunctions.setTileWidth(this, width);
	}
	
	@Override
	public void bind(MemberSelectRoomModel dataSource) {
		m_data = dataSource;
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberName, "Text")), m_data, "MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberImage, "ImageBitmap")), m_data, "MemberImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_memberImage, "BackgroundColor")), m_data, "BackgroundColor", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Width")), m_data, "Width", BindingMode.ONE_WAY);
		//Select Room Spinner
		UiBinder.bind(m_layout, R.id.selectroom_spinner, "OnItemSelectedListener", m_data, "SelectRoomNameListOnItemSelectedListener");
		UiBinder.bind(m_layout, R.id.selectroom_spinner, "Adapter", m_data, "SelectedRoomNameList", new AdapterConverter(
				SelectRoomDropDown.class));
		UiBinder.bind(m_layout, R.id.selectroom_spinner, "Selection", m_data, "DefaultSelection");
		UiBinder.bind(m_layout, R.id.selectroom_spinner, "Enabled", m_data, "Enabled");
		UiBinder.bind(m_layout, R.id.spinner_divider, "Visibility", m_data, "Visibility");
	}
}
