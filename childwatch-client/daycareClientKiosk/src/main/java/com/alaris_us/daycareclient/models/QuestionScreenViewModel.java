package com.alaris_us.daycareclient.models;

import java.util.Arrays;
import java.util.List;

import com.alaris_us.daycareclient.fragments.QuestionScreenFragment;
import com.alaris_us.daycareclient.persistence.Command;
import com.alaris_us.daycareclient.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareclient.persistence.PersistenceLayer;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Question;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

import android.view.View;
import android.view.View.OnClickListener;

public class QuestionScreenViewModel extends ViewModel {
	public QuestionScreenViewModel(PersistenceLayer persistenceLayer, QuestionScreenFragment fragment) {
		super(persistenceLayer);
		this.fragment = fragment;
	}

	private QuestionScreenFragment fragment;
	private TrackableField<TrackableCollection<Cubby>>	m_allCubbies;
	private TrackableField<TrackableCollection<Cubby>>	m_availableCubbies;
	private TrackableField<TrackableCollection<Cubby>>	m_familyCubbies;
	private TrackableCollection<Pager>	m_availablePagers;
	private TrackableCollection<Pager>	m_familyPagers;
	private TrackableField<Facility> m_facility;
	private Question m_question;
	private TrackableField<Family> m_family;
	private TrackableField<Member> m_proxy;
	private TrackableCollection<Member> m_children;

	@Override
	protected void onPopulateModelData() {
		m_allCubbies.get().clear();
		m_availableCubbies.get().clear();
		m_familyCubbies.get().clear();
		m_availablePagers.clear();
		m_familyPagers.clear();
		m_allCubbies.get().addAll(getPersistenceLayer().getPersistenceData().getAllCubbies());
		m_availableCubbies.get().addAll(getPersistenceLayer().getPersistenceData().getAvailableCubbies());
		m_familyCubbies.get().addAll(getPersistenceLayer().getPersistenceData().getFamilyCubbies());
		m_facility.set(getPersistenceLayer().getPersistenceData().getFacility());
		m_question = getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().get(0);
		m_family.set(getPersistenceLayer().getPersistenceData().getPrimaryFamily());
		m_proxy.set(getPersistenceLayer().getPersistenceData().getCurrentMember());
		m_children.addAll(getPersistenceLayer().getPersistenceData().getSelectedDaycareChildren());
	}

	@Override
	public void onPersistModel() {
	}

	public class YesClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			List<Member> children = getPersistenceLayer().getPersistenceData().getSelectedDaycareChildren();
			if (m_question.getType().equals("Bag")) {
				for (Member child : children) {
					child.setHasBag(true);
				}
			} else if (m_question.getType().equals("Pager")) {
				getPersistenceLayer().fetchAvailablePagers(m_facility.get(), new FetchAvailablePagers());
			}

			if (!getPersistenceLayer().getPersistenceData().getAvailableCubbies().isEmpty())
				fragment.nextScreen("CUBBY");
			else if (!getPersistenceLayer().getPersistenceData().getMultiRoomChildren().isEmpty())
				fragment.nextScreen("ROOMS");
			else if (children.size() == 1
					&& getPersistenceLayer().getPersistenceData().getCurrentMember().getObjectId().equals(children.get(0).getObjectId()))
				fragment.nextScreen("CONFIRM");
			else
				fragment.nextScreen("WORKOUT");
		}
	}

	public class NoClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			List<Member> children = getPersistenceLayer().getPersistenceData().getSelectedDaycareChildren();
			if (!getPersistenceLayer().getPersistenceData().getAvailableCubbies().isEmpty())
				fragment.nextScreen("CUBBY");
			else if (!getPersistenceLayer().getPersistenceData().getMultiRoomChildren().isEmpty())
				fragment.nextScreen("ROOMS");
			else if (children.size() == 1
					&& getPersistenceLayer().getPersistenceData().getCurrentMember().getObjectId().equals(children.get(0).getObjectId()))
				fragment.nextScreen("CONFIRM");
			else
				fragment.nextScreen("WORKOUT");
		}
	}

	public String getTitleText() {
		return getPersistenceLayer().getPersistenceData().getFacilityFullScreenQuestions().get(0).getTitle();
	}

	public class FetchAvailablePagers implements Command.CommandStatusListener {

		@Override
		public void starting(Command<?> command) {

		}

		@Override
		public void finished(Command<?> command) {

		}

		@Override
		public void valueChanged(Command<?> command) {
			m_availablePagers.addAll((List<Pager>) command.getResult());
			m_familyPagers.clear();
			m_familyPagers.addAll(Arrays.asList(m_availablePagers.get(0)));
			getPersistenceLayer().getPersistenceData().setFamilyPagers(m_familyPagers);
		}

		@Override
		public void exception(Command<?> command, Exception e) {

		}
	}

	@Override
	public void onInitialize() {
		m_allCubbies = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		m_availableCubbies = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		m_familyCubbies = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		m_facility = new TrackableField<Facility>();
		m_availablePagers = new TrackableCollection<Pager>();
		m_familyPagers = new TrackableCollection<Pager>();
		m_family = new TrackableField<Family>();
		m_proxy = new TrackableField<Member>();
		m_children = new TrackableCollection<Member>();
		addWatch(CHANGE_ID.ALLCUBBIES);
		addWatch(CHANGE_ID.AVAILABLECUBBIES);
	}
}
